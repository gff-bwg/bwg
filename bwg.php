<?php

/*
Plugin Name: BWG
Description: Beyond Wild Guess
Author: A. Suter
Author URI: https://www.gff.ch/
Text Domain: bwg
Domain Path: /locales
Version: 1.16
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Absolute filesystem path to the bwg plugin root file.
 */
define( 'BWG_ABS_PLUGIN_FILE', __FILE__ );

/**
 * Absolute filesystem path to the bwg plugin.
 */
define( 'BWG_ABS_PATH', dirname( __FILE__ ) );

/**
 * The BWG database revision.
 */
define( 'BWG_DB_VERSION', '1.10' );


///
// Check for plugin dependencies ('BWG' depends on 'BWG Vendor').
///
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'bwg-vendor/bwg-vendor.php' ) ) {
	include_once( BWG_ABS_PATH . '/includes/bootstrap.php' );
} else {
	add_action( 'admin_notices', function () {
		$class   = 'notice notice-error';
		$message = __( 'BWG needs the BWG Vendor plugin to be installed and activated.', 'bwg' );

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
	} );
}
