const path = require('path');
const profile_field_types = ['text', 'choice', 'checkbox', 'year'];

module.exports = function (grunt) {
    const package_name = 'bwg';

    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.loadNpmTasks('@lodder/grunt-postcss');
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-wp-i18n');

    let evaluation_profile_fields_meta_box_js = [
        'js/admin/evaluation/profile-fields/meta-box.js'
    ];
    for (let i = 0; i < profile_field_types.length; i++) {
        evaluation_profile_fields_meta_box_js.push('js/admin/evaluation/profile-fields/meta-box.' + profile_field_types[i] + '.js');
    }

    grunt.initConfig({
        compress: {
            main: {
                options: {
                    archive: function () {
                        return '../' + package_name + '.zip';
                    },
                    mode: 'zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: '../',
                        src: [
                            '**',
                            '!_dev/**',
                            '!_fonts/**',
                            '!assets/*.xcf',
                            '!vendor/**',
                            '!.gitignore',
                            '!.gitlab-ci.yml',
                            '!' + package_name + '.zip',
                            '!composer.json',
                            '!composer.lock',
                            '!phpstan.*',
                            '!tcpdf-make-fonts.bat',
                            '!README.md',
                        ],
                        dest: package_name + '/'
                    }
                ]
            }
        }
        ,
        uglify: {
            js: {
                files: {
                    '../js/front/bwg.js': [
                        'js/front/bwg/bwg.js',
                        'js/front/bwg/bwg-utils.js',
                        'js/front/bwg/profile-fields/profile-fields.js',
                        'js/front/bwg/profile-fields/profile-fields-checkbox.js',
                        'js/front/bwg/profile-fields/profile-fields-choice.js',
                        'js/front/bwg/profile-fields/profile-fields-text.js',
                        'js/front/bwg/profile-fields/profile-fields-year.js'
                    ],
                    '../js/front/bwg-prodomo.js': [
                        'js/front/bwg-prodomo/bwg-prodomo.js'
                    ],
                    '../js/admin/bwg.js': [
                        'js/admin/bwg/bwg.js'
                    ],
                    '../js/admin/evaluation/meta-box.js': [
                        'js/admin/evaluation/meta-box/meta-box.js',
                        'js/admin/evaluation/meta-box/item-dialog.js',
                        'js/admin/evaluation/meta-box/preset-dialog.js',
                        'js/admin/evaluation/meta-box/spider-chart-dialog.js'
                    ],
                    '../js/admin/evaluation/emails/meta-box.js': [
                        'js/admin/evaluation/emails/meta-box.js'
                    ],
                    '../js/admin/evaluation/extras/meta-box.js': [
                        'js/admin/evaluation/extras/meta-box.js'
                    ],
                    '../js/admin/evaluation/profile-fields/meta-box.js': evaluation_profile_fields_meta_box_js,
                    '../js/admin/evaluation/full-view.js': [
                        'js/admin/evaluation/full-view/full-view.js'
                    ],
                    '../js/admin/evaluation/users/view.js': [
                        'js/admin/evaluation/users/view.js'
                    ],
                    '../js/admin/evaluation/users/stats.js': [
                        'js/admin/evaluation/users/stats.js'
                    ],
                    '../js/admin/analyses/analyses.js': [
                        'js/admin/analyses/analyses.js'
                    ],
                    '../js/admin/bwg-accordion.js': [
                        'js/admin/accordion/accordion.js'
                    ],
                    '../js/lib/d3/d3.js': [
                        'js/lib/d3/d3.js'
                    ],
                    '../js/lib/FileSaver/FileSaver.js': [
                        'js/lib/FileSaver/FileSaver.js'
                    ],
                    '../js/lib/moment/moment.js': [
                        'js/lib/moment/moment.js'
                    ],
                    '../js/lib/ifvisible/ifvisible.js': [
                        'js/lib/ifvisible/ifvisible.js'
                    ],
                    '../js/lib/jquery-scrollfix/jquery-scrollfix.js': [
                        'js/lib/jquery-scrollfix-3.0.0/src/scrollfix.js'
                    ]
                }
            }
        }
        ,
        sass: {
            dist: {
                options: {
                    trace: false,
                    require: [
                        path.resolve('scss/sass_math.rb')
                    ],
                    sourcemap: 'none',
                    style: 'compressed'
                },
                files: [{
                    expand: true,
                    cwd: 'scss',
                    src: ['**/*.scss'],
                    dest: '../css',
                    ext: '.css'
                }]
            }
        }
        ,
        postcss: {
            options: {
                processors: [
                    require('autoprefixer')
                ]
            },
            dist: {
                src: '../css/**/*.css'
            }
        }
        ,
        svgstore: {
            options: {
                prefix: 'bwg-icon-',
                cleanupdefs: true,
                includeTitleElement: false,
                preserveDescElement: false
            },
            assets: {
                files: {
                    '../assets/assets.svg': ['assets/*.svg']
                }
            }
        }
        ,
        makepot: {
            target: {
                options: {
                    cwd: '../',
                    mainFile: 'bwg.php',
                    domainPath: 'locales',
                    potHeaders: {
                        'Report-Msgid-Bugs-To': 'adrian.suter@gff.ch'
                    },
                    exclude: [
                        '_dev', '_fonts', 'assets', 'vendor'
                    ],
                    type: 'wp-plugin',
                    updatePoFiles: false
                }
            }
        }
        ,
        watch: {
            scripts: {
                files: ['js/**/*.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false
                }
            },
            svgstore: {
                files: ['assets/**/*.svg'],
                tasks: ['svgstore'],
                options: {
                    spawn: false
                }
            },
            styles: {
                files: ['scss/**/*.scss'],
                tasks: ['sass', 'postcss'],
                options: {
                    spawn: false
                }
            }
        }
    });

    // Register the tasks.
    grunt.registerTask('default', ['uglify', 'sass', 'postcss', 'svgstore', 'watch']);
    grunt.registerTask('pot', ['makepot']);
    grunt.registerTask('build', ['compress']);
};
