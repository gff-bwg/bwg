var bwg_prodomo = {};

(function ($, _prodomo, _l10n) {
    "use strict";
    var _undefined = undefined;

    $.extend(_prodomo, {
        /**
         * The jQuery form object.
         */
        $f: undefined
        ,
        /**
         * Initializes the prodomo stuff.
         */
        init: function () {
            _prodomo.$f = $('#bwg-prodomo-form');

            _prodomo.$f.on('click', '[data-bwg-action]', _prodomo.action);
        }
        ,
        /**
         * Event handler for the click on a data-bwg-action element.
         */
        action: function () {
            var $t = $(this);
            var action = $t.data('bwg-action');

            switch (action) {
                case 'submit':
                    _prodomo.submit();
                    break;
            }
        }
        ,
        /**
         * Submit the form.
         */
        submit: function () {
            if (_prodomo.$f.hasClass('bwg-form-submitting')) {
                alert(_l10n['please_wait']);
            } else {
                _prodomo.$f.addClass('bwg-form-submitting');

                $.post(_prodomo.$f.data('bwg-ajax-url'), _prodomo.$f.serialize(), function (response) {
                    if (response['success']) {
                        if (response['data']['redirect']) {
                            location.href = response['data']['redirect'];
                        } else {
                            alert(_l10n['error_server']);
                        }
                    } else {
                        alert(response['data']['error']);
                    }
                }).fail(function () {
                    alert(_l10n['error_server']);
                }).always(function () {
                    _prodomo.$f.removeClass('bwg-form-submitting');
                });
            }
        }
    });

    ///
    // Initialize BWG.
    ///
    $(document).ready(function () {
        _prodomo.init();
    });

})(jQuery, bwg_prodomo, bwg_prodomo_l10n);
