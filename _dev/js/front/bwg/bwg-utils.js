bwg.utils = {};

(function ($, _b, _u) {
    "use strict";

    /**
     * Checks whether the given elements are currently visible on screen.
     *
     * @returns {boolean}
     */
    $.fn.isOnScreen = function () {
        var $window = $(window);
        var viewport = {};
        viewport.top = $window.scrollTop();
        viewport.bottom = viewport.top + $window.height();
        var bounds = {};
        bounds.top = this.offset().top;
        bounds.bottom = bounds.top + this.outerHeight();
        return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
    };

    $.extend(_u, {
        /**
         * Checks whether the current browser supports local html5 storage.
         *
         * @returns {boolean}
         */
        supports_html5_storage: function () {
            try {
                return 'localStorage' in window && window['localStorage'] !== null;
            } catch (e) {
                return false;
            }
        }
        ,
        /**
         *
         * @param {number} dividend
         * @param {number} divisor
         * @param {number} if_zero
         * @returns {number}
         */
        divide_advanced: function (dividend, divisor, if_zero) {
            if (0 === divisor) {
                return if_zero;
            }
            return dividend / divisor;
        }
        ,
        /**
         * Scrolls the viewport to make sure that the element is visible in the
         * viewport. This would only happen, if the element is not currently visible in
         * the viewport.
         *
         * @param selector
         */
        goto_by_scroll: function (selector) {
            var $elements = $(selector);
            if (!$elements.isOnScreen()) {
                $('html,body').animate({
                    scrollTop: $elements.offset().top
                }, 'slow');
            }
        }
    });

    if (_u.supports_html5_storage()) {
        ///
        // Extend the Storage prototype to be able to set and get a generic object.
        ///
        Storage.prototype.setObject = function (key, value) {
            this.setItem(key, JSON.stringify(value));
        };

        Storage.prototype.getObject = function (key) {
            var value = this.getItem(key);
            return value && JSON.parse(value);
        };
    }

})(jQuery, bwg, bwg.utils);
