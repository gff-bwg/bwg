bwg.epf = {};

(function ($, _b, _epf) {
    "use strict";

    $.extend(_epf, {
        profile_field_types: {}
        ,
        register_profile_field_type: function (name, obj) {
            _epf.profile_field_types[name] = obj;
        }
        ,
        get_profile_field_type: function (name) {
            return _epf.profile_field_types[name];
        }
        ,
        init: function ($element) {
            var type_name = $element.data('epf-type');
            var type = _epf.get_profile_field_type(type_name);
            if (undefined === type || !_.isFunction(type['init'])) {
                return;
            }
            
            type['init']($element);
        }
        ,
        nail_data: function ($element, uid) {
            var type_name = $element.data('epf-type');
            var type = _epf.get_profile_field_type(type_name);
            if (undefined === type) {
                return '';
            }

            if (!_.isFunction(type['nail_data'])) {
                return '';
            }

            return type['nail_data']($element, uid);
        }
    });

})(jQuery, bwg, bwg.epf);
