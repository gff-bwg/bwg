(function ($, _b, _epf) {
    "use strict";

    var epf_year = {
        init: function ($element) {
        }
        ,
        nail_data: function ($element, uid) {
            return $('#epf-' + uid).val();
        }
    };
    
    _epf.register_profile_field_type('year', epf_year);

})(jQuery, bwg, bwg.epf);
