(function ($, _b, _epf) {
    "use strict";

    var epf_text = {
        init: function ($element) {
        }
        ,
        nail_data: function ($element, uid) {
            return $('#epf-' + uid).val();
        }
    };

    _epf.register_profile_field_type('text', epf_text);

})(jQuery, bwg, bwg.epf);
