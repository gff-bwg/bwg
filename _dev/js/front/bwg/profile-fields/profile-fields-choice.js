(function ($, _b, _epf) {
    "use strict";

    var epf_choice = {
        init: function ($element) {
        }
        ,
        nail_data: function ($element, uid) {
            // If the field is a select field, get the selected option.
            if ($('#epf-' + uid, $element).is('select')) {
                return $('#epf-' + uid, $element).val();
            }

            // If the field is a radio-input, then get the value of the currently checked radio-input.
            var v = $element.find('[name="' + uid.replace('-', '_') + '"]:checked').val();
            return (undefined === v) ? '' : v;
        }
    };

    _epf.register_profile_field_type('choice', epf_choice);

})(jQuery, bwg, bwg.epf);
