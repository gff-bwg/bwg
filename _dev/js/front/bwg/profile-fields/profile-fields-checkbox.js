(function ($, _b, _epf) {
    "use strict";

    var epf_checkbox = {
        init: function ($element) {
        }
        ,
        nail_data: function ($element, uid) {
            var value = '|';
            $element.find('input[type="checkbox"]:checked').each(function () {
                var $t = $(this);
                value += $t.val() + '|';
            });

            return value;
        }
    };

    _epf.register_profile_field_type('checkbox', epf_checkbox);

})(jQuery, bwg, bwg.epf);
