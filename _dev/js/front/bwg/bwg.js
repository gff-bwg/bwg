var bwg = {};

(function ($, _b) {
    "use strict";
    var _undefined = undefined;

    $.extend(_b, {
        /**
         * The bwg form container element.
         */
        c: _undefined
        ,
        /**
         * The post id.
         */
        post_ID: _undefined
        ,
        /**
         * The ajax url.
         */
        ajax_url: _undefined
        ,
        /**
         * The user data sequence number.
         */
        user_data_sequence_number: 0
        ,
        /**
         * The ajax action for 'user-persist'.
         */
        user_persist_ajax_action: _undefined
        ,
        /**
         * The ajax action nonce for 'user-persist'.
         */
        user_persist_ajax_action_nonce: _undefined
        ,
        /**
         * The ajax action for 'spider-chart'.
         */
        spider_chart_ajax_action: _undefined
        ,
        /**
         * The ajax action nonce for 'spider-chart'.
         */
        spider_chart_ajax_action_nonce: _undefined
        ,
        /**
         * Flag that indicates whether to use html5 storage.
         */
        use_html5_storage: false
        ,
        /**
         * Layout (Media query) elements.
         */
        layout: {
            $xs: _undefined
            ,
            $sm: _undefined
            ,
            $md: _undefined
            ,
            $lg: _undefined
        }
        ,
        /**
         * Number of total gradings.
         */
        total_gradings: 0
        ,
        /**
         * Number of empty gradings.
         */
        empty_gradings: 0
        ,
        /**
         * The gradings object which holds all the gradings.
         */
        gradings: _undefined
        ,
        /**
         * The notes object which holds all the notes made by the user.
         */
        notes: _undefined
        ,
        /**
         * The meta object which holds all the additional form values that are only available to this evaluation.
         */
        meta: _undefined
        ,
        /**
         * If the score for a category > criteria is below this threshold, then
         * there would be the possibility to enter additional notes.
         */
        note_threshold: 4.0
        ,
        /**
         * The regexp to use to check if the current hash is a valid bwg evaluation area.
         */
        area_regexp: new RegExp(/^#bwg-(.+)$/)
        ,
        /**
         * The action log messages.
         */
        action_log_entries: []
        ,
        /**
         * Property names that can be used later in the script. One source of truth paradigm.
         */
        props: {
            total_gradings: 'total-gradings'
            ,
            empty_gradings: 'empty-gradings'
        }
        ,
        /**
         * Initializes the bwg evaluation form (frontend version).
         */
        init: function () {
            // Set the different media query layout elements that can be used to check which layout currently is
            // active. Use the is(':visible') method to do that.
            _b.layout.$xs = $('#bwg-layout-xs');
            _b.layout.$sm = $('#bwg-layout-sm');
            _b.layout.$md = $('#bwg-layout-md');
            _b.layout.$lg = $('#bwg-layout-lg');

            // Initialize Action.
            _b.action_log('I');

            // Get the evaluation form container.
            _b.c = $('#bwg-e');
            _b.user_data_sequence_number = _b.c.data('sequence-number');

            // Register the event handlers on the ifvisible library.
            if (ifvisible) {
                ifvisible.setIdleDuration(30);
                ifvisible.on('idle', function () {
                    _b.action_log('S');
                });
                ifvisible.on('wakeup', function () {
                    _b.action_log('W');
                });
                ifvisible.onEvery(30, function () {
                    // As long as we are not idle (active), we will server-persist the
                    // data automatically every 30 seconds.
                    _b.server_persist(true);
                });
            } else {
                // Fallback if the ifvisible library is not existing.
                setInterval(function () {
                    _b.server_persist(true);
                }, 30000);
            }

            // Get the evaluation id (post id).
            _b.post_ID = _b.c.data('post-id');
            _b.ajax_url = _b.c.data('bwg-ajax-url');

            _b.user_persist_ajax_action = _b.c.data('bwg-user-persist-ajax-action');
            _b.user_persist_ajax_action_nonce = _b.c.data('bwg-user-persist-ajax-action-nonce');
            _b.spider_chart_ajax_action = _b.c.data('bwg-spider-chart-ajax-action');
            _b.spider_chart_ajax_action_nonce = _b.c.data('bwg-spider-chart-ajax-action-nonce');

            // Flag that indicates whether or not to use the html5 local storage.
            _b.use_html5_storage = _b.utils.supports_html5_storage();

            // Register event listeners.
            _b.c.on('change', '.bwg-grading input[type="radio"]', _b.grading_changed);
            _b.c.on('click', '[data-action]', _b.action_clicked);
            _b.c.on('change', '[data-bwg-change]', _b.general_changed);

            // Initialize the profile fields.
            $('[data-epf]').each(function () {
                _b.epf.init($(this));
            });

            $(window).on('click', function (event) {
                if ($(event.target).closest('[data-toggleable]').length > 0) {
                    // User clicked inside a toggleable.
                    return;
                }

                _b.dismiss_all_descriptions();
            }).on('hashchange', function () {
                _b.goto_area(window.location.hash);
            });

            _b.goto_area(location.hash);

            // Check the local storage for any stored gradings that may be restored.
            if (_b.use_html5_storage) {
                // _b.check_storage();
            }

            // Nail and update.
            _b.nail_and_update();

            // Special textarea maxlength handling (update remaining chars).
            _b.c.find('textarea[maxlength]').each(function () {
                _b.update_remaining_chars($(this));
            }).on('input', function () {
                _b.update_remaining_chars($(this));
            });
        }
        ,
        /**
         * Updates the remaining chars for a textarea.
         *
         * @param $textarea
         */
        update_remaining_chars: function ($textarea) {
            var maxlength = $textarea.attr('maxlength');
            var length = $textarea.val().length;

            $textarea.parent().find('[data-remaining-chars]').html(parseInt(maxlength - length).toLocaleString());
        }
        ,
        /**
         * Checks whether or not the given hash is valid for a bwg evaluation form area.
         *
         * @param hash
         * @returns {boolean}
         */
        is_valid_hash: function (hash) {
            return _b.area_regexp.test(hash);
        }
        ,
        /**
         * Gets the html5 local storage key.
         *
         * @returns {string}
         */
        get_storage_key: function () {
            return 'bwg' + _b.post_ID;
        }
        ,
        check_storage: function () {
            // Check if there are any locally stored gradings.
            var browser_storage = localStorage.getObject(_b.get_storage_key());
            if (!browser_storage) {
                // Nothing stored in the browser storage.
                return;
            }

            if (browser_storage.hasOwnProperty('g')) {
                _b.check_storage_grading(browser_storage['g']);
            }

            if (browser_storage.hasOwnProperty('n')) {
                _b.check_storage_notes(browser_storage['n']);
            }

            if (browser_storage.hasOwnProperty('m')) {
                _b.check_storage_meta(browser_storage['m']);
            }
        }
        ,
        check_storage_meta: function (browser_storage_meta) {
            if (!browser_storage_meta) {
                return;
            }
        }
        ,
        check_storage_notes: function (browser_storage_notes) {
            if (!browser_storage_notes) {
                return;
            }

            var storage_distance = 0;
            var all_undefined = true;
            _.map(_b.notes, function (data, key) {
                if ('' !== data['value'].trim()) {
                    all_undefined = false;
                }

                if (browser_storage_notes.hasOwnProperty(key)) {
                    if (browser_storage_notes[key]['value'] !== data['value']) {
                        storage_distance++;
                    }
                }
            });

            if (storage_distance > 0) {
                var restore = true;
                if (!all_undefined) {
                    restore = confirm('Ihr Browser hat Ihre Bemerkungen gespeichert. Allerdings sind diese nicht identisch mit den jetzigen Daten. Soll der Browser die Bewertungen wiederherstellen?');
                }

                if (restore) {
                    _.map(_b.notes, function (data, key) {
                        if (browser_storage_notes.hasOwnProperty(key)) {
                            $('#bwg-note-' + key.substr(1), _b.c).val(browser_storage_notes[key]['value']);
                        } else {
                            $('#bwg-note-' + key.substr(1), _b.c).val('');
                        }
                    });
                }
            }
        }
        ,
        check_storage_grading: function (browser_storage_gradings) {
            if (!browser_storage_gradings) {
                return;
            }

            // Compare the storage gradings with the current gradings. If they are different, then
            // ...if the current gradings are all undefined, simply replace by stored gradings.
            // ...if there is one defined current grading, ask the user.
            _b.nail_data();

            var all_undefined = true;
            var storage_distance = 0;
            _.map(_b.gradings, function (value, key) {
                if ('' !== value) {
                    all_undefined = false;
                }

                if (browser_storage_gradings.hasOwnProperty(key)) {
                    if (browser_storage_gradings[key] !== value) {
                        storage_distance++;
                    }
                }
            });

            if (storage_distance > 0) {
                var restore = true;
                if (!all_undefined) {
                    restore = confirm('Ihr Browser hat Ihre Bewertungen gespeichert. Allerdings sind diese nicht identisch mit den jetzigen Daten. Soll der Browser die Bewertungen wiederherstellen?');
                }

                if (restore) {
                    _.map(_b.gradings, function (num, key) {
                        if (browser_storage_gradings.hasOwnProperty(key) && browser_storage_gradings[key] !== '') {
                            $('[data-bwg-grading="' + key.substr(1) + '"]', _b.c).find('input[type="radio"][value="' + browser_storage_gradings[key] + '"]').prop('checked', true);
                        } else {
                            $('[data-bwg-grading="' + key.substr(1) + '"]', _b.c).find('input[type="radio"]').prop('checked', false);
                        }
                    });
                }
            }
        }
        ,
        /**
         * Nails the evaluation form data (gradings, notes, meta (profile fields)).
         */
        nail_data: function () {
            _b.user_data_sequence_number++;

            // Reset the gradings, notes and meta objects.
            _b.gradings = {};
            _b.notes = {};
            _b.meta = {};

            // Reset some counters.
            _b.total_gradings = 0;
            _b.empty_gradings = 0;

            var prop_total_gradings = _b.props.total_gradings;
            var prop_empty_gradings = _b.props.empty_gradings;

            // Reset the number of total and empty gradings as well as the grading data for all categories and
            // interests.
            $('[data-bwg-category]', _b.c).data(prop_total_gradings, 0).data(prop_empty_gradings, 0);
            $('[data-bwg-interest]', _b.c).data(prop_total_gradings, 0).data(prop_empty_gradings, 0).each(function () {
                $(this).data('bwg-grading-data', {});
            });

            ///
            // Nail the gradings
            //
            //   Build the sum container and update the dom data attributes #prop_total_gradings# and
            //   #prop_empty_gradings# for the interest-, category-containers.
            ///
            $('[data-bwg-grading]').each(function () {
                _b.total_gradings++;

                var $t = $(this);
                var $category_container = $t.closest('[data-bwg-category]');
                var $interest_container = $category_container.prevAll('[data-bwg-interest]').first();

                // Increment the total number of gradings for the different containers.
                $category_container.data(prop_total_gradings, 1 + $category_container.data(prop_total_gradings));
                $interest_container.data(prop_total_gradings, 1 + $interest_container.data(prop_total_gradings));

                var grading_id = $t.data('bwg-grading');
                var grading_value = $t.find('input[type="radio"]:checked').val();

                // Nail the grading value into the gradings object.
                _b.gradings['#' + grading_id] = (undefined === grading_value ? '' : grading_value);

                if (0 < $interest_container.length) {
                    var grading_data = $interest_container.data('bwg-grading-data');
                    grading_data['#' + grading_id] = _b.gradings['#' + grading_id];
                    $category_container.data('bwg-grading-data', grading_data);
                }

                if (undefined === grading_value) {
                    _b.empty_gradings++;

                    $category_container.data(prop_empty_gradings, 1 + $category_container.data(prop_empty_gradings));
                    $interest_container.data(prop_empty_gradings, 1 + $interest_container.data(prop_empty_gradings));
                }
            });


            ///
            // Nail the notes.
            ///
            $('[data-bwg-category]').each(function () {
                var $category_container = $(this);
                var category_id = $category_container.data('bwg-category');

                _b.notes['#' + category_id] = {
                    value: $('#bwg-note-' + category_id).val()
                };
            });


            ///
            // Nail the meta (profile fields).
            ///
            $('[data-epf]').each(function () {
                var $t = $(this);
                var uid = $t.data('epf');

                // TODO On init we should also init the profile fields (in order to add listeners that would execute a server persist)
                _b.meta[uid] = _b.epf.nail_data($t, uid);
            });

            /*
            if (0 === _b.empty_gradings) {
                $('#bwg-notice-empty-gradings').hide();
            } else {
                $('#bwg-notice-empty-gradings').show();
            }
            */
        }
        ,
        /**
         * Nails the data and updates the user interface.
         */
        nail_and_update: function () {
            // Nail the current data.
            _b.nail_data();

            // Store the current gradings, notes and meta into browser storage.
            if (_b.use_html5_storage) {
                localStorage.setObject(_b.get_storage_key(), {
                    g: _b.gradings,
                    n: _b.notes,
                    m: _b.meta
                });
            }

            // Get the property name used for the empty gradings dom data.
            var prop_empty_gradings = _b.props.empty_gradings;

            /*
            $('[data-bwg-interest]', _b.c).each(function () {
                if (0 === $(this).data(prop_empty_gradings)) {
                    // console.log('THIS ONE SEEMS TO BE DONE!!!!' + $(this).data('bwg-interest'));
                }
            });
            */

            $('[data-bwg-category]', _b.c).each(function () {
                var $t = $(this);
                var category_id = $t.data('bwg-category');
                if (0 === $t.data(prop_empty_gradings)) {
                    $('[href="#bwg-' + category_id + '"]', _b.c).addClass('bwg-complete');
                } else {
                    $('[href="#bwg-' + category_id + '"]', _b.c).removeClass('bwg-complete');
                }
            });

            // Update the progress indicator.
            _b.update_progress_indicator(_b.total_gradings - _b.empty_gradings, _b.total_gradings);
        }
        ,
        update_progress_indicator: function (value, maximum) {
            var p = 0;
            if (maximum > 0) {
                p = Math.max(0, Math.min(100, Math.round(100 * value / maximum)));
            }

            $('.bwg-progress ').css('width', p + '%');
        }
        ,
        /**
         * Event handler for the change event on a grading input radio.
         *
         * @param event
         */
        grading_changed: function (event) {
            var $t = $(event.target);
            var info = $t.closest('[data-bwg-grading]').data('bwg-grading') + '=' + $t.val();

            // Log the action.
            _b.action_log('V' + info);

            // Nail and update.
            _b.nail_and_update();

            // Persist the data on the server.
            _b.server_persist(false);
        }
        ,
        /**
         * Event handler for the change event on a dom element that has a 'data-bwg-change' attribute.
         *
         * Currently the only value allowed is 'persist'.
         *
         * @param event
         */
        general_changed: function (event) {
            var $t = $(this);
            var change = $t.data('bwg-change');

            switch (change) {
                case 'persist':
                    // Nail and update.
                    _b.nail_and_update();

                    // Persist the data on the server.
                    _b.server_persist(false);
                    break;
            }
        }
        ,
        /**
         * Event handler for the click event on a dom element that has a 'data-action' attribute.
         *
         * @param event
         */
        action_clicked: function (event) {
            var $t = $(event.currentTarget);

            // Remove the focus.
            $t.blur();

            switch ($t.data('action')) {
                case 'goto-next':
                    _b.goto_next();
                    break;

                case 'goto-prev':
                    _b.goto_prev();
                    break;

                case 'toggle-description':
                    event.preventDefault();
                    event.stopPropagation();

                    var actionTarget = $t.data('action-target');
                    var $elementTarget = $('#' + actionTarget);
                    _b.dismiss_all_descriptions($elementTarget);

                    $elementTarget.slideToggle();
                    break;

                case 'submit':
                    if (0 === _b.empty_gradings) {
                        if (_b.c.hasClass('bwg-form-submitting')) {
                            alert("Ich bin schon am Senden...");
                        } else {
                            _b.c.addClass('bwg-form-submitting');

                            _b.server_persist(false, true, function (result) {
                                if (0 === result['ajax']) {
                                    alert("HTTP Error - Keine Verbindung oder Server antwortete mit 404 oder einem" +
                                        " anderen Fehler.");
                                    _b.c.removeClass('bwg-form-submitting');
                                    return;
                                }

                                if (!result['response']['success']) {
                                    alert(result['response']['data']['error']);
                                    _b.c.removeClass('bwg-form-submitting');
                                    return;
                                }

                                if (result['response']['data']['redirect']) {
                                    location.href = result['response']['data']['redirect'];
                                } else {
                                    _b.c.removeClass('bwg-form-submitting');
                                }
                            });
                        }
                    } else {
                        alert($('#bwg-required-alert-message').text());
                    }
                    break;
            }
        }
        ,
        dismiss_all_descriptions: function ($exception_elements) {
            if (!_b.layout.$xs.is(':visible')) {
                $('[data-bwg-toggleable]:visible').each(function () {
                    var b = true;
                    if (_undefined !== $exception_elements) {
                        for (var i = 0; i < $exception_elements.length; i++) {
                            if ($exception_elements[i] === this) {
                                b = false;
                                break;
                            }
                        }
                    }
                    if (b) {
                        $(this).hide();
                    }
                });
            }
        }
        ,
        get_first_area_hash: function () {
            var $a = $('.bwg-navigation', _b.c).find('li:first-child a');
            return $a.attr('href');
        }
        ,
        has_next: function () {
            return null !== _b.get_next();
        }
        ,
        get_next: function () {
            // Are we currently in an area?
            var $link = $('.bwg-navigation a.bwg-active', _b.c);
            if (0 === $link.length) {
                // No currently active bwg navigation link.
                return null;
            }

            var $next_link;
            var $sublist = $link.closest('li').find('ul');
            if (0 !== $sublist.length) {
                // There is a sublist.
                $next_link = $sublist.find('li:first-child a');
                if (0 !== $next_link.length) {
                    return $next_link.attr('href');
                }
            }

            $next_link = $link.closest('li').next().find('a');
            if (0 !== $next_link.length) {
                return $next_link.attr('href');
            }

            $next_link = $link.closest('ul').closest('li').next().find('a');
            if (0 !== $next_link.length) {
                return $next_link.attr('href');
            }

            return null;
        }
        ,
        /**
         * Goes to the next slide.
         */
        goto_next: function () {
            var next = _b.get_next();
            if (null !== next) {
                location.href = next;
            }
        }
        ,
        /**
         * Has the current area a previous area?
         *
         * @returns {boolean}
         */
        has_prev: function () {
            return null !== _b.get_prev();
        }
        ,
        /**
         * Gets the previous area of the current area.
         *
         * @returns {*}
         */
        get_prev: function () {
            // Are we currently in an area?
            var $link = $('.bwg-navigation a.bwg-active', _b.c);
            if (0 === $link.length) {
                return null;
            }

            var $prev_link;
            var $prev_list_item = $link.closest('li').prev();
            if (0 === $prev_list_item.length) {
                // There is no more list-items above me (in my list). Check if my list is part of another list.
                var $parent_list_item = $link.closest('ul').closest('li');
                if (0 === $parent_list_item.length) {
                    return null;
                }

                $prev_link = $parent_list_item.find('a');
                if (0 !== $prev_link.length) {
                    return $prev_link.attr('href');
                }

                return null;
            }

            var $sublist = $prev_list_item.find('ul');
            if (0 !== $sublist.length) {
                // There is a sublist.
                $prev_link = $sublist.find('li:last-child a');
                if (0 !== $prev_link.length) {
                    return $prev_link.attr('href');
                }
            }

            $prev_link = $prev_list_item.find('a');
            if (0 !== $prev_link.length) {
                return $prev_link.attr('href');
            }

            return null;
        }
        ,
        /**
         * Goes to the previous slide.
         */
        goto_prev: function () {
            var prev = _b.get_prev();
            if (null !== prev) {
                location.href = prev;
            }
        }
        ,
        /**
         * Goes to the area given the hash.
         *
         * @param hash
         */
        goto_area: function (hash) {
            if ('' === hash) {
                hash = _b.get_first_area_hash();
            }

            if (!_b.is_valid_hash(hash)) {
                return;
            }

            // Blur the element currently in the focus (most probably this is an anchor element).
            $(':focus').blur();

            // Hide all content column children of class "bwg-area".
            $('.bwg-content-col > .bwg-area', _b.c).hide();

            // Remove the active class from all bwg-navigation anchors.
            $('.bwg-navigation a', _b.c).removeClass('bwg-active');

            _b.action_log('G' + hash.substr(5));

            // Show the current category dom element.
            var $element = $(hash);
            $element.show();

            // Get the link that should now be marked as active.
            var $active_link = $('.bwg-navigation a[href="' + hash + '"]', _b.c);
            $active_link.addClass('bwg-active');

            // Set the headline.
            $('[data-bwg-headline]', _b.c).text($active_link.text());

            // For all first level navigation items, check if the currently active link is inside and if so, show
            // any potential sub levels. If not, hide any potential sub levels.
            $('.bwg-navigation > li').each(function () {
                if ($.contains(this, $active_link.get(0))) {
                    $(this).find('> ul').slideDown();
                } else {
                    $(this).find('> ul').slideUp();
                }
            });

            $('#bwg-control-prev').toggle(_b.has_prev());
            $('#bwg-control-next').toggle(_b.has_next());
            $('#bwg-control-submit').toggle(!_b.has_next());

            _b.utils.goto_by_scroll('.bwg-headline h1');

            if (undefined !== $element.data('bwg-spider')) {
                _b.update_spiders([$element.data('bwg-spider')]);
            } else if ('#bwg-overview' === hash) {
                _b.update_all_spiders();
            }
        }
        ,
        /**
         * Updates all spiders.
         */
        update_all_spiders: function () {
            var spiders = [];
            $('[data-bwg-spider]').each(function () {
                spiders.push($(this).data('bwg-spider'));
            });

            _b.update_spiders(spiders);
        }
        ,
        /**
         * Updates the given spiders.
         *
         * @param spiders
         */
        update_spiders: function (spiders) {
            var spidersToBeLoaded = [];
            for (var i = 0; i < spiders.length; i++) {
                var interest_id = spiders[i];
                var $element = $('#bwg-spider-' + interest_id);
                if (0 === $element.length) {
                    continue;
                }

                var $interest_area = $('[data-bwg-interest="' + interest_id + '"]');
                var current_grading_data = $interest_area.data('bwg-grading-data');
                var last_grading_data = $element.data('bwg-json-grading-data');
                if (JSON.stringify(current_grading_data) !== last_grading_data) {
                    $element.addClass('bwg-loading');
                    $element.data('bwg-json-grading-data', JSON.stringify(current_grading_data));

                    $('#bwg-final-spider-' + interest_id).addClass('bwg-loading');

                    spidersToBeLoaded.push(interest_id);
                }
            }

            if (spidersToBeLoaded.length > 0) {
                var data = {};
                data['_ajax_nonce'] = _b.spider_chart_ajax_action_nonce;
                data['action'] = _b.spider_chart_ajax_action;
                data['post'] = _b.post_ID;

                var gradings = [];
                _.map(_b.gradings, function (value, key) {
                    gradings.push(key.substr(1) + "=" + value);
                });

                data['grading'] = gradings.join('|');
                data['charts'] = spidersToBeLoaded.join('|');

                $.post(_b.ajax_url, data, function (response) {
                    for (var i = 0; i < response['data']['charts'].length; i++) {
                        var chart = response['data']['charts'][i];
                        $('#bwg-spider-' + chart['id'] + '-container').html(chart['svg']);
                        $('#bwg-final-spider-' + chart['id'] + '-container').html(chart['svg_big']);

                        $('#bwg-spider-' + chart['id']).removeClass('bwg-loading');
                        $('#bwg-final-spider-' + chart['id']).removeClass('bwg-loading');
                    }
                }).fail(function () {
                    alert("Fehler beim Laden der Spider-Charts.");
                });
            }
        }
        ,
        /**
         * Persist the data to the server.
         *
         * @param {boolean} automatically Is this an automatically called server_persist?
         */
        server_persist: function (automatically) {
            var submit = arguments[1] || false;
            var callback = arguments[2] || undefined;

            var gradings = [];
            _.map(_b.gradings, function (value, key) {
                gradings.push(key.substr(1) + "=" + value);
            });

            _b.user_data_sequence_number++;

            var post_data = {
                _ajax_nonce: _b.user_persist_ajax_action_nonce,
                action: _b.user_persist_ajax_action,
                post: _b.post_ID,
                automatically: automatically ? '1' : '0',
                grading: gradings.join('|'),
                notes: JSON.stringify(_b.notes),
                meta: JSON.stringify(_b.meta),
                action_log_entries: JSON.stringify(_b.action_log_entries)
            };

            if (undefined !== submit) {
                post_data['submit'] = submit ? '1' : '0';
            }

            $.post(_b.ajax_url, post_data, function (response) {
                if (undefined !== callback) {
                    callback.call(this, {'ajax': 1, 'response': response});
                }
            }).fail(function () {
                if (undefined !== callback) {
                    callback.call(this, {'ajax': 0});
                }
            });
        }
        ,
        /**
         * Adds a new action log entry inclusive current timestamp (or timestamp offset).
         *
         * Action Log Identifiers
         * ----------------------
         * I : App initialized.
         * W : App woke up.
         * S : App got idle (sleep).
         * V : Vote, syntax V{id of criteria/focus}={new grading}
         * G : Goto area.
         *
         * @param message
         */
        action_log: function (message) {
            var ts = Math.round((new Date()).getTime() / 1000);
            if (_b.action_log_entries.length > 0) {
                ts = ts - _b.action_log_entries[0][0];
            }

            _b.action_log_entries.push([ts, message]);
        }
    });

    ///
    // Initialize BWG.
    ///
    $(document).ready(function () {
        _b.init();
    });

})(jQuery, bwg);
