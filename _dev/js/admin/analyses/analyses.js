bwg.analyses = {};

(function ($, _b, _a) {
    "use strict";

    $.extend(_a, {
        $f: undefined
        ,
        ajax_filter_groups_action: undefined
        ,
        ajax_filter_groups_nonce: undefined
        ,
        $sex_table: undefined
        ,
        $age_table: undefined
        ,
        f_timeout: undefined
        ,
        init_form: function () {
            _a.$f = $('#edit-analysis-form');
            if (_a.$f.length > 0) {
                _a.ajax_filter_groups_action = _a.$f.data('bwg-filter-groups-action');
                _a.ajax_filter_groups_nonce = _a.$f.data('bwg-filter-groups-nonce');

                _a.$sex_table = _a.$f.find('#bwg_sex_table');
                _a.$age_table = _a.$f.find('#bwg_age_table');

                _a.$f.on('change', 'input', _a.form_change);

                _a.$f.on('submit', function (event) {
                    event.preventDefault();
                });

                _a.$f.on('click', '[data-bwg-action]', function (event) {
                    event.preventDefault();
                    var $t = $(this);

                    switch ($t.data('bwg-action')) {
                        case 'update-filter-groups':
                            _a.$f.serializeArray();
                            var data = _a.$f.serialize() + '&' + $.param({
                                '_ajax_nonce': _a.ajax_filter_groups_nonce,
                                'action': _a.ajax_filter_groups_action
                            });

                            $.post(ajaxurl, data, function (response) {
                                $('#bwg_filter_groups').html(response['data']['filter_groups']);
                            });
                            break;

                        case 'trash':
                            $t.closest('[data-bwg-filter-condition]').fadeSlideUpRemove(200, function () {
                                _a.form_update_ui();
                            });
                            break;

                        case 'add':
                            var $condition_row = $t.closest('[data-bwg-filter-condition]');
                            var $next_condition_row = $condition_row.next('[data-bwg-filter-condition]');

                            var age_limit1 = _a.val_age_limit($condition_row);
                            var age_limit2 = _a.val_age_limit($next_condition_row);

                            var age_limit = '';
                            if (!isNaN(age_limit1) && isNaN(age_limit2)) {
                                age_limit = age_limit1 + 1;
                            } else if (isNaN(age_limit1) && !isNaN(age_limit2)) {
                                age_limit = age_limit2 - 1;
                            } else if (!isNaN(age_limit1) && !isNaN(age_limit2)) {
                                age_limit = Math.round((age_limit1 + age_limit2) / 2);
                            }

                            var $new_row = $(_a.$age_table.find('tr[data-bwg-filter-condition]')[0].outerHTML);
                            _a.val_age_limit($new_row, age_limit);
                            _a.val_percentage($new_row, '0');
                            $new_row.insertAfter($condition_row);

                            _a.form_update_ui();
                            break;
                    }
                });

                _a.form_update_ui();
            }
        }
        ,
        form_change: function (/*event*/) {
            if (undefined !== _a.f_timeout) {
                clearTimeout(_a.f_timeout);
            }
            _a.f_timeout = setTimeout(_a.form_update_ui, 100);
        }
        ,
        form_update_ui: function () {
            var i, rows, age_limit, percentage, total_percentage, $input_percentage;

            ////////////////////////////
            // Sex Filter
            ////////////////////////////

            ///
            // Check the percentages.
            ///
            rows = _a.$sex_table.find('tr[data-bwg-filter-condition]');
            total_percentage = 0;
            for (i = 0; i < rows.length; i++) {
                ///
                // Input: Percentage
                ///
                $input_percentage = _a.input_percentage($(rows[i]));
                percentage = parseInt($input_percentage.val());
                $input_percentage.toggleClass('bwg-input-error', isNaN(percentage));
                if (!isNaN(percentage)) {
                    total_percentage += percentage;
                }
            }

            _a.$sex_table.find('.bwg-total-percentage')
                .text(total_percentage + '%')
                .toggleClass('bwg-input-error', 100 !== total_percentage)
                .toggleClass('bwg-input-success', 100 === total_percentage);


            ////////////////////////////
            // Age Filter
            ////////////////////////////

            ///
            // Make sure that the rows of the age limits are in correct order.
            ///
            rows = _a.$age_table.find('tr[data-bwg-filter-condition]');
            for (i = 0; i < rows.length; i++) {
                age_limit = _a.val_age_limit($(rows[i]));
                if (isNaN(age_limit)) {
                    continue;
                }

                var m = -1;
                for (var j = i - 1; j >= 0; j--) {
                    var age_limit2 = _a.val_age_limit($(rows[j]));
                    if (isNaN(age_limit2)) {
                        continue;
                    }

                    if (age_limit2 > age_limit) {
                        m = j;
                    }
                }

                if (m >= 0) {
                    $(rows[i]).detach().insertBefore(rows[m]);
                }
            }

            ///
            // Update the lower age limits and check the percentages.
            ///
            rows = _a.$age_table.find('tr[data-bwg-filter-condition]');
            if (rows.length < 2) {
                _a.$age_table.find('[data-bwg-action="trash"]').hide();
            } else {
                _a.$age_table.find('[data-bwg-action="trash"]').show();
            }

            total_percentage = 0;
            var last_age_limit = -1;
            for (i = 0; i < rows.length; i++) {
                ///
                // Input: Percentage
                ///
                $input_percentage = _a.input_percentage($(rows[i]));
                percentage = parseInt($input_percentage.val());
                $input_percentage.toggleClass('bwg-input-error', isNaN(percentage));
                if (!isNaN(percentage)) {
                    total_percentage += percentage;
                }

                ///
                // Input: Age limit
                ///
                var $input_age_limit = _a.input_age_limit($(rows[i]));
                age_limit = parseInt($input_age_limit.val());
                $input_age_limit.toggleClass('bwg-input-error', isNaN(age_limit) || age_limit <= last_age_limit);

                var txt = '?';
                if (!isNaN(last_age_limit)) {
                    txt = last_age_limit + 1;
                }

                $(rows[i]).find('.bwg-age-lower-limit').text(txt);
                last_age_limit = age_limit;
            }

            _a.$age_table.find('.bwg-total-percentage')
                .text(total_percentage + '%')
                .toggleClass('bwg-input-error', 100 !== total_percentage)
                .toggleClass('bwg-input-success', 100 === total_percentage);
        }
        ,
        val_percentage: function ($condition_row/*, value*/) {
            if (1 === arguments.length) {
                return parseInt(_a.input_percentage($condition_row).val());
            }

            _a.input_percentage($condition_row).val(arguments[1]);
        }
        ,
        input_percentage: function ($condition_row) {
            return $condition_row.find('.input-percentage');
        }
        ,
        val_age_limit: function ($condition_row/*, value*/) {
            if (1 === arguments.length) {
                return parseInt(_a.input_age_limit($condition_row).val());
            }

            _a.input_age_limit($condition_row).val(arguments[1]);
        }
        ,
        input_age_limit: function ($condition_row) {
            return $condition_row.find('.input-age-limit');
        }
    });

    $(document).ready(function () {
        _a.init_form();
    });
})(jQuery, bwg, bwg.analyses);
