var bwg = {};

(function ($, _b, sr) {
    "use strict";

    $.extend(_b, {
        /**
         * Holds a list of all defined deferred actions. Actually the values
         * are setTimeout numbers.
         */
        _deferred_actions: {}
        ,
        /**
         * Defers an action. The idea behind that is, that sometimes an action
         * should only be executed, if there is no other action that makes the
         * first action useless. For example if you want to store the input
         * a user made via ajax. Then you can say, save the input but only if
         * the user didn't change the input within 500 milliseconds.
         *
         * @param key
         * @param callback
         * @param delay
         */
        defer_action: function (key, callback, delay) {
            if (arguments.length < 3) {
                delay = 500;
            }

            if (undefined !== _b._deferred_actions[key]) {
                window.clearTimeout(_b._deferred_actions[key]);
            }

            _b._deferred_actions[key] = window.setTimeout(callback, delay);
        }
        ,
        // debouncing function from John Hann
        // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
        debounce: function (func, threshold, execAsap) {
            var timeout;

            return function debounced() {
                var obj = this, args = arguments;

                function delayed() {
                    if (!execAsap) {
                        func.apply(obj, args);
                    }
                    timeout = null;
                }

                if (timeout) {
                    clearTimeout(timeout);
                } else if (execAsap) {
                    func.apply(obj, args);
                }

                timeout = setTimeout(delayed, threshold || 100);
            };
        }
        ,
        /**
         *
         * @param $container
         * @param $fromField
         * @param $toField
         * @returns {boolean}
         */
        validate_date_from_to_field: function ($container, $fromField, $toField) {
            var v = _b.validate_date_from_to($fromField.val(), $toField.val());

            if (false === v) {
                $container.addClass('form-invalid');
                return false;
            }

            $fromField.val(v[0]);
            $toField.val(v[1]);
            return true;
        }
        ,
        /**
         *
         * @param date_from
         * @param date_to
         * @returns {*}
         */
        validate_date_from_to: function (date_from, date_to) {
            var moment_from = moment(date_from, 'DD.MM.YYYY');
            var moment_to = moment(date_to, 'DD.MM.YYYY');

            if (date_from != '' && date_to != '') {
                if (!moment_from.isValid() || !moment_to.isValid() || !(moment_from.isSame(moment_to) || moment_from.isBefore(moment_to))) {
                    return false;
                }

                return [moment_from.format('DD.MM.YYYY'), moment_to.format('DD.MM.YYYY')];
            } else if (date_from != '') {
                if (!moment_from.isValid()) {
                    return false;
                }

                return [moment_from.format('DD.MM.YYYY'), ''];
            } else if (date_to != '') {
                if (!moment_to.isValid()) {
                    return false;
                }

                return ['', moment_to.format('DD.MM.YYYY')];
            }

            return ['', ''];
        }
        ,
        /**
         * Replaces a url param.
         *
         * @param url
         * @param param_name
         * @param param_value
         * @returns {string}
         */
        replace_url_param: function (url, param_name, param_value) {
            if (param_value == null) {
                param_value = '';
            }
            var pattern = new RegExp('\\b(' + param_name + '=).*?(&|$)');
            if (url.search(pattern) >= 0) {
                return url.replace(pattern, '$1' + param_value + '$2');
            }
            url = url.replace(/\?$/, '');
            return url + (url.indexOf('?') > 0 ? '&' : '?') + param_name + '=' + param_value;
        }
    });

    $.fn['fadeSlideUpRemove'] = function (speed, cb) {
        return this.fadeTo(speed, 0.00, function () {
            $(this).slideUp(speed, function () {
                $(this).remove();
                if (cb && 'function' === typeof cb) {
                    cb();
                }
            });
        });
    };

    $.fn['bwg'] = function (action) {
        if ('state' === action) {
            var state = (arguments.length < 2) ? 'normal' : arguments[1];
            return this.each(function () {
                $(this).removeClass('state-is-waiting state-is-error');
                if ($.bwg.state.waiting === state) {
                    $(this).addClass('state-is-waiting');
                } else if ($.bwg.state.error === state) {
                    $(this).addClass('state-is-error');
                }
            });
        }

        return this;
    };

    $.fn[sr] = function (fn) {
        return fn ? this.bind('resize', _b.debounce(fn)) : this.trigger(sr);
    };

    $['bwg'] = {
        state: {
            normal: 'normal',
            waiting: 'waiting',
            error: 'error'
        }
    };

})(jQuery, bwg, 'smartresize');
