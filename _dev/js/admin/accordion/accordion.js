let bwg_accordion = {};

(function ($, _a) {
    "use strict";

    $.extend(_a, {
        init: function () {
            $('.bwg-accordion').on('click', '.bwg-accordion-trigger', function () {
                let isExpanded = ('true' === $(this).attr('aria-expanded'));

                if (isExpanded) {
                    $(this).attr('aria-expanded', 'false');
                    $('#' + $(this).attr('aria-controls')).attr('hidden', true);
                } else {
                    $(this).attr('aria-expanded', 'true');
                    $('#' + $(this).attr('aria-controls')).attr('hidden', false);
                }
            });

        }
    });

    $(document).ready(function () {
        _a.init();
    });
})(jQuery, bwg_accordion);
