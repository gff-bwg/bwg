jQuery(document).ready(function ($) {

    $('body').on('click', '[data-action]', function (event) {
        event.preventDefault();
        var $t = $(this);

        switch ($t.data('action')) {
            case 'load-as-strata':
                location.href = bwg.replace_url_param(
                    location.href,
                    'strata_field_uid',
                    $t.data('bwg-pf-uid')
                );
                break;

            case 'toggle-strata-statistics':
                $t.toggleClass('bwg-strata-hidden');
                $('svg g[data-series="' + $t.data('strata-number') + '"]').toggle();
                $('[data-bwg-strata-number="' + $t.data('strata-number') + '"]').toggle();
                break;

            case 'save-chart':
                var t = $t.closest('[data-container="chart"]').find('[data-spider-chart]').html();

                var blob = new Blob(['<?xml version="1.0" encoding="UTF-8" ?>' + t], {type: 'application/svg+xml'});
                saveAs(blob, 'spider-chart.svg');
                break;

            case 'show-details':
                $t.find('[data-details]').slideToggle();
                break;

            case 'sort':
                var $table = $t.closest('table');

                var rows = $table.find('tbody tr').get();
                if ('0' !== $table.data('unsorted')) {
                    console.log("A");
                    rows.sort(function (a, b) {
                        var av = parseFloat($(a).data('value'));
                        var bv = parseFloat($(b).data('value'));

                        return bv - av;
                    });

                    $table.data('unsorted', '0');
                } else {
                    console.log("B");
                    rows.sort(function (a, b) {
                        var av = parseFloat($(a).data('order'));
                        var bv = parseFloat($(b).data('order'));

                        return av - bv;
                    });

                    $table.data('unsorted', '1');
                }

                $.each(rows, function (idx, itm) {
                    $table.append(itm);
                });
                break;
        }
    });

});