(function ($, _b, _epf) {
    "use strict";

    var epf_year = {
        /**
         * Initializes a field.
         *
         * The method could add event listeners to change or input events in order to update the view.
         *
         * @param $item
         * @param uid
         */
        init_field: function ($item, uid) {
        }
        ,
        /**
         * Gets the year start.
         *
         * @param $item
         * @param uid
         * @returns {Number}
         */
        get_year_start: function ($item, uid) {
            return parseInt($item.find('#bwg-epf-' + uid + '-year-start').val());
        }
        ,
        /**
         * Gets the year end.
         *
         * @param $item
         * @param uid
         * @returns {Number}
         */
        get_year_end: function ($item, uid) {
            return parseInt($item.find('#bwg-epf-' + uid + '-year-end').val());
        }
        ,
        /**
         * Gets the field options.
         *
         * @param $item
         * @param uid
         * @returns {{year_start: (*|int), year_end: (*|int)}}
         */
        get_field_options: function ($item, uid) {
            return {
                'year_start': epf_year.get_year_start($item, uid),
                'year_end': epf_year.get_year_end($item, uid)
            };
        }
    };

    _epf.set_profile_field_type('year', epf_year);

})(jQuery, bwg, bwg.epf);
