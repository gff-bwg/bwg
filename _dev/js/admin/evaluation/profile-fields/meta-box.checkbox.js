(function ($, _b, _epf) {
    "use strict";

    var epf_checkbox = {
        /**
         * Initializes a field.
         *
         * The method could add event listeners to change or input events in order to update the view.
         *
         * @param $item
         * @param uid
         */
        init_field: function ($item, uid) {
            $item.find('#bwg-epf-' + uid + '-checkboxes').sortable({
                cursor: 'move',
                handle: '.bwg-epf-checkbox-move-handler',
                axis: 'y',
                placeholder: 'bwg-epf-checkbox-move-placeholder',
                forcePlaceholderSize: true
            });

            $item.on('click', '[data-bwg-epf-checkbox-action]', function () {
                var $t = $(this);
                switch ($t.data('bwg-epf-checkbox-action')) {
                    case 'add':
                        // Check if we are inside a checkbox...
                        var $checkbox = $t.closest('[data-bwg-epf-checkbox]');

                        if (0 < $checkbox.length) {
                            // ...we are, therefore we will add the checkbox after the current one.
                            epf_checkbox.add_checkbox($item, uid, $checkbox);
                        } else {
                            // ...we are not, therefore we will add the checkbox as first element.
                            epf_checkbox.add_checkbox($item, uid);
                        }
                        return;

                    case 'delete':
                        $t.closest('[data-bwg-epf-checkbox]').fadeSlideUpRemove(50, function () {
                        });
                        return;
                }
            });
        }
        ,
        /**
         * Adds a new checkbox to the given field.
         *
         * @param $item The item.
         * @param uid The uid.
         * @param $elem The element after which the new checkbox should be added. If this is undefined, the new checkbox
         * would be added as first checkbox.
         */
        add_checkbox: function ($item, uid, $elem) {
            var unique_key = 'c-' + Date.now().toString(36);

            var template = '<div class="bwg-epf-checkbox-container" data-bwg-epf-checkbox="' + unique_key + '">\n' +
                '<div class="bwg-epf-checkbox-action-container">\n' +
                '<ul class="bwg-epf-checkbox-actions">\n' +
                '<li><a class="bwg-epf-checkbox-move-handler"><span class="icon-move-vertical"></span></a></li>\n' +
                '</ul>\n' +
                '</div>\n' +
                '<div class="bwg-epf-checkbox-text-container">\n' +
                '<input type="text" value="">\n' +
                '</div>\n' +
                '<div class="bwg-epf-checkbox-action-container">\n' +
                '<ul class="bwg-epf-checkbox-actions">\n' +
                '<li><a data-bwg-epf-checkbox-action="delete"><span class="icon-minus"></span></a></li>\n' +
                '<li><a data-bwg-epf-checkbox-action="add"><span class="icon-plus"></span></a></li>\n' +
                '</ul>\n' +
                '</div>\n' +
                '</div>';

            var $template = $(template);
            if (undefined === $elem) {
                $('#bwg-epf-' + uid + '-checkboxes').prepend($template);
            } else {
                $elem.after($template);
            }

            $template.find(':input[type="text"]:enabled:visible:first').focus();
        }
        ,
        /**
         * Gets the checkboxes.
         *
         * @param $item
         * @param uid
         * @returns {{(*|string): (*|string), ... }}
         */
        get_checkboxes: function ($item, uid) {
            var checkboxes = {};
            $item.find('[data-bwg-epf-checkbox]').each(function () {
                var $t = $(this);

                checkboxes[$t.data('bwg-epf-checkbox')] = $t.find('input[type="text"]').val();
            });

            return checkboxes;
        }
        ,
        /**
         * Gets the field options.
         *
         * @param $item
         * @param uid
         * @returns {{checkboxes: (*|{})}}
         */
        get_field_options: function ($item, uid) {
            return {
                'checkboxes': epf_checkbox.get_checkboxes($item, uid)
            };
        }
    };
    _epf.set_profile_field_type('checkbox', epf_checkbox);

})(jQuery, bwg, bwg.epf);
