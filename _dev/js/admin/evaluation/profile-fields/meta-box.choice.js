(function ($, _b, _epf) {
    "use strict";

    var epf_choice = {
        /**
         * Initializes a field.
         *
         * The method could add event listeners to change or input events in order to update the view.
         *
         * @param $item
         * @param uid
         */
        init_field: function ($item, uid) {
            $item.find('#bwg-epf-' + uid + '-choices').sortable({
                cursor: 'move',
                handle: '.bwg-epf-choice-move-handler',
                axis: 'y',
                placeholder: 'bwg-epf-choice-move-placeholder',
                forcePlaceholderSize: true
            });

            $item.on('click', '[data-bwg-epf-choice-action]', function () {
                var $t = $(this);
                switch ($t.data('bwg-epf-choice-action')) {
                    case 'add':
                        // Check if we are inside a choice...
                        var $choice = $t.closest('[data-bwg-epf-choice]');

                        if (0 < $choice.length) {
                            // ...we are, therefore we will add the choice after the current one.
                            epf_choice.add_choice($item, uid, $choice);
                        } else {
                            // ...we are not, therefore we will add the choice as first element.
                            epf_choice.add_choice($item, uid);
                        }
                        return;

                    case 'delete':
                        $t.closest('[data-bwg-epf-choice]').fadeSlideUpRemove(50, function () {
                            epf_choice.choice_default_validation($item, uid);
                        });
                        return;
                }
            });

            epf_choice.choice_default_validation($item, uid);
        }
        ,
        /**
         * Adds a new choice to the given field.
         *
         * @param $item The item.
         * @param uid The uid.
         * @param $elem The element after which the new choice should be added. If this is undefined, the new choice
         * would be added as first choice.
         */
        add_choice: function ($item, uid, $elem) {
            var unique_key = 'c-' + Date.now().toString(36);

            var template = '<div class="bwg-epf-choice-container" data-bwg-epf-choice="' + unique_key + '">\n'
                + '<div class="bwg-epf-choice-action-container">\n'
                + '<ul class="bwg-epf-choice-actions">\n'
                + '<li><a class="bwg-epf-choice-move-handler"><span class="icon-move-vertical"></span></a></li>\n'
                + '</ul>\n'
                + '</div>\n'
                + '<div class="bwg-epf-choice-default-container">\n'
                + '<input type="radio" name="bwg_epf_' + uid + '_choice_default" value="' + unique_key + '">\n'
                + '</div>\n'
                + '<div class="bwg-epf-choice-text-container">\n'
                + '<input type="text" value="">\n'
                + '</div>\n'
                + '<div class="bwg-epf-choice-action-container">\n'
                + '<ul class="bwg-epf-choice-actions">\n'
                + '<li><a data-bwg-epf-choice-action="delete"><span class="icon-minus"></span></a></li>\n'
                + '<li><a data-bwg-epf-choice-action="add"><span class="icon-plus"></span></a></li>\n'
                + '</ul>\n'
                + '</div>\n'
                + '</div>';

            var $template = $(template);
            if (undefined === $elem) {
                $('#bwg-epf-' + uid + '-choices').prepend($template);
            } else {
                $elem.after($template);
            }

            epf_choice.choice_default_validation($item, uid);
            $template.find(':input[type="text"]:enabled:visible:first').focus();
        }
        ,
        /**
         * Makes sure that there is exactly one choice marked as being the default choice.
         *
         * @param $item
         * @param uid
         */
        choice_default_validation: function ($item, uid) {
            var v = epf_choice.get_default_choice($item, uid);
            if ('' === v) {
                // There is currently no choice marked as being the default choice.
                // So we will try to mark the first one.
                $('input[name="bwg_epf_' + uid + '_choice_default"]').first().prop('checked', true);
            }
        }
        ,
        /**
         * Gets the selected mode.
         *
         * @param $item The item.
         * @param uid The uid.
         * @returns string
         */
        get_mode: function ($item, uid) {
            return $item.find('[name="bwg_epf_' + uid + '_mode"]:checked').val();
        }
        ,
        /**
         * Gets the choices.
         *
         * @param $item
         * @param uid
         * @returns {{(*|string): (*|string), ... }}
         */
        get_choices: function ($item, uid) {
            var choices = {};
            $item.find('[data-bwg-epf-choice]').each(function () {
                var $t = $(this);

                choices[$t.data('bwg-epf-choice')] = $t.find('input[type="text"]').val();
            });

            return choices;
        }
        ,
        /**
         * Gets the default choice.
         *
         * @param $item
         * @param uid
         * @returns {*}
         */
        get_default_choice: function ($item, uid) {
            var default_choice = $('input[name="bwg_epf_' + uid + '_choice_default"]:checked').val();
            if (undefined === default_choice) {
                return '';
            }

            return default_choice;
        }
        ,
        /**
         * Gets the field options.
         *
         * @param $item
         * @param uid
         * @returns {{mode: (*|string), choices: (*|{}), default_choice: (*|string)}}
         */
        get_field_options: function ($item, uid) {
            return {
                'mode': epf_choice.get_mode($item, uid),
                'choices': epf_choice.get_choices($item, uid),
                'default_choice': epf_choice.get_default_choice($item, uid)
            };
        }
    };
    _epf.set_profile_field_type('choice', epf_choice);

})(jQuery, bwg, bwg.epf);
