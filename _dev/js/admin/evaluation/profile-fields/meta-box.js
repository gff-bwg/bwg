bwg.epf = {};

// noinspection JSUnresolvedVariable
(function ($, _b, _epf) {
    "use strict";

    $.extend(_epf, {
        container: undefined
        ,
        profile_field_ajax_action: undefined
        ,
        profile_field_ajax_nonce: undefined
        ,
        profile_field_types: {}
        ,
        /**
         * Initializes the profile fields meta box.
         */
        init: function () {
            _epf.container = $('#bwg-epf-meta-box');
            _epf.profile_field_ajax_action = _epf.container.data('bwg-epf-profile-field-action');
            _epf.profile_field_ajax_nonce = _epf.container.data('bwg-epf-profile-field-nonce');

            ///
            // Add bwg epf action click listener.
            ///
            _epf.container.on('click', '[data-bwg-epf-action]', _epf.action);

            ///
            // Initialize the fields that are currently present.
            ///
            _epf.container.find('[data-bwg-epf-field]').each(function () {
                var $t = $(this);
                var type = $t.data('bwg-epf-type');

                var field_type = _epf.get_profile_field_type(type);
                if (undefined === field_type) {
                    $t.css('backgroundColor', '#ff684d');
                    return;
                }

                if (_.isFunction(field_type.init_field)) {
                    field_type.init_field($t, $t.data('bwg-epf-uid'));
                }
            });

            ///
            // Make the profile fields list sortable.
            ///
            $('#bwg-epf-list').sortable({
                cursor: 'move',
                handle: '.bwg-epf-field-actions > li > .bwg-epf-move-handler',
                axis: 'y',
                placeholder: 'bwg-epf-list-move-placeholder',
                forcePlaceholderSize: true
            });

            ///
            // Intercept the form submit event and update the json-field.
            ///
            $('#post').on('submit', function (event) {
                var profile_fields = [];
                _epf.container.find('[data-bwg-epf-field]').each(function () {
                    var $t = $(this);
                    var type = $t.data('bwg-epf-type');
                    var uid = $t.data('bwg-epf-uid');

                    var field_type = _epf.get_profile_field_type(type);
                    if (undefined === field_type) {
                        // The field type could not be found.
                        alert('Field type «' + type + '» could not be found...... I will remove that field.');
                        return;
                    }

                    var options = {};
                    if (_.isFunction(field_type.get_field_options)) {
                        options = field_type.get_field_options($t, uid);
                    }

                    var profile_field = {
                        'type': $t.data('bwg-epf-type'),
                        'uid': uid,
                        'label': $t.find('#bwg-epf-' + uid + '-label').val(),
                        'required': $t.find('#bwg-epf-' + uid + '-required').is(':checked'),
                        'options': options
                    };

                    profile_fields.push(profile_field);
                });

                $('#bwg-epf', _epf.container).val(JSON.stringify({
                    'introduction': tinyMCE.get('epf_introduction').getContent(),
                    'fields': profile_fields
                }));
            });
        }
        ,
        action: function (event) {
            var $t = $(this);
            switch ($t.data('bwg-epf-action')) {
                case 'add-field':
                    var type = $t.data('bwg-epf-field-type');
                    var p = $t.closest('[data-bwg-epf-add-field-position]').data('bwg-epf-add-field-position');

                    _epf.add_profile_field(type, p);
                    return;

                case 'delete-field':
                    if (confirm('Damit wird dieses Bewerterfeld gelöscht.')) {
                        $t.closest('[data-bwg-epf-field]').fadeSlideUpRemove();
                    }
                    return;
            }
        }
        ,
        set_profile_field_type: function (name, obj) {
            _epf.profile_field_types[name] = obj;
        }
        ,
        get_profile_field_type: function (name) {
            return _epf.profile_field_types[name];
        }
        ,
        add_profile_field: function (type, position) {
            var t = _epf.get_profile_field_type(type);
            if (undefined === t) {
                return alert("Sorry, profile field type «" + type + "» could not be found.");
            }

            // Generate a new unique id.
            var dt = Date.now();
            var uid = 'pf-' + dt.toString(36);
            while (_epf.container.find('[data-bwg-epf-uid=' + uid + ']').length > 0) {
                dt++;
                uid = 'pf-' + dt.toString(36);
            }

            // Build the loading template (shown during loading of the type specific options form).
            var template = $('#bwg-epf-profile-field-loading-template').html();
            template = template.replace(new RegExp('###UID###', 'g'), uid);
            template = template.replace(new RegExp('###TYPE###', 'g'), type);

            // Add the loading template.
            var $template = $(template);
            if (undefined === position || 'begin' === position) {
                $('#bwg-epf-list').prepend($template);
            } else {
                $('#bwg-epf-list').append($template);
            }

            // Build the ajax post params.
            var params = {
                '_ajax_nonce': _epf.profile_field_ajax_nonce,
                'action': _epf.profile_field_ajax_action,
                'uid': uid,
                'type': type
            };

            $.post(ajaxurl, params, function (r) {
                var $item = $(r['data']['meta_box']);

                $template.replaceWith($item);
                if (_.isFunction(t.init_field)) {
                    t.init_field($item, $item.data('bwg-epf-uid'));
                }
            }).fail(function (q) {
                $template.remove();

                if (q['responseJSON'] && q['responseJSON']['data'] && q['responseJSON']['data']['message']) {
                    alert(q['responseJSON']['data']['message']);
                }
            });
        }
    });

    // Initialize the evaluation profile fields meta box.
    $(document).ready(function () {
        _epf.init();
    });
})(jQuery, bwg, bwg.epf);
