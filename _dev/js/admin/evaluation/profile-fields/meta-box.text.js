(function ($, _b, _epf) {
    "use strict";

    var epf_text = {
        /**
         * Initializes a field.
         *
         * The method could add event listeners to change or input events in order to update the view.
         *
         * @param $item
         * @param uid
         */
        init_field: function ($item, uid) {
            ///
            // Update the view of this field ($item).
            ///
            epf_text.update_view($item, uid);

            ///
            // Register a change-listener on the field for any input-elements.
            //   On change, we will update the view of this field.
            ///
            $item.on('change', 'input', function () {
                epf_text.update_view($item, uid);
            });
        }
        ,
        /**
         * Gets the selected mode.
         *
         * @param $item The item.
         * @param uid The uid.
         * @returns string
         */
        get_mode: function ($item, uid) {
            return $item.find('[name="bwg_epf_' + uid + '_mode"]:checked').val();
        }
        ,
        /**
         * Gets the maximum length value.
         *
         * @param $item The item.
         * @param uid The uid.
         * @returns string
         */
        get_max_length: function ($item, uid) {
            return $item.find('#bwg-epf-' + uid + '-max-length').val();
        }
        ,
        /**
         * Gets the stats-enabled status.
         *
         * @param $item The item.
         * @param uid The uid.
         * @returns boolean
         */
        get_stats_enabled: function ($item, uid) {
            return $item.find('#bwg-epf-' + uid + '-stats-enabled').prop('checked');
        }
        ,
        /**
         * Updates the view.
         *
         * @param $item The item.
         * @param uid The uid.
         */
        update_view: function ($item, uid) {
            var mode = epf_text.get_mode($item, uid);

            $item.find('[data-bwg-epf-mode-hidden]').each(function () {
                var $t = $(this);
                $t.toggle(
                    -1 === $t.data('bwg-epf-mode-hidden').indexOf(mode)
                );
            });

            $item.find('[data-bwg-epf-mode-visible]').each(function () {
                var $t = $(this);
                $t.toggle(
                    -1 !== $t.data('bwg-epf-mode-visible').indexOf(mode)
                );
            });
        }
        ,
        /**
         * Gets the field options.
         *
         * @param $item The item.
         * @param uid The uid.
         * @returns {{mode: (*|string), max_length: (*|string)}}
         */
        get_field_options: function ($item, uid) {
            return {
                'mode': epf_text.get_mode($item, uid),
                'max_length': epf_text.get_max_length($item, uid),
                'stats_enabled': epf_text.get_stats_enabled($item, uid)
            };
        }
    };
    _epf.set_profile_field_type('text', epf_text);

})(jQuery, bwg, bwg.epf);
