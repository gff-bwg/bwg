bwg.ed = {};

(function ($, _b, _e) {
    "use strict";

    $.extend(_e, {
        /**
         * The evaluation definition container.
         */
        container: null
        ,
        /**
         * Is the evaluation definition fixed (restricted in editing)?
         */
        isFixed: false
        ,
        /**
         * This array would contain the used item ids.
         */
        _used_item_ids: []
        ,
        /**
         * Initializes the evaluation definition meta-box.
         */
        init: function () {
            _e.container = $('#bwg_ed_container');
            _e.isFixed = (1 === parseInt(_e.container.data('bwg-ed-fixed')) );

            // Register the form submit event handler.
            _e.container.closest('form').on('submit', function () {
                $('#bwg_ed').val(_e.jsonify());
            });

            // Register a click event that would toggle the open/close state of the items.
            _e.container.on('click', 'li .bwg-ed-item-header', function (event) {
                if ($(event.target).closest('.row-actions').length > 0) {
                    // If the click happened inside the .row-actions element, then we will not trigger the toggle.
                    return;
                }

                var $t = $(this);
                if ($t.hasClass('bwg-ed-item-header-last-level')) {
                    // The last level would not have any toggle.
                    return;
                }

                $t.closest('li').toggleClass('open');
            });

            // Register a click event for the evaluation definition actions.
            _e.container.on('click', '[data-bwg-ed-action]', _e.do_action);

            _e.item_dialog.init();
            _e.preset.load_dialog.init();
            _e.spider_chart_dialog.init();

            _e.validate_items();
            _e.make_sortable();
        }
        ,
        /**
         * Action handler.
         *
         * @param event
         */
        do_action: function (event) {
            event.preventDefault();
            event.stopPropagation();

            var $t = $(this);
            var action = $t.data('bwg-ed-action');
            switch (action) {
                case 'preset-load':
                    _e.preset.load_dialog.open();
                    break;

                case 'preset-save':
                    alert(';-)');
                    break;

                case 'spider-chart-settings':
                    _e.spider_chart_dialog.open();
                    break;

                case 'delete':
                    // Confirm?
                    $t.closest('li').remove();
                    break;

                case 'edit':
                    var itemElement = $t.closest('li');
                    _e.item_dialog.open('edit', itemElement, itemElement.closest('ul').data('bwg-ed-level'));
                    break;

                case 'add':
                    _e.item_dialog.open('add', $t.closest('.bwg-ed-items-actions').prev(), $t.data('bwg-ed-level'));
                    break;
            }
        }
        ,
        /**
         * Makes the items to be sortable.
         */
        make_sortable: function (level) {
            if (_e.isFixed) {
                return;
            }

            var levels = [0, 1, 2];
            if ('undefined' !== typeof level) {
                levels = [level];
            }

            for (var i = 0; i < levels.length; i++) {
                $('.bwg-ed-level-' + levels[i], _e.container).each(function () {
                    var $t = $(this);
                    if ($t.sortable('instance')) {
                        $t.sortable('refresh');
                    } else {
                        $t.sortable({
                            cursor: 'move',
                            connectWith: levels[i] > 0 ? '.bwg-ed-level-' + levels[i] : false,
                            handle: '> .bwg-ed-item-header',
                            placeholder: 'bwg-ed-items-move-placeholder',
                            forcePlaceholderSize: true
                        });
                    }
                });
            }

            $('ul.bwg-ed-items, ul.bwg-ed-items li', _e.container).disableSelection();
        }
        ,
        /**
         * Gets the auto increment value of the evaluation definition.
         *
         * @param increment
         */
        auto_increment: function (increment) {
            var ai = _e.container.data('bwg-ed-ai');
            if (increment) {
                ai++;

                _e.set_auto_increment(ai);
            }

            return ai;
        }
        ,
        /**
         * Sets the auto increment value.
         *
         * @param value
         */
        set_auto_increment: function (value) {
            _e.container.data('bwg-ed-ai', value);
        }
        ,
        /**
         * Validates the items.
         *
         * This method checks for example, if the level 2 items have a non-empty description.
         */
        validate_items: function () {
            $('[data-bwg-ed-id]').each(function () {
                var $t = $(this);

                var level = parseInt($t.closest('[data-bwg-ed-level]').data('bwg-ed-level'));
                if (2 === level) {
                    // In level 2 a description is required. We will validate that here.
                    var c = $t.find('> [data-bwg-ed-field="description"]').val();
                    if (0 === $.trim(c).length) {
                        $t.find('> .bwg-ed-item-header .bwg-ed-item-warning').css({'display': 'inline-block'});
                    } else {
                        $t.find('> .bwg-ed-item-header .bwg-ed-item-warning').hide({'display': undefined});
                    }
                }
            });
        }
        ,
        /**
         * Builds the definition items (recursively).
         *
         * @param $lis
         * @param level
         * @returns {Array}
         * @private
         */
        _build_items_recursive: function ($lis, level) {
            var items = [];

            $lis.each(function () {
                var $li = $(this);

                var item_id = $li.data('bwg-ed-id');
                if (undefined === item_id) {
                    item_id = _e.auto_increment(true);
                }

                if (_.contains(_e._used_item_ids, item_id)) {
                    do {
                        item_id = _e.auto_increment(true);
                    } while (_.contains(_e._used_item_ids, item_id));
                }

                _e._used_item_ids.push(item_id);

                var item = {
                    'id': item_id,
                    'open': $li.hasClass('open')
                };

                $li.find('> [data-bwg-ed-field]').each(function () {
                    var $tt = $(this);
                    item[$tt.data('bwg-ed-field')] = $tt.val();
                });
                
                if (level < 2) {
                    item['items'] = _e._build_items_recursive($li.find('> .bwg-ed-item-content > .bwg-ed-items > li'), level + 1);
                }

                items.push(item);
            });

            return items;
        }
        ,
        /**
         * Gets the maximum item id recursively.
         *
         * @param $lis
         * @param level
         * @returns {number}
         * @private
         */
        _get_max_item_id_recursive: function ($lis, level) {
            var m = 0;

            $lis.each(function () {
                var $t = $(this);

                var item_id = $t.data('bwg-ed-id');
                if (undefined === item_id) {
                    return;
                }

                m = Math.max(m, item_id);

                if (level < 2) {
                    m = Math.max(m, _e._get_max_item_id_recursive($t.find('> .bwg-ed-item-content > .bwg-ed-items > li'), level + 1));
                }
            });

            return m;
        }
        ,
        /**
         * Builds a json string of the whole evaluation definition.
         */
        jsonify: function () {
            // Get the first level li-items.
            var $lis = $('.bwg-ed-level-0 > li');

            // Get the current maximum item id.
            var current_max_id = _e._get_max_item_id_recursive($lis, 0);

            // If the current maximum item id is bigger or equal than the auto increment value,
            // we will update the auto increment value.
            if (current_max_id >= _e.auto_increment(false)) {
                _e.set_auto_increment(current_max_id + 1);
            }

            // Reset the array that would store the used item ids (would be used to ensure every item id is unique).
            _e._used_item_ids = [];

            var json = {
                'items': _e._build_items_recursive($lis, 0),
                'ai': _e.auto_increment(false)
            };

            return JSON.stringify(json);
        }
    });

    // Extend the jquery-ui dialog widget with some more options.
    $.ui.dialog.prototype.options.autoReposition = true;
    $.ui.dialog.prototype.options.percentWidth = null;
    $.ui.dialog.prototype.options.percentHeight = null;

    // Register a smart resize handler for the jquery-ui dialog widget to be re-positioned or re-sized.
    var $window = $(window);
    $window.smartresize(function () {
        $('.ui-dialog-content:visible').each(function () {
            var $t = $(this);
            if ($t.dialog('option', 'percentWidth')) {
                $t.dialog('option', 'width', $window.width() * parseInt($t.dialog('option', 'percentWidth')) / 100);
            }
            if ($t.dialog('option', 'percentHeight')) {
                $t.dialog('option', 'height', $window.height() * parseInt($t.dialog('option', 'percentHeight')) / 100);
            }
            if ($t.dialog('option', 'autoReposition')) {
                $t.dialog('option', 'position', $(this).dialog('option', 'position'));
            }
        });
    });

    // Initialize the whole evaluation definition meta box.
    $(document).ready(function () {
        _e.init();
    });
})(jQuery, bwg, bwg.ed);
