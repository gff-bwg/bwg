(function ($, _b, _e) {
    /**
     * Evaluation Definition Presets.
     */
    var _p = _e.preset = {};
    $.extend(_p, {
        /**
         *
         * @param data
         * @param cbSuccess
         * @param cbFail
         */
        ajaxDefinitionPresets: function (data, cbSuccess, cbFail) {
            data['_ajax_nonce'] = _e.container.data('bwg-ed-presets-nonce');
            data['action'] = _e.container.data('bwg-ed-presets-action');

            $.post(ajaxurl, data, cbSuccess).fail(cbFail);
        }
    });


    /**
     * Evaluation Definition Presets Load Dialog.
     */
    var _pld = _p.load_dialog = {};
    $.extend(_pld, {
        /**
         * The preset load dialog ui.
         */
        d: null,
        /**
         * The list that holds the presets.
         */
        list: null,
        /**
         * Initializes the preset load dialog.
         */
        init: function () {
            _pld.list = $('#bwg_ed_preset_loader_list');
            _pld.d = $('#bwg_ed_preset_loader').dialog({
                autoOpen: false,
                modal: true,
                height: 400,
                width: 400,
                buttons: [
                    {
                        text: 'Abbrechen',
                        click: _pld.close
                    }
                ]
            });
        }
        ,
        /**
         * Opens the preset load dialog and immediately starts loading the presets.
         */
        open: function () {
            // Add click event listener to the items of the presets list.
            _pld.list.on('click', 'a[data-id]', _pld.loadPreset);

            // Set the state of the loader to be 'waiting' and open the dialog.
            _pld.d.bwg('state', $.bwg.state.waiting).dialog('open');

            _p.ajaxDefinitionPresets(
                {'command': 'preset-load-list'},
                function (response) {
                    if (response.success) {
                        var html = '';
                        var data = response.data;
                        if (data.length == 0) {
                            html = '<li>Keine Vorlagen gefunden.</li>';
                        } else {
                            for (var i = 0; i < data.length; i++) {
                                html += '<li><a href="#" data-id="' + data[i]['id'] + '">' + data[i]['label'] + ' [' + data[i]['id'] + ']</a></li>';
                            }
                        }
                        _pld.list.html(html);

                        _pld.d.bwg('state', $.bwg.state.normal);
                    }
                },
                function () {
                    _pld.d.bwg('state', $.bwg.state.error);
                }
            );
        }
        ,
        /**
         * Closes the preset load dialog.
         */
        close: function () {
            // Remove the click event listener on the items of the presets list.
            _pld.list.off('click', 'a[data-id]', _pld.loadPreset);
            _pld.d.dialog('close');
        }
        ,
        /**
         * Loads the preset evaluation definition and replaces the current definition.
         *
         * This is an event handler that would be called when the user clicks a preset item.
         *
         * @param event
         */
        loadPreset: function (event) {
            event.preventDefault();

            _pld.d.bwg('state', $.bwg.state.waiting);

            _p.ajaxDefinitionPresets(
                {'command': 'preset-load', 'id': $(this).data('id')},
                function (response) {
                    $('#bwg_ed').val(response.data['val']);
                    $('#bwg_ed_tree').html(response.data['html']);

                    _e.make_sortable();
                    _pld.close();
                }, function () {
                    alert('Fehler beim Laden der Vorlage.');
                    _pld.close();
                });
        }
    });


    /**
     * Evaluation Definition Presets Save Dialog.
     */
    var _psd = _p.save_dialog = {};
    $.extend(_psd, {
        d: null,
        init: function () {

        }
    });
})(jQuery, bwg, bwg.ed);
