(function ($, _b, _e) {
    var _id = _e.item_dialog = {};

    $.extend(_id, {
        /**
         * The dialog ui.
         */
        d: null,
        /**
         * The tinyMCE editor id for the description field.
         */
        editor_id: 'bwg_ed_item_editor_description',
        /**
         * The tinyMCE editor id for the details field.
         */
        details_editor_id: 'bwg_ed_item_editor_details',
        /**
         * The label field (jQuery element).
         */
        field_label: undefined,
        /**
         * The weight field (jQuery element).
         */
        field_weight: undefined,
        /**
         * The spider chart field (jQuery element).
         */
        field_spider_chart: undefined,
        /**
         * Initializes the item dialog.
         */
        init: function () {
            _id.d = $('#bwg_ed_item_editor').dialog({
                autoOpen: false,
                width: Math.min(1000, 0.9 * $(window).width()),
                height: Math.min(800, 0.9 * $(window).width()),
                buttons: [
                    {
                        text: 'Ok',
                        click: _id.saveAndClose
                    },
                    {
                        text: 'Abbrechen',
                        click: _id.close
                    }],
                open: function (event, ui) {
                    // Change the z-index of the dialog and the jquery-ui-widget-overlay because of the tinyMCE z-indexes.
                    $('#bwg_ed_item_editor').dialog('widget').css({'z-index': 100050});
                    $('.ui-widget-overlay').css({'z-index': 100049});
                },
                close: function (event, ui) {
                    // Reset the jquery-ui-widget overlay z-index.
                    $('.ui-widget-overlay').css({'z-index': undefined});
                }
            });

            $('form', _id.d).on('submit', function (event) {
                event.preventDefault();
                _id.saveAndClose();
            });

            _id.field_label = $('#bwg_ed_item_editor_label', _id.d);
            _id.field_weight = $('#bwg_ed_item_editor_weight', _id.d);
            _id.field_spider_chart = $('#bwg_ed_item_editor_spider_chart', _id.d);
        }
        ,
        tiny_mce: function (id, contents) {
            var editor = tinyMCE.get(id);

            tinyMCE.remove(editor);
            tinyMCE.init(tinyMCEPreInit.mceInit[editor.id]);

            tinyMCE.get(id).setContent(contents);
        }
        ,
        /**
         * Opens the dialog.
         *
         * @param action Either 'add' or 'edit'.
         * @param itemElement If 'add' this is the <ul> element we want to add into, else this is the item element to be edited.
         * @param level The level.
         */
        open: function (action, itemElement, level) {
            _id.d.data('action', action).data('element', itemElement).data('level', level);

            var title = '';
            if ('edit' === action) {
                title = 'Bearbeiten';
                _id.field_label.val(itemElement.find('[data-bwg-ed-field="label"]').val());
                _id.field_weight.val(itemElement.find('[data-bwg-ed-field="weight"]').val());
                _id.field_spider_chart.prop('checked', itemElement.find('[data-bwg-ed-field="spider_chart"]').val() === '1');

                _id.tiny_mce(_id.editor_id, itemElement.find('[data-bwg-ed-field="description"]').val());
                _id.tiny_mce(_id.details_editor_id, itemElement.find('[data-bwg-ed-field="details"]').val());
            } else {
                title = 'Hinzufügen';
                _id.field_label.val('');
                _id.field_weight.val('1.0');
                _id.field_spider_chart.prop('checked', true);

                _id.tiny_mce(_id.editor_id, '');
                _id.tiny_mce(_id.details_editor_id, '');
            }

            _id.field_weight.prop('disabled', _e.isFixed);

            var level_label = _e.container.find('[data-bwg-level-' + level + '-label]').text();

            _id.d
                .removeClass('level-0 level-1 level-2')
                .addClass('level-' + level)
                .dialog('option', 'title', title + ' (' + level_label + ')')
                .dialog('open');
            _id.d.find('.ui-dialog-buttonpane button').prop('disabled', false);
        }
        ,
        /**
         * Saves and closes the dialog.
         */
        saveAndClose: function () {
            _id.d.find('.ui-dialog-buttonpane button').prop('disabled', true);

            var label = _id.field_label.val();
            var weight = _id.field_weight.val();
            var spider_chart = _id.field_spider_chart.is(':checked') ? '1' : '0';
            var description = tinyMCE.get(_id.editor_id).getContent();
            var details = tinyMCE.get(_id.details_editor_id).getContent();

            var itemElement = _id.d.data('element');
            if ('add' === _id.d.data('action')) {
                var template = $('#bwg_ed_item_template' + _id.d.data('level')).html();
                template = template.replace(/###ID###/g, _e.auto_increment(true));
                template = template.replace(/###LABEL###/g, label);
                template = template.replace(/###DESCRIPTION###/g, description);
                template = template.replace(/###DETAILS###/g, details);
                template = template.replace(/###WEIGHT###/g, weight);
                template = template.replace(/###SPIDER_CHART###/g, spider_chart);
                itemElement.append(template);

                _id.close();

                _e.make_sortable(_id.d.data('level'));
            } else {
                itemElement.find('>[data-bwg-ed-field="label"]').val(label);
                itemElement.find('>[data-bwg-ed-field="description"]').val(description);
                itemElement.find('>[data-bwg-ed-field="details"]').val(details);
                itemElement.find('>[data-bwg-ed-field="weight"]').val(weight);
                itemElement.find('>[data-bwg-ed-field="spider_chart"]').val(spider_chart);
                itemElement.find('>.bwg-ed-item-header strong').text(label);
                _id.close();
            }

            _e.validate_items();
        }
        ,
        /**
         * Closes the dialog.
         */
        close: function () {
            _id.d.dialog('close');
        }
    });
})(jQuery, bwg, bwg.ed);
