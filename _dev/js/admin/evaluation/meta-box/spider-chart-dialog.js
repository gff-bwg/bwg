(function ($, _b, _e) {
    /**
     * Spider Chart Dialog.
     */
    var _scd = _e.spider_chart_dialog = {};
    $.extend(_scd, {
        /**
         * The dialog.
         */
        dialog: null,
        /**
         * Initializes the spider chart dialog.
         */
        init: function () {
            _scd.dialog = $('#bwg_ed_spider_chart_settings').dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: false,
                percentWidth: 80,
                percentHeight: 80,
                buttons: [
                    {
                        text: 'Ok',
                        click: function () {
                            _scd.saveAndClose();
                        }
                    },
                    {
                        text: 'Abbrechen',
                        click: function () {
                            _scd.close();
                        }
                    }
                ],
                open: function () {
                    _scd.dialog
                        .dialog('option', 'width', $(window).width() * .8)
                        .dialog('option', 'height', $(window).height() * .8);
                }
            });
        }
        ,
        /**
         * Opens the dialog and starts loading the content.
         */
        open: function () {
            _scd.dialog.bwg('state', $.bwg.state.waiting).dialog('open');
            _scd.ajax({
                command: 'load-charts',
                ed: _e.jsonify()
            }, function (response) {
                var html = '';

                // TODO: Build the whole spider chart settings html.
                if (response.success) {
                    for (var i = 0; i < response['data']['charts'].length; i++) {
                        var template = $('#bwg_ed_spider_chart_settings_template').html();
                        template = template.replace(/###NAME###/g, 'Chart ' + i).replace(/###SVG###/g, response['data']['charts'][i]);

                        html += template;
                    }
                } else {
                    html += '<p>Fehler beim Laden der Spider Charts.</p>';
                }

                $('#bwg_ed_spider_charts_container').html(html);
                _scd.dialog.bwg('state', $.bwg.state.normal);
            }, function () {
                _scd.dialog.bwg('state', $.bwg.state.error);
            });
        }
        ,
        /**
         * Saves and closes.
         */
        saveAndClose: function () {
            console.log('TODO: Save spider chart settings into data of elements.');
            _scd.close();
        }
        ,
        /**
         * Closes the dialog.
         */
        close: function () {
            _scd.dialog.dialog('close');
        }
        ,
        /**
         * Send an ajax request out to the server.
         *
         * @param data
         * @param cbSuccess
         * @param cbFail
         */
        ajax: function (data, cbSuccess, cbFail) {
            data['_ajax_nonce'] = _e.container.data('bwg-ed-spider-chart-settings-nonce');
            data['action'] = _e.container.data('bwg-ed-spider-chart-settings-action');

            $.post(ajaxurl, data, cbSuccess).fail(cbFail);
        }
    });
})(jQuery, bwg, bwg.ed);
