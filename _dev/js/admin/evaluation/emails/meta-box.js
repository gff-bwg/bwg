bwg.em = {};

(function ($, _b, _m) {
    "use strict";

    $.extend(_m, {
        /**
         * The container.
         */
        container: undefined
        ,
        /**
         * The post id.
         */
        post_ID: undefined
        ,
        /**
         * Ajax emails test action.
         */
        emails_test_action: undefined
        ,
        /**
         * Ajax emails test nonce.
         */
        emails_test_nonce: undefined
        ,
        /**
         * Initializes the extras meta box.
         */
        init: function () {
            _m.container = $('#bwg-em-meta-box');
            _m.post_ID = _m.container.data('bwg-post-id');
            _m.emails_test_action = _m.container.data('bwg-emails-test-action');
            _m.emails_test_nonce = _m.container.data('bwg-emails-test-nonce');

            _m.container.on('click', '[data-bwg-action]', function (event) {
                event.preventDefault();

                var $t = $(event.target);
                switch ($t.data('bwg-action')) {
                    case 'send-test-email':
                        $t.prop('disabled', 'disabled').nextAll('.spinner').css('visibility', 'visible');

                        var post = {
                            '_ajax_nonce': _m.emails_test_nonce,
                            'action': _m.emails_test_action,
                            'post_ID': _m.post_ID,
                            'post_title': $('#title').val(),
                            'email': $t.data('bwg-email'),
                            'text': $('#' + $t.data('bwg-target')).val()
                        };

                        $.post(ajaxurl, post, function () {
                            _m.emails_test_notification($t, true);
                        }).fail(function (q) {
                            if (q['responseJSON'] && q['responseJSON']['data'] && q['responseJSON']['data']['message']) {
                                alert(q['responseJSON']['data']['message']);
                            }

                            _m.emails_test_notification($t, false);
                        }).always(function () {
                            $t.prop('disabled', null).nextAll('.spinner').css('visibility', 'hidden');
                        });
                        break;
                }
            });
        }
        ,
        /**
         * @param $t
         * @param success
         */
        emails_test_notification: function ($t, success) {
            var classes;
            if (success) {
                classes = ['dashicons-yes', 'action-success'];
            } else {
                classes = ['dashicons-no', 'action-failure'];
            }

            var $icon = $('<span class="dashicons action-notification ' + classes.join(' ') + '"></span>');
            $t.after($icon);

            setTimeout(function () {
                $icon.fadeOut(500, function () {
                    $(this).remove();
                });
            }, 2000);
        }
    });

    // Initialize the whole evaluation extras meta box.
    $(document).ready(function () {
        _m.init();
    });
})(jQuery, bwg, bwg.em);
