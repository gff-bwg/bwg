bwg.evaluation_full_view = {};

(function ($, _b, _efw) {
    "use strict";

    $.extend(_efw, {
        container: undefined
        ,
        /**
         * Initializes the evaluation full view.
         */
        init: function () {
            _efw.container = $('.bwg-full-view');

            _efw.container.on('change', 'input', function () {
                _efw.update_grading();
            });

            _efw.update_grading();
        }
        ,
        /**
         * Updates the grading (sub-totals and totals).
         */
        update_grading: function () {
            $('[data-grading-sub-total]').each(function () {
                var $t = $(this);

                var sub_total_id = $t.data('grading-sub-total');
                var v_total = 0;
                var w_total = 0;
                $('[data-grading-sub-total-ref="' + sub_total_id + '"]').each(function () {
                    var $u = $(this);
                    var grading_name = $u.data('grading-name');

                    var v = $('input[name="' + grading_name + '"]:checked').val();
                    if (undefined === v) {
                        return;
                    }

                    var grading_weight = parseFloat($u.data('grading-weight'));
                    w_total += grading_weight;
                    v_total += grading_weight * parseFloat(v);
                });

                if (0 === w_total) {
                    $t.html('1.0');
                } else {
                    var p = v_total / w_total;
                    $t.html(p.toFixed(1));
                }
            });

            $('[data-grading-total]').each(function () {
                var $t = $(this);

                var total_id = $t.data('grading-total');
                var v_total = 0;
                var w_total = 0;
                $('[data-grading-total-ref="' + total_id + '"]').each(function () {
                    var $u = $(this);
                    var grading_name = $u.data('grading-name');
                    var v;

                    if (undefined === grading_name) {
                        // This is a sub-total grading - take the value.
                        v = parseFloat($u.html());
                    } else {
                        v = $('input[name="' + grading_name + '"]:checked').val();
                        if (undefined === v) {
                            return;
                        }
                    }

                    var grading_weight = parseFloat($u.data('grading-weight'));
                    w_total += grading_weight;
                    v_total += grading_weight * parseFloat(v);
                });
                
                if (0 === w_total) {
                    $t.html('1.0');
                } else {
                    var p = v_total / w_total;
                    $t.html(p.toFixed(1));
                }
            });

            _b.defer_action('jh', function () {
                var data = {};
                data['_ajax_nonce'] = _efw.container.data('spider-chart-nonce');
                data['action'] = _efw.container.data('spider-chart-action');
                data['post'] = _efw.container.data('post-id');

                var grading = [];
                $('[data-grading-total]').each(function () {
                    var $t = $(this);
                    var points = Math.round(10 * parseFloat($t.html()));

                    grading.push($t.data('bwg-ed-id') + '=' + points);
                });
                data['grading'] = grading.join('|');

                $.post(ajaxurl, data, function (response) {
                    for (var i = 0; i < response['data']['charts'].length; i++) {
                        var chart = response['data']['charts'][i];
                        $('#spider-chart-' + chart['id']).html(chart['svg']);
                    }
                }).fail(function () {
                    alert("Fehler beim Laden der Spider-Charts.");
                });
            });
        }
    });

    // Initialize the evaluation full view.
    $(document).ready(function () {
        _efw.init();
    });
})(jQuery, bwg, bwg.evaluation_full_view);
