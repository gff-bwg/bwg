bwg.ee = {};

(function ($, _b, _e) {
    "use strict";

    $.extend(_e, {
        container: undefined
        ,
        /**
         * Initializes the extras meta box.
         */
        init: function () {
            _e.container = $('#bwg-ee-meta-box');

            $('#post').on('submit', function (event) {
                $('.form-invalid', _e.container).removeClass('form-invalid');
                var isValid = true;

                if ($('#bwg-ee-postage', _e.container).is(':checked')) {
                    // Postage is on.
                    isValid &= _b.validate_date_from_to_field(
                        $('#bwg-ee-registration-open-from-to', _e.container),
                        $('#bwg-ee-registration-open-from', _e.container),
                        $('#bwg-ee-registration-open-to', _e.container)
                    );

                    var numbers_only = /^\d+$/;
                    if (!numbers_only.test($('#bwg-ee-postage-max', _e.container).val())) {
                        $('#bwg-ee-postage-max-container', _e.container).addClass('form-invalid');
                        isValid &= false;
                    }
                }

                isValid &= _b.validate_date_from_to_field(
                    $('#bwg-ee-rating-open-from-to', _e.container),
                    $('#bwg-ee-rating-open-from', _e.container),
                    $('#bwg-ee-rating-open-to', _e.container)
                );

                if (!isValid) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            });

            var $postage = $('#bwg-ee-postage', _e.container);
            $postage.on('change', function () {
                if ($(this).is(':checked')) {
                    $('#bwg-ee-postage-options', _e.container).slideDown("fast");
                } else {
                    $('#bwg-ee-postage-options', _e.container).slideUp("fast");
                }
            });

            if ($postage.is(':checked')) {
                $('#bwg-ee-postage-options', _e.container).show();
            } else {
                $('#bwg-ee-postage-options', _e.container).hide();
            }
        }
    });

    // Initialize the whole evaluation extras meta box.
    $(document).ready(function () {
        _e.init();
    });
})(jQuery, bwg, bwg.ee);
