<?php
/**
 * Variables
 * ---------
 *
 * @var string[] $title_labels
 * @var string $note
 */

//@formatter:off
?>
<p>&nbsp;</p>

<p><i><?php

for ( $i = 0; $i < count($title_labels); $i++ ) {
    if ( $i > 0 ) {
        echo ' / ';
    }
    esc_html_e($title_labels[$i]);
}

?>:</i><br><?php

if ( empty(trim($note)) ) {
    echo '&ndash;';
} else {
    echo '«'.nl2br(esc_html($note)).'»';
}

?></p>
