<?php
/**
 * Variables
 * ---------
 *
 * @var string $evaluation_title
 * @var string $today
 * @var boolean $others_visible
 * @var string|null $title_suffix
 */

//@formatter:off
?>

<h1><?php echo $evaluation_title; ?> &ndash; <?php
    if ( is_null($title_suffix)) {
	    _e( 'Ihre Bewertung', 'bwg' );
    } else {
        echo esc_html($title_suffix);
    }
?></h1>

<p><?php
    /* translators: %1$s: The date like "23.05.2017". %2$s: Placeholder for additional text in case the averages of the other users are visible. */
    printf( __( 'Danke, dass Sie am %1$s Ihre Bewertung eingereicht haben. Hier sehen Sie Ihre <b>Bewertungen auf einen Blick</b>%2$s:', 'bwg' ),
        $today, $others_visible ? __( ' (orange Ihre Bewertung, blau der Durchschnitt der übrigen Bewertungen)', 'bwg' ) : ''
	); ?></p>

<p>&nbsp;</p>
