<?php
/**
 * Variables
 * ---------
 *
 * @var \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
 * @var string $intro
 * @var \bwg\charts\BWG_Spider_Chart[] $spider_charts
 */

$base = bwg_base();
?>
<div id="bwg-e">
    <div class="bwg-row">
        <div class="bwg-navigation-col">
			<?php if ( has_post_thumbnail( $evaluation_post->get_post() ) ) { ?>
                <div class="bwg-thumbnail-container">
					<?php echo get_the_post_thumbnail( $evaluation_post->get_post(), 'medium' ); ?>
                </div>
			<?php } ?>

            <p>&nbsp;</p></div>
        <div class="bwg-content-col">
			<?php
			if ( ! empty( $intro ) ) {
				echo $intro;
			}
			?>

            <div style="display: flex; flex-wrap: wrap;">
				<?php
				foreach ( $spider_charts as $spider_chart ) {
					?>
                    <div style="max-width: 400px; width: 100%;"><?php
					echo $spider_chart->get_svg( FALSE );
					?></div><?php
				}
				?>
            </div>

	        <?php if ( FALSE ) { ?>
            <p>
                <a href="<?php echo $base->options()->get_option_overview_page_link(); ?>"><?php
					_e( 'Zur Übersicht', 'bwg' ); ?></a>
            </p>
	        <?php } ?>
        </div>
    </div>
</div>
