<?php
/**
 * @var string $bwg_mode
 * @var string $register_url
 * @var string $login_url
 */
?>

<p class="bwg-register-login">
	<?php if ( $bwg_mode !== 'drogothek' ) { ?>
        <a href="<?php esc_attr_e( $register_url ) ?>"><?php
			_e( 'Jetzt registrieren und bewerten.', 'bwg' );
			?></a><br>
	<?php } ?>
    <a href="<?php esc_attr_e( $login_url ); ?>"><?php
		if ( $bwg_mode !== 'drogothek' ) {
			_e( 'Jetzt anmelden und bewerten wenn Sie schon registriert sind.', 'bwg' );
		} else {
			_e( 'Jetzt anmelden und bewerten.', 'bwg' );
		}
		?></a>
</p>
