<?php

/**
 * @var string $message The message to be printed. HTML code allowed. Would be wrapped into <p>-tags.
 */

?>

<div class="wrap">
    <h1>Fehler</h1>

    <div class="error">
        <p><?php echo $message; ?></p>
    </div>

</div>
