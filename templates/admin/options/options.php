<?php

/**
 * Variables
 * ---------
 *
 * @var string $title
 * @var string $option_group
 * @var string $page
 */

?>

<div class="wrap">
    <h1>Einstellungen › <?php esc_html_e( $title ); ?></h1>

    <form action="options.php" method="post">
		<?php settings_fields( $option_group ); ?>
		<?php do_settings_sections( $page ); ?>

        <p class="submit">
            <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e( 'Save Changes' ); ?>"/>
        </p>
    </form>
</div>
