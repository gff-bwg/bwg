<?php
/**
 * Variables
 * ---------
 *
 * @var string $question
 * @var double $avg
 * @var int $count
 * @var double $std
 * @var array $histogram
 */

$histogram_height = 100;
?>

<div>
	<?php echo wpautop( $question ); ?>

    <div style="display: flex">
        <table>
            <tbody>
            <tr>
                <td>Durchschnitt</td>
                <td style="padding-left: 20px; text-align: right;"><?php printf( '%.02f', $avg ); ?></td>
            </tr>
            <tr>
                <td>Anzahl</td>
                <td style="padding-left: 20px; text-align: right;"><?php echo $count; ?></td>
            </tr>
            <tr>
                <td>Standardabweichung</td>
                <td style="padding-left: 20px; text-align: right;"><?php printf( '%.02f', $std ); ?></td>
            </tr>
            </tbody>
        </table>

        <table style="margin-left: 30px; background-color: rgba(255, 255, 255, 0.5);">
            <thead>
            <tr>
                <td colspan="6" style="text-align:center; border-bottom:1px solid rgba(0, 0, 0, 0.2);">
                    Histogramm
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="vertical-align: bottom; width: 20px; height: <?php echo $histogram_height; ?>px;">
                    <div style="width: 100%; height: <?php
					echo $histogram_height * bwg_base()
							->utils()
							->safe_division( $histogram['#1'], $histogram['total'] );
					?>px; background-color: #042037;" title="<?php esc_attr_e( $histogram['#1'] ); ?>">
                    </div>
                </td>
                <td style="vertical-align: bottom; width: 20px; height: <?php echo $histogram_height; ?>px;">
                    <div style="width: 100%; height: <?php
					echo $histogram_height * bwg_base()
							->utils()
							->safe_division( $histogram['#2'], $histogram['total'] );
					?>px; background-color: #123552;" title="<?php esc_attr_e( $histogram['#2']
					); ?>">
                    </div>
                </td>
                <td style="vertical-align: bottom; width: 20px; height: <?php echo $histogram_height; ?>px;">
                    <div style="width: 100%; height: <?php
					echo $histogram_height * bwg_base()
							->utils()
							->safe_division( $histogram['#3'], $histogram['total'] );
					?>px; background-color: #2A4F6E;" title="<?php esc_attr_e( $histogram['#3'] ); ?>">
                    </div>
                </td>
                <td style="vertical-align: bottom; width: 20px; height: <?php echo $histogram_height; ?>px;">
                    <div style="width: 100%; height: <?php
					echo $histogram_height * bwg_base()
							->utils()
							->safe_division( $histogram['#4'], $histogram['total'] );
					?>px; background-color: #41607a;" title="<?php esc_attr_e( $histogram['#4'] ); ?>">
                    </div>
                </td>
                <td style="vertical-align: bottom; width: 20px; height: <?php echo
				$histogram_height;
				?>px;">
                    <div style="width: 100%; height: <?php
					echo $histogram_height * bwg_base()
							->utils()
							->safe_division( $histogram['#5'], $histogram['total'] );
					?>px; background-color: #496C89;" title="<?php esc_attr_e( $histogram['#5'] ); ?>">
                    </div>
                </td>
                <td style="vertical-align: bottom; width: 20px; height: <?php echo $histogram_height; ?>px;">
                    <div style="width: 100%; height: <?php
					echo $histogram_height * bwg_base()
							->utils()
							->safe_division( $histogram['#6'], $histogram['total'] );
					?>px; background-color: #718DA5;" title="<?php esc_attr_e( $histogram['#6'] ); ?>">
                    </div>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;border-top:1px solid rgba(0,0,0,0.2);">1</td>
                <td style="text-align:center;border-top:1px solid rgba(0,0,0,0.2);">2</td>
                <td style="text-align:center;border-top:1px solid rgba(0,0,0,0.2);">3</td>
                <td style="text-align:center;border-top:1px solid rgba(0,0,0,0.2);">4</td>
                <td style="text-align:center;border-top:1px solid rgba(0,0,0,0.2);">5</td>
                <td style="text-align:center;border-top:1px solid rgba(0,0,0,0.2);">6</td>
            </tr>
            </tbody>
        </table>
    </div>

</div>
