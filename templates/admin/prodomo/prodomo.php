<?php
/**
 * Variables
 * ---------
 *
 * @var array $statistics
 * @var array $histograms
 * @var string $question1
 * @var string $question2
 * @var string $question3
 */
?>

<div class="wrap">
    <h1 class="wp-heading-inline">
		<?php _e( 'Prodomo', 'bwg' ); ?>
    </h1>

    <hr class="wp-header-end">

	<?php
	echo bwg_base()->utils()->render_admin_template( 'prodomo/prodomo-question.php', [
		'question'  => $question1,
		'avg'       => $statistics['a1'],
		'count'     => $statistics['c1'],
		'std'       => $statistics['s1'],
		'histogram' => $histograms['q1'],
	] );
	?>

    <hr>

	<?php
	echo bwg_base()->utils()->render_admin_template( 'prodomo/prodomo-question.php', [
		'question'  => $question2,
		'avg'       => $statistics['a2'],
		'count'     => $statistics['c2'],
		'std'       => $statistics['s2'],
		'histogram' => $histograms['q2'],
	] );
	?>

    <hr>

	<?php
	echo bwg_base()->utils()->render_admin_template( 'prodomo/prodomo-question.php', [
		'question'  => $question3,
		'avg'       => $statistics['a3'],
		'count'     => $statistics['c3'],
		'std'       => $statistics['s3'],
		'histogram' => $histograms['q3'],
	] );
	?>

</div>
