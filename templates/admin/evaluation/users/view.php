<?php
/**
 * Variables
 * ---------
 *
 * @var \WP_Post $post
 * @var \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
 * @var \bwg\database\models\BWG_User_Storage $user_storage
 * @var array $weighted_gradings
 * @var \bwg\database\models\BWG_Submission|null $submission The submission.
 * @var string $username
 * @var string $sum_time_active
 * @var string $sum_time_inactive
 * @var string $sum_action_goto
 * @var string $sum_action_vote
 */

$notes = $user_storage->get_notes();
?>

<div class="wrap">
    <h1 class="wp-heading-inline">
		<?php esc_html_e( $post->post_title ); ?>
        &rsaquo; <?php _e( 'Eingereichte Bewertungen', 'bwg' ); ?>
        &rsaquo; <?php esc_html_e( $username ); ?>
    </h1>
    <hr class="wp-header-end">

    <p><a href="javascript:history.back()">&laquo; Zurück</a></p>

    <table class="bwg-user-view-stats-table">
        <tbody>
        <tr>
            <td class="bwg-table-label"><?php _e( 'Total aktive Dauer:', 'bwg' ); ?></td>
            <td class="bwg-table-value-right"><?php esc_html_e( $sum_time_active ); ?></td>
        </tr>
        <tr>
            <td class="bwg-table-label"><?php _e( 'Total inaktive Dauer:', 'bwg' ); ?></td>
            <td class="bwg-table-value-right"><?php esc_html_e( $sum_time_inactive ); ?></td>
        </tr>
        <tr>
            <td class="bwg-table-label"><?php _e( 'Total Anzahl Bereichswechsel:', 'bwg' ); ?></td>
            <td class="bwg-table-value-right"><?php esc_html_e( $sum_action_goto ); ?></td>
        </tr>
        <tr>
            <td class="bwg-table-label"><?php _e( 'Total Anzahl Bewertungsklicks:', 'bwg' ); ?></td>
            <td class="bwg-table-value-right"><?php esc_html_e( $sum_action_vote ); ?></td>
        </tr>
        </tbody>
    </table>

    <hr>
	<?php
	$non_empty_notes = [];
	foreach ( $evaluation_post->get_evaluation_definition()->get_items() as $item1 ) {
		?>
        <h2><?php esc_html_e( $item1->get_label() ); ?></h2>
        <div class="bwg-view-gradings-card-container">
			<?php
			foreach ( $item1->get_items() as $item2 ) {
				?>
                <div class="bwg-view-card bwg-view-gradings-card">
                    <div class="bwg-view-card-title">
                        <div style="float: right;"><?php
							if ( 1 === $item2->get_spider_chart() ) {
								echo isset( $weighted_gradings[ '#' . $item2->get_id() ] ) ?
									sprintf(
										'%.01f',
										$weighted_gradings[ '#' . $item2->get_id() ]
									) :
									'&ndash;';
							}
							?>
                        </div>
                        <b><?php esc_html_e( $item2->get_label() ); ?></b>
                    </div>
                    <div class="bwg-view-card-content">
                        <ul class="bwg-user-view-grading-list">
							<?php
							foreach ( $item2->get_items() as $item3 ) {
								?>
                                <li class="has-grading">
									<?php esc_html_e( $item3->get_label() ); ?>
                                    <ins class="spacer"></ins>
									<?php
									if ( $submission->has_grading( $item3->get_id() ) ) {
										echo sprintf( '%.01f', $submission->get_grading( $item3->get_id() ) );
									} else {
										echo '&ndash;';
									}
									?></li>
								<?php
							}
							?>
                        </ul>
						<?php
						if (
							isset( $notes[ '#' . $item2->get_id() ] ) &&
							! empty( $notes[ '#' . $item2->get_id() ]['value'] )
						) {
							echo '<span class="dashicons dashicons-admin-comments"></span>';
							array_push( $non_empty_notes, [
								'breadcrumb' => $item1->get_label() . ' » ' . $item2->get_label(),
								'value'      => $notes[ '#' . $item2->get_id() ]['value'],
							] );
						}
						?>
                    </div>
                </div>
				<?php
			}
			?>
        </div>
		<?php
	}
	?>

	<?php
	if ( ! empty( $non_empty_notes ) ) {
		?>
        <hr>
        <h2>Anmerkungen</h2>
        <div class="bwg-note-container">
			<?php
			foreach ( $non_empty_notes as $non_empty_note ) {
				?>
                <div class="bwg-view-card">
                    <div class="bwg-view-card-title"><b><?php
							esc_attr_e( $non_empty_note['breadcrumb'] );
							?></b></div>
                    <div class="bwg-view-card-content">
                        <p><?php
							echo nl2br( esc_html( $non_empty_note['value'] ) );
							?></p>
                    </div>
                </div>
				<?php
			}
			?>
        </div>
		<?php
	}
	?>

    <hr>
    <h2>Netzdiagramm</h2>
    <div class="bwg-view-spider-container">
		<?php
		foreach ( $evaluation_post->get_evaluation_definition()->get_items() as $item1 ) {
			$chart = new \bwg\charts\BWG_Spider_Chart( [
				'title' => [
					'text' => $item1->get_label(),
				],
			] );

			$data_points_user = [];
			foreach ( $item1->get_items() as $item2 ) {
				if ( 0 === $item2->get_spider_chart() ) {
					continue;
				}

				$chart->add_leg( $item2->get_label( FALSE ) );
				if ( isset( $weighted_gradings[ '#' . $item2->get_id() ] ) ) {
					array_push( $data_points_user, $weighted_gradings[ '#' . $item2->get_id() ] );
				} else {
					array_push( $data_points_user, 1 );
				}
			}
			$chart->add_series( $data_points_user );

			echo '<div class="bwg-view-card"><div class="bwg-view-card-content">';
			echo $chart->get_svg();
			echo '</div></div>';
		}
		?>
    </div>

</div>
