<?php
/**
 * Variables
 * ---------
 *
 * @var \WP_Post $post
 * @var \bwg\evaluation\BWG_Evaluation_Post $evaluation
 * @var int $total_nr_submissions
 * @var \bwg\stats\strata\BWG_Stats_Strata[] $stratas
 * @var array $strata_gradings
 * @var array $strata_statistics
 * @var array $gradings Weighted gradings
 * @var \bwg\stats\BWG_Stats_Gradings $statistics
 * @var \bwg\charts\BWG_Spider_Chart[] $spider_charts Interest spider charts.
 * @var string[] $profile_field_strata_stats
 * @var string[] $profile_field_stats
 * @var string[] $notes
 */

$utils = bwg_base()->utils();
?>
<div class="wrap">
    <h1 class="wp-heading-inline">
		<?php esc_html_e( $post->post_title ); ?>
        &rsaquo; <?php _e( 'Eingereichte Bewertungen', 'bwg' ); ?>
        &rsaquo; <?php _e( 'Statistik', 'bwg' ); ?>
    </h1>
    <hr class="wp-header-end">

    <p><a href="javascript:history.back()">&laquo; Zurück</a></p>

    <p>
        Total eingereichte Bewertungen: <?php echo number_format_i18n( $total_nr_submissions ); ?>
    </p>

    <div style="margin-bottom: 10px;">
        <h3>Bewerterfelder</h3>

        <div class="bwg-strata-stats-container">
			<?php echo implode( '', $profile_field_strata_stats ); ?>
        </div>
    </div>

    <div style="margin-bottom: 10px;">
        <h3>Bewertungen</h3>

        <p>
            <a href="<?php echo add_query_arg( 'action', 'export-stats' ); ?>">
                » Alles in eine Excel-Datei exportieren
            </a>
        </p>

        <table class="bwg-strata-table">
			<?php
			$i = 0;
			foreach ( $strata_statistics as $strata_statistic ) {
				/** @var \bwg\stats\strata\BWG_Stats_Strata $strata */
				$strata = $strata_statistic['strata'];

				/** @var \bwg\stats\BWG_Stats_Gradings $stats */
				$stats = $strata_statistic['statistics'];
				?>
                <tr class="bwg-strata-legend-item"
                    data-action="toggle-strata-statistics"
                    data-strata-number="<?php echo $i; ?>">
                    <td><span class="bwg-strata-legend-dot" style="background:<?php
						echo $strata->get_color(); ?>;"></span></td>
                    <td><?php esc_html_e( $strata->get_label() ); ?></td>
                    <td class="bwg-strata-legend-amount">[<?php
						echo number_format_i18n( $stats->get_count() );
						?>]
                    </td>
                </tr>
				<?php
				$i ++;
			}
			?>
        </table>

		<?php
		foreach ( $evaluation->get_evaluation_definition()->get_items() as $item1 ) {
			// Interest: $item1
			?>
            <div class="bwg-stats-container">
                <div class="bwg-stats-spider" data-container="chart">
					<?php
					if ( array_key_exists( '#' . $item1->get_id(), $spider_charts ) ) {
						?>
                        <div data-spider-chart><?php
							echo $spider_charts[ '#' . $item1->get_id() ]->get_svg( FALSE );
							?></div>
                        <p><a data-action="save-chart">Speichern</a></p>
						<?php
					}
					?>
                </div>
                <div class="bwg-stats-table">
                    <table class="wp-list-table widefat striped">
                        <thead>
                        <tr>
                            <th colspan="<?php echo 1 + count( $stratas ); ?>"><?php echo $item1->get_label(); ?></th>
                        </tr>
                        </thead>
                        <tbody>
						<?php
						foreach ( $item1->get_items() as $item2 ) {
							// Category: $item2
							if ( 0 === $item2->get_spider_chart() ) {
								continue;
							}
							?>
                            <tr data-action="show-details" style="cursor: pointer;">
                                <td>
									<?php echo $item2->get_label(); ?>
                                    <div data-details style="display: none;">
                                        <div class="bwg-stats-details-container">
											<?php foreach ( $item2->get_items() as $item3 ) { ?>
                                                <div class="bwg-stats-details-item">
                                                    <div>
														<?php echo $item3->get_label(); ?>
														<?php
														for ( $i = count( $stratas ) - 1; $i >= 0; $i -- ) {
															/** @var \bwg\stats\BWG_Stats_Gradings $stats */
															$stats = $strata_statistics[ $i ]['statistics'];
															?>
                                                            <div data-bwg-strata-number="<?php echo $i; ?>"
                                                                 style="float: right; width: 30px; padding: 0 0 10px 0;
                                                                 text-align: right; color:<?php echo
															     $stratas[ $i ]->get_color(); ?>">
																<?php printf( '%0.01f',
																	round(
																		$stats->get_average( $item3->get_id() ),
																		1
																	)
																); ?>
                                                            </div>
														<?php } ?>
                                                    </div>

                                                    <div class="bwg-stats-details-description"><?php
														echo $item3->get_description();
														?></div>

													<?php
													for ( $i = 0; $i < count( $stratas ); $i ++ ) {
														$color = $stratas[ $i ]->get_color();

														/** @var \bwg\stats\BWG_Stats_Gradings $stats */
														$stats = $strata_statistics[ $i ]['statistics'];
														?>
                                                        <div data-bwg-strata-number="<?php echo $i; ?>">
                                                            <div class="bwg-stats-std-deviation" style="color:<?php
															echo $color; ?>;">
                                                                Standardabweichung: <?php
																printf( '%0.02f',
																	round(
																		$stats->get_standard_deviation( $item3->get_id() ),
																		1
																	)
																); ?>
                                                            </div>

                                                            <table class="bwg-stats-histogram">
																<?php
																$histogram = $stats->get_histogram( $item3->get_id() );
																foreach ( $histogram as $k => $v ) {
																	$p = 100 * $utils->safe_division(
																			$v, $total_nr_submissions );
																	?>
                                                                    <tr>
                                                                        <td class="bwg-stats-histogram-grading"><?php echo $k; ?></td>
                                                                        <td class="bwg-stats-histogram-percentage">
                                                                            <div class="bwg-stats-histogram-percentage-value">
																				<?php printf( '%0.01f', $p ); ?>%
                                                                            </div>
                                                                            <div class="bwg-stats-histogram-percentage-bar"
                                                                                 style="width: <?php echo $p; ?>%;
                                                                                         background: <?php echo
																			     $color; ?>
                                                                                         "></div>
                                                                        </td>
                                                                        <td class="bwg-stats-histogram-amount"><?php echo $v; ?></td>
                                                                    </tr>
																	<?php
																}
																?>
                                                            </table>

                                                        </div>
													<?php } ?>
                                                </div>
											<?php } ?>
                                        </div>
                                    </div>
                                </td>
								<?php
								for ( $i = 0; $i < count( $stratas ); $i ++ ) {
									?>
                                    <td data-bwg-strata-number="<?php echo $i; ?>"
                                        style="width: 20px; text-align:right; color:<?php echo
									    $stratas[ $i ]->get_color(); ?>;"><?php
										printf( '%0.01f',
											round( $strata_gradings[ $i ]['gradings'][ '#' . $item2->get_id() ], 1 ) );
										?></td>
									<?php
								}
								?>
                            </tr>
							<?php
						}
						?></tbody>
                    </table>

					<?php
					///
					// Non-Spider-Chart Categories
					///
					foreach ( $item1->get_items() as $item2 ) {
						// Category: $item2
						if ( 0 !== $item2->get_spider_chart() ) {
							continue;
						}
						?>
                        <hr>
                        <table class="wp-list-table widefat striped">
                        <thead>
                        <tr>
                            <th colspan="2">
								<?php echo $item2->get_label(); ?> (nicht im Netzdiagramm)
                                <div style="float:right;">
                                    <a data-action="sort">Sortieren</a>
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody><?php
						$i = 0;
						foreach ( $item2->get_items() as $item3 ) {
							?>
                            <tr data-order="<?php echo $i; ?>"
                                data-value="<?php echo $statistics->get_average( $item3->get_id() ); ?>">
                                <td><?php echo $item3->get_label(); ?></td>
                                <td style="text-align: right;"><?php
									printf( '%0.01f', round( $statistics->get_average( $item3->get_id() ), 1 ) );
									?></td>
                            </tr>
							<?php
							$i ++;
						}
						?></tbody>
                        </table><?php
					}
					?>
                </div>
            </div>
			<?php
		}
		?>
    </div>

    <div style="margin-bottom: 10px;">
        <h3>Kommentare</h3>

		<?php
		foreach ( $evaluation->get_evaluation_definition()->get_items() as $item1 ) {
			foreach ( $item1->get_items() as $item2 ) {
				$k = '#' . $item2->get_id();
				if ( array_key_exists( $k, $notes ) ) {
					echo '<p style="font-weight: bold;">' . $item1->get_label() . ' / ' . $item2->get_label() . '</p>';

					foreach ( $notes[ $k ] as $item ) {
						echo '<div style="background-color: #fff; padding: 10px; margin-bottom: 10px;">';
						echo nl2br( htmlspecialchars( $item ) );
						echo '</div>';
					}
				}
			}
		}
		?>
    </div>

	<?php if ( ! empty( $profile_field_stats ) ) { ?>
        <div style="margin-bottom: 10px;">
            <h3>Bewerterfelder-Statistiken</h3>

            <div class="bwg-stats-container">
				<?php echo implode( '', $profile_field_stats ); ?>
            </div>
        </div>
	<?php } ?>

</div>
