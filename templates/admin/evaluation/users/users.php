<?php
/**
 * Variables
 * ---------
 *
 * @var \WP_Post $post The post.
 * @var \bwg\evaluation\BWG_Evaluation_Users_List_Table $list_table The list table.
 * @var string $bwg_mode The BWG mode.
 * @var string $search The search keywords.
 */

?>

<div class="wrap">
    <h1 class="wp-heading-inline">
		<?php esc_html_e( $post->post_title ); ?> &rsaquo; <?php _e( 'Eingereichte Bewertungen', 'bwg' ); ?>
    </h1>

	<?php
	if ( ! empty( $search ) ) {
		/* translators: %s: search keywords */
		printf(
			'<span class="subtitle">' . __( 'Search results for &#8220;%s&#8221;', 'bwg' ) . '</span>',
			esc_html( $search )
		);
	}
	?>
    <hr class="wp-header-end">

    <p>
        <a href="<?php
		echo add_query_arg( [ 'action' => 'stats' ] );
		?>" class="button button-primary">Statistik aller eingereichter Bewertungen</a>
		<?php if ( $bwg_mode === 'drogothek' ) { ?>
            <a href="<?php echo add_query_arg( [ 'action' => 'mail-sender' ] ); ?>" class="button button-primary">Mail
                Versand</a>
		<?php } ?>
    </p>

    <form id="bwg-users-filter" method="get">
        <input type="hidden" name="post_type" value="<?php echo $_REQUEST['post_type']; ?>">
        <input type="hidden" name="post" value="<?php echo $_REQUEST['post']; ?>">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>">

		<?php $list_table->search_box( __( 'Search', 'bwg' ), 'search' ); ?>
		<?php $list_table->display(); ?>
    </form>
</div>
