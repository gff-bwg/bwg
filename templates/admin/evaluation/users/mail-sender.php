<?php
/**
 * Variables
 * ---------
 *
 * @var \WP_Post $post
 * @var string $bwg_mode
 * @var string $mail_subject
 * @var string $mail_body
 * @var string $set_mail_action
 * @var string $set_mail_nonce
 * @var string $send_action
 * @var string $send_nonce
 * @var string $reset_action
 * @var string $reset_nonce
 * @var array $submissions
 */

$utils    = bwg_base()->utils();
$interval = 20;

wp_enqueue_script( 'jquery' );
wp_enqueue_script( 'jquery-ui-core' );
wp_enqueue_script( 'jquery-ui-dialog' );
wp_enqueue_style( 'wp-jquery-ui-dialog' );

if ( ! function_exists( 'svg_check' ) ) {
	function svg_check( string $color ): string {
		return '<svg xmlns="http://www.w3.org/2000/svg" height="16" width="14" '
		       . 'viewBox="0 0 448 512"><path fill="' . urlencode( $color ) . '" d="'
		       . 'M438.6 105.4c12.5 12.5 12.5 32.8 0 45.3l-256 256c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-12.5-12.5-12.5-32.8 '
		       . '0-45.3s32.8-12.5 45.3 0L160 338.7 393.4 105.4c12.5-12.5 32.8-12.5 45.3 0z"/></svg>';
	}
}

if ( ! function_exists( 'svg_cross' ) ) {
	function svg_cross( string $color ): string {
		return '<svg xmlns="http://www.w3.org/2000/svg" height="16" width="12" '
		       . 'viewBox="0 0 384 512"><path fill="' . urlencode( $color ) . '" d="'
		       . 'M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 '
		       . '0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 '
		       . '406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z"/></svg>';
	}
}

?>
<div class="wrap">
    <h1 class="wp-heading-inline">
		<?php esc_html_e( $post->post_title ); ?>
        &rsaquo; <?php _e( 'Eingereichte Bewertungen', 'bwg' ); ?>
        &rsaquo; <?php _e( 'Mail Versand', 'bwg' ); ?>
    </h1>
    <hr class="wp-header-end">

    <p><a href="javascript:history.back()">&laquo; Zurück</a></p>

    <h2 class="title">Mail</h2>

    <button id="bwg_edit_mail" type="button" class="button">Mail-Inhalt bearbeiten</button>

    <div id="edit-mail-dialog-content" style="display:none" class="dialog-content">
        <form id="bwg_edit_mail_form" method="post" action="#">
            <div style="margin-bottom:1em">
                <input id="bwg_mail_subject" class="regular-text" style="width:100%" type="text" placeholder="Betreff"
                       aria-label="" value="<?php echo esc_attr( $mail_subject ); ?>">
            </div>
            <div style="margin-bottom:1em">
        <textarea id="bwg_mail_body" class="large-text code" rows="15" style="width:100%"
                  placeholder="Inhalt" aria-label=""><?php
	        echo esc_textarea( $mail_body ); ?></textarea>
            </div>
            <div>
                <button type="submit" class="button" style="width:100%">Speichern</button>
            </div>
        </form>
    </div>

    <h2 class="title">Versand</h2>

    <p>
        <button id="bwg_start_button" class="button">
            Auto-Versand (alle <?php echo $interval; ?> Sekunden) starten
        </button>
        <button id="bwg_stop_button" class="button" style="display: none">
            Auto-Versand stoppen
        </button>
        <button id="bwg_reset_button" class="button">
            Alle Status zurücksetzen...
        </button>
        <span id="bwg_auto_info">&nbsp;</span>
    </p>

    <div id="bwg_list"></div>

    <div id="reset-all-dialog-content" style="display:none" class="dialog-content">
        <p>
            Wollen Sie wirklich für alle den Status zurücksetzen?
        </p>
        <p>
            <button id="bwg_reset_all" type="button" class="button" style="width:100%">Jetzt zurücksetzen</button>
        </p>
    </div>

    <div id="dialog-content" style="display:none" class="dialog-content">

        <p>
            Sie können hier einen Demoversand an Ihre Admin E-Mail-Adresse (<?php
			echo wp_get_current_user()->user_email
			?>) verschicken, oder einen Einzelversand vornehmen, oder den Versandstatus zurücksetzen.
        </p>
        <p>
            <button id="bwg_demo_sending" class="button" style="width:100%;">Demoversand</button>
        </p>
        <p>
            <button id="bwg_real_sending" class="button" style="width:100%;">Einzelversand</button>
        </p>
        <p>
            <button id="bwg_reset" class="button" style="width:100%;">Status zurücksetzen</button>
        </p>

    </div>

    <h2 class="title">Log-Nachrichten</h2>

    <ul id="bwg_logs"></ul>

    <textarea id="bwg-initial-data" style="display:none"
              aria-label=""><?php echo json_encode( $submissions ); ?></textarea>
</div>

<style>
    .submission {
        position: relative;
        display: inline-block;
        width: 100px;
        padding: 4px;
        background-color: #ccc;
        text-align: center;
        cursor: pointer;
    }

    .submission:hover {
        font-weight: bold;
    }

    .bwg-send-success:after,
    .bwg-send-failure:after {
        display: block;
        position: absolute;
        top: 4px;
        right: 4px;
        content: " ";
        bottom: 4px;
        width: 1.4em;
        background-size: 100% 100%;
    }

    .bwg-send-success:after {
        background-image: url('data:image/svg+xml,<?php echo svg_check('#1d2327'); ?>');
    }

    .bwg-success.bwg-send-success:after,
    .bwg-failure.bwg-send-success:after {
        background-image: url('data:image/svg+xml,<?php echo svg_check('#fff'); ?>');
    }

    .bwg-send-failure:after {
        background-image: url('data:image/svg+xml,<?php echo svg_cross('#1d2327'); ?>');
    }

    .bwg-success.bwg-send-failure:after,
    .bwg-failure.bwg-send-failure:after {
        background-image: url('data:image/svg+xml,<?php echo svg_cross('#fff'); ?>');
    }

    .bwg-pending {
        background-color: #59e3f8 !important;
        color: #1d2327 !important;
    }

    .bwg-success {
        background-color: #22541c;
        color: #fff;
    }

    .bwg-failure {
        background-color: #5e1212;
        color: #fff;
    }

    .dialog-content :first-child {
        margin-top: 0;
    }

    .dialog-content :last-child {
        margin-bottom: 0;
    }
</style>

<script>
    jQuery(function ($) {
        const $list = $('#bwg_list');
        const $reset_all_dialog = $('#reset-all-dialog-content');
        const $dialog = $('#dialog-content');
        const attempt_interval_seconds = <?php echo $interval; ?>;

        let data = JSON.parse($('#bwg-initial-data').val());
        $('#bwg-initial-data').remove();

        let last_attempt_id = undefined;
        let last_sending = undefined;
        let interval = undefined;
        const $auto_info = $('#bwg_auto_info');
        const $start_button = $('#bwg_start_button');
        const $stop_button = $('#bwg_stop_button');
        const $reset_button = $('#bwg_reset_button');

        $('#bwg_edit_mail').on('click', () => {
            $('#edit-mail-dialog-content').dialog({title: 'Mail', modal: true, width: 500})
        });

        $('#bwg_edit_mail_form').on('submit', (e) => {
            e.preventDefault();

            const post_data = {
                '_ajax_nonce': '<?php echo $set_mail_nonce; ?>',
                'action': '<?php echo $set_mail_action; ?>',
                'evaluation_id': '<?php echo $post->ID ?>',
                'subject': $('#bwg_mail_subject').val(),
                'body': $('#bwg_mail_body').val()
            };

            $.post(ajaxurl, post_data, function () {
                add_log('Mail contents saved.');
            }).fail(function (q) {
                if (q['responseJSON'] && q['responseJSON']['data'] && q['responseJSON']['data']['message']) {
                    add_log('Request failed: ' + q['responseJSON']['data']['message']);
                } else {
                    add_log('Request failed.');
                }

                console.error(q);
            }).always(() => {
                $('#edit-mail-dialog-content').dialog('close');
            });
        });

        const auto_start = () => {
            last_sending = new Date();

            interval = setInterval(interval_tick, 500);
            interval_tick();

            $start_button.hide();
            $reset_button.hide();
            $stop_button.show();
        };

        const auto_stop = () => {
            if (interval !== undefined) {
                clearInterval(interval);
                interval = undefined;
            }

            $stop_button.hide();
            $reset_button.show();
            $start_button.show();
            $auto_info.text('');
        };

        $start_button.on('click', () => {
            auto_start();
        });

        $stop_button.on('click', () => {
            auto_stop();
        });

        $reset_button.on('click', () => {
            $reset_all_dialog.dialog({title: 'Alle Status zurücksetzen', modal: true});
        });

        $('#bwg_reset_all').on('click', () => {
            reset(-1);
            $reset_all_dialog.dialog('close');
        });

        const interval_tick = () => {
            const now = new Date();
            const diff_seconds = Math.round((now.getTime() - last_sending.getTime()) / 1000);

            const remain_seconds = attempt_interval_seconds - diff_seconds;
            if (remain_seconds <= 0) {
                last_sending = new Date();

                $auto_info.text('Versand!');
                next_attempt();
            } else {
                $auto_info.text('Nächster Versand in ' + remain_seconds + '".');
            }
        };

        const next_attempt = () => {
            let user_id;
            if (last_attempt_id !== undefined) {
                user_id = $list.find('.submission[data-bwg-user-id="' + last_attempt_id + '"] ~ ' +
                    '.submission:not(.bwg-success):not(.bwg-pending):not(.bwg-failure)')
                    .first().data('bwg-user-id');

                if (user_id !== undefined) {
                    last_attempt_id = user_id;
                    send(user_id);
                    return;
                }
            }

            user_id = $list.find('.submission:not(.bwg-success):not(.bwg-pending):not(.bwg-failure)')
                .first().data('bwg-user-id');

            if (user_id === undefined) {
                add_log('No one left.');
                auto_stop();
            } else {
                last_attempt_id = user_id;
                send(user_id);
            }
        };

        const add_log = (message) => {
            const now = new Date();
            $('#bwg_logs').prepend('<li>' + now.toLocaleString() + ' ' + message + '</li>');
        };

        const update_list = () => {
            let html = '';
            for (let i = 0; i < data.length; i++) {
                const item = data[i];

                let cssClasses = 'submission';
                if (item['status'] !== '') {
                    const s = item['status'].substring(0, 1);
                    if (s === 'S') {
                        cssClasses += ' bwg-success';
                    } else if (s === 'F') {
                        cssClasses += ' bwg-failure';
                    }
                }

                html += '<div class="' + cssClasses + '" data-bwg-user-id="' + item['user_ID'] + '" ' +
                    'data-bwg-status="' + item['status'] + '">' + item['username'] +
                    '</div>';
            }

            $list.html(html);
        };

        const send = (user_id, demo_enabled) => {
            const user_name = $('[data-bwg-user-id="' + user_id + '"]').addClass('bwg-pending').text();

            const post_data = {
                '_ajax_nonce': '<?php echo $send_nonce; ?>',
                'action': '<?php echo $send_action; ?>',
                'demo': demo_enabled ? 1 : 0,
                'evaluation_id': '<?php echo $post->ID ?>',
                'user_id': user_id
            };

            $.post(ajaxurl, post_data, function (s) {
                data = s['data']['data'];
                const send_success = s['data']['send_success'];

                update_list();

                if (send_success) {
                    add_log('Sending to ' + user_name + ' successful' + (demo_enabled ? ' (Demo)' : '') + '.');
                } else {
                    add_log('Sending to ' + user_name + ' failed' + (demo_enabled ? ' (Demo)' : '') + '.');
                }

                $('[data-bwg-user-id="' + user_id + '"]').addClass(
                    send_success ? 'bwg-send-success' : 'bwg-send-failure'
                );

                setTimeout(function () {
                    $('[data-bwg-user-id="' + user_id + '"]').removeClass('bwg-send-success bwg-send-failure');
                }, 3000);

            }).fail(function (q) {
                if (q['responseJSON'] && q['responseJSON']['data'] && q['responseJSON']['data']['message']) {
                    add_log('Request failed: ' + q['responseJSON']['data']['message']);
                } else {
                    add_log('Request failed.');
                }

                console.error(q);
            }).always(function () {
                $('[data-bwg-user-id="' + user_id + '"]').removeClass('bwg-pending');
            });
        };

        const reset = (user_id) => {
            const user_name = $('[data-bwg-user-id="' + user_id + '"]').text();

            const post_data = {
                '_ajax_nonce': '<?php echo $reset_nonce; ?>',
                'action': '<?php echo $reset_action; ?>',
                'evaluation_id': '<?php echo $post->ID ?>',
                'user_id': user_id
            };

            $.post(ajaxurl, post_data, function (s) {
                data = s['data']['data'];
                update_list();

                if (s['data']['user_id'] === -1) {
                    add_log('Status reset for all.')
                } else {
                    add_log('Status reset for ' + user_name + '.');
                }

            }).fail(function (q) {
                if (q['responseJSON'] && q['responseJSON']['data'] && q['responseJSON']['data']['message']) {
                    add_log('Request failed: ' + q['responseJSON']['data']['message']);
                } else {
                    add_log('Request failed.');
                }

                console.error(q);
            });
        };

        update_list();

        $list.on('click', '.submission', function () {
            const user_id = $(this).data('bwg-user-id');
            const user_name = $(this).text();

            $dialog.data('user_id', user_id);
            $dialog.dialog({title: user_name, modal: true});
        });

        $('#bwg_demo_sending').on('click', function () {
            const user_id = $(this).closest('.dialog-content').data('user_id');
            send(user_id, true);
            $dialog.dialog("close");
        })

        $('#bwg_real_sending').on('click', function () {
            const user_id = $(this).closest('.dialog-content').data('user_id');
            send(user_id, false);
            $dialog.dialog("close");
        });

        $('#bwg_reset').on('click', function () {
            const user_id = $(this).closest('.dialog-content').data('user_id');
            reset(user_id);
            $dialog.dialog("close");
        });
    });
</script>
