<?php
/**
 * @var boolean $postage
 * @var string $registration_open_from
 * @var string $registration_open_to
 * @var boolean $subscriber
 * @var int $postage_max
 * @var string $rating_open_from
 * @var string $rating_open_to
 * @var string $comment
 */

?>
<div id="bwg-ee-meta-box" class="bwg">

    <p class="post-attributes-label-wrapper">
        <label class="post-attributes-label">
            Postversand?
        </label>
    </p>

    <p class="checkbox-wrapper" style="margin-top: .5em;">
        <input id="bwg-ee-postage"
               name="bwg_ee_postage"
               value="1"<?php echo $postage ? ' checked="checked"' : ''; ?>
               type="checkbox">
        <label for="bwg-ee-postage">Mit Postversand</label>
    </p>

    <div id="bwg-ee-postage-options"<?php echo $postage ? '' : ' style="display:none"'; ?>>

        <p class="post-attributes-label-wrapper" style="margin-top:0">
            <label class="post-attributes-label" for="bwg-ee-registration-open-from">
                Registrierung möglich (ab &ndash; bis):
            </label>
        </p>

        <div id="bwg-ee-registration-open-from-to" class="bwg-date-from-to-container">
            <div class="bwg-date-from-container">
                <input id="bwg-ee-registration-open-from"
                       name="bwg_ee_registration_open_from"
                       type="text"
                       value="<?php esc_attr_e( $registration_open_from ); ?>">
            </div>
            <div class="bwg-date-dash">&ndash;</div>
            <div class="bwg-date-to-container">
                <input id="bwg-ee-registration-open-to"
                       name="bwg_ee_registration_open_to"
                       type="text"
                       value="<?php esc_attr_e( $registration_open_to ); ?>">
            </div>
        </div>

        <p class="checkbox-wrapper">
            <input id="bwg-ee-subscriber"
                   name="bwg_ee_subscriber"
                   value="1"<?php echo $subscriber ? ' checked="checked"' : ''; ?>
                   type="checkbox">
            <label for="bwg-ee-subscriber">Bestehendes Abo erfragen?</label>
        </p>

        <div id="bwg-ee-postage-max-container">
            <p class="post-attributes-label-wrapper">
                <label class="post-attributes-label" for="bwg-ee-postage-max">
                    Maximale Anzahl Postversand
                </label>
            </p>

            <input type="text"
                   id="bwg-ee-postage-max"
                   name="bwg_ee_postage_max"
                   value="<?php esc_attr_e( $postage_max ); ?>">

            <p class="description">
                Wird nach bestehendem Abo gefragt, so werden die User mit Abo hier nicht mit eingerechnet, da davon
                ausgegangen wird, dass diesen Usern keine Post geschickt wird.
            </p>
        </div>
    </div>

    <p class="post-attributes-label-wrapper">
        <label class="post-attributes-label" for="bwg-ee-rating-open-from">
            Bewertungen möglich (von &ndash; bis):
        </label>
    </p>

    <div id="bwg-ee-rating-open-from-to" class="bwg-date-from-to-container">
        <div class="bwg-date-from-container">
            <input id="bwg-ee-rating-open-from"
                   name="bwg_ee_rating_open_from"
                   type="text"
                   value="<?php esc_attr_e( $rating_open_from ); ?>">
        </div>
        <div class="bwg-date-dash">&ndash;</div>
        <div class="bwg-date-to-container">
            <input id="bwg-ee-rating-open-to"
                   name="bwg_ee_rating_open_to"
                   type="text"
                   value="<?php esc_attr_e( $rating_open_to ); ?>">
        </div>
    </div>

    <p class="post-attributes-label-wrapper">
        <label class="post-attributes-label" for="bwg-ee-comment">
            Kommentar (Admin)
        </label>
    </p>

    <textarea id="bwg-ee-comment"
              name="bwg_ee_comment"
              style="width: 100%; min-height: 106px; height: 106px;"><?php esc_html_e( $comment ); ?></textarea>

</div>
