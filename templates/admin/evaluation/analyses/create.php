<?php
/**
 * Variables
 * ---------
 *
 * @var string $filter_groups_action
 * @var string $filter_groups_nonce
 * @var \WP_Post $post
 * @var string $analysis_label
 * @var string[] $strata_filter_boxes
 * @var bool $sex_enabled
 * @var array $sex_percentages
 * @var bool $age_enabled
 * @var array $age_limits
 * @var array $age_percentages
 * @var int $sample_size
 * @var string $filter_groups_table
 */

/////////////////////////////////////////

wp_enqueue_script( 'bwg-lib-jquery-scrollfix' );

?>
<div class="wrap">
    <h1 class="wp-heading-inline">
		<?php esc_html_e( $post->post_title ); ?> &rsaquo;
		<?php _e( 'Auswertung erstellen', 'bwg' ); ?>
    </h1>

    <hr class="wp-header-end">

    <form id="edit-analysis-form" action="#"
          data-bwg-filter-groups-action="<?php esc_attr_e( $filter_groups_action ); ?>"
          data-bwg-filter-groups-nonce="<?php esc_attr_e( $filter_groups_nonce ); ?>">
        <input type="hidden" name="post_ID" value="<?php esc_attr_e( $post->ID ); ?>">

        <fieldset>
            <p>
                <a href="<?php echo esc_url( add_query_arg( 'sub', NULL ) ); ?>">
					<?php _e( 'Zurück', 'bwg' ); ?>
                </a>
            </p>

            <table class="form-table">
                <tbody>
                <tr>
                    <th>
                        <label for="bwg_analysis_label"><?php esc_html_e( 'Bezeichnung', 'bwg' ); ?></label>
                    </th>
                    <td>
                        <input id="bwg_analysis_label" name="bwg_analysis_label"
                               style="width:100%;" type="text"
                               value="<?php esc_attr_e( $analysis_label ); ?>">
                    </td>
                </tr>
                </tbody>
            </table>

            <div style="display: flex;">
                <div style="width: 50%">
                    <table class="form-table">
                        <tbody>
						<?php
						foreach ( $strata_filter_boxes as $strata_filter_box ) {
							echo $strata_filter_box;
						}
						?>
                        <!--
                <tr>
                    <th>
                        Filter: Alter
                    </th>
                    <td>
                        <label for="bwg_age_enabled">
                            <input name="bwg_age_enabled" type="checkbox" id="bwg_age_enabled" value="1"
								<?php if ( $age_enabled ) { ?> checked<?php } ?> >
                            Filter aktiviert
                        </label>

                        <table id="bwg_age_table" class="bwg-filter-settings">
							<?php
						$upper_limit = - 1;

						for ( $i = 0; $i < count( $age_limits ) && $i < count( $age_percentages ); $i ++ ) {
							$lower_limit = $upper_limit + 1;
							$upper_limit = $age_limits[ $i ];
							?>
                                <tr data-bwg-filter-condition>
                                    <td>
                                        <span class="bwg-age-lower-limit"><?php echo $lower_limit; ?></span>
                                        &ndash;
                                        <input type="number" class="input-age-limit"
                                               name="bwg_age_limits[]"
                                               value="<?php esc_attr_e( $upper_limit ); ?>">
                                    </td>
                                    <td>
                                        <input type="number" class="input-percentage"
                                               name="bwg_age_percentages[]"
                                               value="<?php esc_attr_e( $age_percentages[ $i ] ); ?>">%
                                    </td>
                                    <td class="bwg-filter-condition-actions">
                                        <a data-bwg-action="trash"><span class="dashicons dashicons-trash"></span></a>
                                        <a data-bwg-action="add"><span class="dashicons dashicons-plus-alt"></span></a>
                                    </td>
                                </tr>
								<?php
						}
						?>
                            <tr>
                                <td>TOTAL</td>
                                <td><span class="bwg-total-percentage">-</span></td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>

                    </td>
                </tr>
                -->
                        <tr>
                            <th>
                                <label for="bwg_sample_size">Gewünschte Anzahl Personen</label>
                            </th>
                            <td>
                                <input id="bwg_sample_size" name="bwg_sample_size" type="number"
                                       value="<?php esc_attr_e( $sample_size ); ?>">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div style="width: 50%; ">
                    <div data-strata-preview style="background-color: #fff; color: #ccc; padding: 20px; overflow:
                    auto;">
                        Hier dann die Strata.
                    </div>
                </div>
            </div>

            <hr>

            <h2>Mengentabelle</h2>

            <div style="display: flex; align-items: center; margin: 1em 0;">
                <button class="button button-primary" type="button" data-bwg-action="update-filter-groups">
                    Aktualisieren
                </button>
                <div style="margin-left: 1em;">(Aktualisierung nötig)</div>
            </div>

            <div id="bwg_filter_groups">
				<?php echo $filter_groups_table; ?>
            </div>

            <p>
                <strong>Legende</strong><br>
                <span class="bwg-group-selected">Gewünschte Anzahl ausgewählter Personen</span><br>
                <span class="bwg-group-size">Maximale zur Verfügung stehende Anzahl Personen</span><br>
            </p>

            <hr>

            <div class="bwg-form-buttons-container">
                <div style="margin-right: 10px;">
					<?php submit_button( __( 'Jetzt erstellen', 'bwg' ), 'primary', 'submit', FALSE ); ?>
                </div>
                <div class="dashicons dashicons-clock hidden"></div>
            </div>
        </fieldset>
    </form>

</div>

<script>
    jQuery(document).ready(function ($) {
        $('[data-strata-preview]').scrollFix({topPosition: 50});
    });
</script>
