<?php
/**
 * Variables
 * ---------
 *
 * @var \WP_Post $post The post.
 * @var \bwg\evaluation\BWG_Evaluation_Analyses_List_Table $list_table The list table.
 * @var string $search The search keywords.
 */

?>

<div class="wrap">
    <h1 class="wp-heading-inline">
		<?php esc_html_e( $post->post_title ); ?> &rsaquo; <?php _e( 'Auswertungen', 'bwg' ); ?>
    </h1>
    <a href="<?php echo esc_url( add_query_arg( 'sub', 'create' ) ); ?>"
       class="page-title-action">
		<?php _e( 'Erstellen', 'bwg' ); ?>
    </a>

	<?php
	if ( ! empty( $search ) ) {
		/* translators: %s: search keywords */
		printf(
			'<span class="subtitle">' . __( 'Search results for &#8220;%s&#8221;', 'bwg' ) . '</span>',
			esc_html( $search )
		);
	}
	?>
    <hr class="wp-header-end">

    <form id="bwg-users-filter" method="get">
        <input type="hidden" name="post_type" value="<?php echo $_REQUEST['post_type']; ?>">
        <input type="hidden" name="post" value="<?php echo $_REQUEST['post']; ?>">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>">

		<?php $list_table->search_box( __( 'Search', 'bwg' ), 'search' ); ?>
		<?php $list_table->display(); ?>
    </form>
</div>