<?php
/**
 * Variables
 * ---------
 *
 * @var \bwg\evaluation\BWG_Evaluation_Selection_Filter_Collection $filter_collection
 * @var array $group_sizes
 * @var array $group_totals
 * @var array $condition_totals
 */

?>

<?php if ( 1 === $filter_collection->count_filters() ) { ?>
    <table class="bwg-filter-groups-table">
        <tbody>
        <tr>
			<?php foreach (
				$filter_collection->get_filter( 0 )->get_condition_labels() as $filter_condition_label
			) { ?>
                <th colspan="2"><?php esc_html_e( $filter_condition_label ); ?></th>
			<?php } ?>
            <td>&nbsp;</td>
        </tr>
        <tr>
			<?php for ( $i = 0; $i < $filter_collection->get_filter( 0 )->count_conditions(); $i ++ ) {
				$key = ( $i + 1 );
				$css = $group_sizes[ $key ] > $group_totals[ $key ] ? 'bwg-group-problem' : '';
				?>
                <td class="bwg-group-selected <?php echo $css; ?>"><?php
					echo number_format_i18n( $group_sizes[ $key ], 0 );
					?>
                </td>
                <td class="bwg-group-size <?php echo $css; ?>"><?php
					echo number_format_i18n( $group_totals[ $key ], 0 );
					?>
                </td>
			<?php } ?>
            <td>&nbsp;</td>
        </tr>
        <tr>
			<?php for ( $i = 0; $i < $filter_collection->get_filter( 0 )->count_conditions(); $i ++ ) { ?>
                <td class="bwg-vertical-sum" colspan="2"><?php
					printf( '%d [%d%%]',
						$condition_totals[0][ $i + 1 ],
						100 * bwg_base()->utils()
						                ->safe_division( $condition_totals[0][ $i + 1 ],
							                array_sum( $condition_totals[0] ) )
					); ?></td>
			<?php } ?>
            <th><?php echo number_format_i18n( array_sum( $condition_totals[0] ), 0 ); ?></th>
        </tr>
        </tbody>
    </table>
<?php } else if ( 2 === $filter_collection->count_filters() ) { ?>
    <table class="bwg-filter-groups-table">
        <tbody>
        <tr>
            <th>&nbsp;</th>
			<?php foreach (
				$filter_collection->get_filter( 0 )->get_condition_labels() as $filter_condition_label
			) { ?>
                <th colspan="2"><?php esc_html_e( $filter_condition_label ); ?></th>
			<?php } ?>
            <th>&nbsp;</th>
        </tr>
		<?php
		for ( $j = 0; $j < $filter_collection->get_filter( 1 )->count_conditions(); $j ++ ) { ?>
            <tr>
            <th><?php esc_html_e( $filter_collection->get_filter( 1 )->get_condition_label( $j ) ); ?></th>
			<?php
			for ( $i = 0; $i < $filter_collection->get_filter( 0 )->count_conditions(); $i ++ ) {
				$key = ( $i + 1 ) . '/' . ( $j + 1 );
				$css = $group_sizes[ $key ] > $group_totals[ $key ] ? 'bwg-group-problem' : '';
				?>
                <td class="bwg-group-selected <?php echo $css; ?>"><?php
					echo number_format_i18n( $group_sizes[ $key ], 0 );
					?>
                </td>
                <td class="bwg-group-size <?php echo $css; ?>"><?php
					echo number_format_i18n( $group_totals[ $key ], 0 );
					?>
                </td>
			<?php } ?>
            <td class="bwg-horizontal-sum"><?php printf(
					'%d [%d%%]',
					$condition_totals[1][ $j + 1 ],
					100 * bwg_base()->utils()->safe_division( $condition_totals[1][ $j + 1 ], array_sum(
						$condition_totals[1] ) )
				); ?></td>
            </tr><?php
		} ?>
        <tr>
            <th>&nbsp;</th>
			<?php for ( $i = 0; $i < $filter_collection->get_filter( 0 )->count_conditions(); $i ++ ) { ?>
                <td class="bwg-vertical-sum" colspan="2"><?php
					printf( '%d [%d%%]',
						$condition_totals[0][ $i + 1 ],
						100 * bwg_base()
							->utils()
							->safe_division( $condition_totals[0][ $i + 1 ], array_sum( $condition_totals[0] ) )
					); ?></td>
			<?php } ?>
            <th><?php echo number_format_i18n( array_sum( $condition_totals[0] ), 0 ); ?></th>
        </tr>
        </tbody>
    </table>
<?php } ?>

<table>
    <tr>
        <td>
            <div style="width: 100px; display: flex; background: linear-gradient(90deg, #FFC0CB 60%, transparent 40%);">
                <div style="width: 50%;">5</div>
                <div style="width: 50%;">15</div>
            </div>
        </td>
    </tr>
</table>

