<?php

/**
 * Variables
 * ---------
 *
 * @var \WP_Post $post
 * @var int $nr_submissions_with_unknown_feup_user
 * @var string[] $submissions_with_level_0_feup_user
 * @var string[] $submissions_with_level_X_feup_user
 * @var string[] $no_submissions_with_level_0_feup_user
 * @var string[] $no_submissions_with_level_X_feup_user
 * @var string[] $no_submissions_with_level_X_feup_user_with_user_storage
 * @var string[] $no_submissions_with_level_X_feup_user_without_user_storage
 */

declare( strict_types=1 );

use bwg\ui\BWG_UI_Accordion;
use bwg\ui\BWG_UI_Accordion_Heading;
use bwg\ui\BWG_UI_Accordion_Heading_Badge;
use bwg\ui\BWG_UI_Accordion_Panel;

?>

<div class="wrap">
    <h1 class="wp-heading-inline">
		<?php esc_html_e( $post->post_title ); ?> &rsaquo; <?php _e( 'Teilnahme Statistiken', 'bwg' ); ?>
    </h1>

	<?php

	$accordion = new BWG_UI_Accordion();

	$accordion->add_panel( 'bwg-participation-statistics-submission-feup-users', new BWG_UI_Accordion_Panel(
			new BWG_UI_Accordion_Heading(
				__( 'FEUP Benutzer mit Teilnahme', 'bwg' ),
				new BWG_UI_Accordion_Heading_Badge(
					number_format_i18n( count( $submissions_with_level_X_feup_user ) ),
					BWG_UI_Accordion_Heading_Badge::COLOR_BLUE )
			),
			'<p style="column-count: 4;">' . nl2br( esc_html( implode( PHP_EOL,
				$submissions_with_level_X_feup_user ) ) ) . '</p>',
		)
	);

	$accordion->add_panel(
		'bwg-participation-statistics-no-submission-feup-users',
		new BWG_UI_Accordion_Panel(
			new BWG_UI_Accordion_Heading(
				__( 'FEUP Benutzer ohne Teilnahme', 'bwg' ),
				new BWG_UI_Accordion_Heading_Badge(
					number_format_i18n( count( $no_submissions_with_level_X_feup_user ) ),
					BWG_UI_Accordion_Heading_Badge::COLOR_BLUE )
			),
			'<p style="column-count: 4;">' . nl2br( esc_html( implode( PHP_EOL,
				$no_submissions_with_level_X_feup_user ) ) ) . '</p>',
		)
	);

	$accordion->add_panel(
		'bwg-participation-statistics-submissions-unknown-feup-users',
		new BWG_UI_Accordion_Panel(
			new BWG_UI_Accordion_Heading(
				__( 'Anzahl Teilnahmen mit unbekanntem FEUP Benutzer', 'bwg' ),
				new BWG_UI_Accordion_Heading_Badge(
					number_format_i18n( $nr_submissions_with_unknown_feup_user ),
					BWG_UI_Accordion_Heading_Badge::COLOR_BLUE
				)
			),
			'<div style="color:#666;"><p>Dies kann dann vorkommen, wenn die mit den Teilnahmen verknüpften FEUP Benutzern dauerhaft gelöscht wurden.</p></div>'
		)
	);

	$accordion->add_panel(
		'bwg-participation-statistics-submissions-level-0-feup-users',
		new BWG_UI_Accordion_Panel(
			new BWG_UI_Accordion_Heading(
				__( 'Anzahl Teilnahmen mit FEUP Benutzer des Levels 0', 'bwg' ),
				new BWG_UI_Accordion_Heading_Badge(
					number_format_i18n( count( $submissions_with_level_0_feup_user ) ),
					BWG_UI_Accordion_Heading_Badge::COLOR_BLUE
				)
			),
			'<div style="color:#666;"><p>Dies kann dann vorkommen, wenn ein Benutzer zum Zeitpunkt der Evaluation nicht Level 0 hatte, jetzt aber schon.</p></div>' .
			'<p style="column-count: 4;">' .
			nl2br( esc_html( implode( PHP_EOL, $submissions_with_level_0_feup_user ) ) ) .
			'</p>'
		)
	);

	$accordion->add_panel(
		'bwg-participation-statistics-no-submission-feup-users-with-user-storage',
		new BWG_UI_Accordion_Panel(
			new BWG_UI_Accordion_Heading(
				__( 'FEUP Benutzer ohne Teilnahme (aber mit vorhandenen Daten)', 'bwg' ),
				new BWG_UI_Accordion_Heading_Badge(
					number_format_i18n( count( $no_submissions_with_level_X_feup_user_with_user_storage ) ),
					BWG_UI_Accordion_Heading_Badge::COLOR_BLUE
				)
			),
			'<p style="column-count: 4;">' . nl2br( esc_html( implode(
				PHP_EOL,
				$no_submissions_with_level_X_feup_user_with_user_storage
			) ) ) . '</p>',
		)
	);

	$accordion->add_panel(
		'bwg-participation-statistics-no-submission-feup-users-without-user-storage',
		new BWG_UI_Accordion_Panel(
			new BWG_UI_Accordion_Heading(
				__( 'FEUP Benutzer ohne Teilnahme (und ohne Daten)', 'bwg' ),
				new BWG_UI_Accordion_Heading_Badge(
					number_format_i18n( count( $no_submissions_with_level_X_feup_user_without_user_storage ) ),
					BWG_UI_Accordion_Heading_Badge::COLOR_BLUE
				)
			),
			'<p style="column-count: 4;">' . nl2br( esc_html( implode(
				PHP_EOL,
				$no_submissions_with_level_X_feup_user_without_user_storage
			) ) ) . '</p>',
		)
	);

	echo $accordion->render();

	?>

</div>
