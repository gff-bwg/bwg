<?php
/**
 * @var WP_Post $post
 * @var string $title
 * @var BWG_Evaluation_Definition $evaluation_definition
 * @var BWG_Evaluation_Grading[] $grading
 * @var string spider_chart_nonce The nonce for the spider chart ajax requests
 * @var string spider_chart_action The spider chart ajax action
 */
?>
<div class="wrap bwg-full-view"
     data-spider-chart-nonce="<?php echo $spider_chart_nonce; ?>"
     data-spider-chart-action="<?php echo $spider_chart_action; ?>"
     data-post-id="<?php echo $post->ID; ?>">

    <h1>BWG Evaluation - Ansicht &rsaquo; <?php echo $title; ?></h1>

	<?php
	///
	// Level 0 : Interest.
	///
	$level0_items = $evaluation_definition->get_items();
	for ( $level0_i = 0; $level0_i < count( $level0_items ); $level0_i ++ ) {
		$level0_item = $level0_items[ $level0_i ];
		$description = $level0_item->get_description( get_the_title( $post ) );
		?>

        <h2><?php esc_html_e( $level0_item->get_label() ); ?></h2>
		<?php if ( ! empty( $description ) ) : ?>
            <div class="description"><?php echo $description; ?></div>
		<?php endif; ?>

		<?php
		///
		// Level 1 : Categories.
		///
		$level1_items = $level0_item->get_items();
		for ( $level1_i = 0; $level1_i < count( $level1_items ); $level1_i ++ ) {
			$level1_item   = $level1_items[ $level1_i ];
			$description   = $level1_item->get_description( get_the_title( $post ) );
			$grading_total = 'bwg-grading-' . $level0_i . '-' . $level1_i;
			?>

            <div class="level-1-container">
                <div class="level-details">
                    <p class="level-title"><?php esc_html_e( $level1_item->get_label() ); ?></p>
					<?php if ( ! empty( $description ) ) : ?>
                        <div class="description"><?php echo $description; ?></div>
					<?php endif; ?>
                </div>
                <div class="level-total" data-bwg-ed-id="<?php echo $level1_item->get_id(); ?>"
                     data-grading-total="<?php echo $grading_total; ?>"></div>
                <div class="level-grading-weight"></div>
            </div>

			<?php
			///
			// Level 2 : Criteria (grading possible).
			///
			$level2_items = $level1_item->get_items();
			for ( $level2_i = 0; $level2_i < count( $level2_items ); $level2_i ++ ) {
				$level2_item       = $level2_items[ $level2_i ];
				$description       = $level2_item->get_description( get_the_title( $post ) );
				$grading_sub_total = 'bwg-grading-' . $level0_i . '-' . $level1_i . '-' . $level2_i;
				$grading_name      = 'bwg-grading-' . $level0_i . '-' . $level1_i . '-' . $level2_i;
				?>

                <div class="level-2-container">
                    <div class="level-details">
                        <p class="level-title"><?php esc_html_e( $level2_item->get_label() ); ?></p>
						<?php if ( ! empty( $description ) ) : ?>
                            <div class="description"><?php echo $description; ?></div>
						<?php endif; ?>
                    </div>

					<?php if ( $level2_item->has_items() ) { ?>

                        <div
                                data-grading-total-ref="<?php echo $grading_total; ?>"
                                data-grading-weight="<?php echo $level2_item->get_weight(); ?>"
                                data-grading-sub-total="<?php echo $grading_sub_total; ?>"
                                class="level-sub-total"></div>

					<?php } else { ?>

                        <div class="level-grading-container"
                             data-grading-total-ref="<?php echo $grading_total; ?>"
                             data-grading-weight="<?php echo $level2_item->get_weight(); ?>"
                             data-grading-name="<?php echo $grading_name; ?>">
							<?php
							foreach ( $grading as $g ) { ?>
                                <div style="display: flex; flex-direction: column;">
                                    <div style="text-align: center;"><?php echo $g->label; ?></div>
                                    <div style="text-align: center;">
                                        <input type="radio" name="<?php echo $grading_name; ?>"
                                               value="<?php echo $g->points; ?>"
                                               style="margin: 0;"/>
                                    </div>
                                </div>
							<?php } ?>
                        </div>

					<?php } ?>
                    <div class="level-grading-weight">
						<?php echo sprintf( '%.01f', $level2_item->get_weight() ); ?>
                    </div>
                </div>

				<?php
				///
				// Level 3 : Focus (grading possible).
				///
				$level3_items = $level2_item->get_items();
				for ( $level3_i = 0; $level3_i < count( $level3_items ); $level3_i ++ ) {
					$level3_item  = $level3_items[ $level3_i ];
					$description  = $level3_item->get_description( get_the_title( $post ) );
					$grading_name = 'bwg-grading-' . $level0_i . '-' . $level1_i . '-' . $level2_i . '-' . $level3_i;
					?>

                    <div class="level-3-container">
                        <div class="level-details">
                            <p class="level-title"><?php esc_html_e( $level3_item->get_label() ); ?></p>
							<?php if ( ! empty( $description ) ) : ?>
                                <div class="description"><?php echo $description; ?></div>
							<?php endif; ?>
                        </div>

                        <div class="level-grading-container"
                             data-grading-sub-total-ref="<?php echo $grading_sub_total; ?>"
                             data-grading-weight="<?php echo $level3_item->get_weight(); ?>"
                             data-grading-name="<?php echo $grading_name; ?>">
							<?php
							foreach ( $grading as $g ) { ?>
                                <div class="level-grading">
                                    <div class="level-grading-label"><?php echo $g->label; ?></div>
                                    <div class="level-grading-control">
                                        <input type="radio" name="<?php echo $grading_name; ?>"
                                               value="<?php echo $g->points; ?>"
                                               style="margin: 0;"/>
                                    </div>
                                </div>
							<?php } ?>
                        </div>
                        <div class="level-grading-weight">
							<?php echo sprintf( '%.01f', $level3_item->get_weight() ); ?>
                        </div>
                    </div>

				<?php } ?>

			<?php } ?>

		<?php } ?>

        <div style="margin-top: 3em; margin-bottom: 10em;">
            <div id="spider-chart-<?php echo $level0_item->get_id(); ?>" style="max-width: 800px;"></div>
        </div>

	<?php } ?>

</div>
