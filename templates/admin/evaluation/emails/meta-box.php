<?php
/**
 * Variables
 * ---------
 *
 * @var \WP_Post $post
 * @var string $emails_test_action
 * @var string $emails_test_nonce
 * @var string $user_confirm
 * @var string[] $feup_fields
 * @var string $emails_test_receiver
 */

?>
<div id="bwg-em-meta-box" class="bwg"
     data-bwg-post-id="<?php echo $post->ID; ?>"
     data-bwg-emails-test-action="<?php echo $emails_test_action; ?>"
     data-bwg-emails-test-nonce="<?php echo $emails_test_nonce; ?>">

    <p class="post-attributes-label-wrapper">
        <label class="post-attributes-label" for="bwg-em-user-confirm">
			<?php _e( 'Bestätigungs-E-Mail an Benutzer bei Abschicken ihrer Bewertung', 'bwg' ); ?>
        </label>
    </p>

    <p class="description">
		<?php if ( ! empty( $feup_fields ) ) { ?>
			<?php _e( 'Felder aus dem Front-End Users Plugin können verwendet werden.', 'bwg' ); ?>
		<?php } ?>
		<?php _e( 'HTML-Code wird unterstützt.', 'bwg' ); ?>
		<?php _e( 'Der Inhalt wird automatisch von &lt;p&gt;-Tags umschlossen.', 'bwg' ); ?>
    </p>

    <textarea id="bwg-em-user-confirm" name="bwg_em_user_confirm"
              class="bwg-em-email-text"><?php echo esc_textarea( $user_confirm ); ?></textarea>

    <div class="bwg-em-test-email-button-container">
        <button type="button" class="button" data-bwg-action="send-test-email"
                data-bwg-email="user-confirm"
                data-bwg-target="bwg-em-user-confirm">
			<?php _e( 'Test E-Mail senden', 'bwg' ); ?>
        </button>
        <span class="spinner"></span>
    </div>

    <div class="bwg-em-info-panel">
        <p class="description">
            <span class="dashicons dashicons-info"></span>
			<?php if ( ! empty( $feup_fields ) ) { ?>
				<?php
				printf(
					__( 'Front-End Users Plugin Felder: %1$s', 'bwg' ),
					'[' . implode( '], [', array_map( 'esc_html', $feup_fields ) ) . ']'
				);
				?><br>
			<?php } ?>
			<?php printf( __( 'Test E-Mail Empfängerin (Aktueller Wordpress Benutzer): %1$s', 'bwg' ),
				esc_html( $emails_test_receiver ) ); ?>
        </p>
    </div>

</div>
