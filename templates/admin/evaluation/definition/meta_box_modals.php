<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * No variables.
 */
?>

<div style="display: none;">

    <div id="bwg_ed_item_editor" title="Bearbeiten" tabindex="-1">
        <form>
            <fieldset>

                <p>
                    <label for="bwg_ed_item_editor_label"><strong>Bezeichnung</strong></label>
                </p>
                <p>
                    <input id="bwg_ed_item_editor_label" type="text" style="width:100%;">
                </p>

                <p>
                    <label for="bwg_ed_item_editor_description"><strong>Beschreibung</strong></label>
                </p>
                <div style="padding-top: 35px;">
					<?php wp_editor(
						'',
						'bwg_ed_item_editor_description',
						[
							'media_buttons'  => TRUE,
							'default_editor' => 'tinymce',
							'textarea_rows'  => 8,
							'editor_height'  => 150,
							'tinymce'        => [
								'toolbar1' => 'formatselect,bold,italic,bullist,numlist,blockquote,alignleft,aligncenter,alignright,link,unlink,wp_adv',
							]
						]
					); ?>
                </div>

                <div class="level-2-visible">
                    <p>
                        <label for="bwg_ed_item_editor_details"><strong>Details</strong></label>
                    </p>
                    <div style="padding-top: 35px;">
                        <?php wp_editor(
                            '',
                            'bwg_ed_item_editor_details',
                            [
                                'media_buttons'  => TRUE,
                                'default_editor' => 'tinymce',
                                'textarea_rows'  => 8,
                                'editor_height'  => 150,
                                'tinymce'        => [
                                    'toolbar1' => 'formatselect,bold,italic,bullist,numlist,blockquote,alignleft,aligncenter,alignright,link,unlink,wp_adv',
                                ]
                            ]
                        ); ?>
                    </div>
                </div>

                <div class="level-2-visible">
                    <p>
                        <label for="bwg_ed_item_editor_weight"><strong>Gewichtung</strong></label>
                    </p>
                    <p>
                        <input id="bwg_ed_item_editor_weight" type="text" style="width: 100%;">
                    </p>
                </div>

                <div class="level-1-visible">
                    <p>
                        <label><strong>Netzdiagramm</strong></label>
                    </p>
                    <p>
                        <input type="checkbox" id="bwg_ed_item_editor_spider_chart">
                        <label for="bwg_ed_item_editor_spider_chart">
                            Ja, diese Kategorie im Netzdiagramm anzeigen
                        </label>
                    </p>
                </div>

                <!-- Allow form submission with keyboard without duplicating the dialog button -->
                <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
            </fieldset>
        </form>
    </div>

    <div id="bwg_ed_preset_loader" title="Vorlage laden">
        <div class="state-waiting">
            <p>Bitte warten...</p>
        </div>
        <div class="state-error">
            <p>Die Vorlagen konnten leider nicht geladen werden.</p>
        </div>
        <div class="state-normal">
            <p>
                Wählen Sie die zu ladende Vorlage aus.<br>
                <strong>ACHTUNG: Dies wird die jetzige Definition ersetzen.</strong>
            </p>
            <ul id="bwg_ed_preset_loader_list" class="list-square"></ul>
        </div>
    </div>

    <div id="bwg_ed_spider_chart_settings" title="Spider-Chart Einstellungen">
        <div class="state-waiting">
            <p>Bitte warten...</p>
        </div>
        <div class="state-normal">

            <form>
                <fieldset>
                    <legend>Generelle Einstellungen</legend>

                </fieldset>

                <div id="bwg_ed_spider_charts_container"></div>

                <!-- Allow form submission with keyboard without duplicating the dialog button -->
                <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
            </form>

        </div>
    </div>

    <script id="bwg_ed_spider_chart_settings_template" type="text/template">
        <fieldset>
            <legend>###NAME###</legend>

            <div style="display: flex;">
                <div style="flex:1;">
                    <input type="checkbox"/>
                </div>
                <div style="width:600px;">
                    ###SVG###
                </div>
            </div>

        </fieldset>
    </script>

</div>
