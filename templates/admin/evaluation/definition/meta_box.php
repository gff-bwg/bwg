<?php

use bwg\evaluation\BWG_Evaluation_Presets;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * @var $nonce_field string HTML code.
 * @var $evaluation_definition array
 * @var $evaluation_definition_json string JSON string.
 * @var $evaluation_definition_html string HTML code.
 * @var $evaluation_definition_fixed boolean Should the definition be fixed (add, delete and reordering are disabled).
 */

?>
<div class="inside<?php echo $evaluation_definition_fixed ? ' bwg-evaluation-definition-fixed' : ''; ?>">

	<?php if ( $evaluation_definition_fixed ) { ?>
        <div class="bwg-notice">
            <p>
                Diese Evaluation hat bereits eingereichte Bewertungen. Sie können somit die Definition nur noch
                eingeschränkt bearbeiten.
            </p>
        </div>
	<?php } ?>

    <div>
        <div id="bwg_ed_container"
             data-bwg-ed-presets-action="<?php echo BWG_Evaluation_Presets::PRESETS_AJAX_ACTION; ?>"
             data-bwg-ed-presets-nonce="<?php echo wp_create_nonce( BWG_Evaluation_Presets::PRESETS_AJAX_ACTION ); ?>"
             data-bwg-ed-fixed="<?php echo $evaluation_definition_fixed ? '1' : '0'; ?>"
             data-bwg-ed-ai="<?php echo esc_attr( isset( $evaluation_definition['ai'] ) ? $evaluation_definition['ai'] : '0' ); ?>">

            <div style="float: right;">
				<?php if ( ! $evaluation_definition_fixed ) { ?>
                    <a href="#" data-bwg-ed-action="preset-load">Vorlage laden</a> |
				<?php } ?>
                <a href="#" data-bwg-ed-action="preset-save">Vorlage speichern</a>
            </div>

            <p>
                Definieren Sie hier die Evaluation. Die Struktur der einzelnen Einträge ist wie folgt:
                <span class="text-level-0" data-bwg-level-0-label>Interesse</span> &raquo;
                <span class="text-level-1" data-bwg-level-1-label>Kategorie</span> &raquo;
                <span class="text-level-2" data-bwg-level-2-label>Aussage</span>.
            </p>

            <textarea id="bwg_ed" name="bwg_ed"
                      style="position:absolute; top:-20000px; left:-20000px; width:0; height:0;"><?php
				echo esc_html( $evaluation_definition_json );
				?></textarea>
            <div id="bwg_ed_tree">
				<?php echo $evaluation_definition_html; ?>
            </div>
        </div>
    </div>

</div>
