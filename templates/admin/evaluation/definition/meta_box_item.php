<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * @var $id int
 * @var $label string
 * @var $description string
 * @var $details string
 * @var $weight string
 * @var $spider_chart string
 * @var $is_open bool
 * @var $sub_level_exists bool
 * @var $sub_items string
 */

?>
<li data-bwg-ed-id="<?php esc_attr_e( $id ); ?>" class="<?php echo( $is_open ? 'open' : '' ); ?>">
    <input type="hidden" value="<?php esc_attr_e( $label ) ?>" data-bwg-ed-field="label">
    <input type="hidden" value="<?php esc_attr_e( $weight ) ?>" data-bwg-ed-field="weight">
    <input type="hidden" value="<?php esc_attr_e( $spider_chart ); ?>" data-bwg-ed-field="spider_chart">
    <textarea data-bwg-ed-field="description" class="hidden"><?php esc_html_e( $description ); ?></textarea>
    <textarea data-bwg-ed-field="details" class="hidden"><?php esc_html_e( $details ); ?></textarea>

    <div class="bwg-ed-item-header <?php echo( ! $sub_level_exists ? ' bwg-ed-item-header-last-level' : '' ) ?>">
        <strong><?php esc_html_e( $label ) ?></strong>

        <span class="bwg-ed-item-warning dashicons dashicons-warning hidden"></span>

        <div class="row-actions">
            <span class="edit">
                <a href="#" data-bwg-ed-action="edit">Bearbeiten</a>
            </span>
            <span class="trash hide-if-ed-fixed">
                |
                <a href="#" class="submitdelete" data-bwg-ed-action="delete">Löschen</a>
            </span>
        </div>
    </div>
	<?php if ( $sub_level_exists ) { ?>
        <div class="bwg-ed-item-content"><?php echo $sub_items; ?></div>
	<?php } ?>
</li>
