<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * @var $level int The current hierarchy level.
 * @var $items string HTML code.
 */
?>

<ul class="bwg-ed-items bwg-ed-level-<?php esc_attr_e( $level ); ?>" data-bwg-ed-level="<?php esc_attr_e( $level ); ?>">
	<?php echo $items; ?>
</ul>

<div class="bwg-ed-items-actions bwg-ed-items-actions-<?php esc_attr_e( $level ) ?>">
    <a href="#" data-bwg-ed-action="add" data-bwg-ed-level="<?php esc_attr_e( $level ) ?>" class="hide-if-ed-fixed">Hinzufügen</a>
</div>
