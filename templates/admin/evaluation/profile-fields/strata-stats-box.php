<?php
/**
 * @var string $uid The profile field unique id.
 * @var string $label The profile field label.
 * @var string $type The profile field type (actually the name).
 * @var bool $strata_supported The profile field supports strata.
 * @var string $content The content as html.
 */

?>

<div class="bwg-strata-stats-box<?php if ( $strata_supported ) { ?> bwg-strata-stats-box-filterable<?php } ?>">
    <div class="bwg-strata-stats-box-header"
		<?php if ( $strata_supported ) { ?>
            data-action="load-as-strata"
		<?php } ?>
         data-bwg-pf-uid="<?php echo $uid; ?>"
         data-bwg-pf-type="<?php echo $type; ?>"><?php echo $label; ?><?php
		if ( ! $strata_supported ) {
			?><span style="float:right;color:#82878c;"
                    class="dashicons dashicons-warning"
                    title="<?php echo esc_attr__( 'Keine Strata-Unterstützung', 'bwg' ); ?>"> </span><?php }
		?></div>
    <div class="bwg-strata-stats-box-content">
		<?php echo $content; ?>
    </div>
</div>
