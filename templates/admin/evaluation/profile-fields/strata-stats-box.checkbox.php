<?php
/**
 * @var array $checkboxes The checkboxes. Each checkbox has the properties "uid", "label", "amount" and
 * "amount_percentage".
 */

echo bwg_base()
	->utils()
	->render_admin_template(
		'evaluation/profile-fields/strata-stats-bar-table.php',
		[
			'items' => $checkboxes,
		]
	);

echo '<p class="bwg-strata-stats-hint">' . __( 'Mehrfach-Auswahl möglich.', 'bwg' ) . '</p>';
