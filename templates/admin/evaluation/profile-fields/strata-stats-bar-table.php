<?php
/**
 * @var array $items The items. Each item has the properties "label", "amount" and "amount_percentage".
 */

?>

<table class="bwg-strata-stats-bar-table wp-list-table widefat striped">
    <tbody>
	<?php foreach ( $items as $item ) { ?>
        <tr>
            <td>
                <div class="bwg-strata-stats-bar-table-bar-container">
                    <div class="bwg-strata-stats-bar-table-bar"
                         style="width: <?php echo $item['amount_percentage']; ?>%;"></div>
                </div>
                <div class="bwg-strata-stats-bar-table-info-container">
                    <div class="bwg-strata-stats-bar-table-info-label"><?php
						echo $item['label']; ?></div>
                    <div class="bwg-strata-stats-bar-table-amount"><?php
						echo number_format_i18n( $item['amount'] ); ?></div>
                    <div class="bwg-strata-stats-bar-table-percentage"><?php
						printf( '%.01f%%', $item['amount_percentage'] ); ?></div>
                </div>
            </td>
        </tr>
	<?php } ?>
    </tbody>
</table>

