<?php
/**
 * @var string $uid
 * @var string $label
 * @var \bwg\profile\fields\BWG_Profile_Field_Options_Choice $field_options
 * @var \bwg\profile\fields\BWG_Profile_Field_Type $field_type
 */

// Get the number of choices.
$c = count( $field_options->get_choices() );
if ( 0 === $c ) {
	return;
}

// Initialize the choice percentages.
$choice_percentages = [];
for ( $i = 0; $i < $c - 1; $i ++ ) {
	array_push(
		$choice_percentages,
		1 === $i % 2 ? ceil( 100 / $c ) : floor( 100 / $c )
	);
}
array_push( $choice_percentages, 100 - array_sum( $choice_percentages ) );

?>
<table class="bwg-filter-settings">
	<?php
	$i = 0;
	foreach ( $field_options->get_choices() as $choice_uid => $choice_label ) {
		?>
        <tr data-bwg-filter-condition>
            <td>
                <label for="sf-<?php esc_attr_e( $uid ); ?>-<?php esc_attr_e( $choice_uid ); ?>">
					<?php esc_html_e( $choice_label ); ?>
                </label>
                <input type="hidden"
                       name="sf-<?php echo $uid; ?>-values[]"
                       value="<?php esc_attr_e( $choice_uid ); ?>">
            </td>
            <td>
                <input id="sf-<?php esc_attr_e( $uid ); ?>-<?php esc_attr_e( $choice_uid ); ?>"
                       name="sf-<?php esc_attr_e( $uid ); ?>-percentages[]"
                       type="number"
                       class="input-percentage"
                       value="<?php esc_attr_e( $choice_percentages[ $i ] ); ?>">%
            </td>
        </tr>
		<?php
		$i ++;
	}
	?>
    <tr>
        <td>TOTAL</td>
        <td><span class="bwg-total-percentage">-</span></td>
        <td>&nbsp;</td>
    </tr>
</table>