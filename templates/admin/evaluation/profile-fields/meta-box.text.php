<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid The unique id of this field.
 * @var string $label The label of this field.
 * @var \bwg\profile\fields\BWG_Profile_Field_Options_Text $options The field options.
 * @var \bwg\profile\fields\BWG_Profile_Field_Type_Text $field_type
 */

use bwg\profile\fields\BWG_Profile_Field_Options_Text;

?>

<div class="bwg-epf-text-field">

    <div>
        <p class="post-attributes-label-wrapper">
            <label class="post-attributes-label">
                Art des Eingabefeldes
            </label>
        </p>

        <div class="bwg-epf-radio-container">
            <label>
                <input type="radio" name="bwg_epf_<?php echo $uid; ?>_mode"
                       value="<?php echo esc_attr( BWG_Profile_Field_Options_Text::MODE_INPUT_TEXT ); ?>"
					<?php echo $options->is_mode( BWG_Profile_Field_Options_Text::MODE_INPUT_TEXT ) ? ' checked' : '';
					?>>
                Text (einzeilig)
            </label>
            <label>
                <input type="radio" name="bwg_epf_<?php echo $uid; ?>_mode"
                       value="<?php echo esc_attr( BWG_Profile_Field_Options_Text::MODE_INPUT_NUMBER ); ?>"
					<?php echo $options->is_mode( BWG_Profile_Field_Options_Text::MODE_INPUT_NUMBER ) ? ' checked' : '';
					?>>
                Zahl
            </label>
            <label>
                <input type="radio" name="bwg_epf_<?php echo $uid; ?>_mode"
                       value="<?php echo esc_attr( BWG_Profile_Field_Options_Text::MODE_INPUT_EMAIL ); ?>"
					<?php echo $options->is_mode( BWG_Profile_Field_Options_Text::MODE_INPUT_EMAIL ) ? ' checked' : '';
					?>>
                E-Mail-Adresse
            </label>
            <label>
                <input type="radio" name="bwg_epf_<?php echo $uid; ?>_mode"
                       value="<?php echo esc_attr( BWG_Profile_Field_Options_Text::MODE_INPUT_TEL ); ?>"
					<?php echo $options->is_mode( BWG_Profile_Field_Options_Text::MODE_INPUT_TEL ) ? ' checked' : '';
					?>>
                Telefonnummer
            </label>
            <label>
                <input type="radio" name="bwg_epf_<?php echo $uid; ?>_mode"
                       value="<?php echo esc_attr( BWG_Profile_Field_Options_Text::MODE_TEXTAREA ); ?>"
					<?php echo $options->is_mode( BWG_Profile_Field_Options_Text::MODE_TEXTAREA ) ? ' checked' : ''; ?>>
                Text (Mehrzeilig)
            </label>
        </div>
    </div>

    <div data-bwg-epf-mode-hidden="<?php echo esc_attr( BWG_Profile_Field_Options_Text::MODE_INPUT_NUMBER ); ?>">
        <p class="post-attributes-label-wrapper">
            <label for="bwg-epf-<?php echo $uid; ?>-max-length" class="post-attributes-label">
                Maximale Anzahl Zeichen
            </label>
        </p>
        <input id="bwg-epf-<?php echo $uid; ?>-max-length" type="number" min="1" value="<?php
		echo esc_attr( $options->get_max_length() ); ?>"/>
    </div>

    <div>
        <p class="post-attributes-label-wrapper">
            <label class="post-attributes-label">
                Auswertung
            </label>
        </p>
        <label>
            <input type="checkbox"
                   id="bwg-epf-<?php echo $uid; ?>-stats-enabled"
                   value="1"<?php echo $options->is_stats_enabled() ? ' checked' : ''; ?>>
            In der Auswertung anzeigen
        </label>
    </div>

</div>
