<?php
/**
 * @var string $uid The uid.
 * @var string $label The label.
 * @var string $type The type.
 * @var string $content Raw html code.
 */

$enabled = FALSE;
?>
<tr>
    <th>
        Filter: <?php esc_html_e( $label ) ?>
    </th>
    <td>
        <label for="sf-<?php echo $uid; ?>-enabled">
            <input id="sf-<?php echo $uid; ?>-enabled"
                   name="sf-<?php echo $uid; ?>-enabled"
                   type="checkbox"
                   value="1"
				<?php if ( $enabled ) { ?> checked<?php } ?>>
            Filter aktiviert
        </label>

		<?php echo $content; ?>
    </td>
</tr>