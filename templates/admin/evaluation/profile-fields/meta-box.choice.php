<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid The unique id of this field.
 * @var string $label The label of this field.
 * @var \bwg\profile\fields\BWG_Profile_Field_Options_Choice $options The field options.
 * @var \bwg\profile\fields\BWG_Profile_Field_Type_Choice $field_type
 */

use bwg\profile\fields\BWG_Profile_Field_Options_Choice;

?>
<div class="bwg-epf-choice-field">

    <div>
        <p class="post-attributes-label-wrapper">
            <label class="post-attributes-label">
                Art des Auswahlfeldes
            </label>
        </p>

        <div class="bwg-epf-radio-container">
            <label>
                <input type="radio" name="bwg_epf_<?php echo $uid; ?>_mode"
                       value="<?php echo esc_attr( BWG_Profile_Field_Options_Choice::MODE_INPUT_RADIO ); ?>"
					<?php
					echo $options->is_mode( BWG_Profile_Field_Options_Choice::MODE_INPUT_RADIO ) ? ' checked' : '';
					?>>
                Radiobutton
            </label>
            <label>
                <input type="radio" name="bwg_epf_<?php echo $uid; ?>_mode"
                       value="<?php echo esc_attr( BWG_Profile_Field_Options_Choice::MODE_INPUT_RADIO_FLOAT ); ?>"
					<?php
					echo $options->is_mode( BWG_Profile_Field_Options_Choice::MODE_INPUT_RADIO_FLOAT ) ? ' checked' : '';
					?>>
                Radiobutton (fliessend)
            </label>
            <label>
                <input type="radio" name="bwg_epf_<?php echo $uid; ?>_mode"
                       value="<?php echo esc_attr( BWG_Profile_Field_Options_Choice::MODE_SELECT ); ?>"
					<?php
					echo $options->is_mode( BWG_Profile_Field_Options_Choice::MODE_SELECT ) ? ' checked' : '';
					?>>
                Selectbutton
            </label>
        </div>
    </div>

    <div>
        <p class="post-attributes-label-wrapper">
            <label class="post-attributes-label">
                Werte (Radiobutton vor Textfeld gibt Standardauswahl an)
            </label>
        </p>

        <div>
            <ul class="bwg-epf-choice-actions">
                <li><a data-bwg-epf-choice-action="add"><span class="icon-plus"></span></a></li>
            </ul>
        </div>

        <div id="bwg-epf-<?php echo $uid; ?>-choices">
			<?php
			$default_choice = $options->get_default_choice();

			foreach ( $options->get_choices() as $choice_key => $choice_label ) {
				///
				// Start of template >>
				//   Note that when you change anything here, change it in the
				//   javascript file "@dev/js/admin/evaluation/profile-fields/meta-box.choice.js"
				//   too. See the $template variable in the method epf_choice.add_choice().
				///
				?>
                <div class="bwg-epf-choice-container" data-bwg-epf-choice="<?php echo esc_attr( $choice_key ); ?>">
                    <div class="bwg-epf-choice-action-container">
                        <ul class="bwg-epf-choice-actions">
                            <li><a class="bwg-epf-choice-move-handler"><span class="icon-move-vertical"></span></a></li>
                        </ul>
                    </div>
                    <div class="bwg-epf-choice-default-container">
                        <input type="radio"
                               name="bwg_epf_<?php echo $uid; ?>_choice_default"
                               value="<?php echo esc_attr( $choice_key ); ?>"<?php
						echo $default_choice === $choice_key ? ' checked' : '';
						?>>
                    </div>
                    <div class="bwg-epf-choice-text-container"><?php
						?><input type="text" value="<?php echo esc_attr( $choice_label ); ?>"><?php
						?></div>
                    <div class="bwg-epf-choice-action-container">
                        <ul class="bwg-epf-choice-actions">
                            <li><a data-bwg-epf-choice-action="delete"><span class="icon-minus"></span></a></li>
                            <li><a data-bwg-epf-choice-action="add"><span class="icon-plus"></span></a></li>
                        </ul>
                    </div>
                </div>
				<?php
				///
				// << End of template
				///
			} ?>
        </div>
    </div>

</div>
