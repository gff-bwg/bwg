<?php
/**
 * @var array $histogram_classes The histogram classes. Each histogram class has the properties "label", "amount"
 * and "amount_percentage".
 */

echo bwg_base()
	->utils()
	->render_admin_template(
		'evaluation/profile-fields/strata-stats-bar-table.php',
		[
			'items' => $histogram_classes,
		]
	);
