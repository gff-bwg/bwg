<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid The unique id of this field.
 * @var string $label The label of this field.
 * @var \bwg\profile\fields\BWG_Profile_Field_Options_Checkbox $options The field options.
 * @var \bwg\profile\fields\BWG_Profile_Field_Type_Checkbox $field_type
 */

?>
<div class="bwg-epf-checkbox-field">

    <div>
        <p class="post-attributes-label-wrapper">
            <label class="post-attributes-label">
                Werte
            </label>
        </p>

        <div>
            <ul class="bwg-epf-checkbox-actions">
                <li><a data-bwg-epf-checkbox-action="add"><span class="icon-plus"></span></a></li>
            </ul>
        </div>

        <div id="bwg-epf-<?php echo $uid; ?>-checkboxes">
			<?php
			foreach ( $options->get_checkboxes() as $checkbox_key => $checkbox_label ) {
				///
				// Start of template >>
				//   Note that when you change anything here, change it in the
				//   javascript file "@dev/js/admin/evaluation/profile-fields/meta-box.checkbox.js"
				//   too. See the $template variable in the method epf_checkbox.add_checkbox().
				///
				?>
                <div class="bwg-epf-checkbox-container"
                     data-bwg-epf-checkbox="<?php echo esc_attr( $checkbox_key ); ?>">
                    <div class="bwg-epf-checkbox-action-container">
                        <ul class="bwg-epf-checkbox-actions">
                            <li>
                                <a class="bwg-epf-checkbox-move-handler"><span class="icon-move-vertical"></span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="bwg-epf-checkbox-text-container"><?php
						?><input type="text" value="<?php echo esc_attr( $checkbox_label ); ?>"><?php
						?></div>
                    <div class="bwg-epf-checkbox-action-container">
                        <ul class="bwg-epf-checkbox-actions">
                            <li><a data-bwg-epf-checkbox-action="delete"><span class="icon-minus"></span></a></li>
                            <li><a data-bwg-epf-checkbox-action="add"><span class="icon-plus"></span></a></li>
                        </ul>
                    </div>
                </div>
				<?php
				///
				// << End of template
				///
			} ?>
        </div>
    </div>

</div>
