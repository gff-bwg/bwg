<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid The unique id of this field.
 * @var string $label The label of this field.
 * @var \bwg\profile\fields\BWG_Profile_Field_Options_Year $options The field options.
 * @var \bwg\profile\fields\BWG_Profile_Field_Type_Year $field_type
 */

$year_min = 1900;
$year_max = date( 'Y' );

$year_start = $options->get_year_start();
$year_end   = $options->get_year_end();

?>
<div class="bwg-epf-choice-field">

    <div>
        <p class="post-attributes-label-wrapper">
            <label class="post-attributes-label" for="bwg-epf-<?php echo $uid; ?>-year-end">
                Jüngster Jahrgang
            </label>
        </p>

        <div>
            <select id="bwg-epf-<?php echo $uid; ?>-year-end"
                    name="bwg_epf_<?php echo $uid; ?>_year_end">
				<?php
				for ( $i = $year_min; $i <= $year_max; $i ++ ) {
					echo '<option value="' . $i . '"';
					if ( $i === $year_end ) {
						echo ' selected';
					}
					echo '>' . $i . '</option>';
				}
				?>
            </select>
        </div>
    </div>
    
    <div>
        <p class="post-attributes-label-wrapper">
            <label class="post-attributes-label" for="bwg-epf-<?php echo $uid; ?>-year-start">
                Ältester Jahrgang
            </label>
        </p>

        <div>
            <select id="bwg-epf-<?php echo $uid; ?>-year-start"
                    name="bwg_epf_<?php echo $uid; ?>_year_start">
				<?php
				for ( $i = $year_min; $i <= $year_max; $i ++ ) {
					echo '<option value="' . $i . '"';
					if ( $i === $year_start ) {
						echo ' selected';
					}
					echo '>' . $i . '</option>';
				}
				?>
            </select>
        </div>
    </div>

</div>
