<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid The uid of this profile field.
 * @var string $type The field type.
 * @var bool $loading_template [OPT] Defaults to FALSE.
 *
 * If $loading_template is FALSE, then the following variables are required.
 * @var string $label The label.
 * @var boolean $required Is the field required?
 * @var string $content The html content.
 */

?>
<div class="bwg-epf-field-container"
     data-bwg-epf-field
     data-bwg-epf-uid="<?php echo $uid; ?>"
     data-bwg-epf-type="<?php echo esc_attr( $type ); ?>">

    <div class="bwg-epf-field-wrapper">
		<?php if ( ! isset( $loading_template ) || FALSE === $loading_template ) { ?>
            <ul class="bwg-epf-field-actions">
                <li>
                    <a class="bwg-action-warning" data-bwg-epf-action="delete-field">
                        Löschen
                    </a>
                </li>
                <li>
                    <a class="bwg-epf-move-handler">
                        <span class="icon-move-vertical"></span>
                    </a>
                </li>
            </ul>

            <div style="display: flex;">
                <div style="flex-grow: 1;">
                    <p class="post-attributes-label-wrapper">
                        <label class="post-attributes-label" for="bwg-epf-<?php echo $uid; ?>-label">
                            Bezeichnung
                        </label>
                    </p>
                    <div>
                        <input type="text" id="bwg-epf-<?php echo $uid; ?>-label"
                               value="<?php echo esc_attr( $label ); ?>">
                    </div>
                </div>

                <div style="width: 200px; padding-left: 10px;">
                    <p class="post-attributes-label-wrapper">
                        <label class="post-attributes-label">
                            Pflichtfeld
                        </label>
                    </p>
                    <label style="padding-top: 8px;">
                        <input type="checkbox"
                               id="bwg-epf-<?php echo $uid; ?>-required"
                               value="1"<?php echo $required ? ' checked' : ''; ?>>
                        Pflichtfeld
                    </label>
                </div>
            </div>

			<?php echo $content; ?>
		<?php } else { ?>
            <div style="text-align: center">
                <span class="spinner" style="visibility: visible; float: none; margin: 0;"></span>
            </div>
		<?php } ?>
    </div>
</div>
