<?php
/**
 * @var array $choices The choices. Each choice has the properties "uid", "label", "amount" and "amount_percentage".
 */

echo bwg_base()
	->utils()
	->render_admin_template(
		'evaluation/profile-fields/strata-stats-bar-table.php',
		[
			'items' => $choices,
		]
	);
