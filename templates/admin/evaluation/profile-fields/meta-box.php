<?php
/**
 * Variables
 * ---------
 *
 * @var \WP_Post $post
 * @var string $profile_field_action
 * @var string $profile_field_nonce
 * @var array $type_list
 * @var string $json_value
 * @var string $introduction
 * @var string[] $fields
 * @var string $field_loading_template
 * @var string[] $admin_colors
 */

$background_color = $admin_colors[1];
$foreground_color = $admin_colors['base'];

?>

<div id="bwg-epf-meta-box" class="bwg"
     data-bwg-epf-profile-field-action="<?php echo esc_attr( $profile_field_action ); ?>"
     data-bwg-epf-profile-field-nonce="<?php echo esc_attr( $profile_field_nonce ); ?>">

    <div style="background-color: <?php echo $background_color; ?>;">
        <div class="bwg-epf-section-header" style="color: <?php echo $foreground_color; ?>;">
            Einleitungstext
        </div>
        <div class="bwg-epf-section-content"><?php
			wp_editor( $introduction, 'epf_introduction', [
				'editor_height' => 200,
				'media_buttons' => FALSE,
				'teeny'         => TRUE,
			] );
			?></div>
    </div>

    <div style="background-color: <?php echo $background_color; ?>;">
        <div class="bwg-epf-section-header" style="color: <?php echo $foreground_color; ?>;">
            Felder
        </div>
        <div class="bwg-epf-section-content">
            <div class="bwg-epf-add-field" data-bwg-epf-add-field-position="begin">
                <span class="dashicons dashicons-plus"></span>
                <ul>
					<?php foreach ( $type_list as $type_name => $type_label ) { ?>
                        <li><a data-bwg-epf-action="add-field" data-bwg-epf-field-type="<?php
							echo esc_attr( $type_name );
							?>"><?php
								echo esc_html( $type_label );
								?></a></li>
					<?php } ?>
                </ul>
            </div>

            <div id="bwg-epf-list">
				<?php foreach ( $fields as $field ) {
					echo $field;
				} ?>
            </div>

            <div class="bwg-epf-add-field" data-bwg-epf-add-field-position="end">
                <span class="dashicons dashicons-plus"></span>
                <ul>
					<?php foreach ( $type_list as $type_name => $type_label ) { ?>
                        <li><a data-bwg-epf-action="add-field" data-bwg-epf-field-type="<?php
							echo esc_attr( $type_name );
							?>"><?php
								echo esc_html( $type_label );
								?></a></li>
					<?php } ?>
                </ul>
            </div>
        </div>
    </div>

	<?php /*<!--suppress HtmlFormInputWithoutLabel -->*/ ?>
    <textarea id="bwg-epf" name="bwg_epf"
              style="position:absolute; top:-1000px; width:0; height:0;"><?php echo esc_html( $json_value ); ?></textarea>
</div>

<script id="bwg-epf-profile-field-loading-template" type="text/template">
	<?php echo $field_loading_template; ?>
</script>
