<?php
/**
 * @var string $uid The profile field unique id.
 * @var string $label The profile field label.
 * @var string $content The content as html.
 */
?>

<div class="bwg-stats-box">
    <div class="bwg-stats-box-header"
         data-bwg-pf-uid="<?php echo $uid; ?>"><?php echo $label; ?>
    </div>
    <div class="bwg-stats-box-content">
		<?php echo $content; ?>
    </div>
</div>
