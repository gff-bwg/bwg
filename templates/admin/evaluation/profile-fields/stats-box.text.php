<?php
/**
 * @var array $values
 */
?>

<table class="wp-list-table widefat fixed striped">
    <thead>
    <tr>
        <th>Eingabewert</th>
        <th>Anzahl</th>
    </tr>
    </thead>
    <tbody>
	<?php foreach ( $values as $value ) { ?>
        <tr>
            <td><?php esc_html_e( $value['value'] ); ?></td>
            <td><?php esc_html_e( $value['amount'] ); ?></td>
        </tr>
	<?php } ?>
    </tbody>
</table>
