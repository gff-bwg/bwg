<?php
/**
 * Variables
 * ---------
 *
 * @var string $heading The heading (would be html escaped).
 * @var string $content
 * @var array $fields
 * @var array $buttons Array of associative arrays containing the following keys
 *                     - string 'label': The label.
 *                     - array 'classes' [OPT]: Classes (the class 'button' would always be added).
 *                     - string 'type' [OPT]: The type ('reset', 'button', 'submit').
 *                     - string 'href' [OPT]: The link href.
 */

?>

<form id="bwg-simple-dialog" method="get">
    <div class="wrap">
		<?php if ( ! empty( $heading ) ) { ?>
            <h1 class="wp-heading-inline"><?php echo esc_html( $heading ); ?></h1>
            <hr class="wp-header-end"/>
		<?php } ?>

		<?php
		foreach ( $fields as $name => $value ) { ?>
            <input type="hidden" name="<?php echo esc_attr( $name ); ?>" value="<?php echo esc_attr( $value ); ?>"/>
		<?php } ?>

        <div class="notice notice-info">

			<?php echo $content; ?>

			<?php if ( ! empty( $buttons ) ) { ?>
                <hr/>

                <p>
					<?php foreach ( $buttons as $button ) {
						if ( ! isset( $button['classes'] ) ) {
							$button['classes'] = [];
						}
						array_push( $button['classes'], 'button' );

						$tag = 'button';
						if ( isset( $button['href'] ) ) {
							$tag = 'a';
						}

						echo '<' . $tag;
						if ( isset( $button['href'] ) ) {
							echo ' href="' . esc_attr( $button['href'] ) . '"';
						}
						if ( isset( $button['type'] ) ) {
							echo ' type="' . esc_attr( $button['type'] ) . '"';
						}
						echo ' class="' . implode( ' ', $button['classes'] ) . '">';
						echo esc_html( $button['label'] );
						echo '</' . $tag . '> ';
					} ?>
                </p>
			<?php } ?>
        </div>

    </div>
</form>
