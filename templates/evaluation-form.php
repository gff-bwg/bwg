<?php
/**
 * Variables:
 *
 * @var \WP_Post $post
 * @var BWG_Evaluation_Definition $evaluation_definition
 * @var BWG_Evaluation_Grading[] $grading
 * @var \bwg\database\models\BWG_User_Storage $user_storage
 * @var string $ajax_url
 * @var string $user_persist_ajax_action
 * @var string $user_persist_ajax_action_nonce
 * @var string $spider_chart_ajax_action
 * @var string $spider_chart_ajax_action_nonce
 * @var string $spider_chart_intro_text
 * @var string $spider_chart_final_intro_text
 * @var string $profile_fields_introduction
 * @var string[] $profile_fields The rendered profile fields.
 */

use bwg\evaluation\BWG_Evaluation_Definition;
use bwg\evaluation\BWG_Evaluation_Grading;

$bwg_globals                = bwg_base()->globals();
$bwg_mode                   = bwg_base()->options()->get_option_bwg_mode();
$placeholder_title          = get_the_title( $post );
$spider_chart_padding_ratio = 100 * $bwg_globals->spider_chart_height() / $bwg_globals->spider_chart_width();

$sequence_number = $user_storage->get_sequence_number();
$user_grading    = $user_storage->get_grading();
$user_notes      = $user_storage->get_notes();
$user_meta       = $user_storage->get_meta();

echo '<div style="display:none;">' . file_get_contents( BWG_ABS_PATH . '/assets/assets.svg' ) . '</div>';
?>

<div id="bwg-e"
     data-post-id="<?php echo $post->ID; ?>"
     data-bwg-ajax-url="<?php echo $ajax_url; ?>"
     data-sequence-number="<?php echo $sequence_number; ?>"
     data-bwg-user-persist-ajax-action="<?php echo $user_persist_ajax_action; ?>"
     data-bwg-user-persist-ajax-action-nonce="<?php echo $user_persist_ajax_action_nonce; ?>"
     data-bwg-spider-chart-ajax-action="<?php echo $spider_chart_ajax_action; ?>"
     data-bwg-spider-chart-ajax-action-nonce="<?php echo $spider_chart_ajax_action_nonce; ?>">

    <div class="bwg-row">
        <div class="bwg-navigation-col">
			<?php if ( has_post_thumbnail( $post ) ) { ?>
                <div class="bwg-thumbnail-container">
					<?php echo get_the_post_thumbnail( $post, 'medium' ); ?>
                </div>
			<?php } ?>

            <ul class="bwg-navigation">
				<?php foreach ( $evaluation_definition->get_items() as $level1 ) { ?>
                    <li>
                        <a href="#bwg-<?php echo $level1->get_id(); ?>"><?php echo esc_html( $level1->get_label() ); ?></a>
                        <ul>
							<?php foreach ( $level1->get_items() as $level1_item ) { ?>
                                <li>
                                    <a href="#bwg-<?php echo $level1_item->get_id(); ?>">
										<?php echo esc_html( $level1_item->get_label() ); ?>
                                    </a>
                                </li>
							<?php } ?>
                            <li>
                                <a href="#bwg-spider-<?php echo $level1->get_id(); ?>">
									<?php _e( 'Auswertung', 'bwg' ); ?>
                                </a>
                            </li>
                        </ul>
                    </li>
				<?php } ?>
                <li>
                    <a href="#bwg-overview"><?php _e( 'Übersicht', 'bwg' ); ?></a>
                </li>
                <li>
                    <a href="#bwg-final"><?php _e( 'Abschluss', 'bwg' ); ?></a>
                </li>
            </ul>
        </div>
        <div class="bwg-content-col">
            <div class="bwg-progress-container">
                <div class="bwg-progress"></div>
            </div>

            <div class="bwg-headline">
                <h1 data-bwg-headline></h1>
            </div>

			<?php
			///
			// Level 0 : Interest.
			///
			foreach ( $evaluation_definition->get_items() as $level1 ) {
				$description = $level1->get_description( $placeholder_title );
				?>

                <div class="bwg-area" data-bwg-interest="<?php echo $level1->get_id(); ?>"
                     id="bwg-<?php echo $level1->get_id(); ?>" style="display: none;">
					<?php if ( ! empty( $description ) ) { ?>
                        <div class="bwg-description"><?php echo $description; ?></div>
					<?php } ?>
                </div>

				<?php
				if ( $level1->has_items() ) {
					///
					// Level 1 : Category.
					///
					foreach ( $level1->get_items() as $level1_item ) {
						$description     = $level1_item->get_description( $placeholder_title );
						$level1_item_key = '#' . $level1_item->get_id();

						?>
                    <div class="bwg-area" data-bwg-category="<?php echo $level1_item->get_id(); ?>"
                         id="bwg-<?php echo $level1_item->get_id(); ?>" style="display: none;">
						<?php
						if ( ! empty( $description ) ) {
							?>
                            <div class="bwg-description"><?php echo $description; ?></div>
							<?php
						}

						if ( $level1_item->has_items() ) {
							///
							// Level 2 : Statement (grading).
							///
							foreach ( $level1_item->get_items() as $level2_item ) {
								$description     = $level2_item->get_description( $placeholder_title );
								$details         = $level2_item->get_details();
								$level2_item_key = '#' . $level2_item->get_id();
								$grading_name    = 'bwg-grading-' . $level2_item->get_id();

								$details_dom_id = 'bwg-' . $level2_item->get_id() . '-details';
								if ( ! empty( $details ) ) {
									// There are details, so we need to search for the placement of the "info-button"
									// inside the description.
									$info_button = ' <span class="info-button" data-action="toggle-description" data-action-target="' . $details_dom_id . '">i</span>';

									$p2 = strripos( $description, '</p>' );
									if ( $p2 !== FALSE ) {
										$tmp = substr( $description, 0, $p2 );
										$tmp .= $info_button;
										$tmp .= substr( $description, $p2 );

										$description = $tmp;
									} else {
										$description .= $info_button;
									}
								}
								?>
                            <div id="bwg-<?php echo $level2_item->get_id(); ?>"
                                 data-bwg-criteria="<?php echo $level2_item->get_id(); ?>"
                                 data-bwg-weight="<?php printf( '%.02f', $level2_item->get_weight() ); ?>"
                                 data-bwg-label="<?php esc_attr_e( $level2_item->get_label() ); ?>"
                                 class="bwg-level2-container">

                                <div class="bwg-statement"><?php echo $description; ?></div>
								<?php if ( ! empty( $details ) ) { ?>
                                    <div id="<?php echo $details_dom_id; ?>"
                                         class="bwg-statement-details" style="display:none;">
                                        <div><?php
											echo $details; ?></div>
                                    </div>
								<?php } ?>

								<?php

								// Level 2 Grading.
								echo bwg_base()
									->utils()
									->render_template( 'evaluation-form/grading.php', [
										'grading'      => $grading,
										'grading_id'   => $level2_item->get_id(),
										'grading_name' => $grading_name,
										'value'        => isset( $user_grading[ $level2_item_key ] ) ? $user_grading[ $level2_item_key ] : 0,
									] );
								?>
                                </div><?php
							}
						}
						?>
                        <div class="bwg-level2-note-container">
                            <div id="bwg-note-<?php echo $level1_item->get_id(); ?>-container"
                                 class="bwg-note-container">
                                <label for="bwg-note-<?php echo $level1_item->get_id(); ?>">
									<?php
									if ( $bwg_mode === 'drogothek' ) {
										_e( 'Notieren Sie hier, welche konkreten Verbesserungsmassnahmen Sie umsetzen wollen.',
											'bwg' );
									} else {
										_e( 'Wenn Sie möchten, können Sie hier Ihre Bewertung kommentieren und Wünsche formulieren.',
											'bwg' );
									}
									?>
                                </label>
                                <textarea id="bwg-note-<?php echo $level1_item->get_id(); ?>"
                                          name="bwg_note_<?php echo $level1_item->get_id(); ?>"
                                          maxlength="1000"
                                          style="width:100%;"
                                          data-bwg-change="persist"><?php
									echo isset( $user_notes[ $level1_item_key ] ) ?
										$user_notes[ $level1_item_key ]['value'] : ''
									?></textarea>
                                <div class="bwg-remaining-chars">
									<?php
									/* translators: %1$s: Number of remaining chars. */
									printf( __( 'verbleibende Anzahl Zeichen: %1$s', 'bwg' ),
										'<span data-remaining-chars></span>' );
									?>
                                </div>
                            </div>
                        </div>
                        </div><?php
					}
				}
				?>

                <div class="bwg-area bwg-loading" data-bwg-spider="<?php echo $level1->get_id(); ?>"
                     id="bwg-spider-<?php echo $level1->get_id() ?>" style="display:none;">

					<?php echo $spider_chart_intro_text; ?>

                    <div>
                        <div style="position:relative; width:100%; padding-bottom:<?php echo $spider_chart_padding_ratio; ?>%;">
                            <div class="bwg-loading-visible"
                                 style="position:absolute; top:0;left:0;bottom:0;right:0;">
                                <svg class="bwg-loading-icon">
                                    <use xlink:href="#bwg-icon-loader"></use>
                                </svg>
                            </div>
                            <div id="bwg-spider-<?php echo $level1->get_id() ?>-container"
                                 style="position:absolute;top:0;right:0;bottom:0;left:0;"
                                 class="bwg-normal-visible"></div>
                        </div>
                    </div>

                </div>

				<?php
			}
			?>

            <div class="bwg-area" id="bwg-overview" style="display: none;">
				<?php echo $spider_chart_final_intro_text; ?>

                <div class="bwg-final-spiders-container">
					<?php
					///
					// Level 0 : Interest.
					///
					foreach ( $evaluation_definition->get_items() as $level1 ) {
						?>
                        <div id="bwg-final-spider-<?php echo $level1->get_id() ?>"
                             class="bwg-loading bwg-final-spider-wrapper">

                            <div>
                                <div style="position:relative;width:100%;padding-bottom:<?php echo $spider_chart_padding_ratio; ?>%;">
                                    <div class="bwg-loading-visible"
                                         style="position:absolute;top:0;left:0;bottom:0;right:0;">
                                        <svg class="bwg-loading-icon">
                                            <use xlink:href="#bwg-icon-loader"></use>
                                        </svg>
                                    </div>
                                    <div id="bwg-final-spider-<?php echo $level1->get_id() ?>-container"
                                         style="position:absolute;top:0;right:0;bottom:0;left:0;"
                                         class="bwg-normal-visible"></div>
                                </div>
                            </div>
                        </div>
						<?php
					}
					?>
                </div>
            </div>

            <div class="bwg-area" id="bwg-final" style="display: none;">
				<?php
				if ( ! empty( $profile_fields_introduction ) ) {
					echo $profile_fields_introduction;
				}

				foreach ( $profile_fields as $profile_field ) {
					echo $profile_field;
				}
				?>
            </div>

            <div class="bwg-control-button-bar">
                <a id="bwg-control-prev" data-action="goto-prev" class="bwg-button-big-fa"
                   title="<?php esc_attr_e( __( 'Previous', 'bwg' ) ); ?>" style="display: none;">
                    <i class="fa fa-arrow-left"></i>
                </a>

                <a id="bwg-control-next" data-action="goto-next" class="bwg-button-big-fa"
                   title="<?php esc_attr_e( __( 'Continue', 'bwg' ) ); ?>" style="display: none;">
                    <i class="fa fa-arrow-right"></i>
                </a>

                <a id="bwg-control-submit" data-action="submit" class="bwg-button-big-text"
                   title="<?php esc_attr_e( __( 'Bewertung abschicken', 'bwg' ) ); ?>" style="display: none;">
					<?php _e( 'Bewertung abschicken', 'bwg' ); ?>
                </a>

            </div>

        </div>
    </div>

</div>

<div id="bwg-layout">
    <div id="bwg-layout-xs">XS</div>
    <div id="bwg-layout-sm">SM</div>
    <div id="bwg-layout-md">MD</div>
    <div id="bwg-layout-lg">LG</div>
</div>

<?php
///
// Iterate over all categories and build the alert message in case the user would like to submit the form but
// did not fill all required gradings.
///
$categories = [];
foreach ( $evaluation_definition->get_items() as $level1 ) {
	array_push( $categories, $level1->get_label() );
}

$categories_text = '';
for ( $i = 0; $i < count( $categories ); $i ++ ) {
	if ( $i > 0 ) {
		if ( count( $categories ) - 1 === $i ) {
			$categories_text .= ' ' . __( 'oder', 'bwg' ) . ' ';
		} else {
			$categories_text .= ', ';
		}
	}

	$categories_text .= '«' . $categories[ $i ] . '»';
}
?>
<div id="bwg-required-alert-message" style="display:none;"><?php
	_e( 'Bitte füllen Sie alle Bewertungen aus. Fragen, die Sie noch nicht beantwortet haben, werden im Menü links schwarz (statt grün) angezeigt.',
		'bwg' );
	echo ' ';
	printf(
		_n(
			'Welche Fragen noch offen sind finden Sie am einfachsten heraus, wenn Sie auf die Kategorie %s klicken.',
			'Welche Fragen noch offen sind finden Sie am einfachsten heraus, wenn Sie auf die Kategorien %s klicken.',
			count( $categories ),
			'bwg'
		),
		$categories_text
	);
	?></div>
