<?php
/**
 * @var array $non_submitted
 * @var array $submitted
 */

?>

<?php if ( empty( $non_submitted ) && empty( $submitted ) ) { ?>
    <p><?php
		_e( 'Wir haben keine Bewertung für Sie gefunden. Bitte geben Sie die Internetadresse (URL) einer Beyond-Wild-Guess Bewertung in die Adresszeile Ihres Browsers ein.',
			'bwg' );
		?></p>
<?php } else { ?>

	<?php if ( ! empty( $non_submitted ) ) { ?>
        <h2><?php
			_e( 'Offene Bewertungen', 'bwg' );
			?></h2>
        <ul class="bwg-overview-list">
			<?php foreach ( $non_submitted as $item ) { ?>
                <li>
                    <div class="bwg-overview-thumbnail">
						<?php echo get_the_post_thumbnail( $item['post_ID'] ); ?>
                    </div>
                    <div class="bwg-overview-details">
                        <h5><?php esc_html_e( get_the_title( $item['post_ID'] ) ); ?></h5>

                        <div>
							<?php if ( ! empty( $item['rating_open_from'] ) && ! empty( $item['rating_open_to'] ) ) {
								echo $item['rating_open_from'] . ' &ndash; ' . $item['rating_open_to'];
							} else if ( ! empty( $item['rating_open_from'] ) ) {
								/* translators: %1$s: The date. */
								printf( __( 'Bewertungen möglich ab %1$s', 'bwg' ),
									esc_html( $item['rating_open_from'] ) );
							} else if ( ! empty( $item['rating_open_to'] ) ) {
								/* translators: %1$s: The date. */
								printf( __( 'Bewertungen möglich bis %1$s', 'bwg' ),
									esc_html( $item['rating_open_to'] ) );
							} ?>
                        </div>

						<?php if ( isset( $item['permalink'] ) ) { ?>
                            <div><a href="<?php esc_attr_e( $item['permalink'] ); ?>"><?php
									_e( 'Jetzt bewerten', 'bwg' );
									?></a></div>
						<?php } ?>
                    </div>
                </li>
			<?php } ?>
        </ul>
	<?php } ?>

	<?php if ( ! empty( $submitted ) ) { ?>
        <h2><?php
			_e( 'Abgeschlossene Bewertungen', 'bwg' );
			?></h2>
        <ul class="bwg-overview-list">
			<?php foreach ( $submitted as $item ) { ?>
                <li>
                    <div class="bwg-overview-thumbnail">
						<?php echo get_the_post_thumbnail( $item['post_ID'] ); ?>
                    </div>
                    <div class="bwg-overview-details">
                        <h5><?php esc_html_e( get_the_title( $item['post_ID'] ) ); ?></h5>

                        <div><a href="<?php esc_attr_e( $item['permalink'] ); ?>"><?php
								_e( 'Jetzt bewerten', 'bwg' );
								?></a></div>
                    </div>
                </li>
			<?php } ?>
        </ul>
	<?php } ?>

<?php } ?>
