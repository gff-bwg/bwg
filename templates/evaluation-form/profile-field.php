<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid The unique id of the profile field.
 * @var string $type The type (name) of the profile field.
 * @var string $field_info (html) The field info usually contains the <label>-tag.
 * @var string $field_control (html) The field control.
 *
 */

?>

<div class="bwg-form-field"
     data-epf="<?php echo esc_attr( $uid ); ?>"
     data-epf-type="<?php echo esc_attr( $type ); ?>">
    <div class="bwg-form-field-info">
		<?php echo $field_info; ?>
    </div>
    <div class="bwg-form-field-control">
		<?php echo $field_control; ?>
    </div>
</div>
