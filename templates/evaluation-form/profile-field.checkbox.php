<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid
 * @var string $label
 * @var \bwg\profile\fields\BWG_Profile_Field_Options_Checkbox $options
 * @var \bwg\profile\fields\BWG_Profile_Field_Type $field_type
 * @var string|array $field_value
 */

$checkbox_name       = str_replace( '-', '_', $uid );
$field_value_checked = explode( '|', trim( $field_value, '|' ) );

?>
<ul class="bwg-form-checkbox-vertical">
	<?php
	foreach ( $options->get_checkboxes() as $checkbox_value => $checkbox_label ) {
		$checkbox_id = 'epf-' . $uid . '-' . $checkbox_value;
		?>
        <li>
            <input id="<?php echo esc_attr( $checkbox_id ); ?>"
                   type="checkbox"
                   name="<?php echo esc_attr( $checkbox_name . '_' . $checkbox_value ); ?>"
                   value="<?php echo esc_attr( $checkbox_value ); ?>"<?php
			if ( in_array( $checkbox_value, $field_value_checked ) ) {
				echo 'checked';
			}
			?>
                   data-bwg-change="persist">
            <label for="<?php echo esc_attr( $checkbox_id ); ?>"><?php
				echo esc_html( $checkbox_label );
				?></label>
        </li>
		<?php
	}
	?>
</ul>
