<?php
/**
 * Variables
 * ---------
 *
 * @var \bwg\evaluation\BWG_Evaluation_Grading[] $grading The grading options.
 * @var int $grading_id The grading id.
 * @var string $grading_name The input field name for this grading.
 * @var mixed $value The current value.
 */

?>

<div class="bwg-grading">
    <ul data-bwg-grading="<?php echo $grading_id; ?>">
		<?php for ( $i = 0; $i < count( $grading ); $i ++ ) { ?>
            <li>
                <input id="<?php echo $grading_name; ?>_<?php echo $i; ?>"
                       name="<?php echo $grading_name; ?>"
                       value="<?php echo $grading[ $i ]->points; ?>"
					<?php if ( isset( $value ) && $value == $grading[ $i ]->points ) { ?>
                        checked="checked"
					<?php } ?>
                       type="radio"><label
                        for="<?php echo $grading_name; ?>_<?php echo $i; ?>"><?php echo $grading[ $i ]->label; ?></label>
            </li>
		<?php } ?>
    </ul>
</div>
