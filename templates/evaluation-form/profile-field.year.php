<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid
 * @var string $label
 * @var boolean $required
 * @var \bwg\profile\fields\BWG_Profile_Field_Options_Year $options
 * @var \bwg\profile\fields\BWG_Profile_Field_Type $field_type
 * @var string|array $field_value
 */

$year_start    = $options->get_year_start();
$year_end      = $options->get_year_end();
$year_selected = intval( $field_value );

?>

<select id="epf-<?php echo esc_attr( $uid ); ?>"
        class="bwg-form-control"
        data-bwg-change="persist"
	<?php
	if ( $required ) {
		echo ' required';
	}
	?>>
	<?php
	if ( ! $required ) {
		echo '<option value="-1">' . esc_html__( 'Keine Angabe', 'bwg' ) . '</option>';
	}

	for ( $year = $year_end; $year >= $year_start; $year -- ) {
		echo '<option value="' . $year . '"';
		if ( $year_selected === $year ) {
			echo ' selected';
		}
		echo '>' . $year . '</option>';
	}
	?>
</select>
