<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid
 * @var string $label
 * @var boolean $required
 * @var \bwg\profile\fields\BWG_Profile_Field_Options_Text $options
 * @var \bwg\profile\fields\BWG_Profile_Field_Type $field_type
 * @var string|array $field_value
 */

use bwg\profile\fields\BWG_Profile_Field_Options_Text;

if ( $options->is_mode( BWG_Profile_Field_Options_Text::MODE_TEXTAREA ) ) {
	?>
    <textarea id="epf-<?php echo esc_attr( $uid ); ?>"
              class="bwg-form-control"
              maxlength="<?php echo esc_attr( $options->get_max_length() ); ?>"
		<?php echo $required ? 'required' : ''; ?>
              data-bwg-change="persist"><?php echo esc_html( $field_value ); ?></textarea>
    <div class="bwg-remaining-chars">
		<?php
		/* translators: %1$s: Number of remaining chars. */
		printf( __( 'verbleibende Anzahl Zeichen: %1$s', 'bwg' ),
			'<span data-remaining-chars></span>' );
		?>
    </div>
	<?php
} else {
	switch ( $options->get_mode() ) {
		case BWG_Profile_Field_Options_Text::MODE_INPUT_NUMBER:
			$type = 'number';
			break;

		case BWG_Profile_Field_Options_Text::MODE_INPUT_EMAIL:
			$type = 'email';
			break;

		case BWG_Profile_Field_Options_Text::MODE_INPUT_TEL:
			$type = 'tel';
			break;

		default:
			$type = 'text';
			break;
	}
	?>
    <input type="<?php echo esc_attr( $type ); ?>"
           id="epf-<?php echo esc_attr( $uid ); ?>"
           class="bwg-form-control"
           value="<?php echo esc_attr( $field_value ); ?>"
           maxlength="<?php echo esc_attr( $options->get_max_length() ); ?>"
		<?php echo $required ? 'required' : ''; ?>
           data-bwg-change="persist">
	<?php
}
