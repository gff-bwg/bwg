<?php
/**
 * Variables
 * ---------
 *
 * @var string $uid
 * @var string $label
 * @var \bwg\profile\fields\BWG_Profile_Field_Options_Choice $options
 * @var \bwg\profile\fields\BWG_Profile_Field_Type $field_type
 * @var string|array $field_value
 */

use bwg\profile\fields\BWG_Profile_Field_Options_Choice;

$default_choice = $options->get_default_choice();

if ( $options->is_mode( BWG_Profile_Field_Options_Choice::MODE_SELECT ) ) {
	?>
    <select id="epf-<?php echo esc_attr( $uid ); ?>"
            class="bwg-form-control"
            data-bwg-change="persist">
		<?php
		foreach ( $options->get_choices() as $choice_value => $choice_label ) {
			?>
            <option value="<?php echo esc_attr( $choice_value ); ?>"<?php
			if ( ( is_null( $field_value ) && $default_choice === $choice_value ) || $field_value === $choice_value ) {
				echo ' selected';
			} ?>><?php
			echo esc_html( $choice_label );
			?></option><?php
		}
		?>
    </select>
	<?php
} else {
	$choice_name = str_replace( '-', '_', $uid );
	?>
    <ul class="<?php echo $options->is_mode( BWG_Profile_Field_Options_Choice::MODE_INPUT_RADIO ) ?
		'bwg-form-radio-vertical' : 'bwg-form-radio-horizontal'; ?>">
		<?php
		foreach ( $options->get_choices() as $choice_value => $choice_label ) {
			$choice_id = 'epf-' . $uid . '-' . $choice_value;
			?>
            <li>
                <input id="<?php echo esc_attr( $choice_id ); ?>"
                       type="radio"
                       name="<?php echo esc_attr( $choice_name ); ?>"
                       value="<?php echo esc_attr( $choice_value ); ?>"<?php
				if ( ( is_null( $field_value ) && $default_choice === $choice_value ) || $field_value === $choice_value ) {
					echo 'checked';
				}
				?>
                       data-bwg-change="persist">
                <label for="<?php echo esc_attr( $choice_id ); ?>"><?php
					echo esc_html( $choice_label );
					?></label>
            </li>
			<?php
		}
		?>
    </ul>
	<?php
}
