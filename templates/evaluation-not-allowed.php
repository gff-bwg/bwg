<?php
/**
 * @var \WP_Post $post
 */
?>

<div id="bwg-e">
    <div class="bwg-row">
        <div class="bwg-navigation-col">
			<?php if ( has_post_thumbnail( $post ) ) { ?>
                <div class="bwg-thumbnail-container">
					<?php echo get_the_post_thumbnail( $post, 'medium' ); ?>
                </div>
			<?php } ?>
        </div>
        <div class="bwg-content-col">
            Sorry, you are not allowed here.
        </div>
    </div>
</div>
