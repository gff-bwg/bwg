<?php
/**
 * Variables
 * ---------
 *
 * @var string $content HTML content.
 * @var string $footer_subtext
 */
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body style="word-wrap:break-word; -webkit-nbsp-mode:space; -webkit-line-break:after-white-space;"
      text="#000000" bgcolor="#ffffff">

<?php echo $content; ?>

<p>
    <font size="-2"><b>beyond <font color="#ff9000">wild</font> guess</b><?php
		echo ' ' . __( 'ist eine Dienstleistung der', 'bwg' );
		echo '<br>';
		echo esc_html( $footer_subtext );
		?>
    </font>
</p>
</body>
</html>