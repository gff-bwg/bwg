<?php

/**
 * @var string $pre_content
 * @var string $register_redirect_page
 * @var int $register_evaluation_field_id
 * @var int $register_evaluation_id
 */

wp_enqueue_style( 'bwg-front' );
wp_add_inline_style( 'bwg-front', '#ewd-feup-field-' . $register_evaluation_field_id . ' { display: none; }' );
?>

<?php echo $pre_content; ?>

<div class="bwg-register-container">
    [register redirect_page="<?php esc_attr_e( $register_redirect_page ); ?>"]
    <script>
        var element = document.getElementById("ewd-feup-register-input-<?php echo $register_evaluation_field_id; ?>");
        if (null !== element) {
            element.value = "<?php echo $register_evaluation_id; ?>";
        }
    </script>
</div>
