<?php
/**
 * Variables
 * ---------
 *
 * @var string $ajax_url
 * @var string $ajax_action
 * @var string $ajax_action_nonce
 * @var int $evaluation_id
 * @var \bwg\evaluation\BWG_Evaluation_Grading $grading
 * @var array $question1 The question1 field. Keys are 'label' and 'value'.
 * @var array $question2 The question2 field. Keys are 'label' and 'value'.
 * @var array $question3 The question3 field. Keys are 'label' and 'value'.
 * @var array $freetext1 The freetext1 field. Keys are 'label' and 'value'.
 */

?>

<form id="bwg-prodomo-form" data-bwg-ajax-url="<?php echo $ajax_url; ?>">
    <input type="hidden" name="action" value="<?php esc_attr_e( $ajax_action ); ?>">
    <input type="hidden" name="_ajax_nonce" value="<?php esc_attr_e( $ajax_action_nonce ); ?>">
    <input type="hidden" name="evaluation_id" value="<?php esc_attr_e( $evaluation_id ); ?>">

    <div class="bwg-prodomo-form-item">
		<?php
		echo wpautop( $question1['label'] );
		echo bwg_base()->utils()
		               ->render_template( 'evaluation-form/grading.php',
			               [
				               'grading'      => $grading,
				               'grading_id'   => 'prodomo_question1',
				               'grading_name' => 'prodomo_question1',
				               'value'        => $question1['value'],
			               ] );
		?>
    </div>

    <div class="bwg-prodomo-form-item">
		<?php
		echo wpautop( $question2['label'] );
		echo bwg_base()->utils()
		               ->render_template( 'evaluation-form/grading.php',
			               [
				               'grading'      => $grading,
				               'grading_id'   => 'prodomo_question2',
				               'grading_name' => 'prodomo_question2',
				               'value'        => $question2['value'],
			               ] );
		?>
    </div>

    <div class="bwg-prodomo-form-item">
		<?php
		echo wpautop( $question3['label'] );
		echo bwg_base()->utils()
		               ->render_template( 'evaluation-form/grading.php',
			               [
				               'grading'      => $grading,
				               'grading_id'   => 'prodomo_question3',
				               'grading_name' => 'prodomo_question3',
				               'value'        => $question3['value'],
			               ] );
		?>
    </div>

    <div class="bwg-prodomo-form-item">
		<?php echo wpautop( $freetext1['label'] ); ?>
        <textarea title="" name="prodomo_freetext1"><?php echo $freetext1['value']; ?></textarea>
    </div>

    <div class="bwg-prodomo-form-item">
        <a data-bwg-action="submit" class="bwg-button-big-text"
           title="<?php esc_attr_e( __( 'Abschicken', 'bwg' ) ); ?>">
			<?php _e( 'Abschicken', 'bwg' ); ?>
        </a>
    </div>
</form>