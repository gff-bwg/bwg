<?php
/**
 * @var string $evaluation_title The title of the evaluation the login is for. Empty if no evaluation ist defined.
 * @var string $login_redirect_url The url to redirect to.
 */

?>
<div class="bwg-login-container">
    [login<?php echo( ! empty( $login_redirect_url ) ? ' redirect_page="' . $login_redirect_url . '"' : '' ); ?>]
</div>