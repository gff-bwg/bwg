# BWG

The WordPress Plugin **BWG**.

## Dependencies

* [BWG Vendor](https://gitlab.com/gff-bwg/bwg-vendor)

## Authors and acknowledgment

Adrian Suter, [adrian.suter@gff.ch](mailto:adrian.suter@gff.ch)

## Copyright

[GFF Integrative Kommunikation GmbH](https://www.gff.ch/)

## License

[GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

## Development

### Staging

Always use a staging environment to develop and test things out before releasing.

### Build a new release

1. Set the new version `X.Y` in the file comments section of `bwg.php`. Commit and push.
2. Add a new repository tag `X.Y` without writing release notes (a message is allowed).
3. Gitlab CI would automatically create a release asset zip file.
4. Once finished (see pipeline or release section), update the stable version in
   `readme.txt` to `X.Y` and commit and push.
