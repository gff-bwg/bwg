Hooks
=====

**bwg_render_template_pre**  
**bwg_render_admin_template_pre**  

Action fired right before we gonna locate a template path (for example from the theme).

*Parameters*
- `$default_template_path` [string] Note that in case of admin, this would begin with 'admin/'
- `$variables` [array]

---

**bwg_template_path**  
**bwg_admin_template_path**

Filter fired right after we have located the template path.

*Parameter*
- `$template_path` [string]

---

**bwg_template_content**
**bwg_admin_template_content**

Filter fired right after the template code 

*Parameters*
- `$template_code` [string]
- `$default_template_path` [string]
- `$template_path` [string]
- `$variables` [array]

---

**bwg_render_template_after**
**bwg_render_admin_template_after**

Action right after the template has been rendered.

*Parameters*
- `$default_template_path` [string]
- `$variables` [array]
- `$template_path` [string]
- `$template_content` [string]

---
