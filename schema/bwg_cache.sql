CREATE TABLE __TABLENAME__ (
  cache_key       VARCHAR(128) NOT NULL,
  cache_value     LONGTEXT     NOT NULL,
  created         DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:00',
  expire          DATETIME              DEFAULT NULL,
  clearable       INT(1)       NOT NULL DEFAULT '1',
  jsonified       INT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY  (cache_key)
)