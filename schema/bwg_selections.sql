CREATE TABLE __TABLENAME__ (
  analysis_ID     BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
  user_ID         MEDIUMINT(9)        NOT NULL DEFAULT '0',
  filter1         INT(2)              DEFAULT NULL,
  filter2         INT(2)              DEFAULT NULL,
  filter3         INT(2)              DEFAULT NULL,
  filter4         INT(2)              DEFAULT NULL,
  PRIMARY KEY  (analysis_ID, user_ID)
)