CREATE TABLE __TABLENAME__ (
  ID              BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  post_ID         BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
  label           TEXT                NOT NULL,
  filters         LONGTEXT            NOT NULL,
  created         DATETIME            NOT NULL DEFAULT '0000-00-00 00:00:00',
  modified        DATETIME            NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY  (ID),
  INDEX  (post_ID)
)