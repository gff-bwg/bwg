CREATE TABLE __TABLENAME__ (
  post_ID             BIGINT(20) UNSIGNED    NOT NULL DEFAULT '0',
  user_ID             MEDIUMINT(9)           NOT NULL DEFAULT '0',
  session_ID          INT(10) UNSIGNED       NOT NULL DEFAULT '0',
  entries             LONGBLOB               DEFAULT NULL,
  time_active         INT(10) UNSIGNED       NOT NULL DEFAULT '0',
  time_inactive       INT(10) UNSIGNED       NOT NULL DEFAULT '0',
  nr_actions_goto     INT(10) UNSIGNED       NOT NULL DEFAULT '0',
  nr_actions_vote     INT(10) UNSIGNED       NOT NULL DEFAULT '0',
  nr_actions_sleep    INT(10) UNSIGNED       NOT NULL DEFAULT '0',
  nr_actions_wake_up  INT(10) UNSIGNED       NOT NULL DEFAULT '0',
  created             DATETIME               NOT NULL DEFAULT '0000-00-00 00:00:00',
  modified            DATETIME               NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY  (post_ID, user_ID, session_ID)
)