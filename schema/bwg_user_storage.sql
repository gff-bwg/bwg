CREATE TABLE __TABLENAME__ (
  post_ID         BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
  user_ID         MEDIUMINT(9)        NOT NULL DEFAULT '0',
  sequence_number BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
  grading         LONGTEXT            NOT NULL,
  notes           LONGTEXT            NOT NULL,
  meta            LONGTEXT            NOT NULL,
  submitted       DATETIME            DEFAULT NULL,
  created         DATETIME            NOT NULL DEFAULT '0000-00-00 00:00:00',
  modified        DATETIME            NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY  (post_ID, user_ID)
)