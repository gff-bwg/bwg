CREATE TABLE __TABLENAME__ (
  post_ID         BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
  user_ID         MEDIUMINT(9)        NOT NULL DEFAULT '0',
  question1       INT(9)              DEFAULT NULL,
  question2       INT(9)              DEFAULT NULL,
  question3       INT(9)              DEFAULT NULL,
  freetext1       LONGTEXT            DEFAULT NULL,
  created         DATETIME            NOT NULL DEFAULT '0000-00-00 00:00:00',
  modified        DATETIME            NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY  (post_ID, user_ID)
)