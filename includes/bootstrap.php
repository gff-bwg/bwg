<?php

use bwg\BWG_Base;
use bwg\BWG_Factory;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Gets the current BWG base instance. If there is currently no base instance, this function would instantiate one.
 *
 * @param BWG_Factory|null $bwg_factory The BWG factory to use. If given, this would force the function to instantiate a
 * new bwg base instance.
 *
 * @return BWG_Base The current bwg base instance.
 */
function bwg_base( BWG_Factory $bwg_factory = NULL ): BWG_Base {
	static $bwg_base = NULL;

	if ( is_null( $bwg_base ) || ! is_null( $bwg_factory ) ) {
		if ( is_null( $bwg_factory ) ) {
			$bwg_factory = new BWG_Factory();
		}

		$bwg_base = new BWG_Base( $bwg_factory );
	}

	return $bwg_base;
}

////////////////////////////// PHP CLASS AUTOLOAD //////////////////////////////

/**
 * The autoload function for BWG classes.
 *
 * Note that the namespaces for all BWG classes should never be prefixed by \.
 *
 * @param string $class_fqn Fully Qualified Name of the class to be loaded.
 */
function bwg_autoload( string $class_fqn ) {
	static $base_path = BWG_ABS_PATH . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;

	// Get the first characters of the class fqn as they would be checked afterwards.
	$class_fqn_begin = substr( $class_fqn, 0, 4 );
	if ( 'bwg\\' !== $class_fqn_begin && 'BWG_' !== $class_fqn_begin ) {
		return;
	}

	$p = strripos( $class_fqn, '\\' );
	if ( FALSE === $p ) {
		$namespace  = '';
		$class_name = $class_fqn;
	} else {
		$namespace  = substr( $class_fqn, 0, $p );
		$class_name = substr( $class_fqn, $p + 1 );
	}

	$class_file = 'class-' . str_replace( '_', '-', strtolower( $class_name ) ) . '.php';
	$class_path = $base_path . ( ! empty( $namespace ) ? str_replace( '\\', DIRECTORY_SEPARATOR,
				$namespace ) . DIRECTORY_SEPARATOR : '' ) . $class_file;

	if ( is_file( $class_path ) ) {
		include_once( $class_path );
	} else {
		trigger_error( 'BWG class <b>' . $class_fqn . '</b> file not found.', E_USER_ERROR );
	}
}

spl_autoload_register( 'bwg_autoload' );

///////////////////// ACTIVATION / DEACTIVATION / UNINSTALL ////////////////////

register_activation_hook( BWG_ABS_PLUGIN_FILE, [ 'bwg\\BWG_System', 'activate' ] );
register_deactivation_hook( BWG_ABS_PLUGIN_FILE, [ 'bwg\\BWG_System', 'deactivate' ] );
register_uninstall_hook( BWG_ABS_PLUGIN_FILE, [ 'bwg\\BWG_System', 'uninstall' ] );

//////////////////////////// BWG-BASE / BWG-FACTORY ////////////////////////////

bwg_base( new BWG_Factory() );

///////////////////////////// PLUGIN UPDATE CHECKER ////////////////////////////

add_action( 'plugins_loaded', function () {
	$group_name     = 'gff-bwg';
	$plugin_name    = basename( BWG_ABS_PLUGIN_FILE, '.php' );
	$update_checker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/' . $group_name . '/' . $plugin_name . '/',
		BWG_ABS_PLUGIN_FILE
	);

	/** @var Puc_v4p11_Vcs_GitLabApi $vcs_api */
	$vcs_api = $update_checker->getVcsApi();
	$vcs_api->enableReleasePackages();
} );
