<?php

namespace bwg\mail;

use bwg\BWG_Base;
use bwg\pdf\BWG_User_Pdf;
use FEUP_User;

/**
 * Class BWG_User_Evaluation_Confirm_Mail.
 *
 * @package bwg\mail
 */
class BWG_User_Evaluation_Confirm_Mail {

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_User_Evaluation_Confirm_Mail constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;
	}

	/**
	 * @param int $post_ID
	 * @param int $user_ID
	 * @param null|string $content
	 * @param bool $attach_pdf
	 * @param null $receiver
	 * @param null $post_title
	 *
	 * @return bool
	 *
	 * @throws \Exception
	 */
	public function build_and_send(
		$post_ID,
		$user_ID,
		$content = NULL,
		$attach_pdf = TRUE,
		$receiver = NULL,
		$post_title = NULL
	) {
		if ( is_null( $content ) ) {
			$content = get_post_meta( $post_ID, 'bwg_em_user_confirm', TRUE );
		}

		if ( is_null( $receiver ) ) {
			if ( $this->_bwg_base->options()->get_option_bwg_mode() === 'drogothek' ) {
				// The receiver would be set in a profile field.
				$receiver = $this->get_receiver_from_profile_fields( $post_ID, $user_ID );

				if ( is_null( $receiver ) ) {
					return FALSE;
				}
			} else {
				$feup     = new FEUP_User();
				$username = $feup->Get_User_Name_For_ID( $user_ID );
				$receiver = $username;
			}
		}

		$replacements = $this->_bwg_base->feup_layer()->get_user_field_replacements( $user_ID );
		$content      = str_replace(
			array_keys( $replacements ),
			array_values( $replacements ),
			nl2br( $content )
		);

		$subject = sprintf(
		/* translators: %1$s: Title of evaluation. */
			__( '%1$s – Ihre Bewertung auf einen Blick', 'bwg' ),
			is_null( $post_title ) ? get_the_title( $post_ID ) : $post_title
		);

		$headers = [
			'Content-Type: text/html; charset=UTF-8',
		];

		$body = $this->_bwg_base->utils()->render_template( 'mail/simple-html.php',
			[
				'content'        => '<p>' . $content . '</p>',
				'footer_subtext' => $this->_bwg_base->options()->get_option_footer_text(),
			]
		);

		$attachments   = [];
		$pdf_file_path = NULL;
		if ( $attach_pdf ) {
			$private_cache_path = $this->_bwg_base->utils()->get_bwg_files_private_cache_path( TRUE );
			if ( FALSE === $private_cache_path ) {
				throw new \Exception( 'Oops, could not assert the bwg private cache files directory.' );
			}

			$evaluation_post = $this->_bwg_base->factory()->bwg_evaluation_post( get_post( $post_ID ) );

			if ( $this->_bwg_base->options()->get_option_bwg_mode() === 'drogothek' ) {
				$pdf_base_name = $evaluation_post->get_post()->post_title;
				$pdf_base_name = preg_replace( '/[^a-z0-9äöüèéà \.-]/i', '', $pdf_base_name );

				$pdf_file_dir = $private_cache_path . DIRECTORY_SEPARATOR . uniqid( 'drogothek', TRUE );
				if ( ! wp_mkdir_p( $pdf_file_dir ) ) {
					throw new \Exception( 'Oops, could not assert the bwg cache directory.' );
				}

				$pdf_file_path = $pdf_file_dir . DIRECTORY_SEPARATOR . $pdf_base_name . '.pdf';
			} else {
				$pdf_file_path = $private_cache_path . DIRECTORY_SEPARATOR . uniqid( 'Summary' ) . '.pdf';
			}


			$pdf = new BWG_User_Pdf( $this->_bwg_base, $evaluation_post, $user_ID );
			$pdf->build();
			$pdf->Output( $pdf_file_path, 'F' );
			$pdf->clear_private_cache_files();

			array_push( $attachments, $pdf_file_path );
		}

		$result = wp_mail( $receiver, $subject, $body, $headers, $attachments );

		if ( $attach_pdf && ! is_null( $pdf_file_path ) && file_exists( $pdf_file_path ) ) {
			wp_delete_file( $pdf_file_path );
			if ( isset( $pdf_file_dir ) && $this->_bwg_base->options()->get_option_bwg_mode() === 'drogothek' ) {
				$this->_bwg_base->utils()->clear_directory( $pdf_file_dir, 0 );
				@rmdir( $pdf_file_dir );
			}
		}

		return $result;
	}

	/**
	 * @param int $post_ID
	 * @param int $user_ID
	 *
	 * @return string|null
	 */
	protected function get_receiver_from_profile_fields( $post_ID, $user_ID ) {
		$evaluation_post = $this->_bwg_base->factory()->bwg_evaluation_post( get_post( $post_ID ) );
		$profile_fields  = $evaluation_post->get_profile_fields();
		$user_storage    = $this->_bwg_base->user_storage_database_helper()->get_user_storage( $post_ID, $user_ID );

		foreach ( $profile_fields->get_fields() as $profile_field ) {
			if ( $profile_field->get_type() !== 'text' ) {
				// The type has to be `text`.
				continue;
			}

			/** @var \bwg\profile\fields\BWG_Profile_Field_Options_Text $profile_field_options */
			$profile_field_options = $profile_field->get_field_options(
				$this->_bwg_base->profile_field_type_registry()
			);

			if ( ! $profile_field_options->is_mode( 'input_email' ) ) {
				// The mode has to be `input_email`.
				continue;
			}

			$user_storage_meta = $user_storage->get_meta();
			if ( array_key_exists( $profile_field->get_uid(), $user_storage_meta ) ) {
				return $user_storage_meta[ $profile_field->get_uid() ];
			}
		}

		return NULL;
	}

}