<?php

namespace bwg\pdf;

use bwg\BWG_Base;
use bwg\charts\BWG_Chart_Styles;
use bwg\charts\BWG_Spider_Chart;
use bwg\evaluation\BWG_Evaluation_Post;
use FEUP_User;

/**
 * Class BWG_User_Pdf.
 *
 * @package bwg\pdf
 */
class BWG_User_Pdf extends BWG_Pdf {

	/**
	 * @var \bwg\evaluation\BWG_Evaluation_Post
	 */
	private $_evaluation_post;

	/**
	 * @var int
	 */
	private $_user_ID;

	/**
	 * @var string
	 */
	private $_today;

	/**
	 * @var string
	 */
	private $_private_cache_path;

	/**
	 * @var array
	 */
	private $_private_cache_files = [];


	/**
	 * BWG_User_Pdf constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
	 * @param int $user_ID
	 * @param string $orientation
	 */
	public function __construct(
		BWG_Base $bwg_base,
		BWG_Evaluation_Post $evaluation_post,
		$user_ID,
		$orientation = 'P'
	) {
		parent::__construct( $bwg_base, $orientation );

		$this->_evaluation_post    = $evaluation_post;
		$this->_user_ID            = $user_ID;
		$this->_private_cache_path = $this->_bwg_base->utils()->get_bwg_files_private_cache_path( TRUE );

		if ( $this->_bwg_base->options()->get_option_bwg_mode() === 'drogothek' ) {
			$this->set_header_logo( BWG_Pdf::LOGO_3 );
		}

		if ( FALSE === $this->_private_cache_path ) {
			trigger_error( 'Oops, could not assert the bwg private cache files directory.' );
		}
	}

	/**
	 * Sets the today value.
	 *
	 * @param $today
	 */
	public function set_today( $today ) {
		$this->_today = $today;
	}

	/**
	 * Gets the 'today' value. If not set, then this method would return the current date as today.
	 *
	 * @return string
	 */
	public function get_today() {
		if ( is_null( $this->_today ) ) {
			return date_i18n( get_option( 'date_format' ) );
		}

		return $this->_today;
	}

	/**
	 * Builds the content of the pdf.
	 */
	public function build() {
		// Get the helpers we will need.
		$gradings_database_helper     = $this->_bwg_base->submissions_database_helper();
		$user_storage_database_helper = $this->_bwg_base->user_storage_database_helper();

		// Should we show the averages of all submitted gradings.
		$all_visible = $this->_bwg_base->evaluation_utils()->is_spider_all_visible( $this->_evaluation_post->get_id() );

		// Get the user gradings.
		$user_gradings = $gradings_database_helper->get_weight_gradings_by_user(
			$this->_evaluation_post->get_id(),
			$this->_evaluation_post->get_evaluation_definition(),
			$this->_user_ID
		);

		// Get the user storage.
		$user_storage = $user_storage_database_helper->get_user_storage(
			$this->_evaluation_post->get_id(),
			$this->_user_ID
		);

		// Get the average gradings of all submitted.
		$all_gradings = $gradings_database_helper->get_weight_gradings(
			$this->_evaluation_post->get_id(),
			$this->_evaluation_post->get_evaluation_definition()
		);

		// Add a page.
		$this->AddPage();

		// Set the font.
		$this->SetFont( 'Lato', '', 10 );

		$title_suffix = NULL;
		if ( $this->_bwg_base->options()->get_option_bwg_mode() === 'drogothek' ) {
			$feup         = new FEUP_User();
			$title_suffix = $feup->Get_Field_Value_For_ID( 'name', $this->_user_ID ) . ', ' .
			                $feup->Get_Field_Value_For_ID( 'ort', $this->_user_ID );
		}

		// Write the introduction.
		$this->writeHTML(
			$this->_bwg_base->utils()->render_template( 'pdf/evaluation-user/introduction.php', [
				'evaluation_title' => get_the_title( $this->_evaluation_post->get_post() ),
				'today'            => $this->get_today(),
				'others_visible'   => $all_visible,
				'title_suffix'     => $title_suffix,
			] ) );

		// Build and add the spider charts.
		$paper_margins = $this->getMargins();
		$chart_width   = ( 210 - $paper_margins['left'] - $paper_margins['right'] ) / 2;

		$charts = [];
		foreach ( $this->_evaluation_post->get_evaluation_definition()->get_items() as $item1 ) {
			$label1 = $item1->get_label( TRUE );

			$chart = new BWG_Spider_Chart( [
				'width_height_attr' => TRUE,
				'title'             => [
					'text'  => $label1,
					'style' => new BWG_Chart_Styles( [
						'font-size'   => '13pt',
						'font-family' => 'Lato',
					] ),
				],
				'spider'            => [
					'leg'    => [
						'label' => [
							'style' => new BWG_Chart_Styles( [
								'font-family' => 'Lato',
								'font-weight' => NULL,
							] ),
						],
					],
					'y_axis' => [
						'label' => [
							'offset' => 7,
						],
					],
				],
				'copyright'         => [
					'text' => '',
				],
			] );

			$series_data_all  = [];
			$series_data_user = [];
			foreach ( $item1->get_items() as $item2 ) {
				if ( 0 === $item2->get_spider_chart() ) {
					continue;
				}

				$label2 = $item2->get_label( FALSE );
				$chart->add_leg( $label2 );

				$key = '#' . $item2->get_id();
				array_push( $series_data_all, isset( $all_gradings[ $key ] ) ? $all_gradings[ $key ] : 1 );
				array_push( $series_data_user, isset( $user_gradings[ $key ] ) ? $user_gradings[ $key ] : 1 );
			}

			if ( $all_visible ) {
				$chart->add_series( $series_data_all,
					new BWG_Chart_Styles( [ 'fill' => '#508FBB' ] ),
					new BWG_Chart_Styles( [ 'fill' => '#508FBB', 'stroke' => 'none' ] ) );
			}
			$chart->add_series( $series_data_user );

			$spider_file_path = $this->_private_cache_path . '/' . wp_unique_filename( $this->_private_cache_path,
					uniqid() . '-' . $item1->get_id() . '.svg' );
			file_put_contents( $spider_file_path, $chart->get_svg() );

			array_push( $charts, $spider_file_path );
			array_push( $this->_private_cache_files, $spider_file_path );
		}

		$c_charts   = count( $charts );
		$chart_rows = floor( $c_charts / 2 );
		for ( $chart_row = 0; $chart_row < $chart_rows; $chart_row ++ ) {
			// Left.
			$this->ImageSVG( $charts[ $chart_row * 2 ], $paper_margins['left'], '', $chart_width, '', '', '', '', 0,
				FALSE );

			// Right.
			$this->ImageSVG( $charts[ $chart_row * 2 + 1 ], $paper_margins['left'] + $chart_width, '', $chart_width, '',
				'', '', '', 0, FALSE );

			$this->SetY( $this->getImageRBY() );
		}

		if ( $c_charts % 2 == 1 ) {
			// Center.
			$this->ImageSVG( $charts[ $c_charts - 1 ], $paper_margins['left'] + $chart_width / 2, '', $chart_width, '',
				'', '', '', 0, FALSE );

			$this->SetY( $this->getImageRBY() );
		}

		$this->SetX( $paper_margins['left'] );
		$this->writeHTML( '<p style="font-size:8pt;">&copy; &ndash; beyond-wild-guess.ch', TRUE, FALSE,
			TRUE, FALSE, 'C' );

		$this->writeHTML( '<p>&nbsp;</p>' );

		$user_note_counter = 0;
		foreach ( $user_storage->get_notes() as $key => $val ) {
			if ( empty( $val['value'] ) ) {
				continue;
			}

			$item_breadcrumb = $this->_evaluation_post->get_evaluation_definition()->find_item_breadcrumb(
				substr( $key, 1 )
			);
			if ( empty( $item_breadcrumb ) ) {
				continue;
			}

			if ( 0 === $user_note_counter ) {
				$this->writeHTML(
					'<p>&nbsp;</p><p>' .
					__( 'Zusätzlich zu Ihrer Bewertung haben Sie folgende <b>Kommentare</b> abgegeben:', 'bwg' ) .
					'</p>'
				);
			}

			$title_labels = [];
			foreach ( $item_breadcrumb as $item_breadcrumb_item ) {
				array_push( $title_labels, $item_breadcrumb_item->get_label() );
			}

			$this->writeHTML(
				$this->_bwg_base->utils()->render_template(
					'pdf/evaluation-user/user-note.php', [
						'title_labels' => $title_labels,
						'note'         => trim( $val['value'] ),
					]
				)
			);

			$user_note_counter ++;
		}

		if ( $user_note_counter > 0 && $this->_bwg_base->options()->get_option_bwg_mode() !== 'drogothek' ) {
			$this->writeHTML( '<p>&nbsp;</p><p>&nbsp;</p><p>' . __( 'Wir leiten diese Kommentare – selbstverständlich in anonymer Form – gerne an die Herausgeber der Publikation weiter – sie werden wertvolle Hinweise für die Verbesserung der Publikation liefern.',
					'bwg' ) . '</p>' );
		}
	}

	/**
	 * Clears the private cache files.
	 */
	public function clear_private_cache_files() {
		foreach ( $this->_private_cache_files as $private_cache_file ) {
			wp_delete_file( $private_cache_file );
		}
	}

}
