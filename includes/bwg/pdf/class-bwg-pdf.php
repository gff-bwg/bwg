<?php

namespace bwg\pdf;

use bwg\BWG_Base;

/**
 * Class BWG_Pdf.
 *
 * @package bwg\pdf
 */
class BWG_Pdf extends \TCPDF {

	/**
	 * Logo 1.
	 */
	const LOGO_1 = 'logo_bwg_rgb.svg';

	/**
	 * Logo 2.
	 */
	const LOGO_2 = 'logo_bwg_2_rgb.svg';

	/**
	 * Logo 3.
	 */
	const LOGO_3 = 'logo_bwg_3_rgb.svg';

	/**
	 * @var \bwg\BWG_Base
	 */
	protected $_bwg_base;

	/**
	 * @var string
	 */
	protected $_header_logo = self::LOGO_2;


	/**
	 * BWG_Pdf constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 * @param string $orientation
	 */
	public function __construct( BWG_Base $bwg_base, $orientation = 'P' ) {
		if ( ! defined( 'PDF_FONT_NAME_MAIN' ) ) {
			define( 'PDF_FONT_NAME_MAIN', 'Lato' );
		}

		parent::__construct( $orientation, 'mm', 'A4', TRUE, 'UTF-8', FALSE, FALSE );

		$this->_bwg_base = $bwg_base;

		$this->setHeaderMargin( 11 );
		$this->setFooterMargin( 27 );
		$this->SetMargins( 20, 11 + 20 + 20 );
		$this->SetAutoPageBreak( TRUE, 35 );

		$this->setHtmlVSpace( [
			'p' => [
				0 => [ 'h' => 0, 'n' => 0 ],
				1 => [ 'h' => 0, 'n' => 0 ]
			]
		] );
		$this->setCellHeightRatio( 1.5 );

		// Note: We define "Lato" as being the LatoLight font. Therefore the bold versions would actually be the
		// LatoRegular versions.
		$this->AddFont( 'Lato', '', BWG_ABS_PATH . '/tcpdf-fonts/latolight.php' );
		$this->AddFont( 'Lato', 'I', BWG_ABS_PATH . '/tcpdf-fonts/latolighti.php' );
		$this->AddFont( 'Lato', 'B', BWG_ABS_PATH . '/tcpdf-fonts/lato.php' );
		$this->AddFont( 'Lato', 'BI', BWG_ABS_PATH . '/tcpdf-fonts/latoi.php' );

		$this->AddFont( 'Arial', '', BWG_ABS_PATH . '/tcpdf-fonts/arial.php' );
		$this->AddFont( 'ArialRoundedMTBold', '', BWG_ABS_PATH . '/tcpdf-fonts/arialroundedmtb.php' );
	}

	/**
	 * Sets the header logo (a filename within the /assets/ directory).
	 *
	 * See also BWG_Pdf::LOGO_1 and BWG_Pdf::LOGO_2.
	 *
	 * @param $header_logo
	 */
	public function set_header_logo( $header_logo ) {
		$this->_header_logo = $header_logo;
	}

	/**
	 * @inheritdoc
	 */
	public function Header() {
		if ( FALSE === $this->header_xobjid ) {
			// Start a new XObject Template.
			$this->header_xobjid = $this->startTemplate( $this->w, $this->tMargin );
			$image_file          = BWG_ABS_PATH . '/assets/' . $this->_header_logo;
			$this->ImageSVG( $image_file, $this->original_lMargin, 0, '', 20, '', '', 'C' );
			$this->endTemplate();
		}

		// print header template.
		$this->printTemplate( $this->header_xobjid, 0, $this->getHeaderMargin(), 0, 0, '', '', FALSE );

		if ( $this->header_xobj_autoreset ) {
			// reset header xobject template at each page.
			$this->header_xobjid = FALSE;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function Footer() {
		$cur_y = $this->GetY();

		// Print page number.
		$this->SetFont( 'Lato', '', 10 );
		$this->SetX( $this->original_lMargin );
		$this->Cell( 0, 0, $this->getAliasRightShift() . $this->getAliasNumPage() . ' / ' .
		                   $this->getAliasNbPages() . $this->getAliasRightShift(), 0, 0, 'C' );

		$this->SetX( $this->original_lMargin );
		$this->SetY( $cur_y + 9 );

		$this->SetFont( 'ArialRoundedMTBold' );
		$this->writeHTML(
			$this->_bwg_base->utils()->render_template(
				'pdf/footer.php',
				[
					'footer_subtext' => $this->_bwg_base->options()->get_option_footer_text(),
				] ),
			FALSE, FALSE, FALSE, FALSE, 'C' );
	}
}