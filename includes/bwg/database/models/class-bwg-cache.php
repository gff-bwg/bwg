<?php

namespace bwg\database\models;

/**
 * Class BWG_Cache.
 *
 * @package bwg\database\models
 */
class BWG_Cache {

	/**
	 * @var string
	 */
	protected $_key;

	/**
	 * @var mixed
	 */
	protected $_value;

	/**
	 * @var \DateTime
	 */
	protected $_created;

	/**
	 * @var \DateTime|null
	 */
	protected $_expire = NULL;

	/**
	 * @var bool
	 */
	protected $_clearable = TRUE;

	/**
	 * @var bool
	 */
	protected $_jsonified = FALSE;
	

	/**
	 * BWG_Cache constructor.
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param \DateTime|null $expire
	 * @param bool $clearable
	 */
	public function __construct( $key, $value, $expire = NULL, $clearable = TRUE ) {
		$this->set_key( $key );
		$this->set_value( $value );
		$this->set_expire( $expire );
		$this->set_clearable( $clearable );
	}

	/**
	 * @return string
	 */
	public function get_key() {
		return $this->_key;
	}

	/**
	 * @param string $key
	 */
	public function set_key( $key ) {
		$this->_key = $key;
	}

	/**
	 * @return mixed
	 */
	public function get_value() {
		return $this->_value;
	}

	/**
	 * @param mixed $value
	 */
	public function set_value( $value ) {
		$this->_value = $value;

		$this->set_jsonified( is_array( $value ) || is_object( $value ) );
	}

	/**
	 * @return \DateTime
	 */
	public function get_created() {
		return $this->_created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function set_created( \DateTime $created ) {
		$this->_created = $created;
	}

	/**
	 * @return \DateTime|null
	 */
	public function get_expire() {
		return $this->_expire;
	}

	/**
	 * @param \DateTime|null $expire
	 */
	public function set_expire( $expire ) {
		$this->_expire = $expire;
	}

	/**
	 * @return bool
	 */
	public function is_expired() {
		$expire = $this->get_expire();

		if ( is_null( $expire ) ) {
			return FALSE;
		}

		$now = new \DateTime();

		return ( $now > $expire );
	}

	/**
	 * @return bool
	 */
	public function is_clearable() {
		return $this->_clearable;
	}

	/**
	 * @param bool $clearable
	 */
	public function set_clearable( $clearable ) {
		$this->_clearable = $clearable;
	}

	/**
	 * @return bool
	 */
	public function is_jsonified() {
		return $this->_jsonified;
	}

	/**
	 * @param bool $jsonified
	 */
	public function set_jsonified( $jsonified ) {
		$this->_jsonified = $jsonified;
	}


}
