<?php

namespace bwg\database\models;

use DateTime;

/**
 * Class BWG_User_Storage.
 *
 * @package bwg\database\models
 */
class BWG_User_Storage {

	/**
	 * @var int
	 */
	private $post_ID;

	/**
	 * @var int
	 */
	private $user_ID;

	/**
	 * @var array
	 */
	private $_grading;

	/**
	 * @var array
	 */
	private $_notes;

	/**
	 * @var array
	 */
	private $_meta;

	/**
	 * @var int
	 */
	private $_sequence_number;

	/**
	 * @var \DateTime|null
	 */
	private $_submitted;

	/**
	 * @var \DateTime
	 */
	private $_created;

	/**
	 * @var \DateTime
	 */
	private $_modified;


	/**
	 * BWG_User_Storage constructor.
	 *
	 * @param int $post_ID
	 * @param int $user_ID
	 * @param array $grading
	 * @param array $notes
	 * @param array $meta
	 * @param int $sequence_number
	 * @param \DateTime|null $submitted
	 */
	public function __construct(
		$post_ID,
		$user_ID,
		array $grading = [],
		array $notes = [],
		array $meta = [],
		$sequence_number = 0,
		DateTime $submitted = NULL
	) {
		$this->set_post_ID( $post_ID );
		$this->set_user_ID( $user_ID );
		$this->set_grading( $grading );
		$this->set_notes( $notes );
		$this->set_meta( $meta );
		$this->set_submitted( $submitted );
	}

	/**
	 * @return int
	 */
	public function get_post_ID() {
		return $this->post_ID;
	}

	/**
	 * @param int $post_ID
	 */
	public function set_post_ID( $post_ID ) {
		$this->post_ID = $post_ID;
	}

	/**
	 * @return int
	 */
	public function get_user_ID() {
		return $this->user_ID;
	}

	/**
	 * @param int $user_ID
	 */
	public function set_user_ID( $user_ID ) {
		$this->user_ID = $user_ID;
	}

	/**
	 * @return array
	 */
	public function get_grading() {
		return $this->_grading;
	}

	/**
	 * @param array $grading
	 */
	public function set_grading( $grading ) {
		$this->_grading = $grading;
	}

	/**
	 * @return array
	 */
	public function get_notes() {
		return $this->_notes;
	}

	/**
	 * @param array $notes
	 */
	public function set_notes( $notes ) {
		$this->_notes = $notes;
	}

	/**
	 * @return array
	 */
	public function get_meta() {
		return $this->_meta;
	}

	/**
	 * @param array $meta
	 */
	public function set_meta( $meta ) {
		$this->_meta = $meta;
	}

	/**
	 * @return int
	 */
	public function get_sequence_number() {
		return $this->_sequence_number;
	}

	/**
	 * @param int $sequence_number
	 */
	public function set_sequence_number( $sequence_number ) {
		$this->_sequence_number = $sequence_number;
	}

	/**
	 * @return \DateTime|null
	 */
	public function get_submitted() {
		return $this->_submitted;
	}

	/**
	 * @param \DateTime|null $submitted
	 */
	public function set_submitted( $submitted ) {
		$this->_submitted = $submitted;
	}

	/**
	 * @return \DateTime
	 */
	public function get_created() {
		return $this->_created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function set_created( $created ) {
		$this->_created = $created;
	}

	/**
	 * @return \DateTime
	 */
	public function get_modified() {
		return $this->_modified;
	}

	/**
	 * @param \DateTime $modified
	 */
	public function set_modified( $modified ) {
		$this->_modified = $modified;
	}

}