<?php

namespace bwg\database\models;

/**
 * Class BWG_Submission.
 *
 * The class is readonly.
 *
 * @package bwg\database\models
 */
class BWG_Submission {

	/**
	 * @var array
	 */
	private $record;


	/**
	 * BWG_Submission constructor.
	 *
	 * @param array $record
	 */
	public function __construct( array $record ) {
		$this->record = $record;
	}

	/**
	 * @return array
	 */
	public function get_record() {
		return $this->record;
	}

	/**
	 * Checks whether the grading exists.
	 *
	 * @param $id
	 *
	 * @return bool
	 */
	public function has_grading( $id ) {
		return array_key_exists( 'g' . $id, $this->record );
	}

	/**
	 * Gets the grading as given by the user. Defaults to 0 if no such grading could be found.
	 *
	 * @param $id
	 *
	 * @return int
	 */
	public function get_grading( $id ) {
		if ( ! $this->has_grading( $id ) ) {
			return 0;
		}

		return intval( $this->record[ 'g' . $id ] );
	}

}