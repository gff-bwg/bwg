<?php

namespace bwg\database\helpers;

/**
 * Class BWG_Analyses_Database_Helper.
 *
 * @package bwg\database\helpers
 */
class BWG_Analyses_Database_Helper extends BWG_Abstract_Database_Helper {

	/**
	 * The plain 'bwg analyses' table name.
	 */
	const TABLE_NAME_ANALYSES = 'bwg_analyses';


	/**
	 * Gets the table name of the 'bwg analyses' table.
	 *
	 * @param bool $prefix
	 *
	 * @return string
	 *
	 * @internal \wpdb $wpdb
	 */
	public function get_table_name( $prefix = TRUE ) {
		global $wpdb;

		if ( ! $prefix ) {
			return self::TABLE_NAME_ANALYSES;
		}

		return $wpdb->prefix . self::TABLE_NAME_ANALYSES;
	}

	/**
	 * Deletes the given analysis.
	 *
	 * @param $analysis_ID
	 *
	 * @return bool
	 *
	 * @internal \wpdb $wpdb
	 */
	public function delete( $analysis_ID ) {
		global $wpdb;

		var_dump( $wpdb->query( 'START TRANSACTION' ) );
		try {
			$this->_bwg_base->selections_database_helper()->delete_by_analysis_ID( $analysis_ID );
			// TODO [AS] Remove the selections too.

			$wpdb->delete( $this->get_table_name(), [ 'ID' => $analysis_ID ], [ '%d' ] );

			var_dump( $wpdb->query( 'COMMIT' ) );

			return TRUE;
		} catch ( \Exception $e ) {
			var_dump( $wpdb->query( 'ROLLBACK' ) );

			return FALSE;
		}
	}

	/**
	 * Gets the total number of analyses for a given post ID.
	 *
	 * @param int $post_ID
	 * @param string $search
	 *
	 * @return int
	 *
	 * @internal \wpdb $wpdb
	 */
	public function list_table_count( $post_ID, $search = '' ) {
		global $wpdb;

		$sql = 'SELECT COUNT(*) FROM ' . $this->get_table_name() . ' '
		       . 'WHERE post_ID = %d AND label LIKE "%%%s%%"';

		return intval(
			$wpdb->get_var(
				$wpdb->prepare(
					$sql,
					$post_ID,
					$search
				)
			)
		);
	}

	public function list_table_items( $page, $per_page, $order_by, $order, $post_ID, $search = '' ) {
		global $wpdb;

		$sql = 'SELECT * FROM ' . $this->get_table_name() . ' '
		       . 'WHERE post_ID = %d AND label LIKE "%%%s%%" '
		       . 'ORDER BY ' . $order_by . ' ' . $order . ' '
		       . 'LIMIT %d, %d';
		
		return $wpdb->get_results(
			$wpdb->prepare(
				$sql,
				$post_ID, $search, ( $page - 1 ) * $per_page,
				$per_page
			), ARRAY_A
		);
	}

}
