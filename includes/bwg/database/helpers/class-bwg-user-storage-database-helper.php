<?php

namespace bwg\database\helpers;

use bwg\database\BWG_Database_Insert_Update_Query;
use bwg\database\models\BWG_User_Storage;
use DateTime;

/**
 * Class BWG_User_Storage_Database_Helper.
 *
 * @package bwg\models
 */
class BWG_User_Storage_Database_Helper extends BWG_Abstract_Database_Helper {

	/**
	 * The plain 'bwg user storage' table name.
	 */
	const TABLE_NAME_USER_STORAGE = 'bwg_user_storage';


	/**
	 * Gets the table name of the 'bwg user storage' table.
	 *
	 * @param bool $prefix
	 *
	 * @return string
	 *
	 * @internal param \wpdb $wpdb
	 */
	public function get_table_name( $prefix = TRUE ) {
		global $wpdb;

		if ( ! $prefix ) {
			return self::TABLE_NAME_USER_STORAGE;
		}

		return $wpdb->prefix . self::TABLE_NAME_USER_STORAGE;
	}

	/**
	 * Gets the current feup user id.
	 *
	 * @return int The user id or 0 if no current feup user could be found.
	 */
	protected function _current_feup_user_id() {
		$feup_user = new \FEUP_User();
		$user_ID   = $feup_user->Get_User_ID();
		if ( is_null( $user_ID ) ) {
			return 0;
		}

		return intval( $user_ID );
	}

	/**
	 * Initializes the user storage.
	 *
	 * @param int $evaluation_ID
	 *
	 * @param int|null $user_ID
	 *
	 * @return bool|null
	 *
	 * @internal param \wpdb $wpdb
	 */
	public function init_user_storage( $evaluation_ID, $user_ID = NULL ) {
		global $wpdb;

		if ( is_null( $user_ID ) ) {
			$user_ID = $this->_current_feup_user_id();
		}

		if ( 0 == $user_ID ) {
			return NULL;
		}

		$current_time_mysql = current_time( 'mysql', 1 );

		$q = new BWG_Database_Insert_Update_Query( 'bwg_user_storage' );
		$q->addField( 'post_ID' )->insert( $evaluation_ID, '%d' );

		$q->addField( 'user_ID' )->insert( $user_ID, '%d' );

		$q->addField( 'sequence_number' )->insert( 0, '%d' )
		  ->update_raw( 'IF((@update_record := sequence_number < VALUES(sequence_number)), VALUES(sequence_number), sequence_number)' );

		$q->addField( 'grading' )->insert( '' );

		$q->addField( 'notes' )->insert( json_encode( [], JSON_UNESCAPED_UNICODE ) );

		$q->addField( 'meta' )->insert( json_encode( [], JSON_UNESCAPED_UNICODE ) );

		$q->addField( 'created' )->insert( $current_time_mysql );

		$q->addField( 'modified' )->insert( $current_time_mysql );

		$r = $wpdb->query( $q->prepare() );

		if ( FALSE === $r ) {
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * @param $evaluation_ID
	 *
	 * @var \wpdb $wpdb
	 *
	 * @return \bwg\database\models\BWG_User_Storage|null
	 */
	public function get_user_storage( $evaluation_ID, $user_ID = NULL ) {
		global $wpdb;

		if ( is_null( $user_ID ) ) {
			$user_ID = $this->_current_feup_user_id();
		}
		if ( 0 == $user_ID ) {
			return NULL;
		}

		$sql    = $wpdb->prepare( 'SELECT * FROM ' . $this->get_table_name() . ' WHERE user_ID = %d AND post_ID = %d',
			$user_ID, $evaluation_ID );
		$record = $wpdb->get_row( $sql, ARRAY_A );
		if ( is_null( $record ) ) {
			return NULL;
		}

		return $this->record_to_user_storage( $record );
	}

	/**
	 * Set the 'submitted' field to the current gmt date time (if this is still null).
	 *
	 * @param int $evaluation_ID
	 * @param int|null $user_ID
	 *
	 * @var \wpdb $wpdb
	 *
	 * @return false|int 1 if 'submitted' could be set, 0 if already 'submitted', false on failure.
	 */
	public function submit( $evaluation_ID, $user_ID = NULL ) {
		global $wpdb;

		if ( is_null( $user_ID ) ) {
			$user_ID = $this->_current_feup_user_id();
		}

		if ( 0 == $user_ID ) {
			return FALSE;
		}

		return $wpdb->query( $wpdb->prepare( 'UPDATE ' . $this->get_table_name() . ' SET submitted = %s WHERE user_ID = %d AND post_ID = %d AND submitted IS NULL',
			current_time( 'mysql', 1 ), $user_ID, $evaluation_ID ) );
	}

	/**
	 * Sets the 'submitted' field to NULL.
	 *
	 * @param int $evaluation_ID
	 * @param int $user_ID
	 *
	 * @var \wpdb $wpdb
	 *
	 * @return false|int
	 */
	public function unsubmit( $evaluation_ID, $user_ID = NULL ) {
		global $wpdb;

		if ( is_null( $user_ID ) ) {
			$user_ID = $this->_current_feup_user_id();
		}

		if ( 0 == $user_ID ) {
			return FALSE;
		}

		return $wpdb->query( $wpdb->prepare(
			'UPDATE ' . $this->get_table_name() . ' SET submitted = NULL WHERE user_ID = %d AND post_ID = %d',
			$user_ID, $evaluation_ID
		) );
	}

	/**
	 * Converts a record into a BWG_User_Storage instance.
	 *
	 * @param array $record
	 *
	 * @return \bwg\database\models\BWG_User_Storage
	 */
	public function record_to_user_storage( array $record ) {
		$grading = [];
		foreach ( explode( '|', $record['grading'] ) as $key_value ) {
			$p = stripos( $key_value, '=' );

			$grading[ '#' . substr( $key_value, 0, $p ) ] = substr( $key_value, $p + 1 );
		}

		$user_storage = new BWG_User_Storage(
			intval( $record['post_ID'] ),
			intval( $record['user_ID'] ),
			$grading,
			json_decode( $record['notes'], TRUE, 512, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ),
			json_decode( $record['meta'], TRUE, 512, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ),
			intval( $record['sequence_number'] ),
			is_null( $record['submitted'] ) ? NULL : new DateTime( $record['submitted'] )
		);
		$user_storage->set_created( new DateTime( $record['created'] ) );
		$user_storage->set_modified( new DateTime( $record['modified'] ) );

		return $user_storage;
	}

	/**
	 * Gets the total number of submitted user storage (gradings) for the given post ID.
	 *
	 * @param int $post_ID The post id.
	 * @param bool $check_feup TRUE in order to count only submission of still existing users.
	 * @param string $search
	 *
	 * @return int
	 * @internal param \wpdb $wpdb
	 */
	public function get_total_nr_submitted( $post_ID, $check_feup = TRUE, $search = '' ) {
		global $wpdb;

		if ( $check_feup || ! empty( $search ) ) {
			$feup_user_table_name = $this->_bwg_base->feup_layer()->get_user_table_name();

			return intval(
				$wpdb->get_var(
					$wpdb->prepare(
						'SELECT COUNT(*) AS c FROM ' . $this->get_table_name() . ' AS s JOIN '
						. $feup_user_table_name . ' AS u ON u.User_ID = s.user_ID '
						. 'WHERE s.submitted IS NOT NULL AND s.post_ID = %d '
						. 'AND u.Username LIKE "%%%s%%"',
						$post_ID,
						$search
					)
				)
			);
		}

		return intval(
			$wpdb->get_var(
				$wpdb->prepare(
					'SELECT COUNT(*) AS c FROM ' . $this->get_table_name() . ' WHERE submitted IS NOT NULL AND post_ID = %d',
					$post_ID
				)
			)
		);
	}

	/**
	 * Gets the total number of submitted gradings for a specific feup field grouped by feup field value.
	 *
	 * @param $post_ID
	 * @param $feup_field_ID
	 *
	 * @return array|null|object
	 */
	public function get_total_nr_submitted_group_feup_field( $post_ID, $feup_field_ID ) {
		global $wpdb;

		$feup_user_table_name        = $this->_bwg_base->feup_layer()->get_user_table_name();
		$feup_user_fields_table_name = $this->_bwg_base->feup_layer()->get_user_fields_table_name();

		return $wpdb->get_results( $wpdb->prepare(
			'SELECT uf.Field_Value, COUNT(*) AS c FROM ' . $this->get_table_name() . ' AS s '
			. 'JOIN ' . $feup_user_table_name . ' AS u ON u.User_ID = s.user_ID '
			. 'JOIN ' . $feup_user_fields_table_name . ' AS uf ON u.User_ID = uf.User_ID '
			. 'WHERE s.submitted IS NOT NULL AND s.post_ID = %d AND uf.Field_ID = %d '
			. 'GROUP BY uf.Field_Value '
			. 'ORDER BY c DESC',
			$post_ID, $feup_field_ID
		), ARRAY_A );
	}

	public function get_total_nr_submitted_grouped_sex_year( $post_ID ) {
		global $wpdb;

		$feup_user_table_name        = $this->_bwg_base->feup_layer()->get_user_table_name();
		$feup_user_fields_table_name = $this->_bwg_base->feup_layer()->get_user_fields_table_name();

		// TODO [AS] We need a central class which actually selects and counts the different groups.
		// TODO [AS] This class would then define the alias and output of its filters and then it calls the filters
		//           to return their sql-information used afterwards by the sql builder.
		$aliasA  = 'uf1';
		$outputA = 'out1';
		$filterA = [
			'sql_select' => '(CASE 
 WHEN ' . $aliasA . '.Field_Value LIKE "Männlich" THEN 1
 WHEN ' . $aliasA . '.Field_Value LIKE "Weiblich" THEN 2
 ELSE 0
 END) AS ' . $outputA,
			'sql_table'  => 'JOIN ' . $feup_user_fields_table_name . ' AS ' . $aliasA . ' ON u.User_ID = ' . $aliasA . '.User_ID',
			'sql_where'  => sprintf( $aliasA . '.Field_ID = %d', 4 ),
		];

		$aliasB  = 'uf2';
		$outputB = 'out2';
		$filterB = [
			'sql_select' => '(CASE 
 WHEN ' . $aliasB . '.Field_Value >= 1950 AND ' . $aliasB . '.Field_Value < 1960 THEN 1
 WHEN ' . $aliasB . '.Field_Value >= 1960 AND ' . $aliasB . '.Field_Value < 1970 THEN 2
 WHEN ' . $aliasB . '.Field_Value >= 1970 AND ' . $aliasB . '.Field_Value < 1980 THEN 3                       
 ELSE 0
 END) AS ' . $outputB,
			'sql_table'  => 'JOIN ' . $feup_user_fields_table_name . ' AS ' . $aliasB . ' ON u.User_ID = ' . $aliasB . '.User_ID',
			'sql_where'  => sprintf( $aliasB . '.Field_ID = %d', 6 ),
		];

		///
		// SQL Builder.
		///
		$sql = 'SELECT COUNT(*) AS c';

		$sql .= ', ' . $filterA['sql_select'];
		$sql .= ', ' . $filterB['sql_select'];

		$sql .= ' FROM ' . $this->get_table_name() . ' AS s ';
		$sql .= ' JOIN ' . $feup_user_table_name . ' AS u ON u.User_ID = s.user_ID ';

		$sql .= ' ' . $filterA['sql_table'];
		$sql .= ' ' . $filterB['sql_table'];

		$sql .= ' WHERE s.submitted IS NOT NULL AND s.post_ID = %d';

		$sql .= ' AND ' . $filterA['sql_where'];
		$sql .= ' AND ' . $filterB['sql_where'];

		$sql .= ' GROUP BY ';

		$sql .= '' . $outputA;
		$sql .= ', ' . $outputB;

		echo str_replace( chr( 10 ), '', $wpdb->prepare( $sql, $post_ID ) );
		exit();


		$data = [
			'sex'        => [],
			'birth_year' => [],
		];
		foreach (
			$wpdb->get_results( $wpdb->prepare(
				'SELECT COUNT(*) AS c, uf1.Field_Value as sex, uf2.Field_Value as birth_year FROM '
				. $this->get_table_name() . ' AS s '
				. 'JOIN ' . $feup_user_table_name . ' AS u ON u.User_ID = s.user_ID '
				. 'JOIN ' . $feup_user_fields_table_name . ' AS uf1 ON u.User_ID = uf1.User_ID '
				. 'JOIN ' . $feup_user_fields_table_name . ' AS uf2 ON u.User_ID = uf2.User_ID '
				. 'WHERE s.submitted IS NOT NULL AND s.post_ID = %d AND uf1.Field_ID = %d AND uf2.Field_ID = %d '
				. 'GROUP BY uf1.Field_Value, uf2.Field_Value',
				$post_ID, 4, 6
			), ARRAY_A ) as $record
		) {
			$c = intval( $record['c'] );

			$sex = 'u';
			if ( 'Männlich' === $record['sex'] ) {
				$sex = 'm';
			} else if ( 'Weiblich' === $record['sex'] ) {
				$sex = 'w';
			}

			$birth_year = intval( $record['birth_year'] );

			if ( ! isset( $data['sex'][ $sex ] ) ) {
				$data['sex'][ $sex ] = [ '#' => $c ];
			} else {
				$data['sex'][ $sex ]['#'] += $c;
			}

			if ( ! isset( $data['sex'][ $sex ][ $birth_year ] ) ) {
				$data['sex'][ $sex ][ $birth_year ] = $c;
			} else {
				$data['sex'][ $sex ][ $birth_year ] += $c;
			}

			if ( ! isset( $data['birth_year'][ $birth_year ] ) ) {
				$data['birth_year'][ $birth_year ] = [ '#' => $c ];
			} else {
				$data['birth_year'][ $birth_year ]['#'] += $c;
			}

			if ( ! isset( $data['birth_year'][ $birth_year ][ $sex ] ) ) {
				$data['birth_year'][ $birth_year ][ $sex ] = $c;
			} else {
				$data['birth_year'][ $birth_year ][ $sex ] += $c;
			}
		}

		return $data;
	}

	/**
	 * @param $post_ID
	 * @param $page
	 * @param $per_page
	 * @param $order_by
	 * @param $order
	 * @param string $search
	 *
	 * @return array
	 * @internal param \wpdb $wpdb
	 */
	public function get_submitted_list( $post_ID, $page, $per_page, $order_by, $order, $search = '' ) {
		global $wpdb;

		$feup_user_table_name        = $this->_bwg_base->feup_layer()->get_user_table_name();
		$user_action_logs_table_name = $this->_bwg_base->user_action_logs_database_helper()->get_table_name();

		$sql = 'SELECT '
		       . 's.user_ID AS user_ID, '
		       . 's.submitted AS submitted, '
		       . 'u.Username AS username, '
		       . 'SUM(l.time_active) as time_active, '
		       . 'SEC_TO_TIME(SUM(l.time_active)) as time_active_human '
		       . 'FROM ' . $this->get_table_name() . ' AS s '
		       . 'JOIN ' . $feup_user_table_name . ' AS u ON u.User_ID = s.user_ID '
		       . 'LEFT JOIN ' . $user_action_logs_table_name . ' as l ON l.user_ID = s.user_ID AND l.post_ID = s.post_ID '
		       . 'WHERE s.submitted IS NOT NULL AND s.post_ID = %d '
		       . 'AND u.Username LIKE "%%%s%%" '
		       . 'GROUP BY l.user_ID '
		       . 'ORDER BY ' . $order_by . ' ' . $order . ' '
		       . 'LIMIT %d, %d';

		return $wpdb->get_results(
			$wpdb->prepare(
				$sql,
				$post_ID,
				$search,
				( $page - 1 ) * $per_page,
				$per_page
			), ARRAY_A
		);
	}


}
