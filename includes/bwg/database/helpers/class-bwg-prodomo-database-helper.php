<?php

namespace bwg\database\helpers;

use bwg\database\BWG_Database_Insert_Update_Query;

/**
 * Class BWG_Prodomo_Database_Helper.
 *
 * @package bwg\database\helpers
 */
class BWG_Prodomo_Database_Helper extends BWG_Abstract_Database_Helper {

	/**
	 * The plain 'bwg prodomo' table name.
	 */
	const TABLE_NAME_PRODOMO = 'bwg_prodomo';


	/**
	 * Gets the table name of the 'bwg prodomo' table.
	 *
	 * @param bool $prefix
	 *
	 * @return string
	 *
	 * @internal \wpdb $wpdb
	 */
	public function get_table_name( $prefix = TRUE ) {
		global $wpdb;

		if ( ! $prefix ) {
			return self::TABLE_NAME_PRODOMO;
		}

		return $wpdb->prefix . self::TABLE_NAME_PRODOMO;
	}

	/**
	 * Saves the data.
	 *
	 * @param int $post_ID
	 * @param int $user_ID
	 * @param int|null $question1
	 * @param int|null $question2
	 * @param int|null $question3
	 * @param string|null $freetext1
	 *
	 * @internal \wpdb $wpdb
	 *
	 * @return false|int 1 on insert, 2 on update, false on error.
	 */
	public function save_data( $post_ID, $user_ID, $question1, $question2, $question3, $freetext1 ) {
		global $wpdb;

		$query = new BWG_Database_Insert_Update_Query( $this->get_table_name( FALSE ) );

		$now = current_time( 'mysql', 1 );
		$query->addField( 'user_ID' )->insert( $user_ID, '%d' );
		$query->addField( 'post_ID' )->insert( $post_ID, '%d' );
		$query->addField( 'question1' )->insert_and_update( $question1, '%d' );
		$query->addField( 'question2' )->insert_and_update( $question2, '%d' );
		$query->addField( 'question3' )->insert_and_update( $question3, '%d' );
		$query->addField( 'freetext1' )->insert_and_update( $freetext1 );
		$query->addField( 'created' )->insert( $now );
		$query->addField( 'modified' )->insert_and_update( $now );

		return $wpdb->query( $query->prepare() );
	}

	/**
	 * @param $post_ID
	 * @param $user_ID
	 *
	 * @return array|null Either the record as array or null if no record could be found.
	 */
	public function get_record( $post_ID, $user_ID ) {
		global $wpdb;

		return $wpdb->get_row( $wpdb->prepare(
			'SELECT * FROM ' . $this->get_table_name() . ' WHERE post_ID = %d AND user_ID = %d',
			$post_ID, $user_ID
		), ARRAY_A );
	}

	/**
	 * Gets some statistics like averages, counts and standard deviations of the questions.
	 *
	 * @param int|null $evaluation_ID
	 *
	 * @return array|null
	 */
	public function get_statistics( $evaluation_ID = NULL ) {
		global $wpdb;

		$sql_select = 'SELECT '
		              . 'AVG(question1) AS a1, COUNT(question1) AS c1, STD(question1) AS s1, '
		              . 'AVG(question2) AS a2, COUNT(question2) AS c2, STD(question2) AS s2, '
		              . 'AVG(question3) AS a3, COUNT(question3) AS c3, STD(question3) AS s3 '
		              . ' FROM ' . $this->get_table_name();

		if ( is_null( $evaluation_ID ) ) {
			return $wpdb->get_row( $sql_select, ARRAY_A );
		}

		return $wpdb->get_row( $wpdb->prepare( $sql_select . ' WHERE post_ID = %d', $evaluation_ID ), ARRAY_A );
	}

	/**
	 * Gets the histograms.
	 *
	 * @return array
	 */
	public function get_histograms() {
		global $wpdb;

		$result = [];
		for ( $i = 1; $i <= 3; $i ++ ) {
			$result[ 'q' . $i ] = [
				'total' => 0,
				'#1'    => 0,
				'#2'    => 0,
				'#3'    => 0,
				'#4'    => 0,
				'#5'    => 0,
				'#6'    => 0
			];

			foreach (
				$wpdb->get_results(
					'SELECT COUNT(*) AS c, question' . $i . ' as q FROM ' . $this->get_table_name() . ' WHERE question' . $i . ' IS NOT NULL GROUP BY question' . $i,
					ARRAY_A
				) AS $record
			) {
				$result[ 'q' . $i ]['total']              += intval( $record['c'] );
				$result[ 'q' . $i ][ '#' . $record['q'] ] = intval( $record['c'] );
			}
		}

		return $result;
	}

}
