<?php

namespace bwg\database\helpers;

/**
 * Class BWG_Selections_Database_Helper.
 *
 * @package bwg\database\helpers
 */
class BWG_Selections_Database_Helper extends BWG_Abstract_Database_Helper {

	/**
	 * The plain 'bwg selections' table name.
	 */
	const TABLE_NAME_SELECTIONS = 'bwg_selections';


	/**
	 * Gets the table name of the 'bwg selections' table.
	 *
	 * @param bool $prefix
	 *
	 * @return string
	 *
	 * @internal \wpdb $wpdb
	 */
	public function get_table_name( $prefix = TRUE ) {
		global $wpdb;

		if ( ! $prefix ) {
			return self::TABLE_NAME_SELECTIONS;
		}

		return $wpdb->prefix . self::TABLE_NAME_SELECTIONS;
	}

	/**
	 * Deletes all selections belonging to an analysis.
	 *
	 * @param int $analysis_ID
	 *
	 * @return false|int The number of rows deleted, or FALSE on error.
	 *
	 * @internal \wpdb $wpdb
	 */
	public function delete_by_analysis_ID( $analysis_ID ) {
		global $wpdb;

		return $wpdb->delete(
			$this->get_table_name(),
			[ 'analysis_ID' => $analysis_ID ],
			[ '%d' ]
		);
	}

}
