<?php

namespace bwg\database\helpers;

use bwg\BWG_Base;

/**
 * Class BWG_Abstract_Database_Helper
 *
 * @package bwg\database\helpers
 */
abstract class BWG_Abstract_Database_Helper {

	/**
	 * @var \bwg\BWG_Base
	 */
	protected $_bwg_base;


	/**
	 * BWG_Abstract_Database_Helper constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;
	}

	/**
	 * Gets a date time string valid for the database, given a DateTime object. If the DateTime object is NULL, then
	 * this method would fallback to the wordpress built-in current_time() function.
	 *
	 * @param \DateTime|NULL $dt
	 * @param int $gmt
	 *
	 * @return false|string
	 */
	public function datetime( \DateTime $dt = NULL, $gmt = 0 ) {
		if ( is_null( $dt ) ) {
			return current_time( 'mysql', $gmt );
		}

		return ( $gmt ) ?
			gmdate( 'Y-m-d H:i:s', $dt->getTimestamp() ) :
			gmdate( 'Y-m-d H:i:s', ( $dt->getTimestamp() + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS ) ) );
	}

}
