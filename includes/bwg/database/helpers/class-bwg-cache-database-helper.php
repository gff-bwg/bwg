<?php

namespace bwg\database\helpers;

use bwg\database\BWG_Database_Insert_Update_Query;
use bwg\database\models\BWG_Cache;
use DateTime;

/**
 * Class BWG_Cache_Model.
 *
 * @package bwg\models
 */
final class BWG_Cache_Database_Helper extends BWG_Abstract_Database_Helper {

	/**
	 * The plain 'bwg cache' table name.
	 */
	const TABLE_NAME_CACHE = 'bwg_cache';


	/**
	 * Gets the table name of the 'bwg cache' table.
	 *
	 * @param bool $prefix Add prefix or not.
	 *
	 * @return string The table name.
	 */
	public function get_table_name( $prefix = TRUE ) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		if ( ! $prefix ) {
			return self::TABLE_NAME_CACHE;
		}

		return $wpdb->prefix . self::TABLE_NAME_CACHE;
	}

	/**
	 * Gets the cache record by key as <code>BWG_Cache</code> instance or <code>NULL</code> if no record could have
	 * been found.
	 *
	 * @param string $cache_key The cache key.
	 *
	 * @return \bwg\database\models\BWG_Cache|null
	 */
	public function load_by_key( $cache_key ) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		$record = $wpdb->get_row( $wpdb->prepare(
			'SELECT * FROM ' . $this->get_table_name() . ' WHERE cache_key = %s', [ $cache_key ]
		), ARRAY_A );

		if ( is_null( $record ) || FALSE === $record ) {
			return NULL;
		}

		return $this->record_to_cache( $record );
	}

	/**
	 * Converts a given record (as array) to a valid <code>BWG_Cache</code> instance.
	 *
	 * @param array $record The record as associative array.
	 *
	 * @return \bwg\database\models\BWG_Cache
	 */
	public function record_to_cache( array $record ) {
		$cache = new BWG_Cache(
			$record['cache_key'],
			$record['jsonified'] ?
				json_decode( $record['cache_value'], TRUE, 512, JSON_UNESCAPED_UNICODE ) : $record['cache_value'],
			is_null( $record['expire'] ) ? NULL : new DateTime( $record['expire'] ),
			$record['clearable'] ? TRUE : FALSE
		);
		$cache->set_created( new DateTime( $record['created'] ) );

		return $cache;
	}

	/**
	 * Saves the cache into the database table.
	 *
	 * @param \bwg\database\models\BWG_Cache $cache The cache element.
	 *
	 * @return false|int
	 *
	 * @internal param \wpdb $wpdb
	 */
	public function save( BWG_Cache $cache ) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		$query = new BWG_Database_Insert_Update_Query( self::TABLE_NAME_CACHE );
		$query->addField( 'cache_key' )->insert( $cache->get_key() );
		$query->addField( 'cache_value' )->insert_and_update(
			$cache->is_jsonified() ?
				json_encode( $cache->get_value(), JSON_UNESCAPED_UNICODE, 512 ) : $cache->get_value()
		);

		$query->addField( 'created' )->insert_and_update( $this->datetime( NULL, 1 ) );

		$expire = $cache->get_expire();
		$query->addField( 'expire' )->insert_and_update( is_null( $expire ) ? NULL : $this->datetime( $expire, 1 ) );

		$query->addField( 'clearable' )->insert_and_update( $cache->is_clearable() ? 1 : 0 );
		$query->addField( 'jsonified' )->insert_and_update( $cache->is_jsonified() ? 1 : 0 );

		return $wpdb->query( $query->prepare() );
	}

	/**
	 * Deletes the cache record by key.
	 *
	 * @param string $key The key.
	 *
	 * @return false|int
	 */
	public function delete_by_key( $key ) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		return $wpdb->delete( $this->get_table_name(), [ 'cache_key' => $key ] );
	}

}
