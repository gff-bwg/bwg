<?php

namespace bwg\database\helpers;

use bwg\database\BWG_Database_Insert_Update_Query;

/**
 * Class BWG_User_Action_Logs_Database_Helper.
 *
 * @package bwg\database\helpers
 */
class BWG_User_Action_Logs_Database_Helper extends BWG_Abstract_Database_Helper {

	/**
	 * The plain 'bwg user action logs' table name.
	 */
	const TABLE_NAME_USER_ACTION_LOGS = 'bwg_user_action_logs';

	/**
	 * Action 'goto'.
	 */
	const ACTION_GOTO = 'goto';

	/**
	 * Action 'vote'.
	 */
	const ACTION_VOTE = 'vote';

	/**
	 * Action 'sleep'.
	 */
	const ACTION_SLEEP = 'sleep';

	/**
	 * Action 'wake up'.
	 */
	const ACTION_WAKE_UP = 'wake_up';


	/**
	 * Gets the table name of the 'bwg user action logs' table.
	 *
	 * @param bool $prefix
	 *
	 * @return string
	 *
	 * @internal param \wpdb $wpdb
	 */
	public function get_table_name( $prefix = TRUE ) {
		global $wpdb;

		if ( ! $prefix ) {
			return self::TABLE_NAME_USER_ACTION_LOGS;
		}

		return $wpdb->prefix . self::TABLE_NAME_USER_ACTION_LOGS;
	}

	/**
	 * @param int $post_ID
	 * @param int $user_ID
	 * @param array $log_entries
	 *
	 * @return bool
	 *
	 * @internal param \wpdb $wpdb
	 */
	public function save_action_log( $post_ID, $user_ID, $log_entries ) {
		global $wpdb;

		$last_ts_offset = 0;
		$time_active    = 0;
		$time_inactive  = 0;

		$nr_actions_sleep   = 0;
		$nr_actions_wake_up = 0;
		$nr_actions_goto    = 0;
		$nr_actions_vote    = 0;

		$session_ID    = $log_entries[0][0];
		$c_log_entries = count( $log_entries );
		for ( $i = 1; $i < $c_log_entries; $i ++ ) {
			$ts_offset = $log_entries[ $i ][0];
			$message   = $log_entries[ $i ][1];
			if ( 'W' === $message ) {
				// Wake-up, i.e. we were in sleep mode before and therefore we were inactive.
				$time_inactive += $ts_offset - $last_ts_offset;
			} else {
				$time_active += $ts_offset - $last_ts_offset;
			}

			if ( 'W' === $message ) {
				$nr_actions_wake_up ++;
			} elseif ( 'S' === $message ) {
				$nr_actions_sleep ++;
			} elseif ( 'G' === substr( $message, 0, 1 ) ) {
				$nr_actions_goto ++;
			} elseif ( 'V' === substr( $message, 0, 1 ) ) {
				$nr_actions_vote ++;
			}

			$last_ts_offset = $ts_offset;
		}

		$current_time_mysql = current_time( 'mysql', 1 );

		$q = new BWG_Database_Insert_Update_Query( $this->get_table_name( FALSE ) );
		$q->addField( 'post_ID' )->insert( $post_ID, '%d' );
		$q->addField( 'user_ID' )->insert( $user_ID, '%d' );
		$q->addField( 'session_ID' )->insert( $session_ID, '%d' );
		$q->addField( 'entries' )->insert_and_update( gzcompress( json_encode( $log_entries ) ) );
		$q->addField( 'time_active' )->insert_and_update( $time_active, '%d' );
		$q->addField( 'time_inactive' )->insert_and_update( $time_inactive, '%d' );
		$q->addField( 'nr_actions_goto' )->insert_and_update( $nr_actions_goto, '%d' );
		$q->addField( 'nr_actions_vote' )->insert_and_update( $nr_actions_vote, '%d' );
		$q->addField( 'nr_actions_sleep' )->insert_and_update( $nr_actions_sleep, '%d' );
		$q->addField( 'nr_actions_wake_up' )->insert_and_update( $nr_actions_wake_up, '%d' );
		$q->addField( 'created' )->insert( $current_time_mysql );
		$q->addField( 'modified' )->insert_and_update( $current_time_mysql );

		$r = $wpdb->query( $q->prepare() );
		if ( FALSE === $r ) {
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * Gets the sum of the time (active or inactive).
	 *
	 * @param int $post_ID
	 * @param array|int $user_IDs
	 *
	 * @return array
	 *
	 * @internal \wpdb $wpdb
	 */
	public function sum_time( $post_ID, $user_IDs ) {
		global $wpdb;

		if ( ! is_array( $user_IDs ) ) {
			$user_IDs = [ $user_IDs ];
		}

		$sum_times = [];
		if ( empty( $user_IDs ) ) {
			return $sum_times;
		}

		$in = '';
		foreach ( $user_IDs as $user_ID ) {
			$in .= ( empty( $in ) ? '' : ',' ) . sprintf( '%d', $user_ID );
		}

		$sql = $wpdb->prepare(
			'SELECT user_ID, '
			. 'SUM(time_active) AS active_raw, '
			. 'SEC_TO_TIME(SUM(time_active)) AS active, '
			. 'SUM(time_inactive) AS inactive_raw, '
			. 'SEC_TO_TIME(SUM(time_inactive)) AS inactive '
			. 'FROM ' . $this->get_table_name() . ' WHERE post_ID = %d AND user_ID IN (' . $in . ') GROUP BY user_ID',
			$post_ID
		);

		foreach ( $wpdb->get_results( $sql, ARRAY_A ) as $record ) {
			$sum_times[ '#' . $record['user_ID'] ] = $record;
		}

		return $sum_times;
	}

	/**
	 * Gets the sum of all actions for a given post-ID, user-ID pair.
	 *
	 * @param int $post_ID
	 * @param array|int $user_IDs
	 *
	 * @return array
	 *
	 * @internal \wpdb $wpdb
	 */
	public function sum_actions( $post_ID, $user_IDs ) {
		global $wpdb;

		if ( ! is_array( $user_IDs ) ) {
			$user_IDs = [ $user_IDs ];
		}

		$sum_actions = [];
		if ( empty( $user_IDs ) ) {
			return $sum_actions;
		}

		$in = '';
		foreach ( $user_IDs as $user_ID ) {
			$in .= ( empty( $in ) ? '' : ',' ) . sprintf( '%d', $user_ID );
		}

		$sql = $wpdb->prepare(
			'SELECT '
			. 'user_ID, '
			. 'SUM(nr_actions_goto) AS nr_actions_goto, '
			. 'SUM(nr_actions_vote) AS nr_actions_vote, '
			. 'SUM(nr_actions_sleep) AS nr_actions_sleep, '
			. 'SUM(nr_actions_wake_up) AS nr_actions_wake_up '
			. 'FROM ' . $this->get_table_name() . ' '
			. 'WHERE post_ID = %d AND user_ID IN (' . $in . ') GROUP BY user_ID',
			$post_ID
		);
		
		foreach ( $wpdb->get_results( $sql, ARRAY_A ) as $record ) {
			$sum_actions[ '#' . $record['user_ID'] ] = [
				self::ACTION_GOTO    => intval( $record['nr_actions_goto'] ),
				self::ACTION_VOTE    => intval( $record['nr_actions_vote'] ),
				self::ACTION_SLEEP   => intval( $record['nr_actions_sleep'] ),
				self::ACTION_WAKE_UP => intval( $record['nr_actions_wake_up'] ),
			];
		}

		return $sum_actions;
	}

}
