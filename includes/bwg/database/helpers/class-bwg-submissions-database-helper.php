<?php

namespace bwg\database\helpers;

use bwg\database\BWG_Database_Insert_Update_Query;
use bwg\database\models\BWG_Cache;
use bwg\database\models\BWG_Submission;
use bwg\database\models\BWG_User_Storage;
use bwg\evaluation\BWG_Evaluation_Definition;
use bwg\profile\fields\BWG_Profile_Field;
use bwg\profile\fields\BWG_Profile_Fields;
use bwg\stats\BWG_Stats_Gradings;
use bwg\stats\strata\BWG_Stats_Strata;
use bwg\stats\strata\selectors\BWG_Stats_Strata_Value_Selector;

/**
 * Class BWG_Submissions_Database_Helper.
 *
 * @package bwg\database\helpers
 */
final class BWG_Submissions_Database_Helper extends BWG_Abstract_Database_Helper {

	/**
	 * Column Name: id.
	 */
	const COLUMN_NAME_ID = 'id';

	/**
	 * Column Name: User id.
	 */
	const COLUMN_NAME_USER_ID = 'user_ID';

	/**
	 * Column Name: Submitted
	 */
	const COLUMN_NAME_SUBMITTED = 'submitted';

	/**
	 * Column Name: Confidence
	 */
	const COLUMN_NAME_CONFIDENCE = 'confidence';

	/**
	 * @var array
	 */
	private $total_nr_submissions_cache = [];


	/**
	 * Gets the table name the submissions would be written to.
	 *
	 * @param int $evaluation_ID The evaluation id.
	 * @param bool $prefix Prefix or not.
	 *
	 * @return string
	 * @internal param \wpdb $wpdb
	 */
	public function get_submissions_table_name( $evaluation_ID, $prefix = TRUE ) {
		global $wpdb;
		$table_name = 'bwg_submissions_' . $evaluation_ID;

		if ( $prefix ) {
			return $wpdb->prefix . $table_name;
		}

		return $table_name;
	}

	/**
	 * Builds the submissions table schema (sql) for the given bwg evaluation.
	 *
	 * @param int $evaluation_ID The evaluation id.
	 * @param BWG_Evaluation_Definition $bwg_evaluation_definition The evaluation definition.
	 * @param BWG_Profile_Fields $bwg_profile_fields The evaluation profile fields.
	 *
	 * @return string
	 * @internal \wpdb $wpdb
	 */
	private function _build_submissions_schema(
		$evaluation_ID,
		BWG_Evaluation_Definition $bwg_evaluation_definition,
		BWG_Profile_Fields $bwg_profile_fields
	) {
		global $wpdb;

		$sql = 'CREATE TABLE ' . $this->get_submissions_table_name( $evaluation_ID ) . ' (' . PHP_EOL;

		// Add the "base" submissions columns.
		foreach ( $this->_get_submissions_schema_columns() as $column_name => $column_definition ) {
			$sql .= '  ' . $column_name . '  ' . $column_definition . ',' . PHP_EOL;
		}

		// Add the profile fields columns.
		$profile_field_schema_columns = $this->_get_profile_field_schema_columns( $bwg_profile_fields );
		foreach ( $profile_field_schema_columns as $column_name => $column_info ) {
			$sql .= '  ' . $column_name . '  ' . $column_info->get_column_definition() . ',' . PHP_EOL;
		}

		// Add the gradings columns.
		foreach (
			$this->_get_gradings_schema_columns( $bwg_evaluation_definition ) as
			$column_name => $column_definition
		) {
			$sql .= '  ' . $column_name . '  ' . $column_definition . ',' . PHP_EOL;
		}

		$sql .= '  PRIMARY KEY  (' . self::COLUMN_NAME_ID . '),' . PHP_EOL;
		foreach ( $profile_field_schema_columns as $column_name => $column_info ) {
			if ( $column_info->is_indexable() ) {
				$sql .= '  KEY ix_' . $column_name . ' (' . $column_name . '),' . PHP_EOL;
			}
		}
		$sql .= '  UNIQUE KEY ix_' . self::COLUMN_NAME_USER_ID . '  (' . self::COLUMN_NAME_USER_ID . ')' . PHP_EOL;
		$sql .= ')' . $wpdb->get_charset_collate() . ';';

		return $sql;
	}

	/**
	 * Gets the "base" columns used in a submission database table.
	 *
	 * @return array
	 */
	private function _get_submissions_schema_columns() {
		return [
			self::COLUMN_NAME_ID         => 'BIGINT(20)    UNSIGNED NOT NULL AUTO_INCREMENT',
			self::COLUMN_NAME_USER_ID    => 'MEDIUMINT(9)  NOT NULL DEFAULT \'0\'',
			self::COLUMN_NAME_CONFIDENCE => 'INT(10)       UNSIGNED NOT NULL DEFAULT \'0\'',
			self::COLUMN_NAME_SUBMITTED  => 'DATETIME      DEFAULT NULL',
		];
	}

	/**
	 * Gets the profile field column used in the submissions database table.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $profile_field
	 *
	 * @return \bwg\database\etc\BWG_Database_Submission_Column_Info
	 */
	private function _get_profile_field_schema_column( BWG_Profile_Field $profile_field ) {
		return $profile_field->db_submission_column_info(
			$this->_bwg_base->profile_field_type_registry()
		);
	}

	/**
	 * Gets the profile fields columns used in the submissions database table.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Fields $bwg_profile_fields
	 *
	 * @return \bwg\database\etc\BWG_Database_Submission_Column_Info[]
	 */
	private function _get_profile_field_schema_columns( BWG_Profile_Fields $bwg_profile_fields ) {
		$columns = [];

		foreach ( $bwg_profile_fields->get_fields() as $profile_field ) {
			$column_info = $this->_get_profile_field_schema_column( $profile_field );

			$columns[ $column_info->get_column_name() ] = $column_info;
		}

		return $columns;
	}

	/**
	 * Gets the gradings columns used in the submissions database table.
	 *
	 * @param BWG_Evaluation_Definition $bwg_evaluation_definition The evaluation definition.
	 *
	 * @return array Associative array where the keys are the column names and the values are the column definitions.
	 */
	private function _get_gradings_schema_columns( BWG_Evaluation_Definition $bwg_evaluation_definition ) {
		$columns = [];

		foreach ( $this->get_gradings_columns( $bwg_evaluation_definition ) as $field_name => $field_definition ) {
			$columns[ $field_name ] = $field_definition;
		}

		return $columns;
	}

	/**
	 * Gets the gradings columns (only the g{\d} columns).
	 *
	 * @param \bwg\evaluation\BWG_Evaluation_Definition $bwg_evaluation_definition
	 *
	 * @return array
	 */
	public function get_gradings_columns( BWG_Evaluation_Definition $bwg_evaluation_definition ) {
		$columns = [];
		foreach ( $bwg_evaluation_definition->get_items() as $item1 ) {
			if ( ! $item1->has_items() ) {
				continue;
			}

			foreach ( $item1->get_items() as $item2 ) {
				if ( ! $item2->has_items() ) {
					continue;
				}

				foreach ( $item2->get_items() as $item3 ) {
					if ( ! $item3->has_items() ) {
						$columns[ 'g' . $item3->get_id() ] = 'INT(2) UNSIGNED DEFAULT NULL';
					} else {
						foreach ( $item3->get_items() as $item4 ) {
							$columns[ 'g' . $item4->get_id() ] = 'INT(2) UNSIGNED DEFAULT NULL';
						}
					}
				}
			}
		}

		return $columns;
	}

	/**
	 * Updates the submissions table for the given evaluation (if needed).
	 *
	 * @param int $evaluation_ID The evaluation id.
	 * @param \bwg\evaluation\BWG_Evaluation_Definition $evaluation_definition The evaluation definition.
	 * @param \bwg\profile\fields\BWG_Profile_Fields $profile_fields The evaluation profile fields.
	 */
	public function db_delta_submissions(
		$evaluation_ID,
		BWG_Evaluation_Definition $evaluation_definition,
		BWG_Profile_Fields $profile_fields
	) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		// Get the table name.
		$table_name = $this->get_submissions_table_name( $evaluation_ID );

		// Get the column names currently in the database.
		$db_column_names = [];
		foreach ( $wpdb->get_col( 'DESC ' . $table_name, 0 ) as $column_name ) {
			array_push( $db_column_names, $column_name );
		}

		// Get the column names that should be currently in the database.
		$column_names = array_merge(
			array_keys( $this->_get_submissions_schema_columns() ),
			array_keys( $this->_get_profile_field_schema_columns( $profile_fields ) ),
			array_keys( $this->_get_gradings_schema_columns( $evaluation_definition ) )
		);

		// Get the column names that are currently in the database but should not be.
		$remove_columns = array_diff( $db_column_names, $column_names );

		// Drop the columns if there are columns to be removed.
		if ( ! empty( $remove_columns ) ) {
			$wpdb->query( 'ALTER TABLE ' . $table_name . ' DROP COLUMN ' .
			              implode( ', DROP COLUMN ', $remove_columns ) . ';' );
		}

		// Check the cache if the schema changed and therefore dbDelta needs to be executed.
		$db_delta    = TRUE;
		$cache_key   = 'schema_submissions_' . $evaluation_ID;
		$schema      = $this->_build_submissions_schema( $evaluation_ID, $evaluation_definition, $profile_fields );
		$schema_hash = $this->calculate_schema_hash( $schema );

		$cache = $this->_bwg_base->cache_database_helper()->load_by_key( $cache_key );
		if ( ! is_null( $cache ) && $schema_hash === substr( $cache->get_value(), 0, strlen( $schema_hash ) ) ) {
			$db_delta = FALSE;
		}

		if ( $db_delta ) {
			// Run dbDelta.
			include_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $schema );

			$this->_bwg_base->cache_database_helper()->save( new BWG_Cache(
				$cache_key, $schema_hash . PHP_EOL . $schema, NULL, FALSE
			) );
		}
	}

	/**
	 * Calculates the schema hash. The hash would be the same for two equal schemas ignoring the order of the lines.
	 * As long as each column and key definition is on a new line (PHP_EOL), then two schemas with the same column
	 * and key definitions would be considered to be equal.
	 *
	 * @param string $schema The schema.
	 *
	 * @return string The schema hash.
	 */
	private function calculate_schema_hash( $schema ) {
		$lines  = explode( PHP_EOL, $schema );
		$result = NULL;
		foreach ( $lines as $line ) {
			if ( is_null( $result ) ) {
				$result = md5( $line );
			} else {
				$result = $result ^ md5( $line );
			}
		}

		return md5( $result );
	}

	/**
	 * Saves the given submission.
	 *
	 * @param \bwg\evaluation\BWG_Evaluation_Definition $evaluation_definition The evaluation definition.
	 * @param \bwg\profile\fields\BWG_Profile_Fields $profile_fields The profile fields.
	 * @param \bwg\database\models\BWG_User_Storage $user_storage The user storage.
	 *
	 * @return false|int 2 on update, 1 on insert, 0 on nothing changed.
	 */
	public function save_submission(
		BWG_Evaluation_Definition $evaluation_definition,
		BWG_Profile_Fields $profile_fields,
		BWG_User_Storage $user_storage
	) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		$sql = new BWG_Database_Insert_Update_Query(
			$this->get_submissions_table_name( $user_storage->get_post_ID(), FALSE )
		);

		$sql->addField( self::COLUMN_NAME_USER_ID )->insert( $user_storage->get_user_ID(), '%d' );
		$sql->addField( self::COLUMN_NAME_SUBMITTED )->insert_and_update( current_time( 'mysql', TRUE ) );

		$user_storage_profile_fields = $user_storage->get_meta();
		foreach ( $profile_fields->get_fields() as $profile_field ) {
			$column_info = $this->_get_profile_field_schema_column( $profile_field );
			if ( array_key_exists( $profile_field->get_uid(), $user_storage_profile_fields ) ) {
				$sql->addField( $column_info->get_column_name() )->insert_and_update(
					$user_storage_profile_fields[ $profile_field->get_uid() ],
					$column_info->get_column_query_type()
				);
			}
		}

		$user_storage_gradings = $user_storage->get_grading();
		foreach ( $this->get_gradings_columns( $evaluation_definition ) as $field_name => $field_definition ) {
			$key = '#' . substr( $field_name, 1 );

			$sql->addField( $field_name )->insert_and_update(
				isset( $user_storage_gradings[ $key ] ) ? $user_storage_gradings[ $key ] : 0, '%d'
			);
		}

		return $wpdb->query( $sql->prepare() );
	}

	/**
	 * Deletes the given submission.
	 *
	 * @param int $evaluation_ID The evaluation id.
	 * @param int $user_ID The user id.
	 *
	 * @return false|int
	 */
	public function delete_submission( $evaluation_ID, $user_ID ) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		return $wpdb->delete(
			$this->get_submissions_table_name( $evaluation_ID ),
			[
				'user_ID' => $user_ID
			],
			[ '%d' ]
		);
	}

	/**
	 * Gets the sql weight expressions.
	 *
	 * @param \bwg\evaluation\BWG_Evaluation_Definition $bwg_evaluation_definition
	 *
	 * @return array
	 */
	public function get_sql_weight_expressions( BWG_Evaluation_Definition $bwg_evaluation_definition ) {
		$sql_weight_expressions = [];

		foreach ( $bwg_evaluation_definition->get_items() as $item1 ) {
			if ( ! $item1->has_items() ) {
				continue;
			}

			foreach ( $item1->get_items() as $item2 ) {
				if ( ! $item2->has_items() ) {
					continue;
				}

				$total_weight_2     = 0;
				$sql_weight_parts_2 = [];
				foreach ( $item2->get_items() as $item3 ) {
					$weight3        = $item3->get_weight();
					$total_weight_2 += $weight3;

					if ( ! $item3->has_items() ) {
						array_push( $sql_weight_parts_2, $weight3 . '*g' . $item3->get_id() );
					} else {
						$total_weight_3     = 0;
						$sql_weight_parts_3 = [];
						foreach ( $item3->get_items() as $item4 ) {
							$weight4 = $item4->get_weight();

							array_push( $sql_weight_parts_3, $weight4 . '*g' . $item4->get_id() );
							$total_weight_3 += $weight4;
						}

						if ( $total_weight_3 > 0 ) {
							$s = '(' . implode( '+', $sql_weight_parts_3 ) . ')/' . $total_weight_3;
							array_push( $sql_weight_parts_2, $weight3 . '*(' . $s . ')' );
						}
					}
				}

				$sql_weight_expressions[ '#' . $item2->get_id() ] = '(' . implode( '+',
						$sql_weight_parts_2 ) . ')/' . $total_weight_2;
			}
		}

		return $sql_weight_expressions;
	}

	/**
	 * Gets the submission record as associative array.
	 *
	 * @param $evaluation_ID
	 * @param $user_ID
	 *
	 * @return BWG_Submission|null
	 */
	public function get_submission_by_user( $evaluation_ID, $user_ID ) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		$record = $wpdb->get_row(
			$wpdb->prepare(
				'SELECT * FROM ' . $this->get_submissions_table_name( $evaluation_ID ) . ' WHERE user_ID = %d LIMIT 1',
				[ $user_ID ]
			), ARRAY_A
		);
		if ( is_null( $record ) ) {
			return NULL;
		}

		return new BWG_Submission( $record );
	}

	/**
	 *
	 * @param int $evaluation_ID The evaluation id.
	 * @param \bwg\evaluation\BWG_Evaluation_Definition $bwg_evaluation_definition The evaluation definition.
	 * @param int $user_ID The user id.
	 *
	 * @return array The key is a '#' followed by the ID of the category item, the value is the weighted average
	 * grading.
	 */
	public function get_weight_gradings_by_user(
		$evaluation_ID,
		BWG_Evaluation_Definition $bwg_evaluation_definition,
		$user_ID = NULL
	) {
		$user_strata = new BWG_Stats_Strata( 'User', '#ff9000', [
			new BWG_Stats_Strata_Value_Selector( 'user_ID', $user_ID )
		] );

		return $this->get_weight_gradings( $evaluation_ID, $bwg_evaluation_definition, $user_strata );
	}

	/**
	 *
	 * @param int $evaluation_ID The evaluation id.
	 * @param \bwg\evaluation\BWG_Evaluation_Definition $bwg_evaluation_definition The evaluation definition.
	 * @param \bwg\stats\strata\BWG_Stats_Strata|null $bwg_stats_strata
	 *
	 * @return array
	 */
	public function get_weight_gradings(
		$evaluation_ID,
		BWG_Evaluation_Definition $bwg_evaluation_definition,
		BWG_Stats_Strata $bwg_stats_strata = NULL
	) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		$expressions = $this->get_sql_weight_expressions( $bwg_evaluation_definition );

		$first = TRUE;
		$sql   = 'SELECT ';
		foreach ( $expressions as $key => $expression ) {
			if ( ! $first ) {
				$sql .= ', ';
			}
			$sql .= 'AVG(' . $expression . ') AS g' . substr( $key, 1 );

			if ( $first ) {
				$first = FALSE;
			}
		}
		$sql .= ' FROM ' . $this->get_submissions_table_name( $evaluation_ID );

		// Build the sql where query part.
		$sql_args = [];
		if ( ! is_null( $bwg_stats_strata ) ) {
			$sql_where = $bwg_stats_strata->sql_where( $sql_args );
			if ( ! empty( $sql_where ) ) {
				$sql .= ' WHERE ' . $sql_where;
			}
		}

		// Prepare and run the query.
		$r = $wpdb->get_row( $wpdb->prepare( $sql, $sql_args ), ARRAY_A );

		$result = [];
		foreach ( $r as $key => $value ) {
			$result[ '#' . substr( $key, 1 ) ] = doubleval( $value );
		}

		return $result;
	}

	/**
	 * Gets the statistics of all gradings individually.
	 *
	 * @param $evaluation_ID
	 * @param \bwg\evaluation\BWG_Evaluation_Definition $evaluation_definition
	 * @param \bwg\stats\strata\BWG_Stats_Strata|null $bwg_stats_strata
	 *
	 * @return BWG_Stats_Gradings
	 */
	public function get_statistics(
		$evaluation_ID,
		BWG_Evaluation_Definition $evaluation_definition,
		BWG_Stats_Strata $bwg_stats_strata = NULL
	) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		$sql = 'SELECT COUNT(*) AS c';
		foreach ( array_keys( $this->get_gradings_columns( $evaluation_definition ) ) as $column_name ) {
			$sql .= ', ';

			$item_id = substr( $column_name, 1 );

			$sql .= 'AVG(' . $column_name . ') AS a' . $item_id;
			$sql .= ', STD(' . $column_name . ') AS s' . $item_id;
			for ( $i = 1; $i <= 6; $i ++ ) {
				$sql .= ', SUM(IF(' . $column_name . ' = ' . $i . ', 1, 0)) AS h' . $item_id . '_' . $i;
			}
		}

		$sql .= ' FROM ' . $this->get_submissions_table_name( $evaluation_ID );

		// Build the sql where query part.
		$sql_args = [];
		if ( ! is_null( $bwg_stats_strata ) ) {
			$sql_where = $bwg_stats_strata->sql_where( $sql_args );
			if ( ! empty( $sql_where ) ) {
				$sql .= ' WHERE ' . $sql_where;
			}
		}

		// Prepare and run the query.
		$r = $wpdb->get_row( $wpdb->prepare( $sql, $sql_args ), ARRAY_A );

		$result = new BWG_Stats_Gradings();
		foreach ( $r as $key => $value ) {
			if ( 'c' === $key ) {
				$result->set_count( intval( $value ) );
			} elseif ( 'a' === substr( $key, 0, 1 ) ) {
				$result->add_average( intval( substr( $key, 1 ) ), doubleval( $value ) );
			} elseif ( 's' === substr( $key, 0, 1 ) ) {
				$result->add_standard_deviation( intval( substr( $key, 1 ) ), doubleval( $value ) );
			} elseif ( preg_match( '/^h(\d+)_(\d+)$/', $key, $m ) ) {
				$result->add_histogram( intval( $m[1] ), intval( $m[2] ), intval( $value ) );
			}
		}

		return $result;
	}

	/**
	 * Gets the total number of submitted gradings.
	 *
	 * @param int $evaluation_ID The evaluation id.
	 *
	 * @param bool $cache Whether the cache should be used or not.
	 *
	 * @return null|string
	 * @internal \wpdb $wpdb
	 */
	public function get_total_nr_submissions( $evaluation_ID, $cache = TRUE ) {
		global $wpdb;

		if ( $cache && array_key_exists( $evaluation_ID, $this->total_nr_submissions_cache ) ) {
			return $this->total_nr_submissions_cache[ $evaluation_ID ];
		}

		$r = $wpdb->get_var( 'SELECT COUNT(*) FROM ' . $this->get_submissions_table_name( $evaluation_ID ) );

		$this->total_nr_submissions_cache[ $evaluation_ID ] = $r;

		return $r;
	}

	/**
	 * Checks whether or not the given evaluation has at least one submitted grading.
	 *
	 * @param $evaluation_ID
	 *
	 * @return bool
	 */
	public function has_submissions( $evaluation_ID ) {
		global $wpdb;

		if ( ! in_array( $this->get_submissions_table_name( $evaluation_ID ), $wpdb->tables() ) ) {
			return FALSE;
		}

		return $this->get_total_nr_submissions( $evaluation_ID ) > 0;
	}

	/**
	 * @param int $evaluation_id
	 *
	 * @return false|int
	 */
	public function drop_table( $evaluation_id ) {
		global $wpdb;

		return $wpdb->query( 'DROP TABLE IF EXISTS `' . $this->get_submissions_table_name( $evaluation_id ) . '`' );
	}


}
