<?php

namespace bwg\database\etc;

/**
 * Class BWG_Database_Submission_Column_Info.
 *
 * @package bwg\database\etc
 */
class BWG_Database_Submission_Column_Info {

	/**
	 * @var string
	 */
	private $column_name;

	/**
	 * @var string
	 */
	private $column_definition;

	/**
	 * @var string
	 */
	private $column_query_type;

	/**
	 * @var bool
	 */
	private $indexable;


	/**
	 * BWG_Database_Submission_Column_Info constructor.
	 *
	 * @param string $column_name
	 * @param string $column_definition
	 * @param string $column_query_type
	 * @param bool $indexable
	 */
	public function __construct( $column_name, $column_definition, $column_query_type = '%s', $indexable = FALSE ) {
		$this->column_definition = $column_definition;
		$this->column_name       = $column_name;
		$this->column_query_type = $column_query_type;
		$this->indexable         = $indexable;
	}

	/**
	 * @return string
	 */
	public function get_column_name() {
		return $this->column_name;
	}

	/**
	 * @return string
	 */
	public function get_column_definition() {
		return $this->column_definition;
	}
	
	/**
	 * @return string
	 */
	public function get_column_query_type() {
		return $this->column_query_type;
	}

	/**
	 * @return bool
	 */
	public function is_indexable() {
		return $this->indexable;
	}

}