<?php

namespace bwg\database;

use wpdb;

/**
 * Class BWG_Database_Insert_Update_Query.
 *
 * @package bwg\database
 */
class BWG_Database_Insert_Update_Query {

	/**
	 * @var string The table name (non-prefixed).
	 */
	protected string $table_name;

	/**
	 * @var BWG_Database_Insert_Update_Field[]
	 */
	protected array $fields = [];

	/**
	 * @param string $table_name The non-prefixed table name.
	 */
	public function __construct( string $table_name ) {
		$this->table_name = $table_name;
	}

	/**
	 * Adds a new BWG_Database_Insert_Update_Field with the given field name (key).
	 *
	 * @param string $key
	 *
	 * @return BWG_Database_Insert_Update_Field
	 */
	public function addField( string $key ): BWG_Database_Insert_Update_Field {
		$field                = new BWG_Database_Insert_Update_Field( $key );
		$this->fields[ $key ] = $field;

		return $field;
	}

	/**
	 * Prepares the query and returns a valid sql query to be executed.
	 *
	 * @return string
	 */
	public function prepare(): string {
		/** @var wpdb $wpdb */
		global $wpdb;

		$insert_keys        = [];
		$insert_value_types = [];
		$insert_values      = [];
		$update_keys        = [];
		$update_value_types = [];
		$update_values      = [];

		foreach ( $this->fields as $field ) {
			if ( $field->has_insert() ) {
				array_push( $insert_keys, $field->get_key() );
				$type = $field->get_insert_type();
				if ( 'raw' === $type ) {
					array_push( $insert_value_types, $field->get_insert_value() );
				} else {
					array_push( $insert_values, $field->get_insert_value() );
					array_push( $insert_value_types, $field->get_insert_type() );
				}
			}

			if ( $field->has_update() ) {
				array_push( $update_keys, $field->get_key() );
				$type = $field->get_update_type();
				if ( 'raw' === $type ) {
					array_push( $update_value_types, $field->get_update_value() );
				} else {
					array_push( $update_values, $field->get_update_value() );
					array_push( $update_value_types, $field->get_update_type() );
				}
			}
		}

		$sql = 'INSERT INTO ' . $wpdb->prefix . $this->table_name . ' (' . implode( ',',
				$insert_keys ) . ') VALUES (' . implode( ',', $insert_value_types ) . ') ON DUPLICATE KEY UPDATE ';
		for ( $i = 0; $i < count( $update_keys ); $i ++ ) {
			if ( $i > 0 ) {
				$sql .= ', ';
			}
			$sql .= $update_keys[ $i ] . '=' . $update_value_types[ $i ];
		}

		$values = array_merge( $insert_values, $update_values );

		return $wpdb->prepare( $sql, $values );
	}
}
