<?php

namespace bwg\database;

/**
 * Class BWG_Database_Insert_Update_Field.
 *
 * @package bwg\database
 */
class BWG_Database_Insert_Update_Field {

	/**
	 * @var string
	 */
	private $_key;

	/**
	 * @var boolean
	 */
	private $_has_insert = FALSE;

	/**
	 * @var mixed
	 */
	private $_insert_value;

	/**
	 * @var string
	 */
	private $_insert_type;

	/**
	 * @var boolean
	 */
	private $_has_update;

	/**
	 * @var mixed
	 */
	private $_update_value;

	/**
	 * @var string
	 */
	private $_update_type;


	/**
	 * BWG_Database_Insert_Update_Field constructor.
	 *
	 * @param string $key
	 */
	function __construct( $key ) {
		$this->_key = $key;
	}

	/**
	 * Gets the key.
	 *
	 * @return string
	 */
	public function get_key() {
		return $this->_key;
	}

	/**
	 * Has this field a value defined for the insert-case?
	 *
	 * @return bool
	 */
	public function has_insert() {
		return $this->_has_insert;
	}

	/**
	 * Gets the value for the insert-case.
	 *
	 * @return mixed
	 */
	public function get_insert_value() {
		return $this->_insert_value;
	}

	/**
	 * Gets the value type for the insert-case.
	 *
	 * @return string
	 */
	public function get_insert_type() {
		return $this->_insert_type;
	}

	/**
	 * Has this field a value defined for the update-case?
	 *
	 * @return bool
	 */
	public function has_update() {
		return $this->_has_update;
	}

	/**
	 * Gets the value for the update-case.
	 *
	 * @return mixed
	 */
	public function get_update_value() {
		return $this->_update_value;
	}

	/**
	 * Gets the value type for the update-case.
	 *
	 * @return string
	 */
	public function get_update_type() {
		return $this->_update_type;
	}


	/**
	 * Sets the value and the value type (%s, %f, %d) for the insert-case.
	 *
	 * @param mixed $value
	 * @param string $type
	 *
	 * @return $this
	 */
	public function insert( $value, $type = '%s' ) {
		if ( is_null( $value ) ) {
			$value = 'NULL';
			$type  = 'raw';
		}

		$this->_has_insert   = TRUE;
		$this->_insert_value = $value;
		$this->_insert_type  = $type;

		return $this;
	}

	/**
	 * Sets a special value (raw) for the insert-case. Note that this value would be used as is and therefore it should
	 * be already verified.
	 *
	 * @param $raw_insert
	 *
	 * @return $this
	 */
	public function insert_raw( $raw_insert ) {
		return $this->insert( $raw_insert, 'raw' );
	}

	/**
	 * Sets the value and the value type (%s, %f, %d) for the update-case.
	 *
	 * @param mixed $value
	 * @param string $type
	 *
	 * @return $this
	 */
	public function update( $value, $type = '%s' ) {
		if ( is_null( $value ) ) {
			$value = 'NULL';
			$type  = 'raw';
		}

		$this->_has_update   = TRUE;
		$this->_update_value = $value;
		$this->_update_type  = $type;

		return $this;
	}

	/**
	 * Sets a special value (raw) for the update-case. Note that this value would be used as is and therefore it should
	 * be already verified.
	 *
	 * @param $raw_update
	 *
	 * @return $this
	 */
	public function update_raw( $raw_update ) {
		return $this->update( $raw_update, 'raw' );
	}

	/**
	 * Sets the same value and the same value type (%s, %f, %d) for the insert- and update-cases.
	 *
	 * @param mixed $value
	 * @param string $type
	 *
	 * @return $this
	 */
	public function insert_and_update( $value, $type = '%s' ) {
		$this->insert( $value, $type );
		$this->update( $value, $type );

		return $this;
	}

}