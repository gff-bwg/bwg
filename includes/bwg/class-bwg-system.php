<?php

namespace bwg;

use bwg\cron\BWG_Cron_Manager;
use wpdb;

/**
 * Class BWG_System.
 *
 * @package bwg
 */
class BWG_System {

	/**
	 * The option name used to store the current bwg database version.
	 */
	const OPTION_BWG_DB_VERSION = 'bwg_db_version';

	/**
	 * Activates the plugin.
	 */
	public static function activate() {
		$installed_db_version = get_option( self::OPTION_BWG_DB_VERSION );
		if ( $installed_db_version !== BWG_DB_VERSION ) {
			self::update_schema();
		}

		// Register the bwg cron jobs.
		BWG_Cron_Manager::register_cron_jobs();

		// Register the custom post types (because they have to be registered for the recreation process of the rewrite rules).
		bwg_base()->evaluation_post_type_manager()->register_custom_post_type();

		// Flush and recreate rewrite rules.
		flush_rewrite_rules( FALSE );
	}

	/**
	 * Updates the database schema.
	 */
	public static function update_schema() {
		/** @var $wpdb wpdb */
		global $wpdb;

		$installed_db_version = get_option( self::OPTION_BWG_DB_VERSION );
		if ( $installed_db_version !== BWG_DB_VERSION ) {
			include_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			$charset_collate = $wpdb->get_charset_collate();

			$queries = [];
			$dir     = BWG_ABS_PATH . '/schema';
			$files   = glob( $dir . '/*.sql' );
			foreach ( $files as $file ) {
				$table_name = $wpdb->prefix . basename( $file, '.sql' );

				$sql_table = file_get_contents( $file );
				$sql_table = str_replace( '__TABLENAME__', $table_name, $sql_table );
				$sql_table .= $charset_collate . ';';

				array_push( $queries, $sql_table );
			}

			if ( version_compare( $installed_db_version, '1.5' ) <= 0 ) {
				self::remove_schema_table_columns( 'bwg_user_storage', [ 'id' ] );
			}

			// Perform the sql delta.
			dbDelta( $queries );

			update_option( self::OPTION_BWG_DB_VERSION, BWG_DB_VERSION );
		}
	}

	/**
	 * Removes the given columns (if present) from the database table. If the column is already
	 * removed, then this column would simply be ignored.
	 *
	 * @param string $table The table name (without prefix).
	 * @param array $remove_columns The column names to be removed.
	 */
	public static function remove_schema_table_columns( $table, $remove_columns = [] ) {
		/** @var $wpdb wpdb */
		global $wpdb;

		$table_name = $wpdb->prefix . $table;

		$existing_columns = $wpdb->get_col( 'DESC ' . $table_name, 0 );
		$remove_columns   = array_intersect( $remove_columns, $existing_columns );

		if ( ! empty( $remove_columns ) ) {
			$wpdb->query( 'ALTER TABLE ' . $table_name . ' DROP COLUMN ' .
			              implode( ', DROP COLUMN ', $remove_columns ) . ';' );
		}
	}

	/**
	 * Deactivates the plugin.
	 */
	public static function deactivate() {
		BWG_Cron_Manager::cancel_cron_jobs();

		flush_rewrite_rules( FALSE );
	}

	/**
	 * Uninstalls the plugin.
	 */
	public static function uninstall() {
		// delete all data (database, filesystem user files).
		// delete_option( self::OPTION_BWG_DB_VERSION );
	}
}
