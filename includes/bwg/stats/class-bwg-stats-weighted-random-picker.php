<?php

namespace bwg\stats;

/**
 * Class BWG_Stats_Weighted_Random_Picker.
 *
 * @package bwg\stats
 */
class BWG_Stats_Weighted_Random_Picker {

	/**
	 * @var array
	 */
	private $_objects = [];

	/**
	 * @var array
	 */
	private $_weights = [];

	/**
	 * @var array
	 */
	private $_lookup = [];

	/**
	 * @var int
	 */
	private $_total_weight = 0;


	/**
	 * Adds a new weighted object that can be picked afterwards.
	 *
	 * @param $obj
	 * @param $weight
	 */
	public function add( $obj, $weight ) {
		array_push( $this->_objects, $obj );
		array_push( $this->_weights, $weight );

		$this->_total_weight += $weight;
		array_push( $this->_lookup, $this->_total_weight );
	}

	/**
	 * binary_search()
	 * Search a sorted array for a number. Returns the item's index if found. Otherwise
	 * returns the position where it should be inserted, or count($haystack)-1 if the
	 * $needle is higher than every element in the array.
	 *
	 * @param int $needle
	 * @param array $haystack
	 *
	 * @return int
	 */
	private function _binary_search( $needle, $haystack ) {
		$high = count( $haystack ) - 1;
		$low  = 0;

		while ( $low < $high ) {
			$probe = (int) ( ( $high + $low ) / 2 );
			if ( $haystack[ $probe ] < $needle ) {
				$low = $probe + 1;
			} else if ( $haystack[ $probe ] > $needle ) {
				$high = $probe - 1;
			} else {
				return $probe;
			}
		}

		if ( $low != $high ) {
			return isset( $probe ) ? $probe : (int) ( ( $high + $low ) / 2 );
		} else {
			if ( $haystack[ $low ] >= $needle ) {
				return $low;
			} else {
				return $low + 1;
			}
		}
	}

	/**
	 * Picks randomly a weighted object.
	 *
	 * @param bool $remove
	 *
	 * @return mixed
	 */
	public function pick( $remove = TRUE ) {
		$r = mt_rand( 0, $this->_total_weight );

		$idx = $this->_binary_search( $r, $this->_lookup );

		$obj = $this->_objects[ $idx ];
		if ( $remove ) {
			$this->_total_weight -= $this->_weights[ $idx ];

			for ( $i = count( $this->_lookup ) - 1; $i > $idx; $i -- ) {
				$this->_lookup[ $i ] -= $this->_weights[ $idx ];
			}

			array_splice( $this->_lookup, $idx, 1 );
			array_splice( $this->_objects, $idx, 1 );
			array_splice( $this->_weights, $idx, 1 );
		}

		return $obj;
	}

}
