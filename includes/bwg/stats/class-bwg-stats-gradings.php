<?php

namespace bwg\stats;

/**
 * Class BWG_Stats_Gradings.
 *
 * @package bwg\stats
 */
class BWG_Stats_Gradings {

	/**
	 * @var array
	 */
	private $averages = [];

	/**
	 * @var array
	 */
	private $standard_deviations = [];

	/**
	 * @var array
	 */
	private $histograms = [];

	/**
	 * @var int
	 */
	private $count;


	/**
	 * Adds the average for a "statement".
	 *
	 * @param int $id
	 * @param float $value
	 */
	public function add_average( $id, $value ) {
		$this->averages[ '#' . $id ] = $value;
	}

	/**
	 * Adds the standard deviation for a "statement".
	 *
	 * @param int $id
	 * @param float $value
	 */
	public function add_standard_deviation( $id, $value ) {
		$this->standard_deviations[ '#' . $id ] = $value;
	}

	/**
	 * Adds a histogram value for a grading value for a "statement".
	 *
	 * @param int $id
	 * @param int $grading
	 * @param int $value
	 */
	public function add_histogram( $id, $grading, $value ) {
		$k = '#' . $id;
		if ( ! array_key_exists( $k, $this->histograms ) ) {
			$this->histograms[ $k ] = [];
		}

		$this->histograms[ $k ][ $grading ] = $value;
	}

	/**
	 * Gets the average given a statement id.
	 *
	 * @param int $id
	 *
	 * @return float Defaults to 0.
	 */
	public function get_average( $id ) {
		$k = '#' . $id;
		if ( array_key_exists( $k, $this->averages ) ) {
			return $this->averages[ $k ];
		}

		return 0;
	}

	/**
	 * Gets the standard deviation given a statement id.
	 *
	 * @param int $id
	 *
	 * @return float Defaults to 0.
	 */
	public function get_standard_deviation( $id ) {
		$k = '#' . $id;
		if ( array_key_exists( $k, $this->standard_deviations ) ) {
			return $this->standard_deviations[ $k ];
		}

		return 0;
	}

	public function get_histogram( $id, $from = 1, $to = 6 ) {
		$histogram = [];
		$k         = '#' . $id;
		for ( $i = $from; $i <= $to; $i ++ ) {
			if ( array_key_exists( $k, $this->histograms ) && array_key_exists( $i, $this->histograms[ $k ] ) ) {
				$histogram[ $i ] = $this->histograms[ $k ][ $i ];
			} else {
				$histogram[ $i ] = 0;
			}
		}

		return $histogram;
	}

	/**
	 * Sets the count.
	 *
	 * @param int $count
	 */
	public function set_count( $count ) {
		$this->count = $count;
	}

	/**
	 * Gets the count.
	 *
	 * @return int
	 */
	public function get_count() {
		return $this->count;
	}
}