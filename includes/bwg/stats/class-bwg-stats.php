<?php

namespace bwg\stats;

use bwg\BWG_Base;
use bwg\evaluation\BWG_Evaluation_Post;
use bwg\stats\strata\BWG_Stats_Strata_Collection;

/**
 * Class BWG_Stats
 *
 * @package bwg\stats
 */
class BWG_Stats {

	/**
	 * @var \bwg\stats\strata\BWG_Stats_Strata_Collection
	 */
	private $strata_collection;

	/**
	 * @var array
	 */
	private $strata_gradings;

	/**
	 * @var array
	 */
	private $strata_statistics;


	/**
	 * BWG_Stats constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
	 * @param \bwg\stats\strata\BWG_Stats_Strata_Collection $strata_collection
	 */
	public function __construct(
		BWG_Base $bwg_base,
		BWG_Evaluation_Post $evaluation_post,
		BWG_Stats_Strata_Collection $strata_collection
	) {
		$this->strata_collection = $strata_collection;

		// Get the submissions database helper.
		$submissions_database_helper = $bwg_base->submissions_database_helper();

		// Get the weighted gradings for all strata given.
		$strata_gradings   = [];
		$strata_statistics = [];
		foreach ( $strata_collection->get_stratas() as $strata ) {
			array_push( $strata_gradings, [
				'color'    => $strata->get_color(),
				'gradings' => $submissions_database_helper->get_weight_gradings(
					$evaluation_post->get_id(),
					$evaluation_post->get_evaluation_definition(),
					$strata
				),
			] );

			array_push( $strata_statistics, [
				'strata'     => $strata,
				'statistics' => $submissions_database_helper->get_statistics(
					$evaluation_post->get_id(),
					$evaluation_post->get_evaluation_definition(),
					$strata
				),
			] );
		}

		$this->strata_gradings   = $strata_gradings;
		$this->strata_statistics = $strata_statistics;
	}

	/**
	 * @return \bwg\stats\strata\BWG_Stats_Strata_Collection
	 */
	public function get_strata_collection() {
		return $this->strata_collection;
	}

	/**
	 * @return array
	 */
	public function get_strata_gradings() {
		return $this->strata_gradings;
	}

	/**
	 * @return array
	 */
	public function get_strata_statistics() {
		return $this->strata_statistics;
	}

}
