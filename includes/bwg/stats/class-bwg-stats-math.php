<?php

namespace bwg\stats;

use bwg\BWG_Base;

/**
 * Class BWG_Stats_Math.
 *
 * @package bwg\stats
 */
class BWG_Stats_Math {

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Stats_Math constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;
	}

	/**
	 * Calculates the binomial coefficient. Make sure that $n >= $k.
	 *
	 * @param int $n
	 * @param int $k
	 *
	 * @return int
	 */
	public function binomial_coef( $n, $k ) {
		$ans = 1;
		$k   = $k > $n - $k ? $n - $k : $k;
		$j   = 1;
		for ( ; $j <= $k; $j ++, $n -- ) {
			if ( 0 === $n % $j ) {
				$ans *= $n / $j;
			} elseif ( 0 === $ans % $j ) {
				$ans = $ans / $j * $n;
			} else {
				$ans = ( $ans * $n ) / $j;
			}
		}

		return $ans;
	}

	/**
	 * Calculates the binomial coefficient using gmp. Make sure that $n >= $k.
	 *
	 * @param int $n
	 * @param int $k
	 *
	 * @return resource A GMP resource.
	 */
	public function gmp_binomial_coef( $n, $k ) {
		$ans = gmp_init( 1 );

		$j = gmp_init( 1 );
		$k = gmp_init( $k > $n - $k ? $n - $k : $k );
		$n = gmp_init( $n );
		while ( gmp_cmp( $j, $k ) <= 0 ) {
			if ( 0 === gmp_cmp( gmp_mod( $n, $j ), 0 ) ) {
				$ans = gmp_mul( $ans, gmp_divexact( $n, $j ) );
			} elseif ( 0 === gmp_cmp( gmp_mod( $ans, $j ), 0 ) ) {
				$ans = gmp_mul( gmp_divexact( $ans, $j ), $n );
			} else {
				$ans = gmp_divexact( gmp_mul( $ans, $n ), $j );
			}

			$j = gmp_add( $j, '1' );
			$n = gmp_sub( $n, '1' );
		}

		return $ans;
	}

	/**
	 * Calculates the correlation between two datasets.
	 *
	 * @param array $x Array of integers
	 * @param array $y Array of integers. Should be the same size as $x.
	 *
	 * @return float
	 */
	public function correlation( $x, $y ) {
		$length = count( $x );
		$mean1  = array_sum( $x ) / $length;
		$mean2  = array_sum( $y ) / $length;

		$axb = 0;
		$a2  = 0;
		$b2  = 0;
		for ( $i = 0; $i < $length; $i ++ ) {
			$a   = $x[ $i ] - $mean1;
			$b   = $y[ $i ] - $mean2;
			$axb = $axb + ( $a * $b );
			$a2  = $a2 + pow( $a, 2 );
			$b2  = $b2 + pow( $b, 2 );
		}

		return $axb / sqrt( $a2 * $b2 );
	}

	/**
	 * Calculates the maximum possible variance for a data set given the number of data points and the lower and upper
	 * limits for all data points.
	 *
	 * @param int $amount
	 * @param int $lower_limit
	 * @param int $upper_limit
	 *
	 * @return float
	 */
	public function max_variance( $amount, $lower_limit = 1, $upper_limit = 6 ) {
		$v = pow( $upper_limit - $lower_limit, 2 );
		if ( 0 === $amount % 2 ) {
			// $amount is even.
			$v *= $amount / ( 4 * ( $amount - 1 ) );
		} else {
			// $amount is odd.
			$v *= ( $amount + 1 ) / ( 4 * $amount );
		}

		return $v;
	}

}