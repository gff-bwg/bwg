<?php

namespace bwg\stats\strata\selectors;

/**
 * Class BWG_Stats_Strata_Range_Selector.
 *
 * @package bwg\stats\strata\selectors
 */
class BWG_Stats_Strata_Range_Selector implements BWG_Stats_Strata_Selector_Interface {

	protected string $column_name;

	protected ?int $min_value;

	protected ?int $max_value;

	/**
	 * @param string $column_name
	 * @param int|null $min_value
	 * @param int|null $max_value
	 */
	public function __construct( string $column_name, ?int $min_value, ?int $max_value = NULL ) {
		$this->column_name = $column_name;
		$this->min_value   = $min_value;
		$this->max_value   = $max_value;
	}

	/**
	 * @inheritdoc
	 */
	public function sql_where( &$sql_args ) {
		if ( ! is_null( $this->min_value ) && ! is_null( $this->max_value ) ) {
			$sql_args[] = $this->min_value;
			$sql_args[] = $this->max_value;

			return $this->column_name . ' >= %d AND ' . $this->column_name . ' <= %d';
		} elseif ( ! is_null( $this->min_value ) ) {
			$sql_args[] = $this->min_value;

			return $this->column_name . ' >= %d';
		} elseif ( ! is_null( $this->max_value ) ) {
			$sql_args[] = $this->max_value;

			return $this->column_name . ' <= %d';
		} else {
			return '';
		}
	}
}
