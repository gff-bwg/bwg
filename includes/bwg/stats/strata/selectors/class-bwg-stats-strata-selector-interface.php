<?php

namespace bwg\stats\strata\selectors;

/**
 * Interface BWG_Stats_Strata_Selector_Interface.
 *
 * @package bwg\stats\strata\selectors
 */
interface BWG_Stats_Strata_Selector_Interface {

	/**
	 * Builds the sql where-query part for the strata.
	 *
	 * @param array $sql_args
	 *
	 * @return string
	 */
	public function sql_where( &$sql_args );

}