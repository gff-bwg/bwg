<?php

declare( strict_types=1 );

namespace bwg\stats\strata\selectors;

class BWG_Stats_Strata_Checkbox_Value_Selector implements BWG_Stats_Strata_Selector_Interface {

	protected string $column_name;

	protected string $value;

	public function __construct( string $column_name, string $value ) {
		$this->column_name = $column_name;
		$this->value       = $value;
	}

	/**
	 * @inheritDoc
	 */
	public function sql_where( &$sql_args ) {
		$sql_args[] = '%|' . $this->value . '|%';

		return $this->column_name . ' LIKE %s';
	}
}
