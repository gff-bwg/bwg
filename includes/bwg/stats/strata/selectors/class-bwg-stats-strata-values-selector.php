<?php


namespace bwg\stats\strata\selectors;


/**
 * Class BWG_Stats_Strata_Values_Selector.
 *
 * @package bwg\stats\strata\selectors
 */
class BWG_Stats_Strata_Values_Selector implements BWG_Stats_Strata_Selector_Interface {

	/**
	 * @var string
	 */
	protected string $column_name;

	/**
	 * @var string[]
	 */
	protected array $values;

	/**
	 * @param string $column_name
	 * @param string[] $values
	 */
	public function __construct( string $column_name, array $values ) {
		$this->column_name = $column_name;
		$this->values      = $values;
	}

	/**
	 * @inheritDoc
	 */
	public function sql_where( &$sql_args ) {
		$where = '';
		foreach ( $this->values as $value ) {
			$sql_args[] = $value;
			if ( ! empty( $where ) ) {
				$where .= ' OR ';
			}

			$where .= $this->column_name . ' = %s';
		}

		return $where;
	}
}
