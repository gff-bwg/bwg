<?php

namespace bwg\stats\strata\selectors;

/**
 * Class BWG_Stats_Strata_Value_Selector.
 *
 * @package bwg\stats\strata\selectors
 */
class BWG_Stats_Strata_Value_Selector implements BWG_Stats_Strata_Selector_Interface {

	protected string $column_name;

	protected string $value;

	/**
	 * @param string $column_name
	 * @param string $value
	 */
	public function __construct( string $column_name, string $value ) {
		$this->column_name = $column_name;
		$this->value       = $value;
	}

	/**
	 * @inheritdoc
	 */
	public function sql_where( &$sql_args ) {
		$sql_args[] = $this->value;

		return $this->column_name . ' = %s';
	}

}
