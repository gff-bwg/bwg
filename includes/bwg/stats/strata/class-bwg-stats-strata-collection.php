<?php

namespace bwg\stats\strata;

use bwg\stats\strata\selectors\BWG_Stats_Strata_Selector_Interface;

/**
 * Class BWG_Stats_Strata_Collection.
 *
 * @package bwg\stats\strata
 */
class BWG_Stats_Strata_Collection {

	/**
	 * @var \bwg\stats\strata\BWG_Stats_Strata[]
	 */
	private $stratas = [];

	/**
	 * @var array
	 */
	private $colors = [ '#ff9000', '#508FBB', '#FF8D63', '#FFBB63', '#FFD763', '#5F62C6', '#4DC48E' ];

	/**
	 * BWG_Stats_Strata_Collection constructor.
	 */
	public function __construct() {

	}

	/**
	 * Builds and adds a new strata to the strata collection.
	 *
	 * @param string $label
	 * @param BWG_Stats_Strata_Selector_Interface[] $selectors
	 */
	public function build_and_add_strata( $label, $selectors ) {
		$strata = new BWG_Stats_Strata(
			$label,
			$this->colors[ count( $this->stratas ) % count( $this->colors ) ],
			$selectors
		);

		array_push( $this->stratas, $strata );
	}

	/**
	 * @return \bwg\stats\strata\BWG_Stats_Strata[]
	 */
	public function get_stratas() {
		return $this->stratas;
	}

}
