<?php

namespace bwg\stats\strata;

/**
 * Class BWG_Stats_Strata.
 *
 * @package bwg\stats\strata
 */
class BWG_Stats_Strata {

	/**
	 * @var string
	 */
	private $label;

	/**
	 * @var array|\bwg\stats\strata\selectors\BWG_Stats_Strata_Selector_Interface[]
	 */
	private $selectors;

	/**
	 * @var string
	 */
	private $color;


	/**
	 * BWG_Stats_Strata constructor.
	 *
	 * @param string $label The label.
	 * @param string $color The color in '#RRGGBB' format.
	 * @param \bwg\stats\strata\selectors\BWG_Stats_Strata_Selector_Interface[] $selectors
	 */
	public function __construct( $label, $color, $selectors = [] ) {
		$this->label     = $label;
		$this->selectors = $selectors;
		$this->color     = $color;
	}

	/**
	 * Builds the sql where query part.
	 *
	 * @param array $sql_args
	 *
	 * @return string
	 */
	public function sql_where( &$sql_args ) {
		$where = '';
		foreach ( $this->selectors as $selector ) {
			$s = $selector->sql_where( $sql_args );
			if ( empty( $s ) ) {
				continue;
			}

			if ( ! empty( $where ) ) {
				$where .= ' AND ';
			}

			$where .= '(' . $s . ')';
		}

		return $where;
	}

	/**
	 * Gets the label for this strata.
	 *
	 * @return string
	 */
	public function get_label() {
		return $this->label;
	}

	/**
	 * Gets the color.
	 *
	 * @return string The color in '#RRGGBB' format.
	 */
	public function get_color() {
		return $this->color;
	}

}
