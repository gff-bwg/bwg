<?php

namespace bwg\stats\strata;

use bwg\BWG_Base;
use bwg\evaluation\BWG_Evaluation_Post;

/**
 * Class BWG_Stats_Strata_Utils.
 *
 * @package bwg\stats\strata
 */
class BWG_Stats_Strata_Utils {

	/**
	 * @var BWG_Base
	 */
	protected BWG_Base $bwg_base;


	/**
	 * @param BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->bwg_base = $bwg_base;
	}

	/**
	 * Get strata collection given a strata field uid.
	 *
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
	 * @param string|false $strata_field_uid
	 *
	 * @return \bwg\stats\strata\BWG_Stats_Strata_Collection
	 */
	public function get_strata_collection( BWG_Evaluation_Post $evaluation_post, $strata_field_uid = FALSE ): BWG_Stats_Strata_Collection {
		$strata_collection = new BWG_Stats_Strata_Collection();
		if ( $strata_field_uid ) {
			// Get the profile fields.
			$profile_fields = $evaluation_post->get_profile_fields();

			// Get the profile field used for the strata.
			$strata_field = $profile_fields->get_field_by_uid( $strata_field_uid );

			$strata_field->fill_strata_collection(
				$this->bwg_base->profile_field_type_registry(),
				$strata_collection
			);
		} else {
			$strata_collection->build_and_add_strata( 'Alle', [] );
		}

		return $strata_collection;
	}
}
