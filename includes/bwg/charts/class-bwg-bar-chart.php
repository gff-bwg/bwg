<?php

namespace bwg\charts;

/**
 * Class BWG_Bar_Chart
 *
 * @package bwg\charts
 */
class BWG_Bar_Chart extends BWG_Abstract_Chart {

	/**
	 * Gets the svg of the chart.
	 *
	 * @param bool $xmlHeader
	 *
	 * @return string
	 */
	public function get_svg( $xmlHeader = TRUE ) {
		$svg = ( $xmlHeader ? $this->get_xml_header() : '' );
		$svg .= $this->_get_node( 'svg', [
			'id'                  => $this->id(),
			'xmlns'               => 'http://www.w3.org/2000/svg',
			'version'             => '2.0',
			'viewbox'             => '0 0 :w :h',
			'preserveAspectRatio' => 'xMidYMid meet',
			'width'               => $this->width_height_attr() ? ':w' : NULL,
			'height'              => $this->width_height_attr() ? ':h' : NULL,
		], FALSE, [
			':w' => $this->round( $this->width() ),
			':h' => $this->round( $this->height() ),
		] );


		$svg .= '</svg>';

		return $svg;
	}

	/**
	 * Gets the default settings for this chart.
	 *
	 * @return array
	 */
	protected function _default_settings() {
		return [
			'id'        => uniqid( 'bwg-line-' ),
			'margin'    => [
				'left'   => 50,
				'right'  => 50,
				'top'    => 50,
				'bottom' => 50,
			],
			'chart'     => [
				'x_axis' => [
				],
				'y_axis' => [
					'min'           => 1,
					'max'           => 6,
					'main_interval' => 1,
					'sub_interval'  => .5,
					'label'         => [
						'style'  => new BWG_Chart_Styles( [
							'font-size'   => '8pt',
							'font-family' => '"Lato",verdana,sans-serif',
							'font-weight' => '300',
							'fill'        => '#999999',
						] ),
						'offset' => 8,
					],
				],
			],
			'title'     => [
				'text'  => 'Title',
				'style' => new BWG_Chart_Styles( [
					'font-size'   => '12pt',
					'font-family' => '"Lato",verdana,sans-serif',
					'font-weight' => 'bold',
					'fill'        => '#000000',
				] ),
			],
			'copyright' => [
				'text'  => '© ' . date( 'Y' ) . ' – beyond-wild-guess.ch',
				'style' => new BWG_Chart_Styles( [
					'font-size'   => '9pt',
					'font-family' => '"Lato",verdana,sans-serif',
					'font-weight' => '300',
					'fill'        => '#666666',
				] ),
			],
		];
	}
}