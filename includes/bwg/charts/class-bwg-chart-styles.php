<?php

namespace bwg\charts;

/**
 * Class BWG_Chart_Styles.
 */
class BWG_Chart_Styles {

	/**
	 * @var array The styles array.
	 */
	protected $_styles;

	/**
	 * BWG_Chart_Styles constructor.
	 *
	 * @param array $styles
	 */
	public function __construct( $styles = [] ) {
		$this->_styles = $styles;
	}

	/**
	 * Overwrites the given styles. In case a style does not exist, it would simply be created. If you set a style
	 * to NULL, then this style would be removed.
	 *
	 * @param array $styles
	 */
	public function overwrite( $styles = [] ) {
		foreach ( $styles as $k => $v ) {
			if ( is_null( $v ) ) {
				unset( $this->_styles[ $k ] );
			} else {
				$this->_styles[ $k ] = $v;
			}
		}
	}

	/**
	 * Gets the styles array.
	 *
	 * @return array
	 */
	public function get_styles() {
		return $this->_styles;
	}

	/**
	 * Gets the string representing the style-attribute value.
	 *
	 * @param \bwg\charts\BWG_Chart_Styles|null $overwrite
	 *
	 * @return string
	 */
	public function toString( BWG_Chart_Styles $overwrite = NULL ) {
		$styles = $this->get_styles();
		if ( ! is_null( $overwrite ) ) {
			foreach ( $overwrite->get_styles() as $k => $v ) {
				if ( is_null( $v ) ) {
					unset( $styles[ $k ] );
				} else {
					$styles[ $k ] = $v;
				}
			}
		}

		$style_items = [];
		foreach ( $styles as $k => $v ) {
			array_push( $style_items, $k . ':' . $v );
		}

		return implode( ';', $style_items );
	}

}
