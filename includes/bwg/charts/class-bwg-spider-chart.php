<?php

namespace bwg\charts;

/**
 * Class BWG_Spider_Chart.
 */
class BWG_Spider_Chart extends BWG_Abstract_Chart {

	const LEG_ROTATION_NONE = 'none';
	const LEG_ROTATION_HALF = 'half';

	/**
	 * @var array
	 */
	private $_legs = [];

	/**
	 * @var array
	 */
	private $_series = [];

	/**
	 * @var int
	 */
	private $_nr_legs;

	/**
	 * @var int
	 */
	private $_nr_series;

	/**
	 * @var float
	 */
	private $_alpha;

	/**
	 * @var string
	 */
	private $_svg_group = '';

	/**
	 * @var string
	 */
	private $_svg_group_defs = '';


	/**
	 * @inheritdoc
	 */
	protected function _default_settings() {
		$bwg_globals = bwg_base()->globals();

		// 130 is the maximum width a label can occupy (well it can occupy
		// more than that, but 130 is the optimum).
		$default_radius = min(
			( $bwg_globals->spider_chart_height() - 100 ) / 2,
			( $bwg_globals->spider_chart_width() - 2 * 130 ) / 2
		);

		return [
			'id'                => uniqid( 'bwg-spider-' ),
			'width'             => $bwg_globals->spider_chart_width(),
			'height'            => $bwg_globals->spider_chart_height(),
			'precision'         => 5,
			'width_height_attr' => FALSE,
			'spider'            => [
				'center' => [
					'x'           => $bwg_globals->spider_chart_width() / 2,
					'y'           => $bwg_globals->spider_chart_height() / 2,
					'disc_radius' => 4,
				],
				'radius' => $default_radius,
				'leg'    => [
					'rotation' => self::LEG_ROTATION_NONE,
					'jut'      => 5,
					'label'    => [
						'style' => new BWG_Chart_Styles( [
							'font-size'   => '10pt',
							'font-family' => '"Lato",verdana,sans-serif',
							'font-weight' => '300',
						] ),
					],
				],
				'series' => [
					'disc_radius' => 3,
				],
				'y_axis' => [
					'min'           => 1,
					'max'           => 6,
					'main_interval' => 1,
					'sub_interval'  => .5,
					'label'         => [
						'style'  => new BWG_Chart_Styles( [
							'font-size'   => '8pt',
							'font-family' => '"Lato",verdana,sans-serif',
							'font-weight' => '300',
							'fill'        => '#999999',
						] ),
						'offset' => 8,
					],
				],
			],
			'title'             => [
				'text'  => 'Title',
				'style' => new BWG_Chart_Styles( [
					'font-size'   => '12pt',
					'font-family' => '"Lato",verdana,sans-serif',
					'font-weight' => 'bold',
					'fill'        => '#000000',
				] ),
			],
			'copyright'         => [
				'text'  => '© ' . date( 'Y' ) . ' – beyond-wild-guess.ch',
				'style' => new BWG_Chart_Styles( [
					'font-size'   => '9pt',
					'font-family' => '"Lato",verdana,sans-serif',
					'font-weight' => '300',
					'fill'        => '#666666',
				] ),
			],
		];
	}


	/**
	 * Adds a new leg.
	 *
	 * @param string $label The label (note that the char '|' would be treated as new line).
	 *
	 * @return $this
	 */
	public function add_leg( $label ) {
		array_push( $this->_legs, [ 'label' => $label ] );

		return $this;
	}

	/**
	 * Adds a new series.
	 *
	 * @param array $data_points
	 * @param \bwg\charts\BWG_Chart_Styles|null $area_style
	 * @param \bwg\charts\BWG_Chart_Styles|null $dot_style
	 *
	 * @return $this
	 */
	public function add_series( $data_points, $area_style = NULL, $dot_style = NULL ) {
		array_push( $this->_series, [
			'data_points' => $data_points,
			'area_style'  => $area_style,
			'dot_style'   => $dot_style
		] );

		return $this;
	}


	/**
	 * Gets the spider radius.
	 *
	 * @return float
	 */
	public function spider_radius() {
		return $this->_settings['spider.radius'];
	}

	/**
	 * Gets the x-coordinate of the center of the spider.
	 *
	 * @return float
	 */
	public function spider_center_x() {
		return $this->_settings['spider.center.x'];
	}

	/**
	 * Gets the y-coordinate of the center of the spider.
	 *
	 * @return float
	 */
	public function spider_center_y() {
		return $this->_settings['spider.center.y'];
	}

	/**
	 * Gets the center disc radius of the spider.
	 *
	 * @return float
	 */
	public function spider_center_disc_radius() {
		return $this->_settings['spider.center.disc_radius'];
	}

	/**
	 * @return string
	 */
	public function spider_leg_rotation() {
		return $this->_settings['spider.leg.rotation'];
	}

	/**
	 * Gets the leg jut of the spider.
	 *
	 * @return float
	 */
	public function spider_leg_jut() {
		return $this->_settings['spider.leg.jut'];
	}

	/**
	 * @return BWG_Chart_Styles
	 */
	public function spider_leg_label_style() {
		return $this->_settings['spider.leg.label.style'];
	}

	/**
	 * Gets the y-min data value for the spider.
	 *
	 * @return float
	 */
	public function spider_y_min() {
		return $this->_settings['spider.y_axis.min'];
	}

	/**
	 * Gets the y-max data value for the spider.
	 *
	 * @return float
	 */
	public function spider_y_max() {
		return $this->_settings['spider.y_axis.max'];
	}

	/**
	 * @return float
	 */
	public function spider_y_main_interval() {
		return $this->_settings['spider.y_axis.main_interval'];
	}

	/**
	 * @return float
	 */
	public function spider_y_sub_interval() {
		return $this->_settings['spider.y_axis.sub_interval'];
	}

	/**
	 * @return BWG_Chart_Styles
	 */
	public function spider_y_label_style() {
		return $this->_settings['spider.y_axis.label.style'];
	}

	/**
	 * @return float
	 */
	public function spider_y_label_offset() {
		return $this->_settings['spider.y_axis.label.offset'];
	}

	/**
	 * @return float
	 */
	public function spider_series_disc_radius() {
		return $this->_settings['spider.series.disc_radius'];
	}

	/**
	 * @return string
	 */
	public function title_text() {
		return $this->_settings['title.text'];
	}

	/**
	 * @return BWG_Chart_Styles
	 */
	public function title_style() {
		return $this->_settings['title.style'];
	}

	/**
	 * @return string
	 */
	public function copyright_text() {
		return $this->_settings['copyright.text'];
	}

	/**
	 * @return BWG_Chart_Styles
	 */
	public function copyright_style() {
		return $this->_settings['copyright.style'];
	}

	/**
	 * @param $radius
	 * @param $angle
	 *
	 * @return array
	 */
	protected function _polar_to_cartesian( $radius, $angle ) {
		$angleRad = deg2rad( $angle );

		return [
			'x' => $radius * cos( $angleRad ),
			'y' => $radius * sin( $angleRad ),
		];
	}

	/**
	 * @param $y
	 *
	 * @return float
	 */
	protected function _y_to_radius( $y ) {
		return $this->spider_radius() * ( $y - $this->spider_y_min() ) / ( $this->spider_y_max() - $this->spider_y_min() );
	}

	/**
	 * @param $leg
	 *
	 * @return float
	 */
	protected function _leg_to_angle( $leg ) {
		if ( self::LEG_ROTATION_NONE === $this->spider_leg_rotation() ) {
			return ( 270.0 + $leg * $this->_alpha ) % 360.0;
		}

		return ( 270.0 + $this->_alpha / 2 + $leg * $this->_alpha ) % 360.0;
	}

	protected function _get_series_leg_data( $series_index, $leg_index ) {
		if ( ! isset( $this->_series[ $series_index ] ) ) {
			return $this->spider_y_min();
		}
		if ( ! isset( $this->_series[ $series_index ]['data_points'][ $leg_index ] ) ) {
			return $this->spider_y_min();
		}

		$y = $this->_series[ $series_index ]['data_points'][ $leg_index ];

		return max( $this->spider_y_min(), min( $this->spider_y_max(), $y ) );
	}

	/**
	 * @param bool $xmlHeader
	 *
	 * @return string
	 */
	public function get_svg( $xmlHeader = TRUE ) {
		$this->_nr_legs   = count( $this->_legs );
		$this->_nr_series = count( $this->_series );
		$this->_alpha     = 360.0 / $this->_nr_legs;

		$this->_svg_group      = '';
		$this->_svg_group_defs = '';

		$svg = ( $xmlHeader ? $this->get_xml_header() : '' );
		$svg .= $this->_get_node( 'svg', [
			'id'                  => $this->id(),
			'xmlns'               => 'http://www.w3.org/2000/svg',
			'version'             => '2.0',
			'viewbox'             => '0 0 :w :h',
			'preserveAspectRatio' => 'xMidYMid meet',
			'width'               => $this->width_height_attr() ? ':w' : NULL,
			'height'              => $this->width_height_attr() ? ':h' : NULL,
		], FALSE, [
			':w' => $this->round( $this->width() ),
			':h' => $this->round( $this->height() ),
		] );

		// Print the title.
		$svg .= $this->_get_node( 'text', [
			'x'           => $this->round( $this->width() / 2 ),
			'y'           => $this->round( 20 ),
			'text-anchor' => 'middle',
			'style'       => $this->_esc_style( $this->title_style() ),
		], esc_html( $this->title_text() ) );

		// Print the copyright.
		$svg .= $this->_get_node( 'text', [
			'x'           => $this->round( $this->width() - 5 ),
			'y'           => $this->round( $this->height() - 5 ),
			'text-anchor' => 'end',
			'style'       => $this->_esc_style( $this->copyright_style() ),
		], esc_html( $this->copyright_text() ) );

		///
		// Print the group containing the actual spider chart.
		///
		$svg .= $this->_get_node( 'g', [
			'transform' => 'translate(' . $this->round( $this->spider_center_x() ) . ','
			               . $this->round( $this->spider_center_y() ) . ')',
		], FALSE );

		// Print the grid and legs.
		$this->_svg_grid_polygon();
		$this->_svg_legs_and_labels_polygon();

		// Print the data points.
		for ( $i = 0; $i < $this->_nr_series; $i ++ ) {
			$this->_svg_group .= '<g data-series="' . $i . '">';
			$this->_svg_data_points( $i );
			$this->_svg_data_point_discs( $i );
			$this->_svg_group .= '</g>';
		}

		// Print the center disc.
		$this->_svg_center_disc();

		//$svg .= '<defs>' . $this->_svg_group_defs . '</defs>';
		$svg .= $this->_svg_group;
		$svg .= '</g>';
		$svg .= '</svg>';

		return $svg;
	}

	/**
	 * Draws the y-grid and its labels as polygons.
	 */
	protected function _svg_grid_polygon() {
		$grid_label_number = 0;
		$svg_grid_labels   = '';

		for ( $y = $this->spider_y_min(); $y <= $this->spider_y_max(); $y += $this->spider_y_sub_interval() ) {
			$main_interval_division = ( $y - $this->spider_y_min() ) / $this->spider_y_main_interval();
			if ( FALSE !== filter_var( $main_interval_division, FILTER_VALIDATE_INT ) ) {
				// Main-Interval
				$style            = 'stroke-linecap:round;fill:none;stroke:#999;stroke-width:1px;';
				$is_main_interval = TRUE;
			} else {
				// Sub-Interval
				$style            = 'stroke-linecap:round;fill:none;stroke:#ccc;stroke-width:.2px;';
				$is_main_interval = FALSE;
			}

			$p = $this->_polar_to_cartesian(
				$this->_y_to_radius( $y ),
				$this->_leg_to_angle( $this->_nr_legs - 1 )
			);

			$p0        = $p;
			$path_grid = 'M' . $this->round( $p['x'] ) . ' ' . $this->round( $p['y'] );
			for ( $i = 0; $i < $this->_nr_legs; $i ++ ) {
				$p         = $this->_polar_to_cartesian( $this->_y_to_radius( $y ), $this->_leg_to_angle( $i ) );
				$path_grid .= 'L' . $this->round( $p['x'] ) . ' ' . $this->round( $p['y'] );
				if ( $is_main_interval && $i == 0 ) {
					$grid_label_number ++;

					$cx = $p0['x'] + ( $p['x'] - $p0['x'] ) / 2;
					$cy = $p0['y'] + ( $p['y'] - $p0['y'] ) / 2;
					if ( $y > $this->spider_y_min() ) {
						$alpha_rad = atan( ( $p['y'] - $p0['y'] ) / ( $p['x'] - $p0['x'] ) );
						$alpha     = rad2deg( $alpha_rad );

						$cx = $cx - sin( $alpha_rad ) * $this->spider_y_label_offset();
						$cy = $cy + cos( $alpha_rad ) * $this->spider_y_label_offset();

						$svg_grid_labels .=
							$this->_get_node( 'text', [
								'x'           => $this->round( $cx ),
								'y'           => $this->round( $cy ),
								'text-anchor' => 'middle',
								'transform'   => 'rotate(' . $this->round( $alpha ) . ' ' .
								                 $this->round( $cx ) . ' ' . $this->round( $cy ) . ')',
								'style'       => $this->_esc_style( $this->spider_y_label_style() ),
							], sprintf( '%.01f', $y ) );
					}
				}
			}

			$this->_svg_group .= '<path d="' . $path_grid . '" style="' . $style . '" />';
		}

		$this->_svg_group .= $svg_grid_labels;
	}

	protected function _svg_legs_and_labels_polygon( $leg_label_line_height = 1.1 ) {
		// Print the legs (as one path).
		$this->_svg_legs();

		$radius       = $this->spider_radius() + $this->spider_leg_jut() + 5;
		$text_dy_base = .35;

		$label_line_height_pixels = 15;

		// Print the leg labels.
		for ( $leg = 0; $leg < $this->_nr_legs; $leg ++ ) {
			$angle = $this->_leg_to_angle( $leg % $this->_nr_legs );

			$text_lines = explode( '|', $this->_legs[ $leg ]['label'] );
			$text_lines = array_map( 'trim', $text_lines );
			$text_lines = array_map( 'esc_html', $text_lines );

			$p = $this->_polar_to_cartesian( $radius, $angle );

			$nr_text_lines = count( $text_lines );
			if ( abs( $p['x'] ) < 2 ) {
				$text_anchor = 'middle';

				if ( $p['y'] > 0 ) { // Bottom Leg.
					$dy = 0.5;
				} else { // Top Leg.
					$dy = - ( $nr_text_lines - 1 ) * $leg_label_line_height;
				}
			} else {
				if ( $p['x'] > 0 ) {
					$text_anchor = 'start';
				} else {
					$text_anchor = 'end';
				}

				$dy = $text_dy_base - 0.5 * ( $nr_text_lines - 1 ) * $leg_label_line_height;
			}

			$this->_svg_group .= $this->_get_node( 'text', [
				'style'       => $this->_esc_style( $this->spider_leg_label_style() ),
				'x'           => $this->round( $p['x'] ),
				'y'           => $this->round( $p['y'] ),
				'text-anchor' => $text_anchor,
				'transform'   => 'translate(0, ' . $this->round( $label_line_height_pixels * $dy ) . ')',
			], FALSE );

			if ( 1 === $nr_text_lines ) {
				$this->_svg_group .= $text_lines[0];
			} else {
				for ( $line = 0; $line < $nr_text_lines; $line ++ ) {
					$this->_svg_group .= $this->_get_node( 'tspan', [
						'x'  => $this->round( $p['x'] ),
						'dy' => $line > 0 ? $leg_label_line_height . 'em' : NULL,
					], $text_lines[ $line ] );
				}
			}
			$this->_svg_group .= '</text>';
		}
	}

	/**
	 * The legs.
	 */
	protected function _svg_legs() {
		$path_legs = '';
		for ( $i = 0; $i < $this->_nr_legs; $i ++ ) {
			$angle = $this->_leg_to_angle( $i );

			$p_outer   = $this->_polar_to_cartesian( $this->spider_radius() + $this->spider_leg_jut(), $angle );
			$path_legs .= 'M0 0L' . $this->round( $p_outer['x'] ) . ' ' . $this->round( $p_outer['y'] );
		}

		$this->_svg_group .= $this->_get_node( 'path', [
			'd'     => $path_legs,
			'style' => 'stroke-linecap:round;stroke:#333;stroke-width:1px;fill:none;',
		], '' );
	}

	/**
	 * @param $series_index
	 */
	protected function _svg_data_points( $series_index ) {
		$style = new BWG_Chart_Styles( [
			'stroke-linecap' => 'round',
			'stroke'         => 'none',
			'fill'           => '#ff9000',
			'fill-opacity'   => 0.5,
		] );

		// Print the data point lines (with straight lines).
		$path_series = '';
		for ( $leg0 = 0; $leg0 < $this->_nr_legs; $leg0 ++ ) {
			$leg1 = ( $leg0 + 1 ) % $this->_nr_legs;

			$angle0  = $this->_leg_to_angle( $leg0 );
			$radius0 = $this->_y_to_radius( $this->_get_series_leg_data( $series_index, $leg0 ) );
			$angle1  = $this->_leg_to_angle( $leg1 );
			$radius1 = $this->_y_to_radius( $this->_get_series_leg_data( $series_index, $leg1 ) );

			$p0 = $this->_polar_to_cartesian( $radius0, $angle0 );
			$p1 = $this->_polar_to_cartesian( $radius1, $angle1 );

			if ( 0 === $leg0 ) {
				$path_series .= 'M' . $this->round( $p0['x'] ) . ' ' . $this->round( $p0['y'] );
			}
			$path_series .= 'L' . $this->round( $p1['x'] ) . ' ' . $this->round( $p1['y'] );
		}

		$this->_svg_group .= $this->_get_node( 'path', [
			'd'     => $path_series,
			'style' => $style->toString( $this->_series[ $series_index ]['area_style'] ),
		], '' );
	}

	/**
	 * @param $series_index
	 */
	protected function _svg_data_point_discs( $series_index ) {
		$style = new BWG_Chart_Styles( [
			'stroke'       => '#000000',
			'stroke-width' => '1px',
			'fill'         => '#ff9000',
		] );

		for ( $leg = 0; $leg < $this->_nr_legs; $leg ++ ) {
			$p = $this->_polar_to_cartesian(
				$this->_y_to_radius( $this->_get_series_leg_data( $series_index, $leg ) ),
				$this->_leg_to_angle( $leg )
			);

			$this->_svg_group .= $this->_get_node( 'circle', [
				'cx'    => $this->round( $p['x'] ),
				'cy'    => $this->round( $p['y'] ),
				'r'     => $this->round( $this->spider_series_disc_radius() ),
				'style' => $style->toString( $this->_series[ $series_index ]['dot_style'] ),
			], '<title>' . esc_html( str_replace( '|', ' ', $this->_legs[ $leg ]['label'] ) ) . ': '
			   . $this->round( $this->_get_series_leg_data( $series_index, $leg ), 1 ) . '</title>' );
		}
	}

	/**
	 * Build the svg center disc.
	 */
	protected function _svg_center_disc() {
		// Print the center disc.
		$this->_svg_group .= $this->_get_node( 'circle', [
			'cx'    => '0',
			'cy'    => '0',
			'r'     => $this->round( $this->spider_center_disc_radius() ),
			'style' => 'stroke:#333333;stroke-width:1px;fill:#ffffff',
		], '' );
	}

}
