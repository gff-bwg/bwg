<?php

namespace bwg\charts;

use bwg\BWG_Base;
use bwg\evaluation\BWG_Evaluation_Post;

/**
 * Class BWG_Chart_Factory.
 *
 * @package bwg\charts
 */
final class BWG_Chart_Factory {

	/**
	 * @var \bwg\BWG_Base
	 */
	private $bwg_base;


	/**
	 * BWG_Chart_Factory constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->bwg_base = $bwg_base;
	}

	/**
	 * Creates the spider charts used in the frontend if a user wants to see his submitted evaluation.
	 *
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation
	 * @param array $gradings
	 *
	 * @return \bwg\charts\BWG_Spider_Chart[]
	 */
	public function create_frontend_submitted_spider_charts( BWG_Evaluation_Post $evaluation, array $gradings ) {
		$spider_charts = [];

		foreach ( $evaluation->get_evaluation_definition()->get_items() as $item1 ) {
			$spider_chart = $this->bwg_base->factory()->bwg_spider_chart( [
				'title'     => [
					'text'  => $item1->get_label( FALSE ),
					'style' => new BWG_Chart_Styles( [ 'font-size' => '16pt', ] ),
				],
				'spider'    => [
					'center' => [
						'disc_radius' => 6,
					],
					'leg'    => [
						'label' => [
							'style' => new BWG_Chart_Styles( [ 'font-size' => '14pt', ] ),
						],
					],
					'y_axis' => [
						'label' => [
							'style'  => new BWG_Chart_Styles( [ 'font-size' => '10pt', ] ),
							'offset' => - 3,
						],
					],
					'series' => [
						'disc_radius' => 5,
					],
				],
				'copyright' => [
					'style' => new BWG_Chart_Styles( [ 'font-size' => '11pt', ] ),
				],
			] );

			$data_points = [];
			foreach ( $item1->get_items() as $item2 ) {
				if ( 0 === $item2->get_spider_chart() ) {
					continue;
				}

				$spider_chart->add_leg( $item2->get_label( FALSE ) );
				$k = '#' . $item2->get_id();
				array_push( $data_points, array_key_exists( $k, $gradings ) ? $gradings[ $k ] : 1 );
			}

			$area_styles = new BWG_Chart_Styles( [ 'fill' => '#508FBB' ] );
			$dot_styles  = new BWG_Chart_Styles( [ 'fill' => '#508FBB', 'stroke' => '#508FBB' ] );
			$spider_chart->add_series( $data_points, $area_styles, $dot_styles );

			$spider_charts[ '#' . $item1->get_id() ] = $spider_chart;
		}

		return $spider_charts;
	}

	/**
	 * Creates the interest spider charts as used in the backend in the stats view of the users section.
	 *
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation
	 * @param array $strata_gradings
	 *
	 * @return array
	 */
	public function create_interest_spider_charts( BWG_Evaluation_Post $evaluation, array $strata_gradings ) {
		$spider_charts = [];

		foreach ( $evaluation->get_evaluation_definition()->get_items() as $item1 ) {
			$spider_chart = $this->bwg_base->factory()->bwg_spider_chart( [
				'title' => [
					'text' => $item1->get_label( FALSE )
				]
			] );

			$leg_ids = [];
			foreach ( $item1->get_items() as $item2 ) {
				if ( 0 === $item2->get_spider_chart() ) {
					continue;
				}
				$spider_chart->add_leg( $item2->get_label( FALSE ) );
				array_push( $leg_ids, $item2->get_id() );
			}

			foreach ( $strata_gradings as $strata_grading ) {
				$data_points = [];
				foreach ( $leg_ids as $leg_id ) {
					array_push( $data_points, $strata_grading['gradings'][ '#' . $leg_id ] );
				}
				
				$color       = $strata_grading['color'];
				$area_styles = new BWG_Chart_Styles( [ 'fill' => $color ] );
				$dot_styles  = new BWG_Chart_Styles( [ 'fill' => $color, 'stroke' => $color ] );
				$spider_chart->add_series( $data_points, $area_styles, $dot_styles );
			}

			$spider_charts[ '#' . $item1->get_id() ] = $spider_chart;
		}

		return $spider_charts;
	}

}