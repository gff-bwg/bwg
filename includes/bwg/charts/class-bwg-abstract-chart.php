<?php

namespace bwg\charts;

/**
 * Class BWG_Abstract_Chart
 *
 * @package bwg\charts
 */
abstract class BWG_Abstract_Chart {

	/**
	 * @var array
	 */
	protected $_settings = [];

	/**
	 * Chart constructor.
	 *
	 * @param array $settings
	 */
	public function __construct( $settings = [] ) {
		$this->_ow_settings( [
			'id'                => uniqid( 'bwg-chart-' ),
			'width'             => 900,
			'height'            => 600,
			'width_height_attr' => FALSE,
			'precision'         => 5,
		] );
		$this->_ow_settings( $this->_default_settings() );
		$this->_ow_settings( $settings );
	}

	/**
	 * Gets the svg of the chart.
	 *
	 * @param bool $xmlHeader
	 *
	 * @return string
	 */
	abstract public function get_svg( $xmlHeader = TRUE );

	/**
	 * Gets the default settings for this chart.
	 *
	 * @return array
	 */
	abstract protected function _default_settings();

	/**
	 * Build the settings (allow overwrites).
	 *
	 * @param $settings
	 * @param string $prefix
	 */
	protected function _ow_settings( $settings, $prefix = '' ) {
		foreach ( $settings as $key => $value ) {
			$k = ( empty( $prefix ) ? '' : $prefix . '.' ) . $key;

			if ( isset( $this->_settings[ $k ] ) ) {
				if ( $this->_settings[ $k ] instanceof BWG_Chart_Styles ) {
					/** @var BWG_Chart_Styles $styles */
					$styles = $this->_settings[ $k ];
					if ( $value instanceof BWG_Chart_Styles ) {
						$styles->overwrite( $value->get_styles() );
					} else {
						$styles->overwrite( $value );
					}
					continue;
				}
			}

			if ( is_array( $value ) ) {
				$this->_ow_settings( $value, $k );
			} else {
				$this->_settings[ $k ] = $value;
			}
		}
	}

	/**
	 * Gets the id of this chart.
	 *
	 * @return string
	 */
	public function id() {
		return $this->_settings['id'];
	}

	/**
	 * Gets the width of this chart.
	 *
	 * @return float
	 */
	public function width() {
		return $this->_settings['width'];
	}

	/**
	 * Gets the height of this chart.
	 *
	 * @return float
	 */
	public function height() {
		return $this->_settings['height'];
	}

	/**
	 * @return bool
	 */
	public function width_height_attr() {
		return $this->_settings['width_height_attr'];
	}

	/**
	 * Gets the precision, that is the number of decimals.
	 *
	 * @return int
	 */
	public function precision() {
		return $this->_settings['precision'];
	}

	/**
	 * @param number $value
	 * @param int|null $precision
	 *
	 * @return float
	 */
	public function round( $value, $precision = NULL ) {
		if ( is_null( $precision ) ) {
			return round( $value, $this->precision() );
		}

		return round( $value, $precision );
	}

	/**
	 * Escapes the given style.
	 *
	 * @param string|\bwg\charts\BWG_Chart_Styles $styles
	 * @param bool $as_attr
	 *
	 * @return string
	 */
	protected function _esc_style( $styles, $as_attr = TRUE ) {
		if ( $styles instanceof BWG_Chart_Styles ) {
			$styles = $styles->toString();
		}
		
		if ( $as_attr ) {
			return esc_attr( str_replace( [ '{', '}' ], '', $styles ) );
		}

		return str_replace( [ '{', '}' ], '', $styles );
	}

	public function get_xml_header() {
		return '<?xml version="1.0" encoding="UTF-8" ?>';
	}

	protected function _get_node( $name, $attributes, $content = FALSE, $placeholders = [] ) {
		$svg = '<' . $name;
		foreach ( $attributes as $key => $value ) {
			if ( is_null( $value ) ) {
				continue;
			}

			$svg .= ' ' . $key . '="' . $value . '"';
		}

		if ( FALSE === $content ) {
			$svg .= '>';
		} elseif ( empty( $content ) ) {
			$svg .= '/>';
		} else {
			$svg .= '>' . $content . '</' . $name . '>';
		}

		if ( empty( $placeholders ) ) {
			return $svg;
		}

		return str_replace( array_keys( $placeholders ), array_values( $placeholders ), $svg );
	}

}