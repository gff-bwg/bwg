<?php

namespace bwg\feup;

use bwg\BWG_Base;
use FEUP_User;
use wpdb;

/**
 * Class BWG_FEUP_Layer.
 */
class BWG_FEUP_Layer {

	protected BWG_Base $bwg_base;

	protected FEUP_User $feup_user;

	/**
	 * @param BWG_Base $bwg_base
	 * @param FEUP_User $feup
	 */
	public function __construct( BWG_Base $bwg_base, FEUP_User $feup ) {
		$this->bwg_base  = $bwg_base;
		$this->feup_user = $feup;
	}

	/**
	 * Gets the 'ewd_feup_users' table name.
	 *
	 * @return string
	 * @var string $ewd_feup_user_table_name
	 */
	public function get_user_table_name(): string {
		global $ewd_feup_user_table_name;

		return $ewd_feup_user_table_name;
	}

	/**
	 * Gets the 'ewd_feup_fields' table name.
	 *
	 * @return string
	 * @var string $ewd_feup_fields_table_name
	 *
	 */
	public function get_fields_table_name(): string {
		global $ewd_feup_fields_table_name;

		return $ewd_feup_fields_table_name;
	}

	/**
	 * Gets the 'ewd_feup_user_fields' table name.
	 *
	 * @return string
	 * @var string $ewd_feup_user_fields_table_name
	 *
	 */
	public function get_user_fields_table_name(): string {
		global $ewd_feup_user_fields_table_name;

		return $ewd_feup_user_fields_table_name;
	}

	/**
	 * Gets the `ewd_feup_levels` table name.
	 *
	 * @return string
	 */
	public function get_levels_table_name(): string {
		global $ewd_feup_levels_table_name;

		return $ewd_feup_levels_table_name;
	}

	/**
	 * Gets a field value from the 'ewd_feup_users' table for a given user ID.
	 *
	 * @param int $user_ID
	 * @param string $field_name
	 *
	 * @return null|string
	 */
	public function get_user_table_field( int $user_ID, string $field_name ): ?string {
		/** @var $wpdb wpdb */
		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare(
			'SELECT `' . $field_name . '` FROM `' . $this->get_user_table_name() . '` WHERE `User_ID` = %d',
			$user_ID
		) );
	}

	/**
	 * Is the user email for the given user confirmed?
	 *
	 * @param int $user_ID
	 *
	 * @return bool
	 */
	public function is_user_email_confirmed( int $user_ID ): bool {
		return 'Yes' === $this->get_user_table_field( $user_ID, 'User_Email_Confirmed' );
	}

	/**
	 * Gets the feup fields.
	 *
	 * @return array|null
	 * @var \wpdb $wpdb
	 *
	 */
	public function get_fields(): ?array {
		global $wpdb;

		return $wpdb->get_results(
			'SELECT * FROM ' . $this->get_fields_table_name() . ' ORDER BY Field_Order ASC',
			ARRAY_A
		);
	}

	/**
	 * @param int $user_ID
	 *
	 * @return array
	 */
	public function get_user_field_replacements( int $user_ID ): array {
		$username = $this->feup_user->Get_User_Name_For_ID( $user_ID );
		if ( is_null( $username ) ) {
			$user_ID = 0;
		}

		$replacements = [];
		foreach ( $this->bwg_base->feup_layer()->get_fields() as $feup_field ) {
			if ( 0 === $user_ID ) {
				$replacements[ '[' . $feup_field['Field_Name'] . ']' ] = '{' . $feup_field['Field_Name'] . '}';
			} else {
				$replacements[ '[' . $feup_field['Field_Name'] . ']' ] = $this->feup_user->Get_Field_Value_For_ID(
					$feup_field['Field_Name'], $user_ID );
			}
		}

		return $replacements;
	}
}
