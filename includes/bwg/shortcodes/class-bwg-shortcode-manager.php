<?php

namespace bwg\shortcodes;

use bwg\BWG_Base;
use bwg\prodomo\BWG_Prodomo_Ajax;

/**
 * Class BWG_Shortcode.
 *
 * @package bwg\shortcodes
 */
class BWG_Shortcode_Manager {

	/**
	 * @var BWG_Base
	 */
	private $_bwg_base;

	/**
	 * BWG_Shortcode_Manager constructor.
	 *
	 * @param BWG_Base $bwg_base
	 */
	function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;

		add_shortcode( 'bwg_login', [ $this, 'bwg_login_callback' ] );
		add_shortcode( 'bwg_register', [ $this, 'bwg_register_callback' ] );
		add_shortcode( 'bwg_overview', [ $this, 'bwg_overview_callback' ] );
		add_shortcode( 'bwg_register_link', [ $this, 'bwg_register_link_callback' ] );
		add_shortcode( 'bwg', [ $this, 'bwg_callback' ] );
	}

	/**
	 * Callback for the 'bwg_login' shortcode.
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	public function bwg_login_callback( $atts ) {
		$login_redirect_url = FALSE;
		$evaluation_title   = '';

		$e = filter_input( INPUT_GET, 'e', FILTER_VALIDATE_INT );
		if ( ! is_null( $e ) && FALSE !== $e ) {
			// There is an e-parameter.
			$e = (int) $e;
			if ( 'bwg_evaluation' === get_post_type( $e ) ) {
				// The post type is a 'bwg_evaluation'.
				$evaluation_title   = trim( get_the_title( $e ) );
				$login_redirect_url = add_query_arg( [ 'e' => '' ], get_permalink( $e ) );
			}
		}

		if ( FALSE === $login_redirect_url ) {
			// If we do not have a login-redirect-url so far, we will set
			// the redirect to the bwg-overview page.
			$login_redirect_url = $this->_bwg_base->options()->get_option_overview_page_link();
			$evaluation_title   = '';
		}

		// Enqueue the styles used by the frontend.
		$this->_bwg_base->utils()->enqueue_style( 'bwg-front' );

		return do_shortcode( $this->_bwg_base->utils()->render_template(
			'login.php',
			[
				'evaluation_title'   => $evaluation_title,
				'login_redirect_url' => $login_redirect_url,
			]
		) );
	}

	/**
	 * Callback for the 'bwg_register' shortcode.
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public function bwg_register_callback( $atts ) {
		$e = filter_input( INPUT_GET, 'e', FILTER_VALIDATE_INT );
		if ( ! is_null( $e ) && FALSE !== $e ) {
			$e = (int) $e;
		} else {
			$e = 0;
		}

		if ( filter_has_var( INPUT_GET, 'User_ID' ) ) {
			// This is a 'confirm email' action.
			$query_params = [];
			$feup_user_ID = filter_input( INPUT_GET, 'User_ID', FILTER_VALIDATE_INT );
			if ( ! is_null( $feup_user_ID ) && $feup_user_ID !== FALSE ) {
				$feup_user     = new \FEUP_User();
				$evaluation_id = $feup_user->Get_Field_Value_For_ID( 'Registration_Evaluation', $feup_user_ID );

				if ( $evaluation_id > 0 && 'bwg_evaluation' === get_post_type( $evaluation_id ) ) {
					$query_params = [ 'e' => $evaluation_id ];
				}
			}

			$login_url   = $this->_bwg_base->options()->get_option_login_page_link( $query_params );
			$pre_content = str_replace( [ '[login]', '[/login]' ],
				[ '<a href="' . esc_attr( $login_url ) . '">', '</a>' ],
				$this->_bwg_base->options()->get_option_registration_email_confirm_text() );
		} else {
			$pre_content = $this->_bwg_base->options()->get_option_registration_intro_text();
		}


		return do_shortcode( $this->_bwg_base->utils()->render_template(
			'register.php',
			[
				'pre_content'                  => $pre_content,
				'register_evaluation_id'       => $e,
				'register_redirect_page'       => add_query_arg( 'e', $e,
					$this->_bwg_base->options()->get_option_registration_thank_you_page_link()
				),
				'register_evaluation_field_id' => $this->_bwg_base->options()
				                                                  ->get_feup_registration_evaluation_field_id(),
			]
		) );
	}

	/**
	 * Callback for the 'bwg_overview' shortcode.
	 *
	 * @param $atts
	 *
	 * @var \wpdb $wpdb
	 *
	 * @return string
	 */
	public function bwg_overview_callback( $atts ) {
		global $wpdb;

		// Early exit if the user is not logged in (actually the page containing this shortcode should already be set up
		// by the correct feup permission settings such that this callback would never be called).
		$feup = new \FEUP_User();
		if ( ! $feup->Is_Logged_In() ) {
			return '';
		}

		$user_id = $feup->Get_User_ID();

		// Get the evaluation id of the evaluation the feup user registered upon (the registration evaluation).
		$registration_evaluation_id = (int) $feup->Get_Field_Value_For_ID( 'Registration_Evaluation', $user_id );

		// If there is a registration evaluation, and if this evaluation still is published, we need to make sure that the user storage is initialized appropriately.
		if ( $registration_evaluation_id > 0 && 'publish' === get_post_status( $registration_evaluation_id ) ) {
			$this->_bwg_base->user_storage_database_helper()
			                ->init_user_storage( $registration_evaluation_id, $user_id );
		}

		// Initialize the render array.
		$ra = [
			'submitted'     => [],
			'non_submitted' => [],
		];

		// Get the published evaluations the current feup user is linked to.
		$results = $wpdb->get_results(
			$wpdb->prepare( 'SELECT p.ID as post_ID, u.submitted AS submitted FROM ' . $wpdb->prefix . 'bwg_user_storage as u'
			                . ' LEFT JOIN ' . $wpdb->posts . ' AS p ON u.post_ID = p.ID WHERE u.user_ID = %d AND p.post_type = "bwg_evaluation" AND p.post_status = "publish" ORDER BY u.submitted DESC, u.created DESC',
				$user_id ),
			ARRAY_A
		);

		foreach ( $results as $result ) {
			$post            = get_post( $result['post_ID'] );
			$evaluation_post = $this->_bwg_base->factory()->bwg_evaluation_post( $post );

			if ( is_null( $result['submitted'] ) ) {
				if ( $evaluation_post->is_rating_open() ) {
					$result['permalink'] = $evaluation_post->get_permalink_to_form();
				}

				$result['rating_open_from'] = $this->_bwg_base->utils()
				                                              ->convert_ymd_to_format( $evaluation_post->get_rating_open_from() );
				$result['rating_open_to']   = $this->_bwg_base->utils()
				                                              ->convert_ymd_to_format( $evaluation_post->get_rating_open_to() );

				array_push( $ra['non_submitted'], $result );
			} else {
				$result['permalink'] = $evaluation_post->get_permalink_to_form();
				array_push( $ra['submitted'], $result );
			}
		}

		///
		// Automatically redirect user if there is only one non-submitted evaluation which has a permalink defined.
		///
		if ( 1 === count( $ra['non_submitted'] ) ) {
			if ( array_key_exists( 'permalink', $ra['non_submitted'][0] ) ) {
				wp_redirect( $ra['non_submitted'][0]['permalink'], 302 );
			}
		}

		// Enqueue the styles used by the frontend.
		$this->_bwg_base->utils()->enqueue_style( BWG_Base::STYLE_HANDLE_FRONT );

		// Render and return.
		return $this->_bwg_base->utils()->render_template( 'overview.php', $ra );
	}

	/**
	 * Callback for the 'bwg_register_link' shortcode.
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function bwg_register_link_callback( $atts, $content = NULL ) {
		$query_params = [];

		$e = filter_input( INPUT_GET, 'e', FILTER_VALIDATE_INT );
		if ( ! is_null( $e ) && FALSE !== $e ) {
			$query_params['e'] = (int) $e;
		}

		return '<a href="' . $this->_bwg_base->options()
		                                     ->get_option_register_page_link( $query_params ) . '">' . $content . '</a>';
	}

	/**
	 * Callback for the 'bwg' shortcode.
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function bwg_callback( $atts, $content = NULL ) {
		$atts = shortcode_atts( [
			'case' => 'prodomo',
		], $atts );

		switch ( $atts['case'] ) {
			case 'prodomo':
				$this->_bwg_base->utils()->enqueue_style( BWG_Base::STYLE_HANDLE_FRONT );
				$this->_bwg_base->utils()->enqueue_script( BWG_Base::SCRIPT_HANDLE_FRONT_PRODOMO );

				// Check that there is actually a frontend user logged in.
				$feup = new \FEUP_User();
				if ( ! $feup->Is_Logged_In() ) {
					return '<p>' . __( 'Sie sind leider nicht eingeloggt.', 'bwg' ) . '</p>';
				}

				// Get the evaluation id the user would like to give feedback onto.
				$evaluation_ID = isset( $_REQUEST['e'] ) ? intval( $_REQUEST['e'] ) : 0;

				// Get the prodomo record from the database (if any).
				$record = $this->_bwg_base->prodomo_database_helper()
				                          ->get_record( $evaluation_ID, $feup->Get_User_ID() );

				return $this->_bwg_base->utils()->render_template( 'prodomo/form.php', [
					'ajax_url'          => admin_url( 'admin-ajax.php' ),
					'ajax_action'       => BWG_Prodomo_Ajax::AJAX_ACTION,
					'ajax_action_nonce' => wp_create_nonce( BWG_Prodomo_Ajax::AJAX_ACTION ),
					'evaluation_id'     => $evaluation_ID,
					'grading'           => $this->_bwg_base->evaluation()->get_grading(),
					'question1'         => [
						'label' => $this->_bwg_base->options()->get_prodomo_question_1(),
						'value' => is_null( $record ) ? NULL : $record['question1'],
					],
					'question2'         => [
						'label' => $this->_bwg_base->options()->get_prodomo_question_2(),
						'value' => is_null( $record ) ? NULL : $record['question2'],
					],
					'question3'         => [
						'label' => $this->_bwg_base->options()->get_prodomo_question_3(),
						'value' => is_null( $record ) ? NULL : $record['question3'],
					],
					'freetext1'         => [
						'label' => $this->_bwg_base->options()->get_prodomo_freetext_1(),
						'value' => is_null( $record ) ? NULL : $record['freetext1'],
					],
				] );
		}

		return '';
	}

}
