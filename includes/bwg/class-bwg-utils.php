<?php

namespace bwg;

use DateTime;

/**
 * Class BWG_Utils.
 */
class BWG_Utils {


	/**
	 * Helper to get the auto version part (actually the file modification timestamp) that would be used by the auto
	 * version method.
	 *
	 * @param string $src The path relative to the root path of the BWG plugin.
	 *
	 * @return FALSE|int The file modification time as timestamp or <code>FALSE</code> if the file could not be found.
	 */
	protected function _auto_version( $src ) {
		if ( file_exists( BWG_ABS_PATH . '/' . $src ) ) {
			return @filemtime( BWG_ABS_PATH . '/' . $src );
		}

		return FALSE;
	}

	/**
	 * @see wp_register_script()
	 *
	 * @param string $handle
	 * @param string $src
	 * @param string[] $dependencies
	 * @param bool $ver
	 * @param bool $in_footer
	 */
	public function register_script( $handle, $src, $dependencies = [], $ver = FALSE, $in_footer = FALSE ) {
		if ( TRUE === $ver ) {
			$ver = $this->_auto_version( $src );
		}

		wp_register_script( $handle, plugins_url( $src, BWG_ABS_PLUGIN_FILE ), $dependencies, $ver, $in_footer );
	}

	/**
	 * @see wp_register_style()
	 *
	 * @param string $handle
	 * @param string $src
	 * @param string[] $dependencies
	 * @param bool $ver
	 * @param string $media
	 */
	public function register_style( $handle, $src, $dependencies = [], $ver = FALSE, $media = 'all' ) {
		if ( TRUE === $ver ) {
			$ver = $this->_auto_version( $src );
		}

		wp_register_style( $handle, plugins_url( $src, BWG_ABS_PLUGIN_FILE ), $dependencies, $ver, $media );
	}

	/**
	 * @see wp_enqueue_script()
	 *
	 * @param string $handle
	 * @param string $src
	 * @param string[] $dependencies
	 * @param bool $ver
	 * @param bool $in_footer
	 */
	public function enqueue_script( $handle, $src = '', $dependencies = [], $ver = FALSE, $in_footer = FALSE ) {
		if ( TRUE === $ver ) {
			$ver = $this->_auto_version( $src );
		}

		wp_enqueue_script( $handle, plugins_url( $src, BWG_ABS_PLUGIN_FILE ), $dependencies, $ver, $in_footer );
	}

	/**
	 * @see wp_enqueue_style()
	 *
	 * @param string $handle
	 * @param string $src
	 * @param string[] $dependencies
	 * @param bool $ver
	 * @param string $media
	 */
	public function enqueue_style( $handle, $src = '', $dependencies = [], $ver = FALSE, $media = 'all' ) {
		if ( TRUE === $ver ) {
			$ver = $this->_auto_version( $src );
		}

		wp_enqueue_style( $handle, plugins_url( $src, BWG_ABS_PLUGIN_FILE ), $dependencies, $ver, $media );
	}

	/**
	 * Renders an admin template.
	 *
	 * @param string $default_template_path The path to the template, relative to the plugin's `templates/admin` folder
	 * @param array $variables An array of variables to pass into the template's scope, indexed with the variable name
	 *     so that it can be extract()-ed
	 * @param bool $include_once true to use include_once(), false to use include()
	 *
	 * @return string
	 */
	public function render_admin_template( $default_template_path, $variables = [], $include_once = FALSE ) {
		return $this->render_template( 'admin/' . $default_template_path, $variables, $include_once, TRUE );
	}

	/**
	 * Renders a template.
	 *
	 * Allows parent/child themes to override the markup by placing a file named /bwg/{$default_template_path} in their
	 * root folder, and also allows plugins or themes to override the markup by a filter. Themes might prefer that
	 * method if they place their templates in sub-directories to avoid cluttering the root folder. In both cases,
	 * the theme/plugin will have access to the variables so they can fully customize the output.
	 *
	 * @param string $default_template_path The path to the template, relative to the plugin's `templates` folder
	 * @param array $variables An array of variables to pass into the template's scope, indexed with the variable name
	 *     so that it can be extract()-ed
	 * @param bool $include_once true to use include_once(), false to use include()
	 * @param bool $admin
	 *
	 * @return string
	 */
	public function render_template(
		$default_template_path,
		$variables = [],
		$include_once = FALSE,
		$admin = FALSE
	) {
		$admin = ( TRUE === $admin ? 'admin_' : '' );

		do_action( 'bwg_render_' . $admin . 'template_pre', $default_template_path, $variables );

		$template_path = locate_template( 'bwg/' . $default_template_path, FALSE, FALSE );
		if ( ! $template_path ) {
			$template_path = BWG_ABS_PATH . '/templates/' . $default_template_path;
		}

		$template_path = apply_filters( 'bwg_' . $admin . 'template_path', $template_path );

		if ( is_file( $template_path ) ) {
			$template_content = apply_filters(
				'bwg_' . $admin . 'template_content',
				$this->_render_template( $template_path, $variables, $include_once ),
				$default_template_path, $template_path, $variables
			);
		} else {
			$template_content = '';
		}

		do_action(
			'bwg_render_' . $admin . 'template_after', $default_template_path, $variables,
			$template_path, $template_content
		);

		return $template_content;
	}

	/**
	 * Renders the template. This method would actually extract the variables and render the template.
	 *
	 * @param $template_path
	 * @param array $variables
	 * @param bool $include_once
	 *
	 * @return string
	 */
	protected function _render_template( $template_path, $variables = [], $include_once = FALSE ) {
		extract( $variables, EXTR_SKIP );
		ob_start();

		if ( $include_once ) {
			include_once( $template_path );
		} else {
			include( $template_path );
		}

		return ob_get_clean();
	}

	/**
	 * Checks if the given date (or the current day if empty) is in between the from and the to dates.
	 * The check would be done as string comparison. So, as long as you stick to the date formats like
	 * 'Y-m-d' or 'Y-m-d H:i:s', this would work great. Note that from as well as to can be empty, which
	 * means that they do not add any condition.
	 *
	 * @param string $from
	 * @param string $to
	 * @param string|null $date
	 *
	 * @return bool
	 */
	public function in_dates( $from = '', $to = '', $date = NULL ) {
		if ( is_null( $date ) ) {
			$date = current_time( 'Y-m-d' );
		}

		return ( $date >= $from && ( empty( $to ) || $date <= $to ) );
	}

	/**
	 * Transforms a date given in 'ymd'-format (that is 'YYYY-MM-DD') into the given
	 * format.
	 *
	 * @param string $ymd
	 * @param string $format
	 *
	 * @return string The formatted date or an empty string.
	 */
	public function convert_ymd_to_format( $ymd, $format = 'd.m.Y' ) {
		if ( empty( $ymd ) ) {
			return '';
		}

		$dt = DateTime::createFromFormat( 'Y-m-d', $ymd );

		return FALSE === $dt ? '' : $dt->format( $format );
	}

	public function mysql_gmt_to_local_timestamp( $date ) {
		return mysql2date( 'G', $date ) + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
	}

	/**
	 * Gets the absolute path to the bwg-files directory (no trailing slash).
	 *
	 * @param bool $assert If TRUE, the method would assert the existence of the directory.
	 *
	 * @return false|string The path or FALSE if $assert is TRUE but the directory could not be asserted.
	 */
	public function get_bwg_files_path( $assert = FALSE ) {
		$path = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'bwg-files';
		if ( ! $assert ) {
			return $path;
		}

		if ( wp_mkdir_p( $path ) ) {
			return $path;
		}

		return FALSE;
	}

	/**
	 * Gets the absolute path to the private bwg-files (no trailing slash).
	 *
	 * @param bool $assert If TRUE, the method would assert the existence of the directory as well as the .htaccess
	 * file.
	 *
	 * @return false|string The path or FALSE if $assert is TRUE but the directory could not be asserted.
	 */
	public function get_bwg_files_private_path( $assert = FALSE ) {
		$path = $this->get_bwg_files_path( $assert );
		if ( FALSE === $path ) {
			return FALSE;
		}

		$path .= DIRECTORY_SEPARATOR . '_private';
		if ( ! $assert ) {
			return $path;
		}

		if ( wp_mkdir_p( $path ) ) {
			$htaccess_path = $path . DIRECTORY_SEPARATOR . '.htaccess';

			if ( ! is_file( $htaccess_path ) ) {
				file_put_contents( $htaccess_path, 'Deny from all' );
			}

			return $path;
		}

		return FALSE;
	}

	/**
	 * Gets the absolute path to the private bwg-cache-files (no trailing slash).
	 *
	 * @param bool $assert If TRUE, the method would assert the existence of the directory.
	 *
	 * @return false|string The path or FALSE if $assert is TRUE but the directory could not be asserted.
	 */
	public function get_bwg_files_private_cache_path( $assert = TRUE ) {
		$path = $this->get_bwg_files_private_path( $assert );
		if ( FALSE === $path ) {
			return FALSE;
		}

		$path .= DIRECTORY_SEPARATOR . 'cache';
		if ( ! $assert ) {
			return $path;
		}

		if ( wp_mkdir_p( $path ) ) {
			return $path;
		}

		return FALSE;
	}

	/**
	 * @param string $path
	 * @param int $age_in_seconds
	 * @param bool $rm_sub_dirs
	 * @param int $level
	 *
	 * @return bool TRUE if the directory had been cleared, FALSE if the directory had been cleared and removed.
	 */
	public function clear_directory( $path, $age_in_seconds = 86400, $rm_sub_dirs = TRUE, $level = 0 ) {
		if ( $handle = opendir( $path ) ) {
			$nr_children = 0;
			while ( FALSE !== ( $file = readdir( $handle ) ) ) {
				if ( '.' === $file || '..' === $file ) {
					continue;
				}

				$file_path = $path . DIRECTORY_SEPARATOR . $file;
				if ( is_dir( $file_path ) ) {
					$b = $this->clear_directory( $file_path, $age_in_seconds, $rm_sub_dirs, $level + 1 );
					if ( $b ) {
						$nr_children ++;
					}
					continue;
				}

				if ( time() - filemtime( $file_path ) > $age_in_seconds ) {
					wp_delete_file( $file_path );
				} else {
					$nr_children ++;
				}
			}

			if ( $rm_sub_dirs && $level > 0 && 0 === $nr_children ) {
				@rmdir( $path );

				return FALSE;
			}
		}

		return TRUE;
	}

	/**
	 * Divides the dividend by the divisor and returns the $if_zero value, if the
	 * divisor is zero.
	 *
	 * @param $dividend
	 * @param $divisor
	 * @param int $if_zero
	 *
	 * @return float|int
	 */
	public function safe_division( $dividend, $divisor, $if_zero = 0 ) {
		if ( 0 == $divisor ) {
			return $if_zero;
		}

		return $dividend / $divisor;
	}

	/**
	 * Gets the admin colors and icon colors for the current user.
	 *
	 * Possible usage:
	 *   $colors = $this->_bwg_base->utils()->get_admin_colors();
	 *   $custom_inline_style = '#bwg-ee-vote-start::before { color:' . $colors[0] . '; }';
	 *   wp_add_inline_style( 'bwg-admin-evaluation-extras-meta-box', $custom_inline_style );
	 *
	 * @return array
	 *   0: (used for background in admin submenu items).
	 *   1: (used for background in admin menu and top bar).
	 *   2: (used for background in admin item that is currently active or hover as well as primary button backgrounds).
	 *   3: (no idea).
	 *   base: (no idea).
	 *   focus: (no idea).
	 *   current: (no idea).
	 */
	public function get_admin_colors() {
		/** @var $_wp_admin_css_colors array */
		global $_wp_admin_css_colors;
		static $admin_colors;

		if ( is_null( $admin_colors ) ) {
			$current_color_scheme = get_user_meta( get_current_user_id(), 'admin_color', TRUE );

			$admin_colors = array_merge(
				$_wp_admin_css_colors[ $current_color_scheme ]->colors,
				$_wp_admin_css_colors[ $current_color_scheme ]->icon_colors
			);
		}

		return $admin_colors;
	}

}
