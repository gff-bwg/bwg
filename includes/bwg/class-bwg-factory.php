<?php

namespace bwg;

use bwg\charts\BWG_Chart_Factory;
use bwg\charts\BWG_Spider_Chart;
use bwg\cron\BWG_Cron_Handler;
use bwg\database\helpers\BWG_Analyses_Database_Helper;
use bwg\database\helpers\BWG_Cache_Database_Helper;
use bwg\database\helpers\BWG_Prodomo_Database_Helper;
use bwg\database\helpers\BWG_Selections_Database_Helper;
use bwg\database\helpers\BWG_Submissions_Database_Helper;
use bwg\database\helpers\BWG_User_Action_Logs_Database_Helper;
use bwg\database\helpers\BWG_User_Storage_Database_Helper;
use bwg\evaluation\BWG_Evaluation;
use bwg\evaluation\BWG_Evaluation_Ajax;
use bwg\evaluation\BWG_Evaluation_Analyses;
use bwg\evaluation\BWG_Evaluation_Analyses_Ajax;
use bwg\evaluation\BWG_Evaluation_Content;
use bwg\evaluation\BWG_Evaluation_Participation_Statistics;
use bwg\evaluation\BWG_Evaluation_Post;
use bwg\evaluation\BWG_Evaluation_Post_Type_Manager;
use bwg\evaluation\BWG_Evaluation_Presets;
use bwg\evaluation\BWG_Evaluation_Users;
use bwg\evaluation\BWG_Evaluation_Users_Ajax;
use bwg\evaluation\BWG_Evaluation_Utils;
use bwg\feup\BWG_FEUP_Layer;
use bwg\options\BWG_Options;
use bwg\prodomo\BWG_Prodomo_Ajax;
use bwg\prodomo\BWG_Prodomo_Base;
use bwg\profile\fields\BWG_Profile_Field_Type_Registry;
use bwg\profile\fields\BWG_Profile_Field_Utils;
use bwg\shortcodes\BWG_Shortcode_Manager;
use bwg\stats\BWG_Stats_Math;
use bwg\stats\strata\BWG_Stats_Strata_Utils;
use FEUP_User;
use WP_Post;

/**
 * Class BWG_Factory.
 *
 * This class contains methods to instantiate several objects. By extending the factory class, it is possible to
 * override object instantiation.
 */
class BWG_Factory {

	/**
	 * Instantiates a new BWG_Cron_Handler.
	 *
	 * @param BWG_Base $bwg_base
	 *
	 * @return BWG_Cron_Handler
	 */
	public function bwg_cron_handler( BWG_Base $bwg_base ) {
		return new BWG_Cron_Handler( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Analyses_Database_Helper.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\database\helpers\BWG_Analyses_Database_Helper
	 */
	public function bwg_analyses_database_helper( BWG_Base $bwg_base ) {
		return new BWG_Analyses_Database_Helper( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Cache_Database_Helper.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\database\helpers\BWG_Cache_Database_Helper
	 */
	public function bwg_cache_database_helper( BWG_Base $bwg_base ) {
		return new BWG_Cache_Database_Helper( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Submissions_Database_Helper.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return BWG_Submissions_Database_Helper
	 */
	public function bwg_submissions_database_helper( BWG_Base $bwg_base ) {
		return new BWG_Submissions_Database_Helper( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Prodomo_Database_Helper.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\database\helpers\BWG_Prodomo_Database_Helper
	 */
	public function bwg_prodomo_database_helper( BWG_Base $bwg_base ) {
		return new BWG_Prodomo_Database_Helper( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Selections_Database_Helper.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\database\helpers\BWG_Selections_Database_Helper
	 */
	public function bwg_selections_database_helper( BWG_Base $bwg_base ) {
		return new BWG_Selections_Database_Helper( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_User_Action_Logs_Database_Helper.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\database\helpers\BWG_User_Action_Logs_Database_Helper
	 */
	public function bwg_user_action_logs_database_helper( BWG_Base $bwg_base ) {
		return new BWG_User_Action_Logs_Database_Helper( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_User_Storage_Database_Helper.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\database\helpers\BWG_User_Storage_Database_Helper
	 */
	public function bwg_user_storage_database_helper( BWG_Base $bwg_base ) {
		return new BWG_User_Storage_Database_Helper( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Evaluation.
	 *
	 * @param BWG_Base $bwg_base
	 *
	 * @return BWG_Evaluation
	 */
	public function bwg_evaluation( BWG_Base $bwg_base ) {
		return new BWG_Evaluation( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Evaluation_Ajax.
	 *
	 * @param BWG_Base $bwg_base
	 *
	 * @return BWG_Evaluation_Ajax
	 */
	public function bwg_evaluation_ajax( BWG_Base $bwg_base ) {
		return new BWG_Evaluation_Ajax( $bwg_base );
	}

	/**
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Participation_Statistics
	 */
	public function bwg_evaluation_participants_statistics(
		BWG_Base $bwg_base
	): BWG_Evaluation_Participation_Statistics {
		return new BWG_Evaluation_Participation_Statistics( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Evaluation_Analyses.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Analyses
	 */
	public function bwg_evaluation_analyses( BWG_Base $bwg_base ) {
		return new BWG_Evaluation_Analyses( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Evaluation_Analyses_Ajax.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Analyses_Ajax
	 */
	public function bwg_evaluation_analyses_ajax( BWG_Base $bwg_base ) {
		return new BWG_Evaluation_Analyses_Ajax( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Evaluation_Content.
	 *
	 * @param BWG_Base $bwg_base
	 * @param FEUP_User $feup_user
	 * @param BWG_Evaluation_Post $evaluation_post
	 * @param string|null $content
	 *
	 * @return BWG_Evaluation_Content
	 */
	public function bwg_evaluation_content(
		BWG_Base $bwg_base,
		FEUP_User $feup_user,
		BWG_Evaluation_Post $evaluation_post,
		?string $content = NULL
	): BWG_Evaluation_Content {
		return new BWG_Evaluation_Content( $bwg_base, $feup_user, $evaluation_post, $content );
	}

	/**
	 * Instantiates a new BWG_Evaluation_Post.
	 *
	 * @param WP_Post $post
	 *
	 * @return BWG_Evaluation_Post
	 */
	public function bwg_evaluation_post( $post ) {
		return new BWG_Evaluation_Post( $post );
	}

	/**
	 * Instantiates a new BWG_Evaluation_Post_Type_Manager.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Post_Type_Manager
	 */
	public function bwg_evaluation_post_type_manager( BWG_Base $bwg_base ) {
		return new BWG_Evaluation_Post_Type_Manager( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Evaluation_Presets.
	 *
	 * @param BWG_Base $bwg_base
	 *
	 * @return BWG_Evaluation_Presets
	 */
	public function bwg_evaluation_presets( BWG_Base $bwg_base ) {
		return new BWG_Evaluation_Presets( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Evaluation_Users.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Users
	 */
	public function bwg_evaluation_users( BWG_Base $bwg_base ) {
		return new BWG_Evaluation_Users( $bwg_base );
	}

	/**
	 * Instantiates a new `BWG_Evaluation_Users_Ajax`.
	 *
	 * @param BWG_Base $bwg_base
	 *
	 * @return BWG_Evaluation_Users_Ajax
	 */
	public function bwg_evaluation_users_ajax( BWG_Base $bwg_base ): BWG_Evaluation_Users_Ajax {
		return new BWG_Evaluation_Users_Ajax( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Evaluation_Utils.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Utils
	 */
	public function bwg_evaluation_utils( BWG_Base $bwg_base ) {
		return new BWG_Evaluation_Utils( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_FEUP_Layer.
	 *
	 * @param BWG_Base $bwg_base
	 *
	 * @param \FEUP_User $feup
	 *
	 * @return \bwg\feup\BWG_FEUP_Layer
	 */
	public function bwg_feup_layer( BWG_Base $bwg_base, FEUP_User $feup ) {
		return new BWG_FEUP_Layer( $bwg_base, $feup );
	}

	/**
	 * Instantiates a new BWG_Options.
	 *
	 * @param BWG_Base $bwg_base
	 *
	 * @return BWG_Options
	 */
	public function bwg_options( BWG_Base $bwg_base ) {
		return new BWG_Options( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Prodomo_Ajax.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\prodomo\BWG_Prodomo_Ajax
	 */
	public function bwg_prodomo_ajax( BWG_Base $bwg_base ) {
		return new BWG_Prodomo_Ajax( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Prodomo_Base.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\prodomo\BWG_Prodomo_Base
	 */
	public function bwg_prodomo_base( BWG_Base $bwg_base ) {
		return new BWG_Prodomo_Base( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Profile_Field_Type_Registry.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field_Type_Registry
	 */
	public function bwg_profile_field_type_registry( BWG_Base $bwg_base ) {
		return new BWG_Profile_Field_Type_Registry( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Profile_Field_Utils.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field_Utils
	 */
	public function bwg_profile_field_utils( BWG_Base $bwg_base ) {
		return new BWG_Profile_Field_Utils( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Shortcode_Manager.
	 *
	 * @param BWG_Base $bwg_base
	 *
	 * @return BWG_Shortcode_Manager
	 */
	public function bwg_shortcode_manager( BWG_Base $bwg_base ) {
		return new BWG_Shortcode_Manager( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Stats_Strata_Utils.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\stats\strata\BWG_Stats_Strata_Utils
	 */
	public function bwg_stats_strata_utils( BWG_Base $bwg_base ) {
		return new BWG_Stats_Strata_Utils( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Stats_Math.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\stats\BWG_Stats_Math
	 */
	public function bwg_stats_math( BWG_Base $bwg_base ) {
		return new BWG_Stats_Math( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Globals.
	 *
	 * @return BWG_Globals
	 */
	public function bwg_globals() {
		return new BWG_Globals();
	}

	/**
	 * Instantiates a new BWG_Utils.
	 *
	 * @return BWG_Utils
	 */
	public function bwg_utils() {
		return new BWG_Utils();
	}

	/**
	 * Instantiates a new BWG_Chart_Factory.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 *
	 * @return \bwg\charts\BWG_Chart_Factory
	 */
	public function bwg_chart_factory( BWG_Base $bwg_base ) {
		return new BWG_Chart_Factory( $bwg_base );
	}

	/**
	 * Instantiates a new BWG_Spider_Chart.
	 *
	 * @param $settings
	 *
	 * @return BWG_Spider_Chart
	 */
	public function bwg_spider_chart( $settings = [] ) {
		return new BWG_Spider_Chart( $settings );
	}

}