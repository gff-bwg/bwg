<?php

namespace bwg\evaluation;

use bwg\BWG_Base;
use bwg\charts\BWG_Chart_Styles;
use bwg\charts\BWG_Spider_Chart;
use bwg\database\BWG_Database_Insert_Update_Query;
use bwg\mail\BWG_User_Evaluation_Confirm_Mail;
use bwg\profile\fields\BWG_Profile_Field;
use bwg\profile\fields\BWG_Profile_Field_Settings;
use wpdb;

/**
 * Class BWG_Evaluation_Ajax.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation_Ajax {

	/**
	 * Ajax action: spider chart.
	 */
	const SPIDER_CHART_AJAX_ACTION = 'bwg_spider_chart';

	/**
	 * Ajax action: user server_persist.
	 */
	const USER_PERSIST_AJAX_ACTION = 'bwg_up';

	/**
	 * Ajax action: spider chart settings.
	 */
	const SPIDER_CHART_SETTINGS_AJAX_ACTION = 'bwg_ed_spider_chart_settings';

	/**
	 * Ajax action: emails test.
	 */
	const EMAILS_TEST_AJAX_ACTION = 'bwg_e_emails_test';

	/**
	 * Ajax action: add new profile field.
	 */
	const PROFILE_FIELD_AJAX_ACTION = 'bwg_e_epf_add';

	/**
	 * @var BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Evaluation_Ajax constructor.
	 *
	 * @param BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;
	}

	/**
	 * Registers public ajax callbacks.
	 *
	 * @param bool $admin
	 */
	public function register_public_ajax_callbacks( $admin = FALSE ) {
		add_action( 'wp_ajax_' . ( $admin ? '' : 'nopriv_' ) . self::USER_PERSIST_AJAX_ACTION,
			[ $this, 'wp_ajax_user_persist' ] );
		add_action( 'wp_ajax_' . ( $admin ? '' : 'nopriv_' ) . self::SPIDER_CHART_AJAX_ACTION,
			[ $this, 'wp_ajax_spider_chart' ] );
	}

	/**
	 * Registers private (admin only) ajax callbacks.
	 */
	public function register_private_ajax_callbacks() {
		add_action( 'wp_ajax_' . self::SPIDER_CHART_SETTINGS_AJAX_ACTION,
			[ $this, 'wp_ajax_ed_spider_chart_settings' ] );
		add_action( 'wp_ajax_' . self::EMAILS_TEST_AJAX_ACTION,
			[ $this, 'wp_ajax_e_emails_test' ] );
		add_action( 'wp_ajax_' . self::PROFILE_FIELD_AJAX_ACTION,
			[ $this, 'wp_ajax_e_profile_field' ] );
	}

	/**
	 * Ajax Action Handler for the USER_PERSIST_AJAX_ACTION.
	 */
	public function wp_ajax_user_persist() {
		check_ajax_referer( self::USER_PERSIST_AJAX_ACTION );

		/** @var wpdb $wpdb */
		global $wpdb;

		$feup = new \FEUP_User();
		if ( $feup->Is_Logged_In() ) {
			$user_ID = $feup->Get_User_ID();
			$post_ID = intval( $_REQUEST['post'] );

			$current_time_mysql = current_time( 'mysql', 1 );
			$sequence_number    = time();

			$q = new BWG_Database_Insert_Update_Query( 'bwg_user_storage' );
			$q->addField( 'post_ID' )->insert( $_REQUEST['post'], '%d' );

			$q->addField( 'user_ID' )->insert( $user_ID, '%d' );

			$q->addField( 'sequence_number' )->insert( $sequence_number, '%d' )
			  ->update_raw( 'IF((@update_record := sequence_number < VALUES(sequence_number)), VALUES(sequence_number), sequence_number)' );

			$q->addField( 'grading' )->insert( filter_input( INPUT_POST, 'grading' ) )
			  ->update_raw( 'IF(@update_record, VALUES(grading), grading)' );

			$q->addField( 'notes' )->insert( filter_input( INPUT_POST, 'notes' ) )
			  ->update_raw( 'IF(@update_record, VALUES(notes), notes)' );

			$q->addField( 'meta' )->insert( filter_input( INPUT_POST, 'meta' ) )
			  ->update_raw( 'IF(@update_record, VALUES(meta), meta)' );

			$q->addField( 'created' )->insert( $current_time_mysql );

			$q->addField( 'modified' )->insert( $current_time_mysql )
			  ->update_raw( 'IF(@update_record, VALUES(modified), modified)' );

			$r = $wpdb->query( $q->prepare() );
			if ( $r > 0 ) {
				// Inserted or updated the user storage.

				///
				// Handle the action log entries.
				///
				$action_log_entries = json_decode( filter_input( INPUT_POST, 'action_log_entries' ), TRUE, 512 );
				if ( ! is_null( $action_log_entries ) && is_array( $action_log_entries ) ) {
					$this->_bwg_base->user_action_logs_database_helper()->save_action_log(
						$post_ID, $user_ID, $action_log_entries
					);
				}
			}

			$json = [];
			if ( isset( $_REQUEST['submit'] ) && '1' === $_REQUEST['submit'] ) {
				$user_storage_db_helper = $this->_bwg_base->user_storage_database_helper();
				$submissions_db_helper  = $this->_bwg_base->submissions_database_helper();
				$evaluation_post        = $this->_bwg_base->factory()->bwg_evaluation_post( get_post( $post_ID ) );

				$user_storage                = $user_storage_db_helper->get_user_storage( $post_ID, $user_ID );
				$user_storage_profile_fields = $user_storage->get_meta();

				///
				// Submission validation
				//   Make sure that all gradings have a valid value and that the required profile fields do have a
				//   valid value too.
				///
				$profile_field_validations = [];
				foreach ( $evaluation_post->get_profile_fields()->get_fields() as $profile_field ) {
					$value = NULL;
					if ( array_key_exists( $profile_field->get_uid(), $user_storage_profile_fields ) ) {
						$value = $user_storage_profile_fields[ $profile_field->get_uid() ];
					}

					$profile_field_validations = array_merge(
						$profile_field_validations,
						$profile_field->validate(
							$this->_bwg_base->profile_field_type_registry(),
							$value
						)
					);
				}

				if ( ! empty( $profile_field_validations ) ) {
					return wp_send_json_error( [
						'error' => implode( PHP_EOL, $profile_field_validations ),
					] );
				}

				$submitted = $this->_bwg_base->user_storage_database_helper()->submit( $post_ID, $user_ID );
				if ( FALSE === $submitted ) {
					return wp_send_json_error( [
						'error' => __( 'Ihre Bewertung konnte leider nicht gespeichert werden.', 'bwg' ),
					] );
				}

				$json['redirect'] = $this->_bwg_base
					->options()->get_option_evaluation_thank_you_page_link( [ 'e' => $_REQUEST['post'] ] );

				$evaluation_definition     = $evaluation_post->get_evaluation_definition();
				$evaluation_profile_fields = $evaluation_post->get_profile_fields();
				$user_storage              = $user_storage_db_helper->get_user_storage( $post_ID, $user_ID );

				$submissions_db_helper->save_submission(
					$evaluation_definition,
					$evaluation_profile_fields,
					$user_storage
				);

				if ( 0 === $submitted ) {
					// Already submitted! That's fine, we then simply return the redirect.
					return wp_send_json_success( $json );
				}

				$mail = new BWG_User_Evaluation_Confirm_Mail( $this->_bwg_base );
				if ( $mail->build_and_send( $post_ID, $user_ID ) ) {
					return wp_send_json_success( $json );
				} else {
					return wp_send_json_error( [
						'error' => __( 'Ihre Bewertung konnte leider nicht gespeichert werden.', 'bwg' ),
					] );
				}
			}

			return wp_send_json_success( $json );
		}

		return wp_send_json_error( [ 'error' => __( 'Sie sind nicht eingeloggt.', 'bwg' ) ] );
	}

	/**
	 * Ajax Action Handler for the SPIDER_CHART_AJAX_ACTION.
	 */
	public function wp_ajax_spider_chart() {
		check_ajax_referer( self::SPIDER_CHART_AJAX_ACTION );

		///
		// Get the post.
		///
		$post = get_post( intval( $_REQUEST['post'] ) );
		if ( ! $post ) {
			wp_send_json_error( [], 403 );

			return;
		}

		///
		// Get the evaluation post.
		///
		$evaluation_post = new BWG_Evaluation_Post( $post );

		// Initialize the json.
		$json = [
			'charts' => [],
		];

		$grading = [];
		foreach ( explode( '|', $_REQUEST['grading'] ) as $grade ) {
			list ( $id, $points ) = explode( '=', $grade );

			$grading[ '#' . $id ] = empty( $points ) ? 1 : (int) $points;
		}

		if ( isset( $_REQUEST['charts'] ) ) {
			$charts = explode( '|', $_REQUEST['charts'] );
		} else {
			$charts = TRUE;
		}

		// Get the evaluation definition.
		$evaluation_definition = $evaluation_post->get_evaluation_definition();
		foreach ( $evaluation_definition->get_items() as $item0 ) {
			if ( ! ( TRUE === $charts || in_array( $item0->get_id(), $charts ) ) ) {
				continue;
			}

			$chart = new BWG_Spider_Chart( [
				'title'     => [
					'text'  => $item0->get_label(),
					'style' => [ 'font-size' => '11px' ],
				],
				'spider'    => [
					'leg' => [
						'label' => [
							'style' => [ 'font-size' => '11px', ],
						],
					],
				],
				'y_axis'    => [
					'label' => [ 'style' => [ 'font-size' => '9px', ], ],
				],
				'copyright' => [ 'style' => [ 'font-size' => '9px', ], ],
			] );

			$chart_big = new BWG_Spider_Chart( [
				'title'     => [
					'text'  => $item0->get_label(),
					'style' => [ 'font-size' => '15px' ]
				],
				'spider'    => [
					'center' => [ 'disc_radius' => 6 ],
					'leg'    => [
						'label' => [
							'style' => [ 'font-size' => '15px', ],
						],
					],
					'series' => [
						'disc_radius' => 6,
					],
				],
				'y_axis'    => [
					'label' => [ 'style' => [ 'font-size' => '12px', ], ],
				],
				'copyright' => [ 'style' => [ 'font-size' => '12px' ] ],
			] );

			$data_all = NULL;
			if ( $this->_bwg_base->evaluation_utils()->is_spider_all_visible( $evaluation_post->get_id() ) ) {
				$data_all = $this->_bwg_base->submissions_database_helper()
				                            ->get_weight_gradings( $evaluation_post->get_id(), $evaluation_definition );
			}

			$data_points_all  = [];
			$data_points_user = [];
			foreach ( $item0->get_items() as $item1 ) {
				if ( 0 === $item1->get_spider_chart() ) {
					continue;
				}

				$chart->add_leg( $item1->get_label( FALSE ) );
				$chart_big->add_leg( $item1->get_label( FALSE ) );

				$item1_total_grade  = 0;
				$item1_total_weight = 0;
				foreach ( $item1->get_items() as $item2 ) {
					if ( $item2->has_items() ) {
						$item2_total_grade  = 0;
						$item2_total_weight = 0;
						foreach ( $item2->get_items() as $item3 ) {
							$grade              = isset( $grading[ '#' . $item3->get_id() ] ) ? $grading[ '#' . $item3->get_id() ] : 1;
							$item2_total_grade  += $item3->get_weight() * $grade;
							$item2_total_weight += $item3->get_weight();
						}

						$item1_total_grade  += $item2->get_weight() * ( $this->_bwg_base->utils()
						                                                                ->safe_division( $item2_total_grade,
							                                                                $item2_total_weight ) );
						$item1_total_weight += $item2->get_weight();
					} else {
						$grade              = isset( $grading[ '#' . $item2->get_id() ] ) ? $grading[ '#' . $item2->get_id() ] : 1;
						$item1_total_grade  += $item2->get_weight() * $grade;
						$item1_total_weight += $item2->get_weight();
					}
				}

				array_push(
					$data_points_user,
					$this->_bwg_base->utils()->safe_division( $item1_total_grade, $item1_total_weight )
				);

				if ( ! is_null( $data_all ) ) {
					array_push( $data_points_all, $data_all[ '#' . $item1->get_id() ] );
				}
			}

			if ( ! is_null( $data_all ) ) {
				// Average of all users!!!
				$other_area_styles = new BWG_Chart_Styles( [ 'fill' => '#508FBB' ] );
				$other_dot_styles  = new BWG_Chart_Styles( [ 'fill' => '#508FBB', 'stroke' => '#508FBB' ] );

				// GFF: BWG decided not to show the averages of the other users in the intermediate spider charts.
				// $chart->add_series( $data_points_all, $other_area_styles, $other_dot_styles );
				$chart_big->add_series( $data_points_all, $other_area_styles, $other_dot_styles );
			}

			// Main series.
			$chart->add_series( $data_points_user );
			$chart_big->add_series( $data_points_user );


			array_push( $json['charts'], [
				'data_points' => $data_points_user,
				'id'          => $item0->get_id(),
				'svg'         => $chart->get_svg( FALSE ),
				'svg_big'     => $chart_big->get_svg( FALSE ),
			] );
		}

		wp_send_json_success( $json );
	}

	public function wp_ajax_ed_spider_chart_settings() {
		check_ajax_referer( self::SPIDER_CHART_SETTINGS_AJAX_ACTION );
		if ( ! current_user_can( 'edit_others_posts' ) ) {
			wp_die( - 1, 403 );
		}

		switch ( $_REQUEST['command'] ) {
			case 'load-charts':
				$json = [
					'charts' => [],
				];

				$ed = json_decode( stripslashes( $_REQUEST['ed'] ), TRUE, 512,
					JSON_UNESCAPED_UNICODE );
				if ( ! is_null( $ed ) ) {
					foreach ( $ed['items'] as $itemLevel0 ) {
						// $title = $itemLevel0['label'];

						$series_data = [];
						$chart       = $this->_bwg_base->factory()
						                               ->bwg_spider_chart();
						foreach ( $itemLevel0['items'] as $itemLevel1 ) {
							$label = $itemLevel1['label'];

							$chart->add_leg( $label );
							array_push( $series_data, rand( 10, 60 ) / 10 );
						}
						$chart->add_series( $series_data );

						array_push( $json['charts'],
							$chart->get_svg( FALSE )
						);
					}
				}

				wp_send_json_success( $json );
				break;
		}

		wp_send_json_error( [ 'message' => 'Command not found.' ], 403 );
	}

	/**
	 * Ajax Action Handler for the EMAILS_TEST_AJAX_ACTION.
	 */
	public function wp_ajax_e_emails_test() {
		check_ajax_referer( self::EMAILS_TEST_AJAX_ACTION );
		if ( ! current_user_can( 'edit_others_posts' ) ) {
			wp_die( - 1, 403 );
		}

		$receiver   = wp_get_current_user()->user_email;
		$post_ID    = intval( $_REQUEST['post_ID'] );
		$post_title = stripslashes( $_REQUEST['post_title'] );

		$email = $_REQUEST['email'];
		switch ( $email ) {
			case 'user-confirm':
				$content = stripslashes( $_REQUEST['text'] );
				$user_ID = $this->_bwg_base->options()->get_feup_test_email_user_id();

				$mail = new BWG_User_Evaluation_Confirm_Mail( $this->_bwg_base );
				if ( $mail->build_and_send( $post_ID, $user_ID, $content, FALSE, $receiver, $post_title ) ) {
					wp_send_json_success( [ 'status' => 'success' ] );

					return;
				}
				break;

			default:
				wp_send_json_error( [ 'message' => 'Keine E-Mail Vorlage gefunden.' ], 403 );

				return;
		}

		wp_send_json_error( [ 'message' => 'Die E-Mail konnte nicht erfolgreich verschickt werden.' ], 403 );
	}

	/**
	 * Ajax Action Handler for the PROFILE_FIELD_AJAX_ACTION.
	 */
	public function wp_ajax_e_profile_field() {
		check_ajax_referer( self::PROFILE_FIELD_AJAX_ACTION );
		if ( ! current_user_can( 'edit_posts' ) ) {
			wp_die( - 1, 403 );
		}

		$type       = filter_input( INPUT_POST, 'type' );
		$field_type = $this->_bwg_base->profile_field_type_registry()->get_type( $type );
		if ( is_null( $field_type ) ) {
			wp_send_json_error( [ 'message' => 'Keine Profiltyp Definition gefunden.' ], 403 );

			return;
		}

		$field = new BWG_Profile_Field(
			new BWG_Profile_Field_Settings(
				$type, filter_input( INPUT_POST, 'uid' )
			)
		);

		wp_send_json_success( [
			'status'   => 'success',
			'meta_box' => $field->render_meta_box(
				$this->_bwg_base->utils(),
				$this->_bwg_base->profile_field_type_registry()
			),
		] );
	}

}
