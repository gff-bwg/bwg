<?php

namespace bwg\evaluation;

use bwg\BWG_Base;
use FEUP_User;

/**
 * Class BWG_Evaluation_Content.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation_Content {

	/**
	 * @var BWG_Base
	 */
	protected BWG_Base $bwg_base;

	/**
	 * @var FEUP_User
	 */
	protected FEUP_User $feup_user;

	/**
	 * @var BWG_Evaluation_Post
	 */
	protected BWG_Evaluation_Post $evaluation_post;

	/**
	 * @var null|string
	 */
	protected ?string $content;

	/**
	 * @var boolean
	 */
	protected bool $feup_user_is_logged_in = FALSE;

	/**
	 * @param BWG_Base $bwg_base
	 * @param FEUP_User $feup_user
	 * @param BWG_Evaluation_Post $evaluation_post
	 * @param string|null $content
	 */
	function __construct(
		BWG_Base $bwg_base,
		FEUP_User $feup_user,
		BWG_Evaluation_Post $evaluation_post,
		?string $content
	) {
		$this->bwg_base        = $bwg_base;
		$this->feup_user       = $feup_user;
		$this->evaluation_post = $evaluation_post;
		$this->content         = $content;
	}

	/**
	 * Renders the content for the evaluation post.
	 *
	 * @return string
	 */
	public function render(): string {
		// Enqueue the front css styles.
		$this->bwg_base
			->utils()->enqueue_style( 'bwg-front', 'css/front_bwg.css', [], TRUE );

		$this->feup_user_is_logged_in = $this->feup_user->Is_Logged_In();
		if ( $this->feup_user_is_logged_in && is_null( $this->feup_user->Get_User_Level_Name() ) ) {
			return $this->render_not_allowed();
		}

		if ( ! filter_has_var( INPUT_GET, 'e' ) || ! $this->feup_user_is_logged_in ) {
			return $this->render_intro();
		}

		return $this->render_form();
	}

	/**
	 * Renders the evaluation form.
	 *
	 * Note that when calling this method, the feup user needs to be logged in.
	 *
	 * @return string
	 */
	protected function render_form(): string {
		if ( $this->evaluation_post->is_rating_not_yet_open() ) {
			return '<p>Rating not yet open.</p>';
		}

		// Get the post id of the evaluation post.
		$post_ID = $this->evaluation_post->get_id();

		$user_storage_database_helper = $this->bwg_base->user_storage_database_helper();
		$user_storage_database_helper->init_user_storage(
			$post_ID,
			$this->feup_user->Get_User_ID()
		);

		// Get the user storage of the current feup user linked to the current evaluation post.
		$user_storage = $user_storage_database_helper->get_user_storage(
			$post_ID,
			$this->feup_user->Get_User_ID()
		);

		// Enqueue the scripts and styles used by the frontend.
		$this->bwg_base->utils()->enqueue_script( BWG_Base::SCRIPT_HANDLE_FRONT );
		$this->bwg_base->utils()->enqueue_style( BWG_Base::STYLE_HANDLE_FRONT );

		if ( $this->evaluation_post->is_rating_closed() ) {
			// Ratings are closed...
			if ( ! is_null( $user_storage ) && ! is_null( $user_storage->get_submitted() ) ) {
				// ...and the user has submitted its gradings.
				return $this->render_submitted();
			} else {
				// ...and the user has not submitted its gradings or is not even linked to this evaluation > Show warning message.
				return '<p>' . __( 'Die Bewertungszeit ist vorbei.', 'bwg' ) . '</p>';
			}
		}

		if ( ! is_null( $user_storage ) && ! is_null( $user_storage->get_submitted() ) ) {
			return $this->render_submitted();
		}

		// Get the spider chart intro text.
		$spider_chart_intro_text = $this->bwg_base->options()->get_option_spider_chart_intro_text();

		// Get the spider chart final intro text and filter the [others][/others] sections.
		$spider_chart_final_intro_text = $this->bwg_base->options()->get_option_spider_chart_final_intro_text();
		if ( $this->bwg_base->evaluation_utils()->is_spider_all_visible( $post_ID ) ) {
			$spider_chart_intro_text = preg_replace( '/\[\/?others\]/', '', $spider_chart_intro_text );
		} else {
			$spider_chart_intro_text = preg_replace( '/\[others\](.*)\[\/others\]/', '', $spider_chart_intro_text );
		}

		// Get the profile fields for the current evaluation.
		$profile_fields = $this->evaluation_post->get_profile_fields();

		$ra = [
			'post'                           => $this->evaluation_post->get_post(),
			'evaluation_definition'          => $this->evaluation_post->get_evaluation_definition(),
			'grading'                        => $this->bwg_base->evaluation()->get_grading(),
			'user_storage'                   => $user_storage,
			'ajax_url'                       => admin_url( 'admin-ajax.php' ),
			'user_data_sequence_number'      => 0,
			'user_persist_ajax_action'       => BWG_Evaluation_Ajax::USER_PERSIST_AJAX_ACTION,
			'user_persist_ajax_action_nonce' => wp_create_nonce( BWG_Evaluation_Ajax::USER_PERSIST_AJAX_ACTION ),
			'spider_chart_ajax_action'       => BWG_Evaluation_Ajax::SPIDER_CHART_AJAX_ACTION,
			'spider_chart_ajax_action_nonce' => wp_create_nonce( BWG_Evaluation_Ajax::SPIDER_CHART_AJAX_ACTION ),
			'spider_chart_intro_text'        => wpautop( $spider_chart_intro_text ),
			'spider_chart_final_intro_text'  => wpautop( $spider_chart_final_intro_text ),
			'profile_fields_introduction'    => $profile_fields->get_introduction(),
			'profile_fields'                 => $profile_fields->render_evaluation_form_fields(
				$this->bwg_base->utils(),
				$this->bwg_base->profile_field_type_registry(),
				$user_storage
			),
		];

		return $this->bwg_base->utils()->render_template(
			'evaluation-form.php', $ra
		);
	}

	/**
	 * Renders the evaluation in submitted mode (i.e. the spider charts).
	 *
	 * @return string
	 */
	protected function render_submitted(): string {
		// User has submitted his gradings.
		$gradings = $this->bwg_base->submissions_database_helper()->get_weight_gradings_by_user(
			$this->evaluation_post->get_id(),
			$this->evaluation_post->get_evaluation_definition(),
			$this->feup_user->Get_User_ID()
		);

		$spider_charts = $this->bwg_base->chart_factory()->create_frontend_submitted_spider_charts(
			$this->evaluation_post,
			$gradings
		);

		return $this->bwg_base->utils()->render_template(
			'evaluation-form.submitted.php', [
				'evaluation_post' => $this->evaluation_post,
				'intro'           => '<p>' .
				                     __( 'Vielen Dank. Wir haben Ihre Bewertung erhalten.', 'bwg' ) .
				                     '</p>',
				'spider_charts'   => $spider_charts,
			]
		);
	}

	/**
	 * Renders the intro.
	 *
	 * The intro consists of the default content of the bwg_evaluation post expanded by
	 * registration and login links.
	 *
	 * @return string
	 */
	protected function render_intro(): string {
		$content = ( is_null( $this->content ) ) ? '' : $this->content;

		// Make sure that the [bwg] pseudo-shortcode is present.
		if ( FALSE === strpos( $content, '[-bwg-]' ) ) {
			$content .= PHP_EOL . '[-bwg-]';
		}

		// Get the special bwg content (which would replace the [-bwg-] pseudo placeholder).
		if ( $this->feup_user_is_logged_in ) {
			// User is logged in...
			if ( $this->evaluation_post->is_rating_closed() ) {
				// ...but the rating had been closed.
				$bwg_content = '<p>Rating closed. User has submitted? Show link to the static form : Sorry, ratings are closed!</p>';
			} else if ( $this->evaluation_post->is_rating_not_yet_open() ) {
				// ...and the rating is not yet open.
				$bwg_content = '<p>Rating not yet open. Try again when it started.</p>';
			} else {
				// ...and the rating is open.
				$bwg_content = '<p class="bwg-register-login"><a href="?e">' . __( 'Jetzt bewerten',
						'bwg' ) . '</a></p>';
			}
		} else {
			// User is not logged in.
			if ( $this->evaluation_post->is_rating_closed() ) {
				$bwg_content = '<p>Bewertungen sind nicht mehr möglich.</p>';
			} else {
				$bwg_content = $this->bwg_base->utils()->render_template(
					'intro-login-register-actions.php',
					[
						'bwg_mode'     => $this->bwg_base->options()->get_option_bwg_mode(),
						'register_url' => $this->bwg_base->options()
						                                 ->get_option_register_page_link( [ 'e' => $this->evaluation_post->get_id() ] ),
						'login_url'    => $this->bwg_base->options()
						                                 ->get_option_login_page_link( [ 'e' => $this->evaluation_post->get_id() ] ),
					]
				);
			}
		}

		return $this->bwg_base->utils()->render_template(
			'evaluation-intro.php',
			[
				'post'    => $this->evaluation_post->get_post(),
				'content' => str_replace( '[-bwg-]', $bwg_content, $content ),
			]
		);
	}

	/**
	 * Renders the "not allowed" screen.
	 *
	 * @return string
	 */
	protected function render_not_allowed(): string {
		return $this->bwg_base->utils()->render_template(
			'evaluation-not-allowed.php',
			[
				'post' => $this->evaluation_post->get_post(),
			]
		);
	}
}
