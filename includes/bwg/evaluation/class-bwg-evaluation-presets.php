<?php

namespace bwg\evaluation;

use bwg\BWG_Base;
use wpdb;

/**
 * Class BWG_Evaluation_Presets.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation_Presets {

	/**
	 * Ajax action: presets.
	 */
	const PRESETS_AJAX_ACTION = 'bwg_ed_presets';

	/**
	 * @var BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Evaluation_Presets constructor.
	 *
	 * @param BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;
	}

	public function register_ajax_callbacks() {
		add_action( 'wp_ajax_' . self::PRESETS_AJAX_ACTION,
			[ $this, 'wp_ajax_ed_preset' ] );
	}

	protected function _get_definition_preset( $id ) {
		/** @var wpdb $wpdb */
		global $wpdb;

		return $wpdb->get_row(
			$wpdb->prepare( 'SELECT * FROM ' . $wpdb->prefix . $this->_bwg_base->globals()
			                                                                   ->db_table_evaluation_presets() . ' WHERE id = %d',
				$id ),
			ARRAY_A
		);
	}

	/**
	 * Gets a list of all evaluation definition presets.
	 *
	 * @return array|null
	 */
	protected function _list_definition_presets() {
		/** @var wpdb $wpdb */
		global $wpdb;

		return $wpdb->get_results( 'SELECT * FROM ' . $wpdb->prefix . $this->_bwg_base->globals()
		                                                                              ->db_table_evaluation_presets() . ' ORDER BY label ASC',
			ARRAY_A );
	}


	/**
	 * Ajax Action Handler for the BWG_ED_PRESETS_AJAX_ACTION.
	 */
	public function wp_ajax_ed_preset() {
		check_ajax_referer( self::PRESETS_AJAX_ACTION );
		if ( ! current_user_can( 'edit_others_posts' ) ) {
			wp_die( - 1, 403 );
		}

		switch ( $_REQUEST['command'] ) {
			case 'preset-load':
				$json = [];

				$ed = $this->_get_definition_preset( $_REQUEST['id'] );
				if ( is_null( $ed ) ) {
					wp_send_json_error( [ 'message' => 'Preset not found.' ],
						403 );
				}

				$json['html'] = $this->_bwg_base->evaluation()->buildAdminEvaluationDefinitionHtml( $ed['definition'] );
				$json['val']  = $ed['definition'];

				wp_send_json_success( $json );
				break;

			case 'preset-load-list':
				$json = [];
				foreach ( $this->_list_definition_presets() as $record ) {
					array_push( $json, [
						'id'    => $record['id'],
						'label' => $record['label'],
					] );
				}

				wp_send_json_success( $json );
				break;
		}

		wp_send_json_error( [ 'message' => 'Command not found.' ], 403 );
	}
}