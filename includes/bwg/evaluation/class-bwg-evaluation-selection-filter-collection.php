<?php

namespace bwg\evaluation;

/**
 * Class BWG_Evaluation_Selection_Filter_Collection.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation_Selection_Filter_Collection {

	const SQL_AMOUNTS = 'amounts';

	const SQL_SELECTS = 'selects';

	/**
	 * @var \bwg\evaluation\BWG_Evaluation_Selection_Filter[]
	 */
	private $_filters = [];


	/**
	 * @param \bwg\evaluation\BWG_Evaluation_Selection_Filter $filter
	 */
	public function add_filter( BWG_Evaluation_Selection_Filter $filter ) {
		array_push( $this->_filters, $filter );
	}

	public function count_filters() {
		return count( $this->_filters );
	}

	public function get_filter( $index ) {
		return $this->_filters[ $index ];
	}

	private function _sql( $post_ID, $mode ) {
		global $wpdb;

		$sql = 'SELECT';
		switch ( $mode ) {
			case self::SQL_AMOUNTS:
				$sql .= ' COUNT(*) AS c';
				break;

			case self::SQL_SELECTS:
			default:
				$sql .= ' s.user_ID AS user_ID';
		}

		$user_storage_table_name = bwg_base()->user_storage_database_helper()->get_table_name();
		$feup_user_table_name    = bwg_base()->feup_layer()->get_user_table_name();

		$c_filters = count( $this->_filters );
		for ( $i = 0; $i < $c_filters; $i ++ ) {
			$sql .= ', ' . $this->_filters[ $i ]->sql_select( 'at' . $i, 'o' . $i );
		}

		$sql .= ' FROM ' . $user_storage_table_name . ' AS s ';
		$sql .= ' JOIN ' . $feup_user_table_name . ' AS u ON u.User_ID = s.user_ID ';

		for ( $i = 0; $i < $c_filters; $i ++ ) {
			$sql .= ' ' . $this->_filters[ $i ]->sql_table( 'at' . $i );
		}

		$sql .= ' WHERE s.submitted IS NOT NULL AND s.post_ID = %d';

		for ( $i = 0; $i < $c_filters; $i ++ ) {
			$w = trim( $this->_filters[ $i ]->sql_where( 'at' . $i, 'o' . $i ) );
			if ( empty( $w ) ) {
				continue;
			}

			$sql .= ' AND (' . $w . ')';
		}

		switch ( $mode ) {
			case self::SQL_AMOUNTS:
				$sql .= ' GROUP BY ';
				for ( $i = 0; $i < $c_filters; $i ++ ) {
					if ( $i > 0 ) {
						$sql .= ',';
					}
					$sql .= ' o' . $i;
				}
				break;

			case self::SQL_SELECTS:
			default:
		}

		return $wpdb->prepare( $sql, $post_ID );
	}

	public function sql_amounts( $post_ID ) {
		return $this->_sql( $post_ID, self::SQL_AMOUNTS );
	}

	public function sql_selects( $post_ID ) {
		return $this->_sql( $post_ID, self::SQL_SELECTS );
	}

	private function _build_group_percentages( &$group_percentages, $level = 0, $k = '', $pp = 1 ) {
		if ( $this->count_filters() <= $level ) {
			$group_percentages[ $k ] = $pp;

			return;
		}

		$percentages   = $this->get_filter( $level )->get_condition_percentages();
		$c_percentages = count( $percentages );
		for ( $i = 0; $i < $c_percentages; $i ++ ) {
			$new_k = $level > 0 ? $k . '/' . ( $i + 1 ) : ( $i + 1 );
			$this->_build_group_percentages( $group_percentages, $level + 1, $new_k, $pp * $percentages[ $i ] / 100 );
		}

		return;
	}

	public function get_group_percentages() {
		$group_percentages = [];

		// Build the group percentages.
		$this->_build_group_percentages( $group_percentages );

		return $group_percentages;
	}

	public function get_group_sizes( $sample_size ) {
		$group_sizes = [];

		// Calculate the group sizes.
		foreach ( $this->get_group_percentages() as $group_key => $group_percentage ) {
			$group_sizes[ $group_key ] = intval( round( $group_percentage * $sample_size ) );
		}

		// TODO [AS] Maybe we should optimize the group sizes in order to match the desired sample size, respecting more or less the percentages.

		return $group_sizes;
	}

	public function get_group_totals( $post_ID ) {
		global $wpdb;

		$groups = $this->get_group_sizes( 0 );

		$results = $wpdb->get_results( $this->sql_amounts( $post_ID ), ARRAY_A );
		foreach ( $results as $result ) {
			$k = '';
			for ( $i = 0; $i < $this->count_filters(); $i ++ ) {
				if ( $i > 0 ) {
					$k .= '/';
				}
				$k .= $result[ 'o' . $i ];
			}

			$groups[ $k ] = intval( $result['c'] );
		}

		return $groups;
	}

}
