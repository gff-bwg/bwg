<?php

namespace bwg\evaluation;

use bwg\BWG_Base;
use FEUP_User;

/**
 * Class BWG_Evaluation_Utils.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation_Utils {

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Evaluation_Utils constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;
	}

	/**
	 * Should we show the averages of all submitted gradings in the spider chart?
	 *
	 * @param $evaluation_ID
	 *
	 * @return bool
	 */
	public function is_spider_all_visible( $evaluation_ID ) {
		return $this->_bwg_base->user_storage_database_helper()->get_total_nr_submitted( $evaluation_ID ) > 0;
	}

	/**
	 * @param $post_ID
	 * @param $user_ID
	 *
	 * @return string|null
	 */
	public function get_evaluation_user_receiver( $post_ID, $user_ID ): ?string {
		if ( $this->_bwg_base->options()->get_option_bwg_mode() === 'drogothek' ) {
			// The receiver would be set in a profile field.
			return $this->get_receiver_from_profile_fields( $post_ID, $user_ID );
		}

		$feup = new FEUP_User();

		return $feup->Get_User_Name_For_ID( $user_ID );
	}

	/**
	 * @param int $post_ID
	 * @param int $user_ID
	 *
	 * @return string|null
	 */
	protected function get_receiver_from_profile_fields( $post_ID, $user_ID ) {
		$evaluation_post = $this->_bwg_base->factory()->bwg_evaluation_post( get_post( $post_ID ) );
		$profile_fields  = $evaluation_post->get_profile_fields();
		$user_storage    = $this->_bwg_base->user_storage_database_helper()->get_user_storage( $post_ID, $user_ID );

		foreach ( $profile_fields->get_fields() as $profile_field ) {
			if ( $profile_field->get_type() !== 'text' ) {
				// The type has to be `text`.
				continue;
			}

			/** @var \bwg\profile\fields\BWG_Profile_Field_Options_Text $profile_field_options */
			$profile_field_options = $profile_field->get_field_options(
				$this->_bwg_base->profile_field_type_registry()
			);

			if ( ! $profile_field_options->is_mode( 'input_email' ) ) {
				// The mode has to be `input_email`.
				continue;
			}

			$user_storage_meta = $user_storage->get_meta();
			if ( array_key_exists( $profile_field->get_uid(), $user_storage_meta ) ) {
				return $user_storage_meta[ $profile_field->get_uid() ];
			}
		}

		return NULL;
	}
}
