<?php

namespace bwg\evaluation;

/**
 * Class BWG_Evaluation_Definition_Item.
 */
class BWG_Evaluation_Definition_Item {

	/**
	 * @var array
	 */
	private $_json;

	/**
	 * @var int
	 */
	private $_level;


	/**
	 * BWG_Evaluation_Definition_Item constructor.
	 *
	 * @param int $level The level (starting at 0).
	 * @param array $json
	 */
	public function __construct( $level, $json = [] ) {
		$this->_level = $level;
		$this->_json  = $json;
	}

	/**
	 * @return int
	 */
	public function get_id() {
		return intval( $this->_json['id'] );
	}

	/**
	 * Gets the label of this item.
	 *
	 * @param bool $sanitize Should we sanitize the label (trim and replace '|' by one white space).
	 *
	 * @return string
	 */
	public function get_label( $sanitize = TRUE ) {
		if ( ! isset( $this->_json['label'] ) ) {
			return '';
		}

		if ( $sanitize ) {
			return trim( str_replace( '|', ' ', $this->_json['label'] ) );
		}

		return $this->_json['label'];
	}

	/**
	 * Gets the description.
	 *
	 * @param string $placeholder_title
	 *
	 * @return string
	 */
	public function get_description( $placeholder_title = '' ) {
		if ( ! isset( $this->_json['description'] ) ) {
			return '';
		}

		return str_replace( [
			'#TITLE#'
		], [
			$placeholder_title
		], $this->_json['description'] );
	}

	/**
	 * Gets the details.
	 *
	 * @return string
	 */
	public function get_details(): string {
		if ( ! isset( $this->_json['details'] ) ) {
			return '';
		}

		return $this->_json['details'];
	}

	/**
	 * @return float
	 */
	public function get_weight() {
		if ( ! isset( $this->_json['weight'] ) ) {
			return 1.0;
		}

		return floatval( $this->_json['weight'] );
	}

	/**
	 * Should this item be shown in the spider chart? 1 or 0.
	 *
	 * Caution: This setting is only for level 2 (category).
	 *
	 * @return int
	 */
	public function get_spider_chart() {
		if ( ! isset( $this->_json['spider_chart'] ) ) {
			return 1;
		}

		return intval( $this->_json['spider_chart'] );
	}

	/**
	 * @return bool
	 */
	public function has_items() {
		return ( isset( $this->_json['items'] ) && ! empty( $this->_json['items'] ) );
	}

	/**
	 * @return BWG_Evaluation_Definition_Item[]
	 */
	public function get_items() {
		if ( ! isset( $this->_json['items'] ) ) {
			return [];
		}

		$items = [];
		for ( $i = 0; $i < count( $this->_json['items'] ); $i ++ ) {
			if ( ! isset( $this->_json['items'][ $i ]['_obj'] ) ) {
				$this->_json['items'][ $i ]['_obj'] = new BWG_Evaluation_Definition_Item(
					$this->_level + 1,
					$this->_json['items'][ $i ]
				);
			}

			array_push( $items, $this->_json['items'][ $i ]['_obj'] );
		}

		return $items;
	}

	/**
	 * Finds an item by id. The method would also search the descendants.
	 *
	 * @param int $id
	 *
	 * @param array $breadcrumb
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Definition_Item[]
	 */
	public function find_item_breadcrumb( $id, array $breadcrumb = [] ) {
		foreach ( $this->get_items() as $item ) {
			if ( intval( $id ) === intval( $item->get_id() ) ) {
				array_push( $breadcrumb, $item );

				return $breadcrumb;
			}

			$item_breadcrumb = $item->find_item_breadcrumb( $id );
			if ( ! empty( $item_breadcrumb ) ) {
				return array_merge( $breadcrumb, $item_breadcrumb );
			}
		}

		return [];
	}

}
