<?php

namespace bwg\evaluation;

use bwg\BWG_Base;

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Class BWG_Evaluation_Analyses_List_Table.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation_Analyses_List_Table extends \WP_List_Table {

	/**
	 * Bulk action 'delete'
	 */
	const ACTION_DELETE = 'delete';

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;

	/**
	 * @var int
	 */
	private $_post_ID;

	/**
	 * @var int
	 */
	public $per_page = 20;

	/**
	 * @var string
	 */
	public $search = '';


	/**
	 * BWG_Evaluation_Analyses_List_Table constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 * @param int $post_ID
	 */
	public function __construct( BWG_Base $bwg_base, $post_ID ) {
		parent::__construct( [
			'singular' => 'analysis',
			'plural'   => 'analyses',
			'ajax'     => FALSE,
		] );

		$this->_bwg_base = $bwg_base;
		$this->_post_ID  = $post_ID;
	}

	/**
	 * Renders the column 'cb'.
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	public function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="%1$s[]" value="%2$s" />',
			$this->_args['singular'], $item['ID']
		);
	}

	/**
	 * Renders the column 'label'.
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	public function column_label( $item ) {
		return esc_html( $item['label'] );
	}

	// TODO [AS] Add other column renderer.

	/**
	 * Gets the columns.
	 *
	 * @return array
	 */
	public function get_columns() {
		$columns = [
			'cb'    => '<input type="checkbox" />',
			'label' => __( 'Label', 'bwg' ),
		];

		return $columns;
	}

	/**
	 * Gets the sortable columns.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = [
			'label' => [ 'label', FALSE ],
		];

		return $sortable_columns;
	}

	/**
	 * Gets the bulk actions.
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
			self::ACTION_DELETE => __( 'Delete', 'bwg' ),
		];

		return $actions;
	}

	/**
	 * Prepares the items.
	 */
	public function prepare_items() {
		$columns  = $this->get_columns();
		$hidden   = get_hidden_columns( get_current_screen() );
		$sortable = $this->get_sortable_columns();

		$this->_column_headers = [ $columns, $hidden, $sortable ];

		$total_items = $this->_bwg_base->analyses_database_helper()->list_table_count(
			$this->_post_ID, $this->search
		);

		$this->items = $this->_bwg_base->analyses_database_helper()->list_table_items(
			$this->get_pagenum(),
			$this->per_page,
			isset( $_REQUEST['orderby'] ) ? $_REQUEST['orderby'] : 'label',
			isset( $_REQUEST['order'] ) ? $_REQUEST['order'] : 'asc',
			$this->_post_ID,
			$this->search
		);

		$this->set_pagination_args( [
			'total_items' => $total_items,
			'per_page'    => $this->per_page,
			'total_pages' => ceil( $total_items / $this->per_page ),
		] );
	}

	/**
	 * @return string|false
	 */
	public function current_action_run() {
		if ( isset( $_REQUEST['action_run'] ) ) {
			return $_REQUEST['action_run'];
		}

		return FALSE;
	}

}