<?php

namespace bwg\evaluation;

/**
 * Class BWG_Evaluation_Grading.
 */
class BWG_Evaluation_Grading {

	/**
	 * @var float
	 */
	public $points;

	/**
	 * @var string
	 */
	public $label;


	/**
	 * BWG_Evaluation_Grading constructor.
	 *
	 * @param float $points
	 * @param string $label
	 */
	public function __construct( $points, $label ) {
		$this->points = $points;
		$this->label  = $label;
	}

}