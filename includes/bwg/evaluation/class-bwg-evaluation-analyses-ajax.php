<?php

namespace bwg\evaluation;

use bwg\BWG_Base;

class BWG_Evaluation_Analyses_Ajax {

	/**
	 * Ajax action: filter groups.
	 */
	const FILTER_GROUPS_AJAX_ACTION = 'bwg_filter_groups';

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Evaluation_Analyses_Ajax constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;
	}

	/**
	 * Registers private (admin only) ajax callbacks.
	 */
	public function register_private_ajax_callbacks() {
		add_action( 'wp_ajax_' . self::FILTER_GROUPS_AJAX_ACTION,
			[ $this, 'wp_ajax_filter_groups' ] );
	}

	/**
	 * Ajax Action Handler for the FILTER_GROUPS_AJAX_ACTION.
	 */
	public function wp_ajax_filter_groups() {
		check_ajax_referer( self::FILTER_GROUPS_AJAX_ACTION );
		
		$json = [
			'filter_groups' => $this->_bwg_base->evaluation_analyses()->render_filter_groups_table(
				'1' === $_REQUEST['bwg_sex_enabled'],
				$_REQUEST['bwg_sex_percentages'],
				'1' === $_REQUEST['bwg_age_enabled'],
				$_REQUEST['bwg_age_limits'],
				$_REQUEST['bwg_age_percentages'],
				intval( $_REQUEST['bwg_sample_size'] ),
				intval( $_REQUEST['post_ID'] )
			),
		];

		return wp_send_json_success( $json );
	}

}