<?php

namespace bwg\evaluation;

use bwg\BWG_Base;
use bwg\database\helpers\BWG_User_Action_Logs_Database_Helper;
use bwg\profile\fields\BWG_Profile_Field_Options_Checkbox;
use bwg\profile\fields\BWG_Profile_Field_Options_Choice;
use bwg\stats\BWG_Stats;
use bwg\stats\strata\BWG_Stats_Strata_Collection;
use bwg\stats\strata\selectors\BWG_Stats_Strata_Checkbox_Value_Selector;
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Chart\Axis;
use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use WP_Post;
use WP_Screen;

/**
 * Class BWG_Evaluation_Users.
 *
 * @package bwg\evaluation
 */
final class BWG_Evaluation_Users {

	/**
	 * Slug for the admin evaluation users page.
	 */
	const PAGE_SLUG = 'bwg_users';

	/**
	 * Name for the screen option 'per_page'.
	 */
	const SCREEN_OPTION_PER_PAGE = 'bwg_evaluation_users_per_page';

	/**
	 * Name for the user meta for run feedback of unsubmitted action.
	 */
	const RUN_FEEDBACK_UNSUBMITTED = 'bwg_run_feedback_evaluation_user_unsubmitted';

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;

	/**
	 * @var \bwg\evaluation\BWG_Evaluation_Users_List_Table
	 */
	private $_list_table;


	/**
	 * BWG_Evaluation_Users constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;

		if ( $this->_is_evaluation_user_page() ) {
			add_action( 'init', [ $this, 'init' ] );
			add_action( 'admin_init', [ $this, 'admin_init' ] );
		}

		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
	}

	/**
	 * Handler for 'init' action.
	 *
	 * Note that this handler would only be called when we are in the scope of an evaluation users page.
	 */
	public function init() {
		add_filter( 'set-screen-option', [ $this, 'set_screen_option' ], 10, 3 );
	}

	/**
	 * Handler for 'admin_init' action.
	 *
	 * Note that this handler would only be called when we are in the scope of an evaluation users page.
	 */
	public function admin_init() {
		add_action( 'current_screen', [ $this, 'current_screen' ] );
	}

	/**
	 * Handler for 'admin_menu' action.
	 */
	public function admin_menu() {
		add_submenu_page(
			'', // Not shown in menu.
			__( 'BWG Evaluation Users', 'bwg' ),
			'',
			'manage_options',
			self::PAGE_SLUG,
			[ $this, 'admin_page' ]
		);
	}

	/**
	 * Is the current page an evaluation users page (if so, this class is responsible to handle the request)?
	 *
	 * @return bool
	 */
	protected function _is_evaluation_user_page() {
		return (
			( isset( $_REQUEST['page'] ) && self::PAGE_SLUG === $_REQUEST['page'] ) &&
			( isset( $_REQUEST['post_type'] ) && 'bwg_evaluation' === $_REQUEST['post_type'] )
		);
	}

	/**
	 * Handler for 'current_screen' action.
	 *
	 * Note that this handler would only be called when we are on the screen with post_type = 'bwg_evaluation' and
	 * page = 'bwg_users'.
	 *
	 * @param \WP_Screen $current_screen
	 */
	public function current_screen( WP_Screen $current_screen ) {
		// The current screen is post_type 'bwg_evaluation' and page 'bwg_users'.
		$list_table = $this->get_list_table();

		if ( BWG_Evaluation_Users_List_Table::ACTION_UNSUBMIT === $list_table->current_action_run() ) {
			///
			// Run action: 'Unsubmit'
			///
			$evaluation_ID = intval( $_REQUEST['post'] );
			$user_ids      = explode( ',', $_REQUEST['user_ids'] );
			foreach ( $user_ids as $uid ) {
				$user_ID = intval( $uid );

				if ( $user_ID > 0 ) {
					$this->_bwg_base->user_storage_database_helper()->unsubmit( $evaluation_ID, $user_ID );
					$this->_bwg_base->submissions_database_helper()->delete_submission( $evaluation_ID, $user_ID );
				}
			}

			// We assume that the above worked (actually we should perform error checking).
			add_user_meta( get_current_user_id(), self::RUN_FEEDBACK_UNSUBMITTED, 1, TRUE );

			wp_redirect( $_REQUEST['redirect'] );

			return;
		}

		if ( FALSE !== $list_table->current_action() ) {
			// We are currently in an action "sub-screen" (usually a "confirm the action" screen).
			add_filter( 'screen_options_show_screen', function ( $show_screen, $screen ) {
				return FALSE;
			}, 10, 2 );

			return;
		}

		// We are not in an action "sub-screen". So handle the default list table screen stuff.
		$user_id = get_current_user_id();
		if ( 1 == get_user_meta( $user_id, self::RUN_FEEDBACK_UNSUBMITTED, TRUE ) ) {
			add_action( 'admin_notices', [ $this, 'notice_feedback_unsubmitted' ] );
			delete_user_meta( $user_id, self::RUN_FEEDBACK_UNSUBMITTED );
		}

		add_screen_option(
			'per_page',
			[
				'option'  => self::SCREEN_OPTION_PER_PAGE,
				'default' => 20,
			]
		);
	}

	/**
	 * Handler for 'set-screen-option' filter.
	 */
	function set_screen_option( $status, $option, $value ) {
		if ( self::SCREEN_OPTION_PER_PAGE === $option ) {
			return min( 100, max( 0, $value ) );
		}

		return $status;
	}

	/**
	 * Gets the list table. Note that this method actually caches the object. The post id would be read from the request
	 * parameter 'post'.
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Users_List_Table
	 */
	public function get_list_table() {
		if ( is_null( $this->_list_table ) ) {
			$this->_list_table = new BWG_Evaluation_Users_List_Table( $this->_bwg_base, intval( $_REQUEST['post'] ) );
		}

		return $this->_list_table;
	}

	/**
	 * Menu page handler.
	 */
	public function admin_page() {
		$post = get_post( intval( $_REQUEST['post'] ) );
		if ( ! $post ) {
			wp_die( $this->_bwg_base->admin_render_error_wrap( 'Evaluation konnte nicht geladen werden.' ) );
		}

		$list_table = $this->get_list_table();
		$action     = $list_table->current_action();
		if ( BWG_Evaluation_Users_List_Table::ACTION_UNSUBMIT === $action ) {
			echo $this->_render_unsubmit_confirmation( $post );

			return;
		}

		if ( BWG_Evaluation_Users_List_Table::ACTION_VIEW === $action ) {
			echo $this->_render_user_details_view( $post );

			return;
		}

		if ( ( $this->_bwg_base->options()->get_option_bwg_mode() === 'drogothek' ) ) {
			if ( 'mail-sender' === $action ) {
				echo $this->render_mail_sender_page( $post );

				return;
			}
		}

		if ( 'export-stats' === $action ) {
			$this->export_stats( $post );

			return;
		}

		if ( 'stats' === $action ) {
			echo $this->render_stats_page( $post );

			return;
		}

		echo $this->_render_admin_page( $post, $list_table );
	}

	/**
	 * Renders the default admin page (the list table).
	 *
	 * @param \WP_Post $post
	 *
	 * @param \bwg\evaluation\BWG_Evaluation_Users_List_Table $list_table
	 *
	 * @return mixed|string
	 */
	protected function _render_admin_page( WP_Post $post, BWG_Evaluation_Users_List_Table $list_table ) {
		$per_page = get_user_meta( get_current_user_id(), self::SCREEN_OPTION_PER_PAGE, TRUE );
		if ( empty( $per_page ) ) {
			$per_page = get_current_screen()->get_option( 'per_page', 'default' );
		}

		$list_table->per_page = intval( $per_page );
		if ( isset( $_REQUEST['s'] ) ) {
			$list_table->search = $_REQUEST['s'];
		}
		$list_table->prepare_items();

		$ra = [
			'post'       => $post,
			'list_table' => $list_table,
			'bwg_mode'   => $this->_bwg_base->options()->get_option_bwg_mode(),
			'search'     => ( isset( $_REQUEST['s'] ) ? wp_unslash( $_REQUEST['s'] ) : '' ),
		];

		return $this->_bwg_base->utils()->render_admin_template(
			'evaluation/users/users.php', $ra
		);
	}

	/**
	 * Renders the stats for submissions of the given evaluation.
	 *
	 * @param WP_Post $post
	 *
	 * @return string
	 */
	protected function render_stats_page( WP_Post $post ): string {
		// Get the evaluation post.
		$evaluation_post = $this->_bwg_base->factory()->bwg_evaluation_post( $post );

		//// PLAYGROUND >>>
		if ( FALSE ) {
			/// Gewünschtes Konfidenzniveau 	Z-Wert
			/// 80 % 	1,28
			/// 85 % 	1,44
			/// 90 % 	1,65
			/// 95 % 	1,96
			/// 99 % 	2,58
			$N = 4500;
			$z = 1.96;
			$n = 120;
			$p = 0.5;

			$e2 = $z * $z * $p * ( 1 - $p ) * ( 1 - $n / $N ) / $n;
			echo sprintf( 'Fehlerbereich +/- %0.02f%%', 100 * sqrt( $e2 ) );
		}
		/// >>>>
		if ( FALSE ) {
			$feup                 = new \FEUP_User();
			$submitted_list_items = $this->_bwg_base
				->user_storage_database_helper()->get_submitted_list(
					$evaluation_post->get_id(), 1, 999, 'user_ID', 'asc' );
			foreach ( $submitted_list_items as $submitted_list_item ) {
				$user_email = $feup->Get_User_Name_For_ID( $submitted_list_item['user_ID'] );
				if ( is_null( $user_email ) ) {
					continue;
				}

				$user_submission = $this->_bwg_base
					->submissions_database_helper()
					->get_submission_by_user( $evaluation_post->get_id(), $submitted_list_item['user_ID'] );

				$record = $user_submission->get_record();

				echo $user_email . ";";
				foreach ( $evaluation_post->get_profile_fields()->get_fields() as $profile_field ) {
					$v = $record[ $profile_field->db_submissions_column_name() ];

					if ( 'choice' === $profile_field->get_type() ) {
						/** @var BWG_Profile_Field_Options_Choice $options */
						$options = $profile_field->get_field_options( $this->_bwg_base->profile_field_type_registry() );
						$choices = $options->get_choices();
						echo $choices[ $v ];
					} else {
						echo htmlspecialchars( $v );
					}

					echo ';';
				}
				echo "<br>";
			}
		}
		// <<<<< PLAYGROUND

		///
		// Get the database helpers.
		///
		$submissions_db_helper  = $this->_bwg_base->submissions_database_helper();
		$user_storage_db_helper = $this->_bwg_base->user_storage_database_helper();


		///
		// Notes
		///
		$notes                = [];
		$submitted_list_items = $user_storage_db_helper
			->get_submitted_list( $evaluation_post->get_id(), 1, 999, 'user_ID', 'asc' );
		foreach ( $submitted_list_items as $submitted_list_item ) {
			$user_storage  = $user_storage_db_helper
				->get_user_storage( $evaluation_post->get_id(), $submitted_list_item['user_ID'] );
			$user_gradings = NULL;
			foreach ( $user_storage->get_notes() as $key => $note ) {
				if ( ! empty( $note['value'] ) ) {
					if ( ! array_key_exists( $key, $notes ) ) {
						$notes[ $key ] = [];
					}

					if ( is_null( $user_gradings ) ) {
						$user_gradings = $submissions_db_helper->get_weight_gradings_by_user(
							$evaluation_post->get_id(),
							$evaluation_post->get_evaluation_definition(),
							$user_storage->get_user_ID()
						);
					}

					$notes[ $key ][] = $note['value'] . ' (' . number_format_i18n( $user_gradings[ $key ], 2 ) . ')';
				}
			}
		}

		if ( filter_input( INPUT_GET, 'test' ) > 1 ) {
			echo '<h3>Profilfelder</h3>';

			echo '<table>';
			$profile_fields = $evaluation_post->get_profile_fields();
			foreach ( $profile_fields->get_fields() as $field ) {
				echo '<tr><td>' . esc_html( $field->get_label() ) . '</td><td>[ ' . $field->db_submissions_column_name
					() . ' ]</td></tr>';
			}
			echo '</table>';

			$field_uid = 'pf-l3iodr8x';

			/** @var BWG_Profile_Field_Options_Checkbox $field_options */
			$profile_field = $profile_fields->get_field_by_uid( $field_uid );
			echo '<h3>Werte des Profilfeld: "' . esc_html( $profile_field->get_label() ) . '"</h3>';
			$field_options = $profile_field->get_field_options( $this->_bwg_base->profile_field_type_registry() );
			echo '<table>';
			foreach ( $field_options->get_checkboxes() as $key => $label ) {
				echo '<tr><td> ' . esc_html( $label ) . '</td><td>[ ' . $key . ' ]</td></tr>';
			}
			echo '</table>';
		}

		$stats = $this->build_stats_from_request( $evaluation_post );

		// TODO [AS] Once the strata work as expected we can remove the following condition-block.
		if ( TRUE ) {
			// Get the weighted gradings (averaged over all submissions).
			$gradings = $submissions_db_helper
				->get_weight_gradings(
					$evaluation_post->get_id(),
					$evaluation_post->get_evaluation_definition()
				);

			// Get the statistics.
			$statistics = $submissions_db_helper
				->get_statistics(
					$evaluation_post->get_id(),
					$evaluation_post->get_evaluation_definition()
				);
		}

		// Get the spider charts (interest level).
		$spider_charts = $this->_bwg_base->chart_factory()->create_interest_spider_charts(
			$evaluation_post,
			$stats->get_strata_gradings()
		);

		// Get the profile field "strata" stats.
		$profile_field_strata_stats = [];
		$pft_registry               = $this->_bwg_base->profile_field_type_registry();
		foreach ( $evaluation_post->get_profile_fields()->get_fields() as $profile_field ) {
			if ( ! $profile_field->get_field_type( $pft_registry )->is_strata_stats_supported() ) {
				continue;
			}

			array_push(
				$profile_field_strata_stats,
				$profile_field->render_strata_stats( $this->_bwg_base->utils(), $pft_registry, $evaluation_post )
			);
		}

		// Get the profile field "non-strata" stats.
		$profile_field_stats = [];
		foreach ( $evaluation_post->get_profile_fields()->get_fields() as $profile_field ) {
			if ( ! $profile_field->get_field_type( $pft_registry )->is_stats_enabled( $profile_field ) ) {
				continue;
			}

			array_push(
				$profile_field_stats,
				$profile_field->render_stats( $this->_bwg_base->utils(), $pft_registry, $evaluation_post )
			);
		}

		$ra = [
			'post'                       => $post,
			'evaluation'                 => $evaluation_post,
			'total_nr_submissions'       => intval(
				$submissions_db_helper->get_total_nr_submissions( $evaluation_post->get_id() )
			),
			'stratas'                    => $stats->get_strata_collection()->get_stratas(),
			'strata_gradings'            => $stats->get_strata_gradings(),
			'strata_statistics'          => $stats->get_strata_statistics(),
			'gradings'                   => $gradings,
			'statistics'                 => $statistics,
			'notes'                      => $notes,
			'spider_charts'              => $spider_charts,
			'profile_field_strata_stats' => $profile_field_strata_stats,
			'profile_field_stats'        => $profile_field_stats,
		];

		// Enqueue the styles and scripts used for this view.
		$this->_bwg_base->utils()->enqueue_style(
			'bwg-evaluation-users-stats',
			'css/admin/evaluation/users/stats.css',
			[ BWG_Base::STYLE_HANDLE_ADMIN ], TRUE
		);
		$this->_bwg_base->utils()->enqueue_script(
			'bwg-evaluation-users-stats',
			'js/admin/evaluation/users/stats.js',
			[ 'jquery', BWG_Base::SCRIPT_HANDLE_LIB_FILE_SAVER, BWG_Base::SCRIPT_HANDLE_ADMIN ], TRUE
		);

		// Enqueue the styles and scripts for the strata stats boxes.
		$this->_bwg_base->utils()->enqueue_style(
			'bwg-evaluation-profile-fields-strata-stats',
			'css/admin/evaluation/profile-fields/strata-stats-box.css',
			[], TRUE
		);

		// Enqueue the styles and scripts for the stats boxes.
		$this->_bwg_base->utils()->enqueue_style(
			'bwg-evaluation-profile-fields-stats',
			'css/admin/evaluation/profile-fields/stats-box.css',
			[], TRUE
		);

		return $this->_bwg_base->utils()->render_admin_template(
			'evaluation/users/stats.php', $ra
		);
	}

	/**
	 * Renders the details view of the gradings of a user.
	 *
	 * @param WP_Post $post
	 *
	 * @return string
	 */
	protected function _render_user_details_view( $post ) {
		// Get the FEUP Plugin.
		$feup = new \FEUP_User();

		// Get the user id.
		$user_ID = intval( $_REQUEST['user_ID'] );

		// Build the evaluation post.
		$evaluation_post = new BWG_Evaluation_Post( $post );

		// Load the user storage.
		$user_storage = $this->_bwg_base->user_storage_database_helper()->get_user_storage( $post->ID, $user_ID );

		// Calculate some totals from the logs.
		$sum_times   = $this->_bwg_base->user_action_logs_database_helper()->sum_time( $post->ID, $user_ID );
		$sum_actions = $this->_bwg_base->user_action_logs_database_helper()->sum_actions( $post->ID, $user_ID );

		// Get the submission.
		$submission = $this->_bwg_base->submissions_database_helper()->get_submission_by_user( $post->ID, $user_ID );

		// Get the weighted gradings of the submission for the current user.
		$weighted_gradings = $this->_bwg_base->submissions_database_helper()->get_weight_gradings_by_user(
			$post->ID,
			$evaluation_post->get_evaluation_definition(),
			$user_ID
		);

		$ra = [
			'post'              => $post,
			'evaluation_post'   => $evaluation_post,
			'user_storage'      => $user_storage,
			'submission'        => $submission,
			'weighted_gradings' => $weighted_gradings,
			'username'          => $feup->Get_User_Name_For_ID( $user_ID ),
			'sum_time_active'   => isset( $sum_times[ '#' . $user_ID ] ) ? $sum_times[ '#' . $user_ID ]['active'] : '&ndash;',
			'sum_time_inactive' => isset( $sum_times[ '#' . $user_ID ] ) ? $sum_times[ '#' . $user_ID ]['inactive'] : '&ndash;',
			'sum_action_goto'   => isset( $sum_actions[ '#' . $user_ID ] ) ? $sum_actions[ '#' . $user_ID ][ BWG_User_Action_Logs_Database_Helper::ACTION_GOTO ] : '&ndash;',
			'sum_action_vote'   => isset( $sum_actions[ '#' . $user_ID ] ) ? $sum_actions[ '#' . $user_ID ][ BWG_User_Action_Logs_Database_Helper::ACTION_VOTE ] : '&ndash;',
		];

		$this->_bwg_base->utils()->enqueue_script(
			'bwg-evaluation-users-view',
			'js/admin/evaluation/users/view.js',
			[ 'jquery' ], TRUE
		);
		$this->_bwg_base->utils()->enqueue_style(
			'bwg-evaluation-users-view',
			'css/admin/evaluation/users/view.css',
			[ BWG_Base::STYLE_HANDLE_ADMIN ], TRUE
		);

		return $this->_bwg_base->utils()->render_admin_template( 'evaluation/users/view.php', $ra );
	}

	/**
	 * Renders the unsubmit confirmation page.
	 *
	 * @param \WP_Post $post
	 *
	 * @return bool|string
	 */
	protected function _render_unsubmit_confirmation( WP_Post $post ) {
		$feup = new \FEUP_User();

		// Get a list of usernames and user ids.
		$usernames = [];
		$user_ids  = [];
		if ( isset( $_REQUEST['user_ID'] ) ) {
			$user_ID = intval( $_REQUEST['user_ID'] );
			array_push( $user_ids, $user_ID );
			array_push( $usernames, $feup->Get_User_Name_For_ID( $user_ID ) );
		}

		if ( isset( $_REQUEST['user'] ) ) {
			foreach ( $_REQUEST['user'] as $user_ID ) {
				$user_ID = intval( $user_ID );
				array_push( $user_ids, $user_ID );
				array_push( $usernames, $feup->Get_User_Name_For_ID( $user_ID ) );
			}
		}

		if ( empty( $usernames ) ) {
			return wp_redirect( wp_get_referer() );
		}

		$content = '<h2>' . __( 'Aktion bestätigen', 'bwg' ) . '</h2>'
		           . '<p>' . _n(
			           'Wollen Sie die eingereichte Bewertung des folgenden Benutzers wirklich wieder freigeben?',
			           'Wollen Sie die eingereichten Bewertungen der folgenden Benutzer wirklich wieder freigeben?',
			           count( $usernames ), 'bwg'
		           ) . '</p>'
		           . '<ul class="list-square">';
		foreach ( $usernames as $username ) {
			$content .= '<li>' . $username . '</li>';
		}
		$content .= '</ul><p>' . __( 'Achtung: Diese Aktion kann nicht rückgängig gemacht werden.', 'bwg' ) . '</p>';

		$this->_bwg_base->utils()->enqueue_style( BWG_Base::STYLE_HANDLE_ADMIN );

		return $this->_bwg_base->utils()->render_admin_template( 'simple-dialog.php', [
			'heading' => $post->post_title,
			'content' => $content,
			'fields'  => [
				'post_type'  => $_REQUEST['post_type'],
				'post'       => $_REQUEST['post'],
				'page'       => $_REQUEST['page'],
				'redirect'   => wp_get_referer(),
				'action_run' => BWG_Evaluation_Users_List_Table::ACTION_UNSUBMIT,
				'user_ids'   => implode( ',', $user_ids ),
			],
			'buttons' => [
				[
					'label'   => __( 'Ja - jetzt wieder freigeben', 'bwg' ),
					'classes' => [ 'button-primary', ],
					'type'    => 'submit',
				],
				[
					'label'   => __( 'Cancel', 'bwg' ),
					'classes' => [ 'button-link', ],
					'href'    => 'javascript:history.back()',
				],
			],
		] );
	}

	/**
	 * Handler for the 'admin_notices' action in case the 'unsubmit' action had been run successfully.
	 */
	public function notice_feedback_unsubmitted() {
		echo '<div class="notice notice-success"><p>'
		     . __( 'Die Aktion wurde erfolgreich ausgeführt.', 'bwg' )
		     . '</p></div>';
	}

	/**
	 * Calculates the strata statistics.
	 *
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
	 * @param string|false|null $strata_field_uid
	 *
	 * @return \bwg\stats\BWG_Stats
	 */
	protected function _calculate_statistics( BWG_Evaluation_Post $evaluation_post, $strata_field_uid = FALSE ) {
		$stats_strata_utils = $this->_bwg_base->stats_strata_utils();

		$strata_collection = $stats_strata_utils->get_strata_collection(
			$evaluation_post,
			$strata_field_uid
		);

		return new BWG_Stats(
			$this->_bwg_base,
			$evaluation_post,
			$strata_collection
		);
	}

	protected function build_stats_from_request( BWG_Evaluation_Post $evaluation_post ): BWG_Stats {
		if ( filter_input( INPUT_GET, 'test' ) > 0 ) {
			// TODO This test code should of course be done such that an admin can build the desired strata collection
			//      using a simple ui. Here we just made an example of three strata:
			//      1) All - no selectors at all.
			//      2) Horizont - Only those submissions that have checked the "Horizont" checkbox in the profile field
			//         for "Zugehörigkeit zu einer Gruppierung/Kette" (field uid: pf-l3iodr8x).
			//      3) Verein Naturdrogerien - Only those submissions that have checked the "Verein Naturdrogerien"
			//         checkbox in the profile field for "Zugehörigkeit zu einer Gruppierung/Kette" (field uid:
			//          pf-l3iodr8x).
			$field_uid = 'pf-l3iodr8x';

			$profile_fields = $evaluation_post->get_profile_fields();
			$profile_field  = $profile_fields->get_field_by_uid( $field_uid );

			$c = new BWG_Stats_Strata_Collection();
			$c->build_and_add_strata( 'Alle', [] );
			$c->build_and_add_strata( 'Horizont', [
				new BWG_Stats_Strata_Checkbox_Value_Selector(
					$profile_field->db_submissions_column_name(), 'c-l3iofmza'
				),
			] );
			$c->build_and_add_strata( 'Verein Naturdrogerien', [
				new BWG_Stats_Strata_Checkbox_Value_Selector(
					$profile_field->db_submissions_column_name(), 'c-l3iofwxk'
				),
			] );

			return new BWG_Stats( $this->_bwg_base, $evaluation_post, $c );
		}

		// Get the strata profile field uid from the HTTP GET params (optional).
		$strata_field_uid = filter_input( INPUT_GET, 'strata_field_uid' );
		if ( ! is_string( $strata_field_uid ) ) {
			$strata_field_uid = NULL;
		}

		// Calculate the stats.
		return $this->_calculate_statistics( $evaluation_post, $strata_field_uid );
	}

	/**
	 * @param WP_Post $post
	 *
	 * @return string
	 */
	protected function export_stats( WP_Post $post ): string {
		// Get the evaluation post.
		$evaluation_post = $this->_bwg_base->factory()->bwg_evaluation_post( $post );

		// Get the stats.
		$stats = $this->build_stats_from_request( $evaluation_post );

		$strata_statistics_collection = $stats->get_strata_statistics();
		try {
			$spreadsheet = new Spreadsheet();
			Cell::setValueBinder( new AdvancedValueBinder() );
			$spreadsheet->removeSheetByIndex( 0 );

			$style_heading1      = [
				'font'      => [
					'bold' => TRUE,
					'size' => 20,
				],
				'borders'   => [
					'top' => [
						'borderStyle' => Border::BORDER_MEDIUM,
						'color'       => [ 'rgb' => '333333' ],
					],
				],
				'alignment' => [
					'vertical' => [
						Alignment::VERTICAL_CENTER
					]
				],
			];
			$row_height_heading1 = 40;

			$style_heading2      = [
				'font' => [
					'bold' => TRUE,
					'size' => 13,
				],
			];
			$row_height_heading2 = 16;

			$style_description = [
				'font' => [
					'color' => [ 'rgb' => '999999' ],
				],
			];

			$chart_col_span = 7;

			foreach ( $evaluation_post->get_evaluation_definition()->get_items() as $item1 ) {
				$sheet = $spreadsheet->createSheet();
				// The title should not contain chars outside of a-z and A-Z and 0-9.
				$sheet_title = substr( preg_replace(
					'/[\x00-\x1F\x21-\x2f\x3a-\x40\x5b-\x60\x7b-\xFF]/', '', $item1->get_label()
				), 0, 28 );
				$sheet->setTitle( $sheet_title );
				$sheet_reference = '\'' . $sheet->getTitle() . '\'';

				$row = 1;
				foreach ( $item1->get_items() as $item2 ) {
					if ( ! $item2->get_spider_chart() ) {
						continue;
					}

					$sheet->setCellValueByColumnAndRow( 1, $row, $item2->get_label() );
					$sheet->getStyleByColumnAndRow( 1, $row )->applyFromArray( $style_heading1 );
					$sheet->getRowDimension( $row )->setRowHeight( $row_height_heading1 );
					$sheet->mergeCellsByColumnAndRow(
						1, $row,
						1 + 2 * count( $strata_statistics_collection ) + $chart_col_span, $row
					);
					$row ++;

					foreach ( $item2->get_items() as $item3 ) {
						$sheet->setCellValueByColumnAndRow( 1, $row, $item3->get_label() );
						$sheet->getStyleByColumnAndRow( 1, $row )->applyFromArray( $style_heading2 );
						$sheet->getRowDimension( $row )->setRowHeight( $row_height_heading2 );
						$sheet->mergeCellsByColumnAndRow(
							1, $row,
							1 + 2 * count( $strata_statistics_collection ) + $chart_col_span, $row
						);
						$row ++;

						$sheet->setCellValueByColumnAndRow( 1, $row, strip_tags( $item3->get_description() ) );
						$sheet->getStyleByColumnAndRow( 1, $row )->applyFromArray( $style_description );
						$sheet->mergeCellsByColumnAndRow(
							1, $row,
							1 + 2 * count( $strata_statistics_collection ) + $chart_col_span, $row
						);
						$row ++;

						$dsl = [];
						$dsv = [];

						$col = 2;
						foreach ( $strata_statistics_collection as $strata_statistics_analysis ) {
							/** @var \bwg\stats\strata\BWG_Stats_Strata $strata */
							$strata = $strata_statistics_analysis['strata'];

							/** @var \bwg\stats\BWG_Stats_Gradings $strata_stats_gradings */
							$strata_stats_gradings = $strata_statistics_analysis['statistics'];

							$strata_color_rgb = substr( $strata->get_color(), 1 );
							$style_array      = array(
								'font'      => [
									'bold'  => TRUE,
									'color' => array( 'rgb' => $strata_color_rgb ),
								],
								'alignment' => [
									'horizontal' => Alignment::HORIZONTAL_CENTER,
									'vertical'   => Alignment::VERTICAL_TOP,
								],
							);

							// Put the strata label.
							$r = $row;
							$sheet->setCellValueByColumnAndRow( $col, $r, $strata->get_label() );
							$sheet->getStyleByColumnAndRow( $col, $r )->applyFromArray( $style_array );
							$sheet->mergeCellsByColumnAndRow( $col, $r, $col + 1, $r );

							array_push( $dsl,
								new DataSeriesValues(
									'String',
									$sheet_reference . '!$' . Coordinate::stringFromColumnIndex( $col ) . '$' . $r,
									NULL, 1, [], NULL, $strata_color_rgb
								)
							);

							// Put the strata average.
							$r = $row + 1;
							$sheet->setCellValueByColumnAndRow( $col, $r,
								$strata_stats_gradings->get_average( $item3->get_id() )
							);
							$sheet->mergeCellsByColumnAndRow( $col, $r, $col + 1, $r );
							$sheet->getStyleByColumnAndRow( $col, $r )->applyFromArray(
								[
									'numberFormat' => [
										'formatCode' => '0.0'
									]
								]
							);

							// Put the strata standard deviation.
							$r = $row + 2;
							$sheet->setCellValueByColumnAndRow( $col, $r,
								$strata_stats_gradings->get_standard_deviation( $item3->get_id() )
							);
							$sheet->mergeCellsByColumnAndRow( $col, $r, $col + 1, $r );
							$sheet->getStyleByColumnAndRow( $col, $r )->applyFromArray(
								[
									'font'         => [
										'color' => [ 'rgb' => '999999' ],
									],
									'numberFormat' => [
										'formatCode' => NumberFormat::FORMAT_NUMBER_00
									]
								]
							);

							for ( $i = 1; $i <= 6; $i ++ ) {
								$r = $row + 2 + $i;
								if ( 2 === $col ) {
									$sheet->setCellValueByColumnAndRow( 1, $r, $i );
								}

								$histogram = $strata_stats_gradings->get_histogram( $item3->get_id() );

								// Percentage!
								$sheet->setCellValueByColumnAndRow( $col, $r,
									$this->_bwg_base->utils()->safe_division(
										$histogram[ $i ], $strata_stats_gradings->get_count()
									)
								);
								$sheet->getStyleByColumnAndRow( $col, $r )
								      ->getNumberFormat()
								      ->setFormatCode( NumberFormat::FORMAT_PERCENTAGE_00 );

								// Value!
								$sheet->setCellValueByColumnAndRow( $col + 1, $r, $histogram[ $i ] );
							}

							$c = Coordinate::stringFromColumnIndex( $col );
							array_push( $dsv,
								new DataSeriesValues(
									'Number',
									$sheet_reference . '!$' . $c . '$' . ( $row + 3 ) . ':$' . $c . '$' . ( $row + 8 ),
									NULL,
									6
								)
							);

							$sheet->getStyleByColumnAndRow( $col, $row, $col + 1, $row + 8 )->applyFromArray( [
								'borders' => [
									'left'  => [
										'borderStyle' => Border::BORDER_THIN,
										'color'       => [ 'rgb' => '333333' ],
									],
									'right' => [
										'borderStyle' => Border::BORDER_THIN,
										'color'       => [ 'rgb' => '333333' ],
									],
								],
							] );

							$col += 2;
						}

						// Label data series values for the x-axis of the chart.
						$xal = [
							new DataSeriesValues(
								'String',
								$sheet_reference . '!$A$' . ( $row + 3 ) . ':$A$' . ( $row + 8 ),
								NULL,
								6
							),
						];

						$plot_data_series = new DataSeries(
							DataSeries::TYPE_BARCHART,
							DataSeries::GROUPING_STANDARD,
							range( 0, count( $dsv ) - 1 ),
							$dsl,
							$xal,
							$dsv,
							DataSeries::DIRECTION_BAR,
							FALSE
						);

						$plot_area = new PlotArea( NULL, [ $plot_data_series ] );
						$legend    = new Legend( Legend::POSITION_RIGHT, NULL, FALSE );

						$y_axis = new Axis();
						$y_axis->setAxisOptionsProperties( '', NULL, NULL, NULL, NULL, NULL, 0, 1 );

						$chart = new Chart(
							'chart1',
							new Title( $item2->get_label() . ' / ' . $item3->get_label() ),
							$legend,
							$plot_area,
							TRUE,
							0,
							NULL,
							NULL,
							$y_axis
						);

						$top_left     = Coordinate::stringFromColumnIndex( $col ) . $row;
						$bottom_right = Coordinate::stringFromColumnIndex( $col + $chart_col_span ) . ( $row + 9 + 2 );

						$chart->setTopLeftPosition( $top_left );
						$chart->setBottomRightPosition( $bottom_right );
						$sheet->addChart( $chart );

						$row += 10 + 3;
					}
				}
			}

			$spreadsheet->setActiveSheetIndex( 0 );
			ob_end_clean();

			header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
			header( 'Content-Disposition: attachment;filename="demo.xlsx"' );
			header( 'Cache-Control: max-age=0' );

			$writer = new Xlsx( $spreadsheet );
			$writer->setIncludeCharts( TRUE );
			$writer->save( 'php://output' );
			wp_die();
		} catch ( Exception $e ) {
			return 'Error: ' . $e->getMessage();
		}
	}

	/**
	 * @param \WP_Post $post
	 *
	 * @return string
	 */
	protected function render_mail_sender_page( WP_Post $post ): string {
		$mail = $this->mailer_mail_get( $post->ID );
		$ra   = [
			'post'            => $post,
			'bwg_mode'        => $this->_bwg_base->options()->get_option_bwg_mode(),
			'mail_subject'    => $mail['subject'],
			'mail_body'       => $mail['body'],
			'set_mail_action' => BWG_Evaluation_Users_Ajax::SET_MAIL_AJAX_ACTION,
			'set_mail_nonce'  => wp_create_nonce( BWG_Evaluation_Users_Ajax::SET_MAIL_AJAX_ACTION ),
			'send_action'     => BWG_Evaluation_Users_Ajax::SEND_AJAX_ACTION,
			'send_nonce'      => wp_create_nonce( BWG_Evaluation_Users_Ajax::SEND_AJAX_ACTION ),
			'reset_action'    => BWG_Evaluation_Users_Ajax::RESET_AJAX_ACTION,
			'reset_nonce'     => wp_create_nonce( BWG_Evaluation_Users_Ajax::RESET_AJAX_ACTION ),
			'submissions'     => $this->mailer_get_evaluation_submissions( $post->ID ),
		];

		return $this->_bwg_base->utils()->render_admin_template(
			'evaluation/users/mail-sender.php', $ra
		);
	}

	public function mailer_get_evaluation_submissions( $evaluation_ID ) {
		$option_name  = $this->mailer_get_option_name( $evaluation_ID );
		$option_value = get_option( $option_name );
		if ( $option_value === FALSE ) {
			$option_value = [];
		}

		$submissions = [];
		foreach (
			$this->_bwg_base->user_storage_database_helper()->get_submitted_list(
				$evaluation_ID,
				1,
				999999,
				'username',
				'asc'
			) as $item
		) {
			$status = '';
			if ( array_key_exists( $item['user_ID'], $option_value ) ) {
				$status = $option_value[ $item['user_ID'] ];
			}

			$submissions[] = [
				'user_ID'  => $item['user_ID'],
				'username' => $item['username'],
				'status'   => $status,
			];
		}

		return $submissions;
	}

	protected function mailer_mail_get_option_name( $evaluation_ID ): string {
		return 'bwg_evaluation_users_mailer_mail_' . $evaluation_ID;
	}

	public function mailer_mail_update( $evaluation_ID, $subject, $body ): void {
		$option_name  = $this->mailer_mail_get_option_name( $evaluation_ID );
		$option_value = get_option( $option_name );
		if ( ! is_array( $option_value ) ) {
			$option_value = [];
		}
		$option_value['subject'] = $subject;
		$option_value['body']    = $body;

		update_option( $option_name, $option_value, FALSE );
	}

	/**
	 * @param int $evaluation_ID
	 *
	 * @return array{'subject':string, 'body':string}
	 */
	public function mailer_mail_get( int $evaluation_ID ): array {
		$option_name  = $this->mailer_mail_get_option_name( $evaluation_ID );
		$option_value = get_option( $option_name );

		return [
			'subject' => $option_value['subject'] ?? '',
			'body'    => $option_value['body'] ?? '',
		];
	}

	public function mailer_get_option_name( $evaluation_ID ): string {
		return 'bwg_evaluation_users_mailer_' . $evaluation_ID;
	}

	public function mailer_update_status( $evaluation_id, $user_id, string $status ) {
		$option_name  = $this->mailer_get_option_name( $evaluation_id );
		$option_value = get_option( $option_name );
		if ( ! is_array( $option_value ) ) {
			$option_value = [];
		}

		$option_value[ $user_id ] = $status . date( 'd.m.Y H:i:s' );

		update_option( $option_name, $option_value, FALSE );
	}

	/**
	 * @param int $evaluation_id
	 * @param int $user_id
	 *
	 * @return void
	 */
	public function mailer_reset_status( int $evaluation_id, int $user_id ): void {
		$option_name  = $this->mailer_get_option_name( $evaluation_id );
		$option_value = get_option( $option_name );
		if ( ! is_array( $option_value ) ) {
			$option_value = [];
		}

		if ( $user_id === - 1 ) {
			$option_value = [];
		} else {
			unset( $option_value[ $user_id ] );
		}

		update_option( $option_name, $option_value, FALSE );
	}
}
