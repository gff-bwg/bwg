<?php

namespace bwg\evaluation;

use bwg\BWG_Base;
use bwg\stats\BWG_Stats_Weighted_Random_Picker;
use WP_Screen;

/**
 * Class BWG_Evaluation_Analyses.
 *
 * @package bwg\evaluation
 */
final class BWG_Evaluation_Analyses {

	/**
	 * Slug.
	 */
	const SLUG_ANALYSES = 'bwg_analyses';

	/**
	 * Name for the screen option 'per_page'.
	 */
	const SCREEN_OPTION_PER_PAGE = 'bwg_evaluation_analyses_per_page';

	/**
	 * Name for the analyses meta for run feedback of deleted action.
	 */
	const RUN_FEEDBACK_DELETED = 'bwg_run_feedback_evaluation_analyses_deleted';

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;

	/**
	 * @var \bwg\evaluation\BWG_Evaluation_Analyses_List_Table
	 */
	private $_list_table;


	/**
	 * BWG_Evaluation_Analyses constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;

		add_action( 'init', [ $this, 'init' ] );
		add_action( 'admin_init', [ $this, 'admin_init' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
	}

	/**
	 * Handler for 'init' action.
	 */
	public function init() {
		if ( $this->_is_evaluation_analyses_page() ) {
			add_filter( 'set-screen-option', [ $this, 'set_screen_option' ], 10, 3 );
		}
	}

	/**
	 * Handler for 'admin_init' action.
	 */
	public function admin_init() {
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );

		if ( $this->_is_evaluation_analyses_page() ) {
			add_action( 'current_screen', [ $this, 'current_screen' ] );
		}

		// Register ajax callbacks.
		$this->_bwg_base->evaluation_analyses_ajax()->register_private_ajax_callbacks();
	}

	/**
	 * Handler for 'admin_menu' action.
	 */
	public function admin_menu() {
		add_submenu_page(
			'', // Not shown in menu.
			__( 'BWG Evaluation Analyses', 'bwg' ),
			'',
			'manage_options',
			self::SLUG_ANALYSES,
			[ $this, 'admin_page' ]
		);
	}

	/**
	 * Handler for 'admin_enqueue_scripts' action.
	 */
	public function admin_enqueue_scripts() {
		$this->_bwg_base->utils()->register_script(
			'bwg-admin-analyses',
			'js/admin/analyses/analyses.js',
			[ 'jquery', 'underscore', 'bwg-lib-moment', 'bwg-admin' ],
			TRUE,
			FALSE
		);

		$this->_bwg_base->utils()->register_style( 'bwg-admin-analyses', 'css/admin/analyses/analyses.css', [], TRUE );
	}

	/**
	 * Handler for 'current_screen' action.
	 *
	 * Note that this handler would only be called when we are on the screen with post_type = 'bwg_evaluation' and
	 * page = 'bwg_analyses'.
	 *
	 * @param \WP_Screen $current_screen
	 */
	public function current_screen( WP_Screen $current_screen ) {
		// The current screen is post_type 'bwg_evaluation' and page 'bwg_users'.
		$list_table = $this->get_list_table();

		if ( BWG_Evaluation_Analyses_List_Table::ACTION_DELETE === $list_table->current_action_run() ) {
			///
			// Run action: 'Delete'
			///
			$evaluation_ID = intval( $_REQUEST['post'] );
			$analyses_ids  = explode( ',', $_REQUEST['analyses_ids'] );

			// TODO [AS] Really delete those analyses.

			// We assume that the above worked (actually we should perform error checking).
			add_user_meta(
				get_current_user_id(),
				self::RUN_FEEDBACK_DELETED,
				1,
				TRUE
			);

			wp_redirect( $_REQUEST['redirect'] );

			return;
		}

		if ( FALSE !== $list_table->current_action() || isset( $_REQUEST['sub'] ) ) {
			// We are currently in an action "sub-screen" (usually a "confirm the action" screen) or another sub screen.
			add_filter( 'screen_options_show_screen', function ( $show_screen, $screen ) {
				return FALSE;
			}, 10, 2 );

			return;
		}

		// We are not in an action "sub-screen". So handle the default list table screen stuff.
		$user_id = get_current_user_id();
		if ( 1 == get_user_meta( $user_id, self::RUN_FEEDBACK_DELETED, TRUE ) ) {
			add_action( 'admin_notices', [ $this, 'notice_feedback_deleted' ] );
			delete_user_meta( $user_id, self::RUN_FEEDBACK_DELETED );
		}

		add_screen_option(
			'per_page',
			[
				'option'  => self::SCREEN_OPTION_PER_PAGE,
				'default' => 20,
			]
		);
	}

	/**
	 * Handler for 'set-screen-option' filter.
	 */
	function set_screen_option( $status, $option, $value ) {
		if ( self::SCREEN_OPTION_PER_PAGE === $option ) {
			return min( 100, max( 0, $value ) );
		}

		return $status;
	}

	/**
	 * Gets the list table. Note that this method actually caches the object. The post id would be read from the request
	 * parameter 'post'.
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Analyses_List_Table
	 */
	public function get_list_table() {
		if ( is_null( $this->_list_table ) ) {
			$this->_list_table = new BWG_Evaluation_Analyses_List_Table(
				$this->_bwg_base, intval( $_REQUEST['post'] )
			);
		}

		return $this->_list_table;
	}

	/**
	 * @return bool
	 */
	private function _is_evaluation_analyses_page() {
		return (
			( isset( $_REQUEST['page'] ) && self::SLUG_ANALYSES === $_REQUEST['page'] ) &&
			( isset( $_REQUEST['post_type'] ) && 'bwg_evaluation' === $_REQUEST['post_type'] )
		);
	}

	/**
	 * Menu page handler.
	 */
	public function admin_page() {
		$post = get_post( intval( $_REQUEST['post'] ) );
		if ( ! $post ) {
			wp_die( $this->_bwg_base->admin_render_error_wrap( 'Evaluation konnte nicht geladen werden.' ) );
		}

		if ( isset( $_REQUEST['sub'] ) ) {
			switch ( $_REQUEST['sub'] ) {
				case 'create':
					echo $this->_render_create_page( $post );

					return;
			}
		}

		$list_table = $this->get_list_table();
		if ( BWG_Evaluation_Analyses_List_Table::ACTION_DELETE === $list_table->current_action() ) {
			echo $this->_render_delete_confirmation( $post );

			return;
		}

		echo $this->_render_admin_page( $post, $list_table );
	}

	/**
	 * Renders the default admin page (this list table of all analyses corresponding to the current evaluation).
	 *
	 * @param \WP_Post $post
	 * @param \bwg\evaluation\BWG_Evaluation_Analyses_List_Table $list_table
	 *
	 * @return string
	 */
	private function _render_admin_page( $post, $list_table ) {
		$per_page = get_user_meta( get_current_user_id(), self::SCREEN_OPTION_PER_PAGE, TRUE );
		if ( empty( $per_page ) ) {
			$per_page = get_current_screen()->get_option( 'per_page', 'default' );
		}

		$list_table->per_page = intval( $per_page );
		if ( isset( $_REQUEST['s'] ) ) {
			$list_table->search = $_REQUEST['s'];
		}
		$list_table->prepare_items();

		$ra = [
			'post'       => $post,
			'list_table' => $list_table,
			'search'     => ( isset( $_REQUEST['s'] ) ? wp_unslash( $_REQUEST['s'] ) : '' ),
		];

		/*
		echo '<pre>';
		var_dump(
			$this->_bwg_base->user_storage_database_helper()->get_total_nr_submitted_grouped_sex_year(
				$post->ID
			)
		);
		echo '</pre>';
		*/

		return $this->_bwg_base->utils()->render_admin_template( 'evaluation/analyses/analyses.php', $ra );
	}

	/**
	 * @param \WP_Post $post
	 *
	 * @return string
	 */
	private function _render_create_page( $post ) {
///////// PLAYGROUND
		// TODO [AS] Remove the playground stuff.

		if ( FALSE ) {
			echo $this->_bwg_base->stats_math()->max_variance( 20 );

			$p = new BWG_Stats_Weighted_Random_Picker();
			$p->add( 'A', 20 );
			$p->add( 'B', 7 );
			$p->add( 'C', 1 );

			var_dump( $p->pick() );
			var_dump( $p->pick() );
			var_dump( $p->pick() );
		}
///////// END OF PLAYGROUND

		// Build the evaluation post.
		$evaluation_post = $this->_bwg_base->factory()->bwg_evaluation_post( $post );

		// Get the profile fields that have strata support.
		$strata_filter_boxes = [];
		foreach ( $evaluation_post->get_profile_fields()->get_fields() as $profile_field ) {
			$profile_field_type = $profile_field->get_field_type( $this->_bwg_base->profile_field_type_registry() );
			if ( ! $profile_field_type->is_strata_supported() ) {
				continue;
			}

			array_push( $strata_filter_boxes, $profile_field->render_strata_filter_box(
				$this->_bwg_base->utils(),
				$this->_bwg_base->profile_field_type_registry()
			) );
		}

		$sex_enabled     = TRUE;
		$sex_percentages = [ 50, 50, 0 ];
		$age_enabled     = TRUE;
		$age_limits      = [ 20, 40, 60, 120 ];
		$age_percentages = [ 25, 25, 25, 25 ];
		$sample_size     = 100;

		$ra = [
			'filter_groups_action' => BWG_Evaluation_Analyses_Ajax::FILTER_GROUPS_AJAX_ACTION,
			'filter_groups_nonce'  => wp_create_nonce( BWG_Evaluation_Analyses_Ajax::FILTER_GROUPS_AJAX_ACTION ),
			'post'                 => $post,
			'analysis_label'       => '',
			'strata_filter_boxes'  => $strata_filter_boxes,

			'sex_enabled'     => $sex_enabled,
			'sex_percentages' => $sex_percentages,
			'age_enabled'     => $age_enabled,
			'age_limits'      => $age_limits,
			'age_percentages' => $age_percentages,

			'sample_size'         => $sample_size,
			'filter_groups_table' => $this->render_filter_groups_table(
				$sex_enabled, $sex_percentages, $age_enabled, $age_limits, $age_percentages, $sample_size, $post->ID
			),
		];

		$this->_bwg_base->utils()->enqueue_script( 'bwg-admin-analyses' );
		$this->_bwg_base->utils()->enqueue_style( 'bwg-admin-analyses' );

		return $this->_bwg_base->utils()->render_admin_template( 'evaluation/analyses/create.php', $ra );
	}

	public function render_filter_groups_table(
		$sex_enabled = TRUE,
		$sex_percentages = [ 50, 50, 0 ],
		$age_enabled = TRUE,
		$age_limits = [ 60, 120 ],
		$age_percentages = [ 50, 50 ],
		$sample_size,
		$post_ID
	) {
		$feup_user_fields_table_name = $this->_bwg_base->feup_layer()->get_user_fields_table_name();

// Initialize the filter collection.
		$filter_collection = new BWG_Evaluation_Selection_Filter_Collection();

		if ( $sex_enabled ) {
///
// Filter A: Sex
///
			$filterA = new BWG_Evaluation_Selection_Filter(
				$feup_user_fields_table_name,
				'u.User_ID = {alias}.User_ID',
				sprintf( '{alias}.Field_ID = %d', 4 ) // TODO [AS] Get the id from bwg options.
			);
			$filterA->add_condition( 'Männlich', '{alias}.Field_Value LIKE "Männlich"', $sex_percentages[0] );
			$filterA->add_condition( 'Weiblich', '{alias}.Field_Value LIKE "Weiblich"', $sex_percentages[1] );
			$filterA->add_condition( 'Keine Angabe', '{alias}.Field_Value LIKE ""', $sex_percentages[2] );
			$filter_collection->add_filter( $filterA );
		}

		if ( $age_enabled ) {
///
// Filter B: Age
///
			$y       = date( 'Y' );
			$filterB = new BWG_Evaluation_Selection_Filter(
				$feup_user_fields_table_name,
				'u.User_ID = {alias}.User_ID',
				sprintf( '{alias}.Field_ID = %d', 6 ) // TODO [AS] Get the id from the bwg options.
			);

			$upper_limit = 0;
			for ( $i = 0; $i < count( $age_limits ) && $i < count( $age_percentages ); $i ++ ) {
				$lower_limit = $upper_limit;
				$upper_limit = $age_limits[ $i ];

				$percentage = $age_percentages[ $i ];

				$filterB->add_condition(
					sprintf( '%d &ndash; %d', $lower_limit, $upper_limit ),
					sprintf( '{alias}.Field_Value > %d AND {alias}.Field_Value <= %d', $y - $upper_limit,
						$y - $lower_limit ),
					$percentage
				);
			}

			$filter_collection->add_filter( $filterB );
		}

		$group_sizes = $filter_collection->get_group_sizes( $sample_size );

		$ra = [
			'filter_collection' => $filter_collection,
			'sample_size'       => $sample_size,
			'group_sizes'       => $group_sizes,
			'group_totals'      => $filter_collection->get_group_totals( $post_ID ),
			'condition_totals'  => BWG_Evaluation_Selection_Filter_Utils::groups_to_condition_sums( $group_sizes ),
		];

		return $this->_bwg_base->utils()->render_admin_template( 'evaluation/analyses/filter-groups-table.php', $ra );
	}

	private function _render_delete_confirmation( $post ) {
		return 'asdf';
	}

	/**
	 * Handler for the 'admin_notices' action in case the 'delete' action had been run successfully.
	 */
	public function notice_feedback_unsubmitted() {
		echo '
<div class="notice notice - success"><p>'
		     . __( 'Die Aktion wurde erfolgreich ausgeführt.', 'bwg' )
		     . '</p></div>';
	}


}