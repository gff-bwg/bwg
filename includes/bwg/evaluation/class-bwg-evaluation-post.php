<?php

namespace bwg\evaluation;

use bwg\profile\fields\BWG_Profile_Fields;
use WP_Post;

/**
 * Class BWG_Evaluation_Post.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation_Post {

	/**
	 * Rating open state: Not yet open
	 */
	const RATING_OPEN_STATE_NOT_YET = 1;

	/**
	 * Rating open state: Open
	 */
	const RATING_OPEN_STATE_OPEN = 2;

	/**
	 * Rating open state: Closed
	 */
	const RATING_OPEN_STATE_CLOSED = 3;

	/**
	 * @var WP_Post
	 */
	private $_post;

	/**
	 * @var string Empty string or a date in the format 'Y-m-d'.
	 */
	private $_rating_open_from;

	/**
	 * @var string Empty string or a date in the format 'Y-m-d'.
	 */
	private $_rating_open_to;

	/**
	 * @var
	 */
	private $_postage;

	/**
	 * @var string
	 */
	private $_evaluation_definition_raw;

	/**
	 * @var BWG_Evaluation_Definition
	 */
	private $_evaluation_definition;
	private $_profile_fields_raw;
	private $_profile_fields;


	/**
	 * BWG_Evaluation_Post constructor.
	 *
	 * @param WP_Post $post
	 */
	function __construct( WP_Post $post ) {
		$this->_post = $post;
	}

	/**
	 * @return WP_Post
	 */
	public function get_post() {
		return $this->_post;
	}

	/**
	 * @return int
	 */
	public function get_id() {
		return $this->_post->ID;
	}

	/**
	 * Gets the permalink to the evaluation intro page.
	 *
	 * @return false|string
	 */
	public function get_permalink() {
		return get_permalink( $this->get_id() );
	}

	/**
	 * Gets the permalink to the evaluation form page.
	 *
	 * @return false|string
	 */
	public function get_permalink_to_form() {
		$permalink = $this->get_permalink();
		if ( FALSE === $permalink ) {
			return FALSE;
		}

		return add_query_arg( 'e', $this->get_id(), $permalink );
	}
	
	/**
	 * TODO: Write this docblock - or remove if not needed.
	 *
	 * @return mixed
	 */
	public function get_postage() {
		if ( is_null( $this->_postage ) ) {
			$this->_postage = get_post_meta( $this->get_id(), 'bwg_ee_postage', TRUE );
		}

		return $this->_postage;
	}

	/**
	 * Gets the evaluation definition as raw json string.
	 *
	 * @return string
	 */
	public function get_evaluation_definition_raw() {
		if ( is_null( $this->_evaluation_definition_raw ) ) {
			$this->_evaluation_definition_raw = get_post_meta( $this->get_id(), 'bwg_ed', TRUE );
		}

		return $this->_evaluation_definition_raw;
	}

	/**
	 * Gets the evaluation definition as BWG_Evaluation_Definition.
	 *
	 * @return BWG_Evaluation_Definition
	 */
	public function get_evaluation_definition() {
		if ( is_null( $this->_evaluation_definition ) ) {
			$this->_evaluation_definition = new BWG_Evaluation_Definition( $this->get_evaluation_definition_raw() );
		}

		return $this->_evaluation_definition;
	}

	/**
	 * Gets the profile fields as raw json string.
	 *
	 * @return string
	 */
	public function get_profile_fields_raw() {
		if ( is_null( $this->_profile_fields_raw ) ) {
			$this->_profile_fields_raw = get_post_meta( $this->get_id(), 'bwg_epf', TRUE );
			if ( FALSE === $this->_profile_fields_raw ) {
				$this->_profile_fields_raw = '{"fields":[]}';
			}
		}

		return $this->_profile_fields_raw;
	}

	/**
	 * @return \bwg\profile\fields\BWG_Profile_Fields
	 */
	public function get_profile_fields() {
		if ( is_null( $this->_profile_fields ) ) {
			$this->_profile_fields = new BWG_Profile_Fields( $this->get_profile_fields_raw() );
		}

		return $this->_profile_fields;
	}

	/**
	 * Gets the 'rating open from' setting in the format 'Y-m-d' or empty.
	 *
	 * @return string
	 */
	public function get_rating_open_from() {
		if ( is_null( $this->_rating_open_from ) ) {
			$this->_rating_open_from = get_post_meta( $this->get_id(), 'bwg_ee_rating_open_from', TRUE );
		}

		return $this->_rating_open_from;
	}

	/**
	 * Gets the 'rating open to' setting in the format 'Y-m-d' or empty.
	 *
	 * @return string
	 */
	public function get_rating_open_to() {
		if ( is_null( $this->_rating_open_to ) ) {
			$this->_rating_open_to = get_post_meta( $this->get_id(), 'bwg_ee_rating_open_to', TRUE );
		}

		return $this->_rating_open_to;
	}

	/**
	 * Is the rating not yet open?
	 *
	 * @return bool
	 */
	public function is_rating_not_yet_open() {
		return self::RATING_OPEN_STATE_NOT_YET === $this->get_rating_open_state();
	}

	/**
	 * Is the rating currently open?
	 *
	 * @return bool
	 */
	public function is_rating_open() {
		return self::RATING_OPEN_STATE_OPEN === $this->get_rating_open_state();
	}

	/**
	 * Is the rating already closed?
	 *
	 * @return bool
	 */
	public function is_rating_closed() {
		return self::RATING_OPEN_STATE_CLOSED === $this->get_rating_open_state();
	}

	/**
	 * Gets the 'rating open state'.
	 *
	 * @param string|null $date The date (Y-m-d) or NULL if current date.
	 *
	 * @return int One of
	 * - BWG_Evaluation_Post::RATING_OPEN_STATE_NOT_YET
	 * - BWG_Evaluation_Post::RATING_OPEN_STATE_OPEN
	 * - BWG_Evaluation_Post::RATING_OPEN_STATE_CLOSED
	 */
	public function get_rating_open_state( $date = NULL ) {
		if ( bwg_base()->utils()->in_dates( $this->get_rating_open_from(), $this->get_rating_open_to(), $date ) ) {
			return self::RATING_OPEN_STATE_OPEN;
		} else if ( ! bwg_base()->utils()->in_dates( '', $this->get_rating_open_to(), $date ) ) {
			return self::RATING_OPEN_STATE_CLOSED;
		}

		return self::RATING_OPEN_STATE_NOT_YET;
	}

}
