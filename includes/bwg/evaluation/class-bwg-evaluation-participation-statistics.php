<?php

declare( strict_types=1 );

namespace bwg\evaluation;

use bwg\BWG_Base;

class BWG_Evaluation_Participation_Statistics {

	const SLUG_MAIN = 'participation_statistics';

	protected BWG_Base $bwg_base;

	public function __construct( BWG_Base $bwg_base ) {
		$this->bwg_base = $bwg_base;

		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
	}

	/**
	 * Handler for `admin_menu` action.
	 *
	 * @return void
	 */
	public function admin_menu(): void {
		add_submenu_page(
			'', // Not shown in menu.
			__( 'BWG Evaluation Teilnahme Statistiken', 'bwg' ),
			'',
			'manage_options',
			self::SLUG_MAIN,
			[ $this, 'admin_page' ]
		);
	}

	/**
	 * Admin page callback.
	 *
	 * @return void
	 */
	public function admin_page(): void {
		/** @var \wpdb $wpdb */
		global $wpdb;

		$post = get_post( intval( $_REQUEST['post'] ) );
		if ( ! $post ) {
			wp_die( $this->bwg_base->admin_render_error_wrap( 'Evaluation konnte nicht geladen werden.' ) );
		}

		wp_enqueue_script( BWG_Base::SCRIPT_HANDLE_ADMIN_ACCORDION );
		wp_enqueue_style( BWG_Base::STYLE_HANDLE_ADMIN_ACCORDION );

		// Get the evaluation post.
		$evaluation_post = $this->bwg_base->factory()->bwg_evaluation_post( $post );

		$feup_layer           = $this->bwg_base->feup_layer();
		$feup_user_table_name = $feup_layer->get_user_table_name();

		// Get the database helper for submissions.
		$submissions_database_helper  = $this->bwg_base->submissions_database_helper();
		$user_storage_database_helper = $this->bwg_base->user_storage_database_helper();
		$submissions_table_name       = $submissions_database_helper->get_submissions_table_name( $evaluation_post->get_id() );

		$results = $wpdb->get_results( 'SELECT S.id, S.submitted, FU.User_ID, FU.Username, FU.Level_ID FROM ' . $submissions_table_name .
		                               ' AS S LEFT JOIN ' .
		                               $feup_user_table_name .
		                               ' AS FU ON FU.User_ID = S.user_ID ORDER BY FU.Username ASC' );

		$nr_submissions_with_unknown_feup_user = 0;
		$submissions_with_level_0_feup_user    = [];
		$submissions_with_level_X_feup_user    = [];
		foreach ( $results as $result ) {
			if ( is_null( $result->User_ID ) ) {
				$nr_submissions_with_unknown_feup_user ++;
				continue;
			}

			if ( intval( $result->Level_ID ) === 0 ) {
				$submissions_with_level_0_feup_user[] = $result->Username;
			} else {
				// We can assume that the `submitted` field is non-null.
				$submissions_with_level_X_feup_user[] = $result->Username;
			}
		}

		$no_submissions_with_level_0_feup_user                      = [];
		$no_submissions_with_level_X_feup_user                      = [];
		$no_submissions_with_level_X_feup_user_with_user_storage    = [];
		$no_submissions_with_level_X_feup_user_without_user_storage = [];
		foreach (
			$wpdb->get_results( 'SELECT S.id, FU.User_ID, FU.Username, FU.Level_ID FROM ' . $feup_user_table_name .
			                    ' AS FU LEFT JOIN ' . $submissions_table_name .
			                    ' AS S ON FU.User_ID = S.user_ID' .
			                    ' WHERE S.id IS NULL' )
			as $result
		) {
			if ( intval( $result->Level_ID ) === 0 ) {
				$no_submissions_with_level_0_feup_user[] = $result->Username;
			} else {
				$user_storage = $user_storage_database_helper->get_user_storage(
					$evaluation_post->get_id(),
					$result->User_ID
				);

				if ( is_null( $user_storage ) ) {
					// NOT EVEN IN USER STORAGE!
					$no_submissions_with_level_X_feup_user_without_user_storage[] = $result->Username;
				} else {
					// AT LEAST IN USER STORAGE - SO USER MIGHT CURRENTLY FILL THIS EVALUATION.
					$no_submissions_with_level_X_feup_user_with_user_storage[] = $result->Username;
				}

				$no_submissions_with_level_X_feup_user[] = $result->Username;
			}
		}

		$ra = [
			'post'                                                       => $post,
			'nr_submissions_with_unknown_feup_user'                      => $nr_submissions_with_unknown_feup_user,
			'submissions_with_level_0_feup_user'                         => $submissions_with_level_0_feup_user,
			'submissions_with_level_X_feup_user'                         => $submissions_with_level_X_feup_user,
			'no_submissions_with_level_0_feup_user'                      => $no_submissions_with_level_0_feup_user,
			'no_submissions_with_level_X_feup_user'                      => $no_submissions_with_level_X_feup_user,
			'no_submissions_with_level_X_feup_user_with_user_storage'    =>
				$no_submissions_with_level_X_feup_user_with_user_storage,
			'no_submissions_with_level_X_feup_user_without_user_storage' =>
				$no_submissions_with_level_X_feup_user_without_user_storage,
		];

		echo $this->bwg_base->utils()->render_admin_template(
			'evaluation/participation-statistics/participation-statistics.php',
			$ra
		);
	}
}
