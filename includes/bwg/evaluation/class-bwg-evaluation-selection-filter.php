<?php

namespace bwg\evaluation;

class BWG_Evaluation_Selection_Filter {

	private $_conditions = [];

	/**
	 * @var string
	 */
	private $_join_table;

	/**
	 * @var string
	 */
	private $_join_on;

	/**
	 * @var string
	 */
	private $_where;


	public function __construct( $join_table, $join_on, $where ) {
		$this->_join_table = $join_table;
		$this->_join_on    = $join_on;
		$this->_where      = $where;
	}

	/**
	 * @param string $label
	 * @param string $when
	 * @param int $percentage An integer from [0, 100].
	 */
	public function add_condition( $label, $when, $percentage ) {
		array_push( $this->_conditions, [ 'label' => $label, 'when' => $when, 'percentage' => $percentage ] );
	}

	public function get_condition_labels() {
		$labels = [];
		foreach ( $this->_conditions as $condition ) {
			array_push( $labels, $condition['label'] );
		}

		return $labels;
	}

	public function get_condition_percentages() {
		$percentages = [];
		foreach ( $this->_conditions as $condition ) {
			array_push( $percentages, $condition['percentage'] );
		}

		return $percentages;
	}


	public function get_condition_when_clause( $condition_index ) {
		return $this->_conditions[ $condition_index ]['when'];
	}

	public function get_condition_label( $condition_index ) {
		return $this->_conditions[ $condition_index ]['label'];
	}

	public function count_conditions() {
		return count( $this->_conditions );
	}

	public function sql_table( $alias ) {
		return 'JOIN ' . $this->_join_table . ' AS ' . $alias . ' ON ' .
		       $this->_replace( [ 'alias' => $alias ], $this->_join_on );
	}

	public function sql_select( $alias, $out_as ) {
		$sql          = '(CASE  ';
		$c_conditions = $this->count_conditions();
		for ( $i = 0; $i < $c_conditions; $i ++ ) {
			$sql .= 'WHEN ' . $this->_replace( [ 'alias' => $alias, 'out_as' => $out_as ],
					$this->get_condition_when_clause( $i ) ) . ' THEN ' . ( $i + 1 ) . ' ';
		}
		$sql .= 'ELSE 0 END) AS ' . $out_as;

		return $sql;
	}

	public function sql_where( $alias, $out_as ) {
		return $this->_replace( [ 'alias' => $alias, 'out_as' => $out_as ], $this->_where );
	}

	private function _replace( $data, $subject ) {
		$searches = [];
		$replaces = [];
		foreach ( $data as $key => $value ) {
			array_push( $searches, '{' . $key . '}' );
			array_push( $replaces, $value );
		}

		return str_replace( $searches, $replaces, $subject );
	}
}