<?php

declare( strict_types=1 );

namespace bwg\evaluation;

use bwg\BWG_Base;
use bwg\pdf\BWG_User_Pdf;
use DateTimeImmutable;
use Exception;

class BWG_Evaluation_Users_Ajax {
	public const SET_MAIL_AJAX_ACTION = 'bwg_eu_set_mail';

	public const SEND_AJAX_ACTION = 'bwg_eu_send';

	public const RESET_AJAX_ACTION = 'bwg_eu_reset';

	protected BWG_Base $bwg_base;

	/**
	 * @param BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->bwg_base = $bwg_base;
	}

	/**
	 *  Registers private (admin only) ajax callbacks.
	 *
	 * @return void
	 */
	public function register_private_ajax_callbacks(): void {
		add_action( 'wp_ajax_' . self::SET_MAIL_AJAX_ACTION, [ $this, 'wp_ajax_eu_set_mail' ] );
		add_action( 'wp_ajax_' . self::SEND_AJAX_ACTION, [ $this, 'wp_ajax_eu_send' ] );
		add_action( 'wp_ajax_' . self::RESET_AJAX_ACTION, [ $this, 'wp_ajax_eu_reset' ] );
	}

	/**
	 * @return void
	 */
	public function wp_ajax_eu_set_mail(): void {
		check_ajax_referer( self::SET_MAIL_AJAX_ACTION );
		if ( ! current_user_can( 'edit_others_posts' ) ) {
			wp_die( - 1, 403 );
		}

		$evaluation_id = intval( filter_input( INPUT_POST, 'evaluation_id' ) );
		$mail_subject  = filter_input( INPUT_POST, 'subject' );
		$mail_body     = filter_input( INPUT_POST, 'body' );

		$this->bwg_base->evaluation_users()->mailer_mail_update( $evaluation_id, $mail_subject, $mail_body );

		wp_send_json_success( [] );
	}

	/**
	 * @return void
	 * @throws Exception
	 */
	public function wp_ajax_eu_send(): void {
		check_ajax_referer( self::SEND_AJAX_ACTION );
		if ( ! current_user_can( 'edit_others_posts' ) ) {
			wp_die( - 1, 403 );
		}

		$evaluation_id = filter_input( INPUT_POST, 'evaluation_id' );
		$user_id       = filter_input( INPUT_POST, 'user_id' );
		$demo          = boolval( filter_input( INPUT_POST, 'demo' ) );

		// Get the evaluation post.
		$evaluation_post = $this->bwg_base->factory()->bwg_evaluation_post( get_post( $evaluation_id ) );

		//////
		/// Build the mail
		//////
		$real_receiver = $this->bwg_base->evaluation_utils()
		                                ->get_evaluation_user_receiver( $evaluation_id, $user_id );
		$receiver      = wp_get_current_user()->user_email;
		if ( ! $demo ) {
			$receiver = $real_receiver;
		}

		$mail = $this->bwg_base->evaluation_users()->mailer_mail_get( $evaluation_post->get_id() );

		$subject  = $mail['subject'] . ( $demo ? ' [Demo]' : '' );
		$contents = nl2br( $mail['body'] );

		$headers = [
			'Content-Type: text/html; charset=UTF-8',
		];

		if ( $demo ) {
			$contents = '<span style="color:#333;background-color:#ccc">Demoversand: Diese E-Mail würde eigentlich an «'
			            . $real_receiver
			            . '» verschickt.</span></p><p>' . $contents;
		}

		$body = $this->bwg_base->utils()->render_template( 'mail/simple-html.php',
			[
				'content'        => '<p>' . $contents . '</p>',
				'footer_subtext' => $this->bwg_base->options()->get_option_footer_text(),
			]
		);

		$attachments   = [];
		$pdf_file_path = $this->get_pdf_file_path( $evaluation_post );
		if ( $this->is_drogothek_mode() ) {
			register_shutdown_function( function () use ( $pdf_file_path ) {
				wp_delete_file( $pdf_file_path );
				$pdf_file_directory = dirname( $pdf_file_path );

				bwg_base()->utils()->clear_directory( $pdf_file_directory, 0 );
				@rmdir( $pdf_file_directory );
			} );
		}

		///
		// Create the pdf file.
		///
		$pdf = new BWG_User_Pdf( $this->bwg_base, $evaluation_post, $user_id );
		try {
			// Set the date time of submission to be the `today` date shown in the pdf.
			$submission = $this->bwg_base
				->submissions_database_helper()
				->get_submission_by_user( $evaluation_id, $user_id );
			$date       = new DateTimeImmutable( $submission->get_record()['submitted'] );
			$pdf->set_today( date_i18n( get_option( 'date_format' ), $date->getTimestamp() ) );
		} catch ( Exception $e ) {
		}

		$pdf->build();
		$pdf->Output( $pdf_file_path, 'F' );
		$pdf->clear_private_cache_files();

		$attachments[] = $pdf_file_path;
		$result        = wp_mail( $receiver, $subject, $body, $headers, $attachments );

		if ( ! $demo ) {
			$this->bwg_base->evaluation_users()->mailer_update_status( $evaluation_id, $user_id, $result ? 'S' : 'F' );
		}

		wp_send_json_success( [
			'data'         => $this->bwg_base->evaluation_users()->mailer_get_evaluation_submissions( $evaluation_id ),
			'send_success' => $result,
		] );
	}

	/**
	 * @param BWG_Evaluation_Post $evaluation_post
	 *
	 * @return string
	 * @throws Exception
	 */
	protected function get_pdf_file_path( BWG_Evaluation_Post $evaluation_post ): string {
		$private_cache_path = $this->bwg_base->utils()->get_bwg_files_private_cache_path( TRUE );
		if ( FALSE === $private_cache_path ) {
			throw new Exception( 'Oops, could not assert the bwg private cache files directory.' );
		}

		if ( $this->is_drogothek_mode() ) {
			$pdf_base_name = $evaluation_post->get_post()->post_title;
			$pdf_base_name = preg_replace( '/[^a-z0-9äöüèéà \.-]/i', '', $pdf_base_name );

			$pdf_file_dir = $private_cache_path . DIRECTORY_SEPARATOR . uniqid( 'drogothek', TRUE );
			if ( ! wp_mkdir_p( $pdf_file_dir ) ) {
				throw new Exception( 'Oops, could not assert the bwg cache directory.' );
			}

			return $pdf_file_dir . DIRECTORY_SEPARATOR . $pdf_base_name . '.pdf';
		}

		return $private_cache_path . DIRECTORY_SEPARATOR . uniqid( 'Summary' ) . '.pdf';
	}

	protected function is_drogothek_mode(): bool {
		return ( $this->bwg_base->options()->get_option_bwg_mode() === 'drogothek' );
	}

	/**
	 * @return void
	 * @throws Exception
	 */
	public function wp_ajax_eu_reset() {
		check_ajax_referer( self::RESET_AJAX_ACTION );
		if ( ! current_user_can( 'edit_others_posts' ) ) {
			wp_die( - 1, 403 );
		}

		$evaluation_id = intval( filter_input( INPUT_POST, 'evaluation_id' ) );
		$user_id       = intval( filter_input( INPUT_POST, 'user_id' ) );

		$this->bwg_base->evaluation_users()->mailer_reset_status( $evaluation_id, $user_id );

		wp_send_json_success( [
			'user_id' => $user_id,
			'data'    => $this->bwg_base->evaluation_users()->mailer_get_evaluation_submissions( $evaluation_id ),
		] );
	}
}
