<?php

namespace bwg\evaluation;

use bwg\BWG_Base;

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Class BWG_Evaluation_Users_List_Table.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation_Users_List_Table extends \WP_List_Table {

	/**
	 * Action: Unsubmit.
	 */
	const ACTION_UNSUBMIT = 'unsubmit';

	/**
	 * Action: View.
	 */
	const ACTION_VIEW = 'view';

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;

	/**
	 * @var int
	 */
	private $_post_ID;

	/**
	 * @var int
	 */
	public $per_page = 20;

	/**
	 * @var string
	 */
	public $search = '';


	/**
	 * BWG_Evaluation_Users_List_Table constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 * @param int $post_ID
	 */
	public function __construct( BWG_Base $bwg_base, $post_ID ) {
		parent::__construct( [
			'singular' => 'user',
			'plural'   => 'users',
			'ajax'     => FALSE,
		] );

		$this->_bwg_base = $bwg_base;
		$this->_post_ID  = $post_ID;
	}

	/**
	 * Renders the column 'cb'.
	 *
	 * @param object $item
	 *
	 * @return string
	 */
	public function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="%1$s[]" value="%2$s" />',
			/*$1%s*/
			$this->_args['singular'],
			/*$2%s*/
			$item['user_ID']
		);
	}

	/**
	 * Renders the column 'username'.
	 *
	 * @param $item
	 *
	 * @return string
	 */
	public function column_username( $item ) {
		$actions = [
			self::ACTION_UNSUBMIT => sprintf(
				'<a href="?post_type=%s&page=%s&post=%d&action=%s&user_ID=%s">' . __( 'Unsubmit', 'bwg' ) . '</a>',
				$_REQUEST['post_type'],
				$_REQUEST['page'],
				$this->_post_ID,
				'unsubmit',
				$item['user_ID']
			),
		];

		$main_action_url = sprintf(
			'?post_type=%s&page=%s&post=%d&user_ID=%d&action=%s',
			$_REQUEST['post_type'],
			$_REQUEST['page'],
			$this->_post_ID,
			$item['user_ID'],
			self::ACTION_VIEW
		);

		return sprintf( '<a href="%1$s">%2$s</a> <span style="color:silver">(id:%3$s)</span>%4$s',
			$main_action_url,
			$item['username'],
			$item['user_ID'],
			$this->row_actions( $actions )
		);
	}

	/**
	 * Renders the column 'submitted'.
	 *
	 * @param $item
	 *
	 * @return string
	 */
	public function column_submitted( $item ) {
		$ts = $this->_bwg_base->utils()->mysql_gmt_to_local_timestamp( $item['submitted'] );

		return date_i18n( get_option( 'date_format' ) . ', ' . get_option( 'time_format' ), $ts ) . ' ('
		       /* translators: %s: the human time diff */
		       . sprintf( __( '%s ago', 'bwg' ), human_time_diff( $ts, current_time( 'timestamp' ) ) ) . ')';
	}

	/**
	 * Renders the column 'time_active'.
	 *
	 * @param $item
	 *
	 * @return string
	 */
	public function column_time_active( $item ) {
		return is_null( $item['time_active_human'] ) ? '-' : $item['time_active_human'];
	}

	/**
	 * Gets the columns.
	 *
	 * @return array
	 */
	public function get_columns() {
		$columns = [
			'cb'          => '<input type="checkbox" />',
			'username'    => __( 'Username', 'bwg' ),
			'submitted'   => __( 'Submitted on', 'bwg' ),
			'time_active' => __( 'Duration (active)', 'bwg' ),
		];

		return $columns;
	}

	/**
	 * Gets the sortable columns.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = [
			'username'    => [ 'username', FALSE ],
			'submitted'   => [ 'submitted', FALSE ],
			'time_active' => [ 'time_active', FALSE ],
		];

		return $sortable_columns;
	}

	/**
	 * Gets the bulk actions.
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
			self::ACTION_UNSUBMIT => __( 'Unsubmit', 'bwg' ),
		];

		return $actions;
	}

	/**
	 * Prepares the items.
	 */
	public function prepare_items() {
		$columns  = $this->get_columns();
		$hidden   = get_hidden_columns( get_current_screen() );
		$sortable = $this->get_sortable_columns();

		$this->_column_headers = [ $columns, $hidden, $sortable ];

		$current_page = $this->get_pagenum();
		$per_page     = ( $this->per_page > 0 ? $this->per_page : 20 );
		$total_items  = $this->_bwg_base->user_storage_database_helper()->get_total_nr_submitted(
			$this->_post_ID, TRUE, $this->search
		);

		$this->items = $this->_bwg_base->user_storage_database_helper()->get_submitted_list(
			$this->_post_ID,
			$current_page,
			$per_page,
			isset( $_REQUEST['orderby'] ) ? $_REQUEST['orderby'] : 'username',
			isset( $_REQUEST['order'] ) ? $_REQUEST['order'] : 'asc',
			$this->search
		);

		$this->set_pagination_args( [
			'total_items' => $total_items,
			'per_page'    => $per_page,
			'total_pages' => ceil( $total_items / $per_page ),
		] );
	}

	/**
	 * @return string|false
	 */
	public function current_action_run() {
		if ( isset( $_REQUEST['action_run'] ) ) {
			return $_REQUEST['action_run'];
		}

		return FALSE;
	}

}
