<?php

namespace bwg\evaluation;

/**
 * Class BWG_Evaluation_Definition.
 */
class BWG_Evaluation_Definition {

	/**
	 * @var array
	 */
	private $_json;

	/**
	 * BWG_Evaluation_Definition constructor.
	 *
	 * @param array|string $json
	 */
	public function __construct( $json = [] ) {
		if ( is_string( $json ) ) {
			$this->_json = json_decode( $json, TRUE, 512, JSON_UNESCAPED_UNICODE );
			if ( is_null( $this->_json ) ) {
				$this->_json = [];
			}
		} else {
			$this->_json = $json;
		}
	}

	/**
	 * @return int
	 */
	public function get_ai() {
		return isset( $this->_json['ai'] ) ? intval( $this->_json['ai'] ) : 0;
	}

	/**
	 * @return BWG_Evaluation_Definition_Item[]
	 */
	public function get_items() {
		if ( ! isset( $this->_json['items'] ) ) {
			return [];
		}

		$items = [];
		for ( $i = 0; $i < count( $this->_json['items'] ); $i ++ ) {
			if ( ! isset( $this->_json['items'][ $i ]['_obj'] ) ) {
				$this->_json['items'][ $i ]['_obj'] = new BWG_Evaluation_Definition_Item(
					0,
					$this->_json['items'][ $i ]
				);
			}

			array_push( $items, $this->_json['items'][ $i ]['_obj'] );
		}

		return $items;
	}

	/**
	 * Finds an item by id. The method would also search the descendants.
	 *
	 * @param int $id
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Definition_Item[]
	 */
	public function find_item_breadcrumb( $id ) {
		foreach ( $this->get_items() as $item ) {
			if ( intval( $id ) === intval( $item->get_id() ) ) {
				return [ $item ];
			}

			$item_breadcrumb = $item->find_item_breadcrumb( $id, [ $item ] );
			if ( ! empty( $item_breadcrumb ) ) {
				return $item_breadcrumb;
			}
		}

		return [];
	}

}
