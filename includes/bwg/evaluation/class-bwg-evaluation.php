<?php

namespace bwg\evaluation;

use bwg\BWG_Base;
use DateTime;
use FEUP_User;
use WP_Post;

/**
 * Class BWG_Evaluation.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation {

	/**
	 * Meta box nonce action.
	 */
	const META_BOX_DEFINITION_NONCE_ACTION = 'meta_box_evaluation_definition';

	/**
	 * Meta box nonce name.
	 */
	const META_BOX_DEFINITION_NONCE_NAME = 'bwg_e_definition_nonce';

	/**
	 * Meta box 'Emails' nonce action.
	 */
	const META_BOX_EMAILS_NONCE_ACTION = 'meta_box_evaluation_emails';

	/**
	 * Meta box 'Email' nonce name.
	 */
	const META_BOX_EMAILS_NONCE_NAME = 'bwg_e_emails_nonce';

	/**
	 * Meta box 'Profile Fields' nonce action.
	 */
	const META_BOX_PROFILE_FIELDS_NONCE_ACTION = 'meta_box_evaluation_profile_fields';

	/**
	 * Meta box 'Profile Fields' nonce name.
	 */
	const META_BOX_PROFILE_FIELDS_NONCE_NAME = 'bwg_e_profile_fields_nonce';

	/**
	 * Meta box 'Extras' nonce action.
	 */
	const META_BOX_EXTRAS_NONCE_ACTION = 'meta_box_evaluation_extras';

	/**
	 * Meta box 'Extras' nonce name.
	 */
	const META_BOX_EXTRAS_NONCE_NAME = 'bwg_e_extras_nonce';

	/**
	 * Slug for the admin evaluation test page.
	 */
	const SLUG_TEST = 'bwg_test';

	/**
	 * @var BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Evaluation constructor.
	 *
	 * @param BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;

		add_action( 'init', [ $this, 'init' ] );
		add_action( 'admin_init', [ $this, 'admin_init' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
	}

	/**
	 * Handler for 'init' action.
	 */
	public function init() {
		// Register the custom post type 'bwg_evaluation'.
		$this->_bwg_base->evaluation_post_type_manager()->register_custom_post_type();

		// Add a filter for 'the_content'.
		add_filter( 'the_content', [ $this, 'the_content' ] );

		// Add non-admin ajax callbacks.
		$this->_bwg_base->evaluation_ajax()->register_public_ajax_callbacks();
	}

	/**
	 * Handler for 'admin_init' action.
	 */
	public function admin_init() {
		add_action( 'save_post_bwg_evaluation', [ $this, 'save_post' ], 10, 3 );
		add_action( 'delete_post', [ $this, 'delete_post' ], 10, 1 );

		// Register ajax callbacks.
		$this->_bwg_base->evaluation_ajax()->register_public_ajax_callbacks( TRUE );
		$this->_bwg_base->evaluation_ajax()->register_private_ajax_callbacks();
		$this->_bwg_base->evaluation_presets()->register_ajax_callbacks();
		$this->_bwg_base->evaluation_users_ajax()->register_private_ajax_callbacks();

		// Register the handler and filter that would set the posts columns shown on the admin overview table.
		$this->_bwg_base->evaluation_post_type_manager()->setup_admin_post();

		// Add the evaluation definition meta box.
		add_meta_box( 'bwg-evaluation-definition',
			__( 'Evaluationsdefinition', 'bwg' ),
			[ $this, 'meta_box_evaluation_definition' ],
			'bwg_evaluation',
			'normal',
			'high'
		);

		// Add the evaluation emails meta box.
		add_meta_box( 'bwg-evaluation-emails',
			__( 'E-Mail Texte', 'bwg' ),
			[ $this, 'meta_box_evaluation_emails' ],
			'bwg_evaluation',
			'normal',
			'high'
		);

		// Add the evaluation profile-fields meta box.
		add_meta_box( 'bwg-evaluation-profile-fields',
			__( 'Bewerterfelder', 'bwg' ),
			[ $this, 'meta_box_evaluation_profile_fields' ],
			'bwg_evaluation',
			'normal',
			'high'
		);

		// Add the evaluation extras meta box.
		add_meta_box( 'bwg-evaluation-extras',
			__( 'Evaluation', 'bwg' ),
			[ $this, 'meta_box_evaluation_extras' ],
			'bwg_evaluation',
			'side',
			'high'
		);
	}

	/**
	 * Handler for 'admin_menu' action.
	 */
	public function admin_menu() {
		add_submenu_page(
			'', // No parent (should not be visible in menu)
			'BWG Evaluation Test',
			'', // Not visible in menu
			'manage_options',
			self::SLUG_TEST,
			[ $this, 'admin_page_full_view' ]
		);
	}

	/**
	 * Handler for 'admin_footer' action registered by the meta box 'Evaluation Definition'.
	 */
	public function meta_admin_footer() {
		echo $this->_bwg_base->utils()
		                     ->render_admin_template( 'evaluation/definition/meta_box_modals.php' );
	}


	/**
	 * Handler for `the_content` filter.
	 *
	 * @param string|null $content
	 *
	 * @return string|null
	 */
	public function the_content( ?string $content = NULL ): ?string {
		if ( 'bwg_evaluation' != get_post_type() ) {
			return $content;
		}

		/*
		if ( current_user_can( 'manage_options' ) && is_preview() ) {
			// This actually would only be true if the evaluation is not published.
			echo '<div style="position:fixed;bottom:0;left:0;right:0;background-color:#ff6;padding:10px;">This is just an admin preview.</div>';
		}
		*/

		$content = $this->_bwg_base->factory()->bwg_evaluation_content(
			$this->_bwg_base,
			new FEUP_User(),
			$this->_bwg_base->factory()->bwg_evaluation_post( get_post() ),
			$content
		);

		return $content->render();
	}

	/**
	 * Meta box 'Evaluation Definition'.
	 */
	public function meta_box_evaluation_definition() {
		/** @var $post WP_Post */
		global $post;

		// Register the admin footer action.
		add_action( 'admin_footer', [ $this, 'meta_admin_footer' ] );

		$custom = get_post_custom( $post->ID );
		$bwg_ed = isset( $custom['bwg_ed'] ) ? $custom['bwg_ed'][0] : '{"ai":0}';
		$ed     = json_decode( $bwg_ed, TRUE, 512, JSON_UNESCAPED_UNICODE );

		// Build the render array.
		$ra = [
			'evaluation_definition'       => $ed,
			'evaluation_definition_json'  => $bwg_ed,
			'evaluation_definition_html'  => $this->buildAdminEvaluationDefinitionHtml( $bwg_ed ),
			'evaluation_definition_fixed' => $this->_bwg_base->submissions_database_helper()
			                                                 ->has_submissions( $post->ID ),
		];

		// Enqueue scripts and styles used by this meta box.
		$this->_bwg_base
			->utils()
			->enqueue_script( 'bwg-admin-evaluation-meta-box',
				'js/admin/evaluation/meta-box.js',
				[ 'jquery', 'jquery-ui-dialog', 'bwg-admin' ], TRUE, FALSE
			);
		$this->_bwg_base
			->utils()
			->enqueue_style( 'bwg-admin-evaluation-meta-box',
				'css/admin/evaluation/definition/meta-box.css',
				[ 'bwg-admin', 'wp-jquery-ui-dialog' ], TRUE
			);

		// Render.
		wp_nonce_field(
			self::META_BOX_DEFINITION_NONCE_ACTION,
			self::META_BOX_DEFINITION_NONCE_NAME
		);
		echo $this->_bwg_base
			->utils()
			->render_admin_template( 'evaluation/definition/meta_box.php', $ra, TRUE );
		echo $this->buildAdminEvaluationDefinitionTemplates();
	}

	/**
	 * Meta box 'Evaluation Emails'.
	 *
	 * @var WP_Post $post
	 */
	public function meta_box_evaluation_emails() {
		global $post;

		// Get the utils class.
		$utils = $this->_bwg_base->utils();

		$custom = get_post_custom( $post->ID );

		$feup_excludes = [ $this->_bwg_base->options()->get_feup_registration_evaluation_field_id() ];
		$feup_fields   = [];
		foreach ( $this->_bwg_base->feup_layer()->get_fields() as $feup_field ) {
			if ( in_array( $feup_field['Field_ID'], $feup_excludes ) ) {
				continue;
			}
			array_push( $feup_fields, $feup_field['Field_Name'] );
		}

		$ra = [
			'post'                 => $post,
			'emails_test_action'   => BWG_Evaluation_Ajax::EMAILS_TEST_AJAX_ACTION,
			'emails_test_nonce'    => wp_create_nonce( BWG_Evaluation_Ajax::EMAILS_TEST_AJAX_ACTION ),
			'user_confirm'         => isset( $custom['bwg_em_user_confirm'] ) ?
				$custom['bwg_em_user_confirm'][0] : '',
			'feup_fields'          => $feup_fields,
			'emails_test_receiver' => wp_get_current_user()->user_email,
		];

		// Enqueue scripts and styles used by this meta box.
		$utils->enqueue_script( 'bwg-admin-emails-meta-box', 'js/admin/evaluation/emails/meta-box.js',
			[ 'jquery', 'bwg-admin' ], TRUE, FALSE );
		$utils->enqueue_style( 'bwg-admin-emails-meta-box', 'css/admin/evaluation/emails/meta-box.css',
			[ 'bwg-admin' ], TRUE );

		// Render the meta box.
		wp_nonce_field( self::META_BOX_EMAILS_NONCE_ACTION, self::META_BOX_EMAILS_NONCE_NAME );
		echo $this->_bwg_base->utils()->render_admin_template(
			'evaluation/emails/meta-box.php', $ra, TRUE );
	}

	/**
	 * Meta box 'Evaluation Profile Fields'.
	 *
	 * @var WP_Post $post
	 */
	public function meta_box_evaluation_profile_fields() {
		global $post;

		// Get the utils class.
		$utils = $this->_bwg_base->utils();

		// Get the profile fields from the current post.
		$evaluation_post = $this->_bwg_base->factory()->bwg_evaluation_post( $post );
		$profile_fields  = $evaluation_post->get_profile_fields();

		// Initialize the render array.
		$ra = [
			'post'                   => $post,
			'profile_field_action'   => BWG_Evaluation_Ajax::PROFILE_FIELD_AJAX_ACTION,
			'profile_field_nonce'    => wp_create_nonce( BWG_Evaluation_Ajax::PROFILE_FIELD_AJAX_ACTION ),
			'type_list'              => $this->_bwg_base->profile_field_type_registry()->get_type_list(),
			'introduction'           => $profile_fields->get_introduction(),
			'fields'                 => $profile_fields->render_meta_box_fields(
				$this->_bwg_base->utils(),
				$this->_bwg_base->profile_field_type_registry()
			),
			'json_value'             => $profile_fields->get_json(),
			'field_loading_template' => $this->_bwg_base->utils()->render_admin_template(
				'evaluation/profile-fields/meta-box-field.php', [
					'uid'              => '###UID###',
					'type'             => '###TYPE###',
					'loading_template' => TRUE
				]
			),
			'admin_colors'           => $this->_bwg_base->utils()->get_admin_colors(),
		];

		// Enqueue scripts and styles used by this meta box.
		$utils->enqueue_script( 'bwg-admin-profile-fields-meta-box', 'js/admin/evaluation/profile-fields/meta-box.js',
			[ 'jquery', 'bwg-admin', 'underscore' ], TRUE, FALSE );
		$utils->enqueue_style( 'bwg-admin-profile-fields-meta-box', 'css/admin/evaluation/profile-fields/meta-box.css',
			[ 'bwg-admin' ], TRUE );

		// Render the meta box.
		wp_nonce_field( self::META_BOX_PROFILE_FIELDS_NONCE_ACTION, self::META_BOX_PROFILE_FIELDS_NONCE_NAME );
		echo $utils->render_admin_template(
			'evaluation/profile-fields/meta-box.php', $ra, TRUE
		);
	}

	/**
	 * Meta box 'Evaluation Extras'.
	 */
	public function meta_box_evaluation_extras() {
		/** @var $post WP_Post */
		global $post;

		$custom = get_post_custom( $post->ID );
		$ra     = [
			'postage'                => isset( $custom['bwg_ee_postage'] ) ? $custom['bwg_ee_postage'][0] : FALSE,
			'registration_open_from' => $this->_custom_date_value_to_input_value( $custom,
				'bwg_ee_registration_open_from' ),
			'registration_open_to'   => $this->_custom_date_value_to_input_value( $custom,
				'bwg_ee_registration_open_to' ),
			'subscriber'             => isset( $custom['bwg_ee_subscriber'] ) ? $custom['bwg_ee_subscriber'][0] : FALSE,
			'postage_max'            => isset( $custom['bwg_ee_postage_max'] ) ? $custom['bwg_ee_postage_max'][0] : 0,
			'rating_open_from'       => $this->_custom_date_value_to_input_value( $custom,
				'bwg_ee_rating_open_from' ),
			'rating_open_to'         => $this->_custom_date_value_to_input_value( $custom,
				'bwg_ee_rating_open_to' ),
			'comment'                => isset( $custom['bwg_ee_comment'] ) ? $custom['bwg_ee_comment'][0] : '',
		];

		// Enqueue scripts and styles used by this meta box.
		$this->_bwg_base->utils()
		                ->enqueue_script( 'bwg-admin-evaluation-extras-meta-box',
			                'js/admin/evaluation/extras/meta-box.js',
			                [ 'jquery', 'bwg-admin' ], TRUE, FALSE );
		$this->_bwg_base->utils()
		                ->enqueue_style( 'bwg-admin-evaluation-extras-meta-box',
			                'css/admin/evaluation/extras/meta-box.css',
			                [ 'bwg-admin' ], TRUE );

		// Render.
		wp_nonce_field( self::META_BOX_EXTRAS_NONCE_ACTION,
			self::META_BOX_EXTRAS_NONCE_NAME );
		echo $this->_bwg_base->utils()
		                     ->render_admin_template( 'evaluation/extras/meta-box.php',
			                     $ra, TRUE );
	}

	/**
	 * @param $custom
	 * @param $name
	 *
	 * @return string
	 */
	private function _custom_date_value_to_input_value( $custom, $name ) {
		$dt = isset( $custom[ $name ] ) ? DateTime::createFromFormat( 'Y-m-d',
			$custom[ $name ][0] ) : FALSE;

		return FALSE === $dt ? '' : $dt->format( 'd.m.Y' );
	}

	/**
	 * @param $input_value
	 *
	 * @return string
	 */
	private function _custom_date_input_value_to_value( $input_value ) {
		$dt = DateTime::createFromFormat( 'd.m.Y', $input_value );

		return FALSE === $dt ? '' : $dt->format( 'Y-m-d' );
	}

	/**
	 * Handler for 'save_post_bwg_evaluation' action.
	 *
	 * @param int $post_id
	 * @param WP_Post $post
	 * @param bool $update
	 *
	 * @return int
	 */
	public function save_post( $post_id, WP_Post $post, $update ) {
		// Update?
		if ( ! $update ) {
			return $post_id;
		}

		// Check auto save.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		// Check capabilities.
		if ( ! current_user_can( 'edit_others_posts' ) ) {
			return $post_id;
		}

		// Check if there was a multi site switch before.
		if ( is_multisite() && ms_is_switched() ) {
			return $post_id;
		}

		// If this is a revision, get real post ID.
		if ( $parent_id = wp_is_post_revision( $post_id ) ) {
			$post_id = $parent_id;
		}

		// Flag that indicates whether the evaluation schema for this evaluation should be updated or not.
		$update_db_evaluation_schema = FALSE;

		// Check nonce for meta box 'Evaluation Definition'.
		// -> Meta Box 'Evaluation Definition'.
		if (
			isset( $_REQUEST[ self::META_BOX_DEFINITION_NONCE_NAME ] ) &&
			check_admin_referer( self::META_BOX_DEFINITION_NONCE_ACTION,
				self::META_BOX_DEFINITION_NONCE_NAME )
		) {
			// Update the post meta.
			update_post_meta( $post_id, 'bwg_ed', $_REQUEST['bwg_ed'] );

			$update_db_evaluation_schema = TRUE;
		}

		// Check nonce for meta box 'Emails'.
		// -> Meta Box 'Emails'.
		if (
			isset( $_REQUEST[ self::META_BOX_EMAILS_NONCE_NAME ] ) &&
			check_admin_referer( self::META_BOX_EMAILS_NONCE_ACTION,
				self::META_BOX_EMAILS_NONCE_NAME )
		) {
			// Update the post meta.
			update_post_meta( $post_id, 'bwg_em_user_confirm', $_REQUEST['bwg_em_user_confirm'] );
		}

		// Check nonce for meta box 'Profile Fields'.
		// -> Meta Box 'Profile Fields'.
		if (
			isset( $_REQUEST[ self::META_BOX_PROFILE_FIELDS_NONCE_NAME ] ) &&
			check_admin_referer( self::META_BOX_PROFILE_FIELDS_NONCE_ACTION,
				self::META_BOX_PROFILE_FIELDS_NONCE_NAME )
		) {
			update_post_meta( $post_id, 'bwg_epf', $_REQUEST['bwg_epf'] );

			$update_db_evaluation_schema = TRUE;
		}

		// Check nonce for meta box 'Evaluation Extras'.
		// -> Meta Box 'Evaluation Extras'.
		if (
			isset( $_REQUEST[ self::META_BOX_EXTRAS_NONCE_NAME ] ) &&
			check_admin_referer( self::META_BOX_EXTRAS_NONCE_ACTION,
				self::META_BOX_EXTRAS_NONCE_NAME )
		) {
			// Meta Box 'Evaluation Extras'.
			update_post_meta( $post_id, 'bwg_ee_postage',
				array_key_exists( 'bwg_ee_postage', $_REQUEST ) ? $_REQUEST['bwg_ee_postage'] == 1 : FALSE );
			update_post_meta( $post_id, 'bwg_ee_registration_open_from',
				$this->_custom_date_input_value_to_value( $_REQUEST['bwg_ee_registration_open_from'] ) );
			update_post_meta( $post_id, 'bwg_ee_registration_open_to',
				$this->_custom_date_input_value_to_value( $_REQUEST['bwg_ee_registration_open_to'] ) );
			/*
			update_post_meta( $post_id, 'bwg_ee_subscriber',
				$_REQUEST['bwg_ee_subscriber'] == 1 );
			update_post_meta( $post_id, 'bwg_ee_postage_max',
				intval( $_REQUEST['bwg_ee_postage_max'] ) );
			*/
			update_post_meta( $post_id, 'bwg_ee_rating_open_from',
				$this->_custom_date_input_value_to_value( $_REQUEST['bwg_ee_rating_open_from'] ) );
			update_post_meta( $post_id, 'bwg_ee_rating_open_to',
				$this->_custom_date_input_value_to_value( $_REQUEST['bwg_ee_rating_open_to'] ) );
			update_post_meta( $post_id, 'bwg_ee_comment',
				$_REQUEST['bwg_ee_comment'] );
		}

		if ( $update_db_evaluation_schema ) {
			// Load the evaluation post.
			$evaluation_post = $this->_bwg_base->factory()->bwg_evaluation_post( get_post( $post_id ) );

			// Get the post meta evaluation definition.
			$evaluation_definition = $evaluation_post->get_evaluation_definition();

			// Get the post meta profile fields.
			$profile_fields = $evaluation_post->get_profile_fields();

			// Update the database schema for storing the evaluation specific gradings.
			$this->_bwg_base->submissions_database_helper()
			                ->db_delta_submissions(
				                $post_id,
				                $evaluation_definition,
				                $profile_fields
			                );
		}

		return $post_id;
	}

	/**
	 * Handler for 'delete_post' action.
	 *
	 * @param int $post_id
	 */
	public function delete_post( $post_id ) {
		if ( 'bwg_evaluation' !== get_post_type( $post_id ) ) {
			return;
		}

		// Drop the submissions table if it exists.
		$this->_bwg_base->submissions_database_helper()->drop_table( $post_id );
	}

	/**
	 * Admin page "Full View" callback.
	 */
	public function admin_page_full_view() {
		$post = get_post( intval( $_REQUEST['post'] ) );
		if ( ! $post ) {
			echo $this->_bwg_base->admin_render_error_wrap( 'Evaluation konnte nicht geladen werden.' );

			return;
		}

		$ra = [
			'post'                  => $post,
			'title'                 => get_the_title( $post ),
			'evaluation_definition' => new BWG_Evaluation_Definition(
				get_post_meta( $post->ID, 'bwg_ed', TRUE )
			),
			'grading'               => $this->get_grading(),
			'spider_chart_nonce'    => wp_create_nonce( BWG_Evaluation_Ajax::SPIDER_CHART_AJAX_ACTION ),
			'spider_chart_action'   => BWG_Evaluation_Ajax::SPIDER_CHART_AJAX_ACTION,
		];

		$this->_bwg_base->utils()->enqueue_script(
			'bwg-admin-full-view',
			'js/admin/evaluation/full-view.js',
			[ 'bwg-admin' ],
			TRUE
		);
		$this->_bwg_base->utils()->enqueue_style(
			'bwg-admin-full-view', 'css/admin/full-view/full-view.css',
			[],
			TRUE
		);

		echo $this->_bwg_base->utils()
		                     ->render_admin_template( 'evaluation/full-view/full-view.php',
			                     $ra );
	}

	/**
	 * Builds the admin evaluation definition templates used by javascript when a new evaluation definition item gets
	 * added. Note that for levels 0 and 1 the the sub items would be of course empty arrays. Level 2 does not
	 * support sub items.
	 *
	 * @return string
	 */
	public function buildAdminEvaluationDefinitionTemplates() {
		return '<script id="bwg_ed_item_template0" type="text/template">'
		       . $this->_render_admin_evaluation_definition_item(
				'###ID###', '###LABEL###', '###DESCRIPTION###', '###DETAILS###', '###WEIGHT###', '###SPIDER_CHART###',
				FALSE, $this->_render_admin_evaluation_definition_items( [], 1 )
			) . '</script>'
		       . '<script id="bwg_ed_item_template1" type="text/template">'
		       . $this->_render_admin_evaluation_definition_item(
				'###ID###', '###LABEL###', '###DESCRIPTION###', '###DETAILS###', '###WEIGHT###', '###SPIDER_CHART###',
				FALSE, $this->_render_admin_evaluation_definition_items( [], 2 )
			) . '</script>'
		       . '<script id="bwg_ed_item_template2" type="text/template">'
		       . $this->_render_admin_evaluation_definition_item(
				'###ID###', '###LABEL###', '###DESCRIPTION###', '###DETAILS###', '###WEIGHT###', '###SPIDER_CHART###',
				FALSE, ''
			) . '</script>';
	}

	/**
	 * Builds the admin evaluation definition html used in the meta box.
	 *
	 * @param $edJson
	 *
	 * @return string
	 */
	public function buildAdminEvaluationDefinitionHtml( $edJson ) {
		$ed = json_decode( $edJson, TRUE, 512, JSON_UNESCAPED_UNICODE );

		return $this->_render_admin_evaluation_definition_items(
			isset( $ed['items'] ) ? $ed['items'] : [], 0
		);
	}

	/**
	 * Renders the admin evaluation definition items used in the meta box.
	 *
	 * @param array $data
	 * @param int $level
	 *
	 * @return string
	 */
	protected function _render_admin_evaluation_definition_items( $data, $level ) {
		$sub_level_exists = $level < 2;

		$items = '';
		foreach ( $data as $item_data ) {
			$id           = isset( $item_data['id'] ) ? $item_data['id'] : NULL;
			$label        = isset( $item_data['label'] ) ? $item_data['label'] : '';
			$description  = isset( $item_data['description'] ) ? $item_data['description'] : '';
			$details      = isset( $item_data['details'] ) ? $item_data['details'] : '';
			$weight       = isset( $item_data['weight'] ) ? $item_data['weight'] : '1.0';
			$spider_chart = isset( $item_data['spider_chart'] ) ? $item_data['spider_chart'] : 1;
			$open         = isset( $item_data['open'] ) ? $item_data['open'] : FALSE;

			if ( $sub_level_exists ) {
				if ( ! isset( $item_data['items'] ) ) {
					$item_data['items'] = [];
				}

				// Generate the html code for the sub items (Recursive call).
				$sub_items = $this->_render_admin_evaluation_definition_items(
					$item_data['items'], $level + 1
				);

				// Generate the html code for the item (append the sub items) and append to the items html code.
				$items .= $this->_render_admin_evaluation_definition_item(
					$id, $label, $description, $details, $weight, $spider_chart, $open, $sub_items
				);
			} else {
				// As there are no sub levels, we can immediately append the html code for the item to the items html
				// code.
				$items .= $this->_render_admin_evaluation_definition_item(
					$id, $label, $description, $details, $weight, $spider_chart, $open, ''
				);
			}
		}

		// Build the render array.
		$ra = [
			'subLevelExists' => $sub_level_exists,
			'level'          => $level,
			'items'          => $items,
		];

		// Render.
		return $this->_bwg_base
			->utils()
			->render_admin_template(
				'evaluation/definition/meta_box_items.php', $ra, FALSE
			);
	}

	/**
	 * Renders the admin evaluation definition item used in the meta box.
	 *
	 * @param int $id
	 * @param string $label
	 * @param string $description
	 * @param string $details
	 * @param double $weight
	 * @param bool $spider_chart
	 * @param bool $is_open
	 * @param string $sub_items
	 *
	 * @return string
	 */
	protected function _render_admin_evaluation_definition_item(
		$id,
		$label,
		$description,
		$details,
		$weight,
		$spider_chart,
		$is_open,
		$sub_items
	) {
		// Build the render array.
		$ra = [
			'id'               => $id,
			'label'            => $label,
			'description'      => $description,
			'details'          => $details,
			'weight'           => $weight,
			'spider_chart'     => $spider_chart,
			'is_open'          => $is_open,
			'sub_level_exists' => ! empty( $sub_items ),
			'sub_items'        => $sub_items,
		];

		// Render.
		return $this->_bwg_base
			->utils()
			->render_admin_template(
				'evaluation/definition/meta_box_item.php', $ra, FALSE
			);
	}

	/**
	 * Gets the bwg grading array.
	 *
	 * @return BWG_Evaluation_Grading[] Array of BWG_Evaluation_Grading objects.
	 */
	public function get_grading() {
		static $grading = NULL;

		if ( is_null( $grading ) ) {
			$grading = [];
			for ( $i = 1; $i <= 6; $i ++ ) {
				array_push( $grading, new BWG_Evaluation_Grading( $i, $i ) );
			}
		}

		return $grading;
	}

}
