<?php

namespace bwg\evaluation;

use bwg\BWG_Base;
use WP_Post;

/**
 * Class BWG_Evaluation_Post_Type_Manager.
 *
 * This class manages the custom post type 'bwg_evaluation'.
 *
 * @package bwg\evaluation
 */
class BWG_Evaluation_Post_Type_Manager {

	/**
	 * Post column name for 'evaluation users' column.
	 */
	const POST_COLUMN_USERS = 'bwg_e_users';

	/**
	 * Post column name for 'post name' (alias, permalink) column.
	 */
	const POST_COLUMN_ALIAS = 'bwg_e_post_name';

	/**
	 * @var BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Evaluation_Post_Type_Manager constructor.
	 *
	 * @param BWG_Base $bwg_base
	 */
	function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;
	}

	/**
	 * Registers the custom post type 'bwg_evaluation'.
	 */
	public function register_custom_post_type() {
		$labels = [
			'name'          => _x( 'BWG Evaluations', 'post type general name' ),
			'singular_name' => _x( 'BWG Evaluation', 'post type singular name' ),
			'menu_name'     => 'BWG Evaluations',
		];

		$args = [
			'labels'        => $labels,
			'description'   => 'Holds BWG evaluations.',
			'map_meta_cap'  => TRUE,
			'public'        => TRUE,
			'show_ui'       => TRUE,
			'menu_icon'     => 'dashicons-carrot',
			'menu_position' => 5,
			'supports'      => [ 'title', 'editor', 'thumbnail' ],
			'taxonomies'    => [],
			'has_archive'   => TRUE,
			'rewrite'       => [ 'slug' => 'bwg' ],
		];

		register_post_type( 'bwg_evaluation', $args );
	}

	/**
	 * Sets up some stuff used in the administration of 'bwg_evaluation' posts.
	 */
	public function setup_admin_post() {
		add_filter( 'manage_bwg_evaluation_posts_columns',
			[ $this, 'manage_posts_columns' ], 10 );
		add_action( 'manage_bwg_evaluation_posts_custom_column',
			[ $this, 'manage_posts_custom_column' ], 10, 2 );

		add_filter( 'bulk_actions-edit-bwg_evaluation',
			[ $this, 'bulk_actions' ], 10, 1 );
		add_filter( 'post_row_actions',
			[ $this, 'post_row_actions' ], 15, 2 );
	}

	/**
	 * Manages the posts columns shown in the list of posts in the administration.
	 *
	 * @param $defaults
	 *
	 * @return array
	 */
	public function manage_posts_columns( $defaults ) {
		return [
			'cb'                    => $defaults['cb'],
			'title'                 => $defaults['title'],
			self::POST_COLUMN_ALIAS => __( 'Permalink', 'bwg' ),
			self::POST_COLUMN_USERS => __( 'Eingereicht', 'bwg' ),
			'date'                  => $defaults['date'],
		];
	}

	/**
	 * Manages the content of the posts columns in the list of posts in the administration.
	 *
	 * @param string $column_name
	 * @param int $post_ID
	 */
	public function manage_posts_custom_column( $column_name, $post_ID ) {
		if ( $column_name === self::POST_COLUMN_USERS ) {
			$href = admin_url( sprintf(
					'edit.php?post_type=bwg_evaluation&page=%1$s&post=%2$d',
					BWG_Evaluation_Users::PAGE_SLUG, $post_ID
				)
			);

			$label = $this->_bwg_base->user_storage_database_helper()->get_total_nr_submitted( $post_ID );

			echo sprintf( '<a href="%1$s" rel="permalink">%2$s</a>', $href, $label );
		} else if ( self::POST_COLUMN_ALIAS === $column_name ) {
			$post = get_post( $post_ID );
			echo $post->post_name;
		}
	}

	/**
	 * Handler for 'bulk_actions-edit-bwg_evaluation' filter.
	 *
	 * @param array $actions
	 *
	 * @return array
	 */
	public function bulk_actions( $actions ) {
		unset( $actions['edit'] );

		return $actions;
	}

	/**
	 * Handler for 'post_row_actions' filter.
	 *
	 * @param array $actions
	 * @param WP_Post $post
	 *
	 * @return array
	 */
	public function post_row_actions( $actions, WP_Post $post ) {
		if ( 'bwg_evaluation' !== $post->post_type ) {
			return $actions;
		}

		if ( 'trash' === $post->post_status ) {
			return $actions;
		}

		// Remove the 'quick-edit'.
		foreach ( $actions as $key => $val ) {
			if ( FALSE !== stripos( $key, 'inline' ) ) {
				unset( $actions[ $key ] );
			}
		}

		if ( isset( $actions['view'] ) ) {
			// Remove the trash action (we will re-insert at the end of this if-scope).
			$trash = $actions['trash'];
			unset( $actions['trash'] );

			///
			// New Action: 'Analyses'.
			///
			if ( FALSE ) {
				$link = admin_url( sprintf(
					'edit.php?post_type=bwg_evaluation&page=%s&post=%d',
					BWG_Evaluation_Analyses::SLUG_ANALYSES, $post->ID
				) );

				$actions['analyses'] = sprintf(
					'<a href="%s" rel="permalink">%s</a>',
					esc_url( $link ),
					__( 'Auswertungen', 'bwg' )
				);
			}

			///
			// New Action: 'Participants statistics'.
			///
			if ( $this->_bwg_base->options()->get_option_bwg_mode() === 'drogothek' ) {
				$link = admin_url( sprintf(
					'edit.php?post_type=bwg_evaluation&page=%s&post=%d',
					BWG_Evaluation_Participation_Statistics::SLUG_MAIN,
					$post->ID
				) );

				$actions['participation-statistics'] = sprintf(
					'<a href="%s" rel="permalink">%s</a>',
					esc_url( $link ),
					__( 'Teilnahme Statistiken', 'bwg' )
				);
			}

			// Add a new action.
			/*
			$link                       = admin_url( sprintf( 'edit.php?post_type=bwg_evaluation&page=%s&post=%d',
				BWG_Evaluation::SLUG_TEST, $post->ID ) );
			$actions['view-admin-test'] = sprintf(
				'<a href="%s" rel="permalink">%s</a>',
				esc_url( $link ),
				__( 'Test-Vorschau' )
			);
			*/

			// Re-insert thrash link preserved from the default $actions.
			$actions['trash'] = $trash;
		}

		return $actions;
	}
}
