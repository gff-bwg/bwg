<?php

namespace bwg\evaluation;

class BWG_Evaluation_Selection_Filter_Utils {
	public static function groups_to_condition_sums( $groups ) {
		$condition_sums = [];
		foreach ( $groups as $group_key => $value ) {
			$conditions = explode( '/', $group_key );

			for ( $i = 0; $i < count( $conditions ); $i ++ ) {
				if ( ! isset( $condition_sums[ $i ][ $conditions[ $i ] ] ) ) {
					$condition_sums[ $i ][ $conditions[ $i ] ] = $value;
				} else {
					$condition_sums[ $i ][ $conditions[ $i ] ] += $value;
				}
			}
		}

		return $condition_sums;
	}

}