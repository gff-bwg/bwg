<?php

namespace bwg;

/**
 * Class BWG_Globals.
 *
 * This class contains some global values that would be used by BWG.
 *
 * @package bwg
 */
class BWG_Globals {

	/**
	 * The default spider chart width.
	 *
	 * @return int
	 */
	public function spider_chart_width() {
		return 600;
	}

	/**
	 * The default spider chart height.
	 *
	 * @return int
	 */
	public function spider_chart_height() {
		return 450;
	}

	/**
	 * The database table name 'bwg_evaluation_presets'.
	 *
	 * @return string
	 */
	public function db_table_evaluation_presets() {
		return 'bwg_evaluation_presets';
	}

}
