<?php

namespace bwg\cron;

use bwg\BWG_Base;

/**
 * Class BWG_Cron_Handler.
 *
 * @package bwg\cron
 */
class BWG_Cron_Handler {

	/**
	 * @var BWG_Base
	 */
	protected BWG_Base $bwg_base;

	/**
	 * @param BWG_Base $bwg_base
	 */
	function __construct( BWG_Base $bwg_base ) {
		$this->bwg_base = $bwg_base;
	}

	/**
	 * The 'check users' cron.
	 */
	public function check_users(): void {
		// When a user would be deleted in FEUP, then we can also delete its storage in BWG.
		// TODO [AS] Implement this - carefully.
	}

	/**
	 * The 'maintenance' cron.
	 */
	public function maintenance(): void {
		// TODO [AS] Calculate some full range averages for all evaluations. The system would use them to generate
		//      the spider charts series "Gradings of the other users".

		// Build and calculate some reports and/or mid-term reports.
		// TODO [AS] Implement this.

		// Clear the private cache files (older than 24 hours).
		$private_cache_path = $this->bwg_base->utils()->get_bwg_files_private_cache_path( TRUE );
		if ( FALSE !== $private_cache_path ) {
			$this->bwg_base->utils()->clear_directory( $private_cache_path, 86400 );
		}
	}
}
