<?php

namespace bwg\cron;


/**
 * Class BWG_Cron_Manager.
 *
 * Contains static methods that would be used during activation and deactivation
 * of the plugin.
 *
 * @package bwg\cron
 */
class BWG_Cron_Manager {

	/**
	 * The key for the 10-minutes cron schedule.
	 */
	const CRON_SCHEDULE_10MINUTES = '10minutes';

	/**
	 * The key for the 15-minutes cron schedule.
	 */
	const CRON_SCHEDULE_15MINUTES = '15minutes';

	/**
	 * The key for the 60-minutes cron schedule.
	 */
	const CRON_SCHEDULE_60MINUTES = '60minutes';

	/**
	 * The cron hook name for the cron 'check_users'.
	 */
	const CRON_NAME_CHECK_USERS = 'bwg_cron_check_users';

	/**
	 * The cron hook name for the cron 'maintenance'.
	 */
	const CRON_NAME_MAINTENANCE = 'bwg_cron_maintenance';


	/**
	 * @param $schedules
	 *
	 * @return mixed
	 */
	public static function cron_schedules( $schedules ) {
		$schedules[ self::CRON_SCHEDULE_10MINUTES ] = array(
			'interval' => 600,
			'display'  => __( 'Every 10 minutes', 'bwg' ),
		);

		$schedules[ self::CRON_SCHEDULE_15MINUTES ] = array(
			'interval' => 900,
			'display'  => __( 'Every 15 minutes', 'bwg' ),
		);

		$schedules[ self::CRON_SCHEDULE_60MINUTES ] = array(
			'interval' => 3600,
			'display'  => __( 'Every 60 minutes', 'bwg' ),
		);

		return $schedules;
	}

	/**
	 * Registers the bwg cron jobs.
	 */
	public static function register_cron_jobs() {
		self::schedule_cron_action( self::CRON_SCHEDULE_15MINUTES, self::CRON_NAME_CHECK_USERS );
		self::schedule_cron_action( self::CRON_SCHEDULE_60MINUTES, self::CRON_NAME_MAINTENANCE );
	}

	/**
	 * Schedules a cron action/hook.
	 *
	 * @param string $recurrence
	 * @param string $hook
	 * @param array $args
	 */
	public static function schedule_cron_action( $recurrence, $hook, $args = [] ) {
		$timestamp = wp_next_scheduled( $hook );
		
		// If $timestamp == false > schedule.
		if ( FALSE === $timestamp ) {
			wp_schedule_event( time(), $recurrence, $hook, $args );
		}
	}

	/**
	 * Cancel the bwg cron jobs.
	 */
	public static function cancel_cron_jobs() {
		wp_clear_scheduled_hook( self::CRON_NAME_CHECK_USERS );
		wp_clear_scheduled_hook( self::CRON_NAME_MAINTENANCE );
	}

}