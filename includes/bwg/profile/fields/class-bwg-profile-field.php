<?php

namespace bwg\profile\fields;

use bwg\BWG_Utils;
use bwg\evaluation\BWG_Evaluation_Post;
use bwg\stats\strata\BWG_Stats_Strata_Collection;

/**
 * Class BWG_Profile_Field.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field {

	/**
	 * @var \bwg\profile\fields\BWG_Profile_Field_Settings
	 */
	private $settings;


	/**
	 * BWG_Profile_Field constructor.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field_Settings $settings
	 */
	public function __construct( BWG_Profile_Field_Settings $settings ) {
		$this->settings = $settings;
	}

	/**
	 * Gets the settings of this field.
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field_Settings
	 */
	public function get_settings() {
		return $this->settings;
	}

	/**
	 * Gets the type (actually the type name) from the settings of this field.
	 *
	 * @return string
	 */
	public function get_type() {
		return $this->get_settings()->get_type();
	}

	/**
	 * Gets the uid from the settings of this field.
	 *
	 * @return string
	 */
	public function get_uid() {
		return $this->get_settings()->get_uid();
	}

	/**
	 * Gets the label from the settings of this field.
	 *
	 * @return string
	 */
	public function get_label() {
		return $this->get_settings()->get_label();
	}

	/**
	 * Is this field required.
	 *
	 * @return boolean
	 */
	public function is_required() {
		return $this->get_settings()->is_required();
	}

	/**
	 * Gets the options from the settings of this field.
	 *
	 * @return array
	 */
	public function get_options() {
		return $this->get_settings()->get_options();
	}

	/**
	 * Gets the options converted into a field type specific object from the settings of this field.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field_Options|null
	 */
	public function get_field_options( BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry ) {
		$type = $this->get_field_type( $bwg_profile_field_type_registry );
		if ( is_null( $type ) ) {
			return NULL;
		}

		return $type->field_options( $this->get_options() );
	}

	/**
	 * Gets the profile field type of this field.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field_Type|null
	 */
	public function get_field_type( BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry ) {
		return $bwg_profile_field_type_registry->get_type(
			$this->get_type()
		);
	}

	/**
	 * Fills the strata collection with all possible stratas for the current field.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 * @param \bwg\stats\strata\BWG_Stats_Strata_Collection $strata_collection
	 */
	public function fill_strata_collection(
		BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry,
		BWG_Stats_Strata_Collection $strata_collection
	) {
		$type = $this->get_field_type( $bwg_profile_field_type_registry );
		if ( is_null( $type ) ) {
			return;
		}

		$type->fill_strata_collection(
			$strata_collection,
			$this,
			$this->get_field_options( $bwg_profile_field_type_registry )
		);
	}

	/**
	 * Renders the meta box.
	 *
	 * @param \bwg\BWG_Utils $bwg_utils
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 *
	 * @return string
	 */
	public function render_meta_box(
		BWG_Utils $bwg_utils,
		BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	) {
		$type = $this->get_field_type( $bwg_profile_field_type_registry );
		if ( is_null( $type ) ) {
			return '';
		}

		return $bwg_utils->render_admin_template(
			'evaluation/profile-fields/meta-box-field.php', [
			'uid'      => $this->get_uid(),
			'label'    => $this->get_label(),
			'required' => $this->is_required(),
			'type'     => $this->get_type(),
			'content'  => $type->render_meta_box( $this ),
		] );
	}

	/**
	 * Renders the profile field used by the evaluation form.
	 *
	 * @param \bwg\BWG_Utils $bwg_utils
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 * @param string|array|null $field_value
	 *
	 * @return string
	 */
	public function render_evaluation_form(
		BWG_Utils $bwg_utils,
		BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry,
		$field_value = NULL
	) {
		$type = $this->get_field_type( $bwg_profile_field_type_registry );
		if ( is_null( $type ) ) {
			return '';
		}

		return $bwg_utils->render_template(
			'evaluation-form/profile-field.php', [
			'uid'           => $this->get_uid(),
			'type'          => $this->get_type(),
			'field_info'    => $type->render_evaluation_form_info( $this ),
			'field_control' => $type->render_evaluation_form_control( $this, $field_value ),
		] );
	}

	/**
	 * Renders the profile field strata filter box.
	 *
	 * @param \bwg\BWG_Utils $bwg_utils
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 *
	 * @return string
	 */
	public function render_strata_filter_box(
		BWG_Utils $bwg_utils,
		BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	) {
		$type = $this->get_field_type( $bwg_profile_field_type_registry );
		if ( is_null( $type ) ) {
			return '';
		}

		return $bwg_utils->render_admin_template(
			'evaluation/profile-fields/strata-filter-box.php', [
			'uid'     => $this->get_uid(),
			'label'   => $this->get_label(),
			'type'    => $this->get_type(),
			'content' => $type->render_strata_filter_box( $this ),
		] );
	}

	/**
	 * Renders the profile field strata stats (as used in the administration full stats view).
	 *
	 * @param \bwg\BWG_Utils $bwg_utils
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
	 *
	 * @return string
	 */
	public function render_strata_stats(
		BWG_Utils $bwg_utils,
		BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry,
		BWG_Evaluation_Post $evaluation_post
	) {
		$type = $this->get_field_type( $bwg_profile_field_type_registry );
		if ( is_null( $type ) ) {
			return '';
		}

		if ( ! $type->is_strata_stats_supported() ) {
			return '';
		}

		return $bwg_utils->render_admin_template(
			'evaluation/profile-fields/strata-stats-box.php', [
			'uid'              => $this->get_uid(),
			'label'            => $this->get_label(),
			'type'             => $this->get_type(),
			'strata_supported' => $type->is_strata_supported(),
			'content'          => $type->render_strata_stats( $this, $evaluation_post ),
		] );
	}

	public function render_stats(
		BWG_Utils $bwg_utils,
		BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry,
		BWG_Evaluation_Post $bwg_evaluation_post
	) {
		$type = $this->get_field_type( $bwg_profile_field_type_registry );
		if ( is_null( $type ) ) {
			return '';
		}

		if ( ! $type->is_stats_enabled( $this ) ) {
			return '';
		}

		return $bwg_utils->render_admin_template(
			'evaluation/profile-fields/stats-box.php', [
			'uid'              => $this->get_uid(),
			'label'            => $this->get_label(),
			'content'          => $type->render_stats( $this, $bwg_evaluation_post ),
		] );
	}


	/**
	 * Gets the database column information (for the submission table) for the current profile field. The information
	 * is mostly dependent on the profile field type and sometimes on the field options.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 *
	 * @return \bwg\database\etc\BWG_Database_Submission_Column_Info
	 */
	public function db_submission_column_info( BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry ) {
		$field_options = $this->get_field_options( $bwg_profile_field_type_registry );
		$column_name   = $this->db_submissions_column_name();

		return $this->get_field_type( $bwg_profile_field_type_registry )
		            ->db_submission_column_info( $column_name, $field_options );
	}

	/**
	 * Gets the database column name (for the submission table).
	 *
	 * @return string
	 */
	public function db_submissions_column_name() {
		return str_replace( '-', '_', $this->get_uid() );
	}

	public function validate( BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry, $value ) {
		$field_options = $this->get_field_options( $bwg_profile_field_type_registry );

		return $this->get_field_type( $bwg_profile_field_type_registry )
		            ->validate( $this, $field_options, $value );
	}

}
