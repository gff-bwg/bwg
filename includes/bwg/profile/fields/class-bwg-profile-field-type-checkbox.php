<?php

namespace bwg\profile\fields;

use bwg\database\etc\BWG_Database_Submission_Column_Info;
use bwg\evaluation\BWG_Evaluation_Post;
use bwg\stats\strata\BWG_Stats_Strata_Collection;
use RuntimeException;

/**
 * Class BWG_Profile_Field_Type_Checkbox.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field_Type_Checkbox extends BWG_Profile_Field_Type {
	/**
	 * @inheritdoc
	 */
	public function is_strata_supported() {
		return FALSE;
	}

	/**
	 * @inheritdoc
	 */
	public function is_strata_stats_supported() {
		return TRUE;
	}

	/**
	 * @inheritdoc
	 */
	public function is_stats_enabled( BWG_Profile_Field $profile_field ) {
		return FALSE;
	}

	/**
	 * @inheritDoc
	 */
	public function fill_strata_collection(
		BWG_Stats_Strata_Collection $strata_collection,
		BWG_Profile_Field $profile_field,
		BWG_Profile_Field_Options $profile_field_options
	) {
		// No Strata possible for this profile field type.
		return;
	}

	/**
	 * @inheritDoc
	 */
	public function field_options( $options = [] ) {
		return new BWG_Profile_Field_Options_Checkbox( $options );
	}

	/**
	 * @inheritDoc
	 */
	public function db_submission_column_info( $column_name, BWG_Profile_Field_Options $options ) {
		if ( ! ( $options instanceof BWG_Profile_Field_Options_Checkbox ) ) {
			throw new RuntimeException( 'Field options exception.' );
		}

		return new BWG_Database_Submission_Column_Info(
			$column_name,
			'VARCHAR(' . ( 30 * 11 + 1 ) . ') DEFAULT NULL',
			'%s',
			TRUE
		);
	}

	/**
	 * @inheritDoc
	 */
	public function validate( BWG_Profile_Field $field, BWG_Profile_Field_Options $options, $value ) {
		if ( ! ( $options instanceof BWG_Profile_Field_Options_Checkbox ) ) {
			throw new RuntimeException( 'Field options exception.' );
		}

		if ( ! $field->is_required() ) {
			return [];
		}

		$value = trim( $value, '|' );
		if ( ! empty( $value ) ) {
			return [];
		}

		return [
			sprintf( __( 'Das Bewerterfeld «%s» ist erforderlich.', 'bwg' ), $field->get_label() ),
		];
	}

	/**
	 * @inheritDoc
	 */
	protected function render_strata_stats_internal(
		BWG_Profile_Field $field,
		BWG_Profile_Field_Options $options,
		BWG_Evaluation_Post $evaluation_post
	) {
		/** @var \bwg\profile\fields\BWG_Profile_Field_Options_Checkbox $options */
		global $wpdb;

		$table_name  = $this->bwg_base
			->submissions_database_helper()
			->get_submissions_table_name( $evaluation_post->get_id() );
		$column_name = $field->db_submissions_column_name();

		$total = $this->bwg_base->submissions_database_helper()->get_total_nr_submissions( $evaluation_post->get_id() );

		$ra = [
			'checkboxes' => [],
		];
		foreach ( $options->get_checkboxes() as $key => $label ) {
			$sql    = 'SELECT COUNT(*) AS amount FROM '
			          . $table_name . ' WHERE '
			          . $column_name . ' LIKE "%|' . $key . '|%"';
			$amount = $wpdb->get_var( $sql );

			array_push( $ra['checkboxes'], [
				'uid'               => $key,
				'label'             => esc_html( $label ),
				'amount'            => (int) $amount,
				'amount_percentage' => 100 * $amount / $total,
			] );
		}

		return $this->bwg_base
			->utils()
			->render_admin_template( 'evaluation/profile-fields/strata-stats-box.checkbox.php', $ra );
	}
}
