<?php

namespace bwg\profile\fields;

use bwg\database\etc\BWG_Database_Submission_Column_Info;
use bwg\evaluation\BWG_Evaluation_Post;
use bwg\stats\strata\BWG_Stats_Strata_Collection;
use bwg\stats\strata\selectors\BWG_Stats_Strata_Range_Selector;
use bwg\stats\strata\selectors\BWG_Stats_Strata_Value_Selector;

/**
 * Class BWG_Profile_Field_Type_Year.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field_Type_Year extends BWG_Profile_Field_Type {

	/**
	 * @inheritdoc
	 */
	public function is_strata_supported() {
		return TRUE;
	}

	/**
	 * @inheritdoc
	 */
	public function is_strata_stats_supported() {
		return TRUE;
	}

	/**
	 * @inheritdoc
	 */
	public function is_stats_enabled( BWG_Profile_Field $profile_field ) {
		return FALSE;
	}

	/**
	 * @inheritdoc
	 */
	public function field_options( $options = [] ) {
		return new BWG_Profile_Field_Options_Year( $options );
	}

	/**
	 * Gets the histogram classes.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $field
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $options
	 * @param int $low_age
	 * @param int $age_period
	 * @param int $high_age
	 *
	 * @return array
	 */
	public function get_histogram_classes(
		BWG_Profile_Field $field,
		BWG_Profile_Field_Options $options,
		$low_age = 20,
		$age_period = 10,
		$high_age = 60
	) {
		$histogram_classes = [];
		if ( ! $field->is_required() ) {
			array_push( $histogram_classes, [
				'filter' => [ 'no_value' => TRUE ],
				'label'  => __( 'Keine Angabe', 'bwg' ),
				'amount' => 0,
			] );
		}

		array_push( $histogram_classes, [
			'filter' => [ 'max_age' => $low_age ],
			'label'  => sprintf( __( 'Jünger als %d Jahre', 'bwg' ), $low_age ),
			'amount' => 0,
		] );

		for ( $i = $low_age; $i < $high_age; $i += $age_period ) {
			array_push( $histogram_classes, [
				'filter' => [ 'min_age' => $i, 'max_age' => min( $high_age, $i + $age_period ) ],
				'label'  => sprintf( __( 'Zwischen %d und %d Jahre', 'bwg' ), $i, min( $high_age, $i + $age_period ) ),
				'amount' => 0,
			] );
		}

		array_push( $histogram_classes, [
			'filter' => [ 'min_age' => $high_age ],
			'label'  => sprintf( __( 'Älter als %d Jahre', 'bwg' ), $high_age ),
			'amount' => 0,
		] );

		return $histogram_classes;
	}

	/**
	 * @inheritdoc
	 *
	 * @var \wpdb $wpdb
	 */
	protected function render_strata_stats_internal(
		BWG_Profile_Field $field,
		BWG_Profile_Field_Options $options,
		BWG_Evaluation_Post $evaluation_post
	) {
		/** @var \bwg\profile\fields\BWG_Profile_Field_Options_Year $options */
		global $wpdb;

		// Get the bwg base.
		$base = $this->bwg_base;

		$table_name  = $base
			->submissions_database_helper()
			->get_submissions_table_name( $evaluation_post->get_id() );
		$column_name = $field->db_submissions_column_name();

		// Define the histogram classes.
		$histogram_classes = $this->get_histogram_classes( $field, $options );

		// Get the current year.
		$current_year = date( 'Y' );

		// Select grouped from the submissions table.
		$sql     = 'SELECT COUNT(*) AS amount, '
		           . $column_name . ' AS value FROM '
		           . $table_name . ' GROUP BY '
		           . $column_name;
		$records = $wpdb->get_results( $sql );
		foreach ( $records as $record ) {
			for ( $i = 0, $c = count( $histogram_classes ); $i < $c; $i ++ ) {
				$hc       = $histogram_classes[ $i ];
				$no_value = ( '' === $record->value || is_null( $record->value ) || - 1 === intval( $record->value ) );

				if ( array_key_exists( 'no_value', $hc['filter'] ) && $hc['filter']['no_value'] ) {
					if ( $no_value ) {
						$histogram_classes[ $i ]['amount'] += $record->amount;
						break;
					}
				} else {
					if ( $no_value ) {
						continue;
					}

					$min_age = array_key_exists( 'min_age', $hc['filter'] ) ? $hc['filter']['min_age'] : 0;
					$max_age = array_key_exists( 'max_age', $hc['filter'] ) ? $hc['filter']['max_age'] : PHP_INT_MAX;
					$age     = $current_year - $record->value;

					if ( $age > $min_age && $age < $max_age ) {
						$histogram_classes[ $i ]['amount'] += $record->amount;
						break;
					}
				}
			}
		}

		$total = $this->bwg_base->submissions_database_helper()->get_total_nr_submissions( $evaluation_post->get_id() );
		for ( $i = 0, $c = count( $histogram_classes ); $i < $c; $i ++ ) {
			$histogram_classes[ $i ]['amount_percentage'] = 100 * $this->bwg_base->utils()->safe_division(
					$histogram_classes[ $i ]['amount'], $total
				);
		}

		$ra = [
			'histogram_classes' => $histogram_classes,
		];

		return $this->bwg_base
			->utils()
			->render_admin_template( 'evaluation/profile-fields/strata-stats-box.year.php', $ra );
	}

	/**
	 * @inheritdoc
	 */
	public function db_submission_column_info( $column_name, BWG_Profile_Field_Options $options ) {
		if ( ! ( $options instanceof BWG_Profile_Field_Options_Year ) ) {
			throw new \Exception( 'Field options exception.' );
		}

		return new BWG_Database_Submission_Column_Info(
			$column_name,
			'INT NULL DEFAULT NULL',
			'%d',
			TRUE
		);
	}

	/**
	 * @inheritdoc
	 */
	public function fill_strata_collection(
		BWG_Stats_Strata_Collection $strata_collection,
		BWG_Profile_Field $profile_field,
		BWG_Profile_Field_Options $profile_field_options
	) {
		// Get the histogram classes.
		$histogram_classes = $this->get_histogram_classes(
			$profile_field,
			$profile_field_options
		);

		// Get the current year.
		$current_year = date( 'Y' );

		foreach ( $histogram_classes as $histogram_class ) {
			$selector = NULL;
			if ( array_key_exists( 'no_value', $histogram_class['filter'] ) &&
			     $histogram_class['filter']['no_value']
			) {
				$selector = new BWG_Stats_Strata_Value_Selector(
					$profile_field->db_submissions_column_name(), '-1'
				);
			} else {
				if (
					array_key_exists( 'min_age', $histogram_class['filter'] ) &&
					array_key_exists( 'max_age', $histogram_class['filter'] )
				) {
					$selector = new BWG_Stats_Strata_Range_Selector(
						$profile_field->db_submissions_column_name(),
						$current_year - $histogram_class['filter']['max_age'] + 1,
						$current_year - $histogram_class['filter']['min_age'] - 1
					);
				} elseif ( array_key_exists( 'max_age', $histogram_class['filter'] ) ) {
					$selector = new BWG_Stats_Strata_Range_Selector(
						$profile_field->db_submissions_column_name(),
						$current_year - $histogram_class['filter']['max_age'] + 1,
						NULL
					);
				} elseif ( array_key_exists( 'min_age', $histogram_class['filter'] ) ) {
					$selector = new BWG_Stats_Strata_Range_Selector(
						$profile_field->db_submissions_column_name(),
						NULL,
						$current_year - $histogram_class['filter']['min_age'] - 1
					);
				}
			}

			if ( is_null( $selector ) ) {
				continue;
			}

			$strata_collection->build_and_add_strata(
				$histogram_class['label'],
				[ $selector ]
			);
		}
	}

}
