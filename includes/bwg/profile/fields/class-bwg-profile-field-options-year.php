<?php

namespace bwg\profile\fields;

/**
 * Class BWG_Profile_Field_Options_Year.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field_Options_Year extends BWG_Profile_Field_Options {


	/**
	 * @return int
	 */
	public function get_year_start() {
		return intval( isset( $this->options['year_start'] ) ? $this->options['year_start'] : date( 'Y' ) - 100 );
	}

	/**
	 * @return int
	 */
	public function get_year_end() {
		return intval( isset( $this->options['year_end'] ) ? $this->options['year_end'] : date( 'Y' ) );
	}

}