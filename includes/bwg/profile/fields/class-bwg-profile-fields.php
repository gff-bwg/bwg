<?php

namespace bwg\profile\fields;

use bwg\BWG_Utils;
use bwg\database\models\BWG_User_Storage;

/**
 * Class BWG_Profile_Fields.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Fields {

	/**
	 * @var string
	 */
	private $json;

	/**
	 * @var bool
	 */
	private $decoded = FALSE;

	/**
	 * @var string
	 */
	private $introduction;

	/**
	 * @var \bwg\profile\fields\BWG_Profile_Field[]
	 */
	private $fields = [];


	/**
	 * BWG_Profile_Fields constructor.
	 *
	 * @param string $json
	 */
	public function __construct( $json ) {
		$this->json = $json;
	}

	/**
	 * Gets the json value.
	 *
	 * @return string
	 */
	public function get_json() {
		return $this->json;
	}

	/**
	 * Decodes the json string.
	 */
	private function decode() {
		if ( $this->decoded ) {
			// Do not decode again.
			return;
		}

		$data = json_decode( $this->json, TRUE, 512, JSON_UNESCAPED_UNICODE );
		if ( isset( $data['introduction'] ) ) {
			$this->introduction = $data['introduction'];
		} else {
			$this->introduction = '';
		}

		if ( isset( $data['fields'] ) ) {
			foreach ( $data['fields'] as $field_data ) {
				if ( ! isset( $field_data['type'] ) || ! isset( $field_data['uid'] ) ) {
					// As 'type' and 'uid' are required, we will skip the field if they are not defined.
					continue;
				}

				array_push( $this->fields, new BWG_Profile_Field(
					new BWG_Profile_Field_Settings(
						$field_data['type'], $field_data['uid'],
						isset( $field_data['label'] ) ? $field_data['label'] : '',
						isset( $field_data['required'] ) ? $field_data['required'] : FALSE,
						isset( $field_data['options'] ) ? $field_data['options'] : []
					)
				) );
			}
		}

		$this->decoded = TRUE;
	}

	/**
	 * Gets the introduction text.
	 *
	 * @return string
	 */
	public function get_introduction() {
		if ( ! $this->decoded ) {
			$this->decode();
		}

		return $this->introduction;
	}

	/**
	 * Gets the fields.
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field[]
	 */
	public function get_fields() {
		if ( ! $this->decoded ) {
			$this->decode();
		}

		return $this->fields;
	}

	/**
	 * Gets the field given an uid.
	 *
	 * @param string $uid
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field|null The field or NULL if not found.
	 */
	public function get_field_by_uid( $uid ) {
		$fields = $this->get_fields();
		foreach ( $fields as $field ) {
			if ( $uid === $field->get_uid() ) {
				return $field;
			}
		}

		return NULL;
	}

	/**
	 * Gets the fields rendered to be used in the meta box.
	 *
	 * @param \bwg\BWG_Utils $bwg_utils
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 *
	 * @return string[]
	 */
	public function render_meta_box_fields(
		BWG_Utils $bwg_utils,
		BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	) {
		$rendered_meta_box_fields = [];
		foreach ( $this->get_fields() as $profile_field ) {
			array_push(
				$rendered_meta_box_fields,
				$profile_field->render_meta_box( $bwg_utils, $bwg_profile_field_type_registry )
			);
		}

		return $rendered_meta_box_fields;
	}

	/**
	 * Renders for each field the view used by the evaluation form.
	 *
	 * @param \bwg\BWG_Utils $bwg_utils
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry
	 * @param \bwg\database\models\BWG_User_Storage $user_storage
	 *
	 * @return string[]
	 */
	public function render_evaluation_form_fields(
		BWG_Utils $bwg_utils,
		BWG_Profile_Field_Type_Registry $bwg_profile_field_type_registry,
		BWG_User_Storage $user_storage
	) {
		$user_storage_meta = $user_storage->get_meta();

		$rendered_evaluation_form_fields = [];
		foreach ( $this->get_fields() as $profile_field ) {
			$profile_field_value = NULL;
			if ( isset( $user_storage_meta[ $profile_field->get_uid() ] ) ) {
				$profile_field_value = $user_storage_meta[ $profile_field->get_uid() ];
			}

			array_push(
				$rendered_evaluation_form_fields,
				$profile_field->render_evaluation_form(
					$bwg_utils,
					$bwg_profile_field_type_registry,
					$profile_field_value
				)
			);
		}

		return $rendered_evaluation_form_fields;
	}

}