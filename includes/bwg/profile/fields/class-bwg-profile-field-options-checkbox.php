<?php

namespace bwg\profile\fields;

/**
 * Class BWG_Profile_Field_Options_Checkbox.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field_Options_Checkbox extends BWG_Profile_Field_Options {

	/**
	 * Gets the checkboxes.
	 *
	 * @return array|mixed
	 */
	public function get_checkboxes() {
		if ( ! isset( $this->options['checkboxes'] ) ) {
			return [];
		}

		return $this->options['checkboxes'];
	}
}
