<?php

namespace bwg\profile\fields;

/**
 * Class BWG_Profile_Field_Settings.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field_Settings {

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string
	 */
	private $uid;

	/**
	 * @var string
	 */
	private $label;

	/**
	 * @var boolean
	 */
	private $required;

	/**
	 * @var array
	 */
	private $options;


	/**
	 * BWG_Profile_Field_Settings constructor.
	 *
	 * @param string $type
	 * @param string $uid
	 * @param string $label
	 * @param bool $required
	 * @param array $options
	 */
	public function __construct( $type, $uid, $label = '', $required = TRUE, $options = [] ) {
		$this->type     = $type;
		$this->uid      = $uid;
		$this->label    = $label;
		$this->required = $required;
		$this->options  = $options;
	}

	/**
	 * Gets the type (actually the type name).
	 *
	 * @return string
	 */
	public function get_type() {
		return $this->type;
	}

	/**
	 * Gets the uid.
	 *
	 * @return string
	 */
	public function get_uid() {
		return $this->uid;
	}

	/**
	 * Gets the label.
	 *
	 * @return string
	 */
	public function get_label() {
		return $this->label;
	}

	/**
	 * Is this field required.
	 *
	 * @return bool
	 */
	public function is_required() {
		return $this->required;
	}

	/**
	 * Gets the options which are field type specific.
	 *
	 * @return array
	 */
	public function get_options() {
		return $this->options;
	}

}