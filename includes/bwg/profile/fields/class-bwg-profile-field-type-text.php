<?php

namespace bwg\profile\fields;

use bwg\database\etc\BWG_Database_Submission_Column_Info;
use bwg\evaluation\BWG_Evaluation_Post;
use bwg\stats\strata\BWG_Stats_Strata_Collection;
use Exception;

/**
 * Class BWG_Profile_Field_Type_Text.
 *
 * @package bwg\profile\fields
 */
final class BWG_Profile_Field_Type_Text extends BWG_Profile_Field_Type {


	/**
	 * @inheritdoc
	 */
	public function is_strata_supported() {
		return FALSE;
	}

	/**
	 * @inheritdoc
	 */
	public function is_strata_stats_supported() {
		return FALSE;
	}

	/**
	 * @inheritdoc
	 */
	public function is_stats_enabled( BWG_Profile_Field $profile_field ) {
		$options = $this->field_options( $profile_field->get_options() );

		return $options->is_stats_enabled();
	}

	/**
	 * @inheritdoc
	 */
	public function field_options( $options = [] ) {
		return new BWG_Profile_Field_Options_Text( $options );
	}

	/**
	 * @inheritdoc
	 */
	public function db_submission_column_info( $column_name, BWG_Profile_Field_Options $options ) {
		if ( ! ( $options instanceof BWG_Profile_Field_Options_Text ) ) {
			throw new Exception( 'Field options exception.' );
		}

		return new BWG_Database_Submission_Column_Info(
			$column_name,
			'TEXT NOT NULL DEFAULT \'\'',
			'%s',
			FALSE
		);
	}

	/**
	 * @inheritdoc
	 */
	public function fill_strata_collection(
		BWG_Stats_Strata_Collection $strata_collection,
		BWG_Profile_Field $profile_field,
		BWG_Profile_Field_Options $profile_field_options
	) {
		// No Strata possible for this profile field type.
	}

	/**
	 * @inheritdoc
	 */
	protected function render_stats_internal(
		BWG_Profile_Field $field,
		BWG_Profile_Field_Options $options,
		BWG_Evaluation_Post $evaluation_post
	) {
		/** @var \bwg\profile\fields\BWG_Profile_Field_Options_Checkbox $options */
		global $wpdb;

		$ra = [
			'values' => [],
		];

		// Get the values from the database for this specific profile field.
		$table_name  = $this->bwg_base
			->submissions_database_helper()
			->get_submissions_table_name( $evaluation_post->get_id() );
		$column_name = $field->db_submissions_column_name();

		$sql     = 'SELECT COUNT(*) AS amount, '
		           . $column_name . ' AS value FROM '
		           . $table_name . ' GROUP BY '
		           . $column_name . ' ORDER BY amount DESC, value ASC';
		$records = $wpdb->get_results( $sql );
		foreach ( $records as $record ) {
			array_push(
				$ra['values'], [
				'value'  => $record->value,
				'amount' => $record->amount,
			] );
		}

		return $this->bwg_base
			->utils()
			->render_admin_template( 'evaluation/profile-fields/stats-box.text.php', $ra );
	}
}
