<?php

namespace bwg\profile\fields;

use bwg\BWG_Base;

/**
 * Class BWG_Profile_Field_Type_Registry.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field_Type_Registry {

	/**
	 * @var \bwg\BWG_Base
	 */
	private $bwg_base;

	/**
	 * @var \bwg\profile\fields\BWG_Profile_Field_Type[]
	 */
	private $registry = [];


	/**
	 * BWG_Profile_Field_Type_Registry constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->bwg_base = $bwg_base;

		// Initialize the registry.
		$this->register( new BWG_Profile_Field_Type_Text( $this->bwg_base, 'text', 'Freitext' ) );
		$this->register( new BWG_Profile_Field_Type_Choice( $this->bwg_base, 'choice', 'Einzel-Auswahl' ) );
		$this->register( new BWG_Profile_Field_Type_Checkbox( $this->bwg_base, 'checkbox', 'Multi-Auswahl' ) );
		$this->register( new BWG_Profile_Field_Type_Year( $this->bwg_base, 'year', 'Jahrgang' ) );
	}

	/**
	 * Registers a new profile field type.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field_Type $type
	 */
	public function register( BWG_Profile_Field_Type $type ) {
		$this->registry[ $type->get_name() ] = $type;
	}

	/**
	 * Gets a registered profile field type.
	 *
	 * @param string $name
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field_Type|null
	 */
	public function get_type( $name ) {
		if ( ! isset( $this->registry[ $name ] ) ) {
			return NULL;
		}

		return $this->registry[ $name ];
	}

	/**
	 * Gets a list of all registered field types. The key would be the name, the value would be the label of a type.
	 *
	 * @return array
	 */
	public function get_type_list() {
		$type_list = [];
		foreach ( $this->registry as $type ) {
			$type_list[ $type->get_name() ] = $type->get_label();
		}

		return $type_list;
	}

}