<?php

namespace bwg\profile\fields;

/**
 * Class BWG_Profile_Field_Options.
 *
 * @package bwg\profile\fields
 */
abstract class BWG_Profile_Field_Options {

	/**
	 * @var array
	 */
	protected $options;
	

	/**
	 * BWG_Profile_Field_Options constructor.
	 *
	 * @param array $options
	 */
	public function __construct( $options = [] ) {
		$this->options = $options;
	}

}