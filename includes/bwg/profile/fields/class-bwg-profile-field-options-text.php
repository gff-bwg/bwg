<?php

namespace bwg\profile\fields;

/**
 * Class BWG_Profile_Field_Options_Text.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field_Options_Text extends BWG_Profile_Field_Options {

	/**
	 * Mode: Input "Text"
	 */
	const MODE_INPUT_TEXT = 'input_text';

	/**
	 * Mode: Input "Number"
	 */
	const MODE_INPUT_NUMBER = 'input_number';

	/**
	 * Mode: Input "Email"
	 */
	const MODE_INPUT_EMAIL = 'input_email';

	/**
	 * Mode: Input "Tel"
	 */
	const MODE_INPUT_TEL = 'input_tel';

	/**
	 * Mode: Textarea
	 */
	const MODE_TEXTAREA = 'textarea';


	/**
	 * Gets the mode for this text field.
	 *
	 * @return string
	 */
	public function get_mode() {
		return isset( $this->options['mode'] ) ? $this->options['mode'] : self::MODE_INPUT_TEXT;
	}

	/**
	 * Checks whether the field is of the given mode.
	 *
	 * @param string $mode
	 *
	 * @return bool
	 */
	public function is_mode( $mode ) {
		return $mode === $this->get_mode();
	}

	/**
	 * Gets the maximum length.
	 *
	 * @return int
	 */
	public function get_max_length() {
		return isset( $this->options['max_length'] ) ? intval( $this->options['max_length'] ) : 200;
	}

	/**
	 * @return bool
	 */
	public function is_stats_enabled() {
		return array_key_exists( 'stats_enabled', $this->options ) ? $this->options['stats_enabled'] : FALSE;
	}

	/**
	 * @param bool $stats_enabled
	 */
	public function set_stats_enabled( $stats_enabled ) {
		$this->options['stats_enabled'] = $stats_enabled;
	}

}
