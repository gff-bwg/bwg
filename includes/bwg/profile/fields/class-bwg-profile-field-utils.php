<?php

namespace bwg\profile\fields;

use bwg\BWG_Base;

/**
 * Class BWG_Profile_Field_Utils.
 *
 * @package bwg\profile\fields
 *
 * @deprecated
 */
class BWG_Profile_Field_Utils {

	/**
	 * @var \bwg\BWG_Base
	 */
	private $bwg_base;


	/**
	 * BWG_Profile_Field_Utils constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->bwg_base = $bwg_base;
	}

}