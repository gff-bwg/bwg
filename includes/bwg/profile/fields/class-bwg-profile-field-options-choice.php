<?php

namespace bwg\profile\fields;

/**
 * Class BWG_Profile_Field_Options_Choice.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field_Options_Choice extends BWG_Profile_Field_Options {

	/**
	 * Mode: Input "Radio"
	 */
	const MODE_INPUT_RADIO = 'input_radio';

	/**
	 * Mode: Input "Radio" floated
	 */
	const MODE_INPUT_RADIO_FLOAT = 'input_radio_floated';

	/**
	 * Mode: Select
	 */
	const MODE_SELECT = 'select';

	/**
	 * Gets the mode for this text field.
	 *
	 * @return string
	 */
	public function get_mode() {
		return isset( $this->options['mode'] ) ? $this->options['mode'] : self::MODE_INPUT_RADIO;
	}

	/**
	 * Checks whether the field is of the given mode.
	 *
	 * @param string $mode
	 *
	 * @return bool
	 */
	public function is_mode( $mode ) {
		return $mode === $this->get_mode();
	}

	/**
	 * Gets the choices.
	 *
	 * @return array
	 */
	public function get_choices() {
		if ( ! isset( $this->options['choices'] ) ) {
			return [];
		}

		return $this->options['choices'];
	}

	/**
	 * Gets the key of the default choice.
	 *
	 * @return string
	 */
	public function get_default_choice() {
		$choice_keys = array_keys( $this->get_choices() );
		if (
			! array_key_exists( 'default_choice', $this->options ) ||
			! in_array( $this->options['default_choice'], $choice_keys )
		) {
			// Either there is no default choice defined or the defined one does not exist.
			if ( empty( $choice_keys ) ) {
				return '';
			}

			return $choice_keys[0];
		}

		return $this->options['default_choice'];
	}
}