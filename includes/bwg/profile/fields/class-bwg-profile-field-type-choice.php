<?php

namespace bwg\profile\fields;

use bwg\database\etc\BWG_Database_Submission_Column_Info;
use bwg\evaluation\BWG_Evaluation_Post;
use bwg\stats\strata\BWG_Stats_Strata_Collection;
use bwg\stats\strata\selectors\BWG_Stats_Strata_Value_Selector;

/**
 * Class BWG_Profile_Field_Type_Choice.
 *
 * @package bwg\profile\fields
 */
class BWG_Profile_Field_Type_Choice extends BWG_Profile_Field_Type {

	/**
	 * @inheritdoc
	 */
	public function field_options( $options = [] ) {
		return new BWG_Profile_Field_Options_Choice( $options );
	}

	/**
	 * @inheritdoc
	 */
	public function is_strata_supported() {
		return TRUE;
	}

	/**
	 * @inheritdoc
	 */
	public function is_stats_enabled( BWG_Profile_Field $profile_field ) {
		return FALSE;
	}

	/**
	 * @inheritdoc
	 */
	public function is_strata_stats_supported() {
		return TRUE;
	}

	/**
	 * @inheritdoc
	 */
	public function fill_strata_collection(
		BWG_Stats_Strata_Collection $strata_collection,
		BWG_Profile_Field $profile_field,
		BWG_Profile_Field_Options $profile_field_options
	) {
		/** @var \bwg\profile\fields\BWG_Profile_Field_Options_Choice $profile_field_options */
		foreach ( $profile_field_options->get_choices() as $choice_key => $choice_label ) {
			$strata_collection->build_and_add_strata(
				$choice_label,
				[
					new BWG_Stats_Strata_Value_Selector(
						$profile_field->db_submissions_column_name(), $choice_key
					)
				]
			);
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function render_evaluation_form_info_internal(
		$uid,
		$label,
		$required,
		BWG_Profile_Field_Options $options
	) {
		/** @var \bwg\profile\fields\BWG_Profile_Field_Options_Choice $options */
		if ( $options->is_mode( BWG_Profile_Field_Options_Choice::MODE_SELECT ) ) {
			return parent::render_evaluation_form_info_internal( $uid, $label, $required, $options );
		}

		return '<label>' . esc_html( $label ) . $this->render_required( $required ) . '</label>';
	}

	/**
	 * @inheritdoc
	 *
	 * @var \wpdb $wpdb
	 */
	protected function render_strata_stats_internal(
		BWG_Profile_Field $field,
		BWG_Profile_Field_Options $options,
		BWG_Evaluation_Post $evaluation_post
	) {
		/** @var \bwg\profile\fields\BWG_Profile_Field_Options_Choice $options */
		global $wpdb;

		$table_name  = $this->bwg_base
			->submissions_database_helper()
			->get_submissions_table_name( $evaluation_post->get_id() );
		$column_name = $field->db_submissions_column_name();

		$results = [];
		$sql     = 'SELECT COUNT(*) AS amount, '
		           . $column_name . ' AS value FROM '
		           . $table_name . ' GROUP BY '
		           . $column_name;
		$records = $wpdb->get_results( $sql );
		foreach ( $records as $record ) {
			$results[ $record->value ] = $record->amount;
		}

		$ra = [
			'choices' => [],
		];

		$total = $this->bwg_base->submissions_database_helper()->get_total_nr_submissions( $evaluation_post->get_id() );

		foreach ( $options->get_choices() as $choice_uid => $choice_label ) {
			$amount = array_key_exists( $choice_uid, $results ) ? $results[ $choice_uid ] : 0;
			array_push( $ra['choices'], [
				'uid'               => $choice_uid,
				'label'             => $choice_label,
				'amount'            => $amount,
				'amount_percentage' => 100 * $this->bwg_base->utils()->safe_division( $amount, $total ),
			] );
		}

		return $this->bwg_base
			->utils()
			->render_admin_template( 'evaluation/profile-fields/strata-stats-box.choice.php', $ra );
	}

	/**
	 * @inheritdoc
	 */
	public function db_submission_column_info( $column_name, BWG_Profile_Field_Options $options ) {
		return new BWG_Database_Submission_Column_Info(
			$column_name,
			'VARCHAR(12) DEFAULT NULL',
			'%s',
			TRUE
		);
	}
}
