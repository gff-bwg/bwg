<?php

namespace bwg\profile\fields;

use bwg\BWG_Base;
use bwg\evaluation\BWG_Evaluation_Post;
use bwg\stats\strata\BWG_Stats_Strata_Collection;

/**
 * Class BWG_Profile_Field_Type.
 *
 * @package bwg\profile\fields
 */
abstract class BWG_Profile_Field_Type {

	/**
	 * @var \bwg\BWG_Base
	 */
	protected $bwg_base;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $label;


	/**
	 * BWG_Profile_Field_Type constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 * @param string $name The name of this field type. The name has to be unique for all field types.
	 * @param string $label The label of this field type.
	 */
	public function __construct( BWG_Base $bwg_base, $name, $label ) {
		$this->bwg_base = $bwg_base;
		$this->name     = $name;
		$this->label    = $label;
	}

	/**
	 * @return string
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function get_label() {
		return $this->label;
	}

	/**
	 * Renders the "required" label indicator.
	 *
	 * @param bool $required
	 *
	 * @return string
	 */
	protected function render_required( $required = FALSE ) {
		if ( ! $required ) {
			return '';
		}

		return ' <abbr class="bwg-required" title="' . esc_attr__( 'Pflichtfeld', 'bwg' ) . '">*</abbr>';
	}

	/**
	 * Renders the meta box. Please do not override this method, instead use render_meta_box_internal().
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $field
	 *
	 * @return string
	 */
	public function render_meta_box( BWG_Profile_Field $field ) {
		$field_options = $field->get_field_options( $this->bwg_base->profile_field_type_registry() );
		if ( is_null( $field_options ) ) {
			return '<p>Error: Field options could not be loaded.</p>';
		}

		return $this->render_meta_box_internal(
			$field->get_uid(),
			$field->get_label(),
			$field_options
		);
	}

	/**
	 * Internal method that renders the meta box. Please override this method if needed.
	 *
	 * @param string $uid
	 * @param string $label
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $field_options
	 *
	 * @return string
	 */
	protected function render_meta_box_internal( $uid, $label, BWG_Profile_Field_Options $field_options ) {
		$ra = [
			'uid'        => $uid,
			'label'      => $label,
			'options'    => $field_options,
			'field_type' => $this,
		];

		return $this->bwg_base->utils()->render_admin_template(
			'evaluation/profile-fields/meta-box.' . $this->get_name() . '.php',
			$ra
		);
	}

	/**
	 * Renders the info, usually the label, used by the evaluation form for the given profile field. Please do not
	 * override this method, instead use render_evaluation_form_info_internal().
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $field
	 *
	 * @return string
	 */
	public function render_evaluation_form_info( BWG_Profile_Field $field ) {
		$field_options = $field->get_field_options( $this->bwg_base->profile_field_type_registry() );
		if ( is_null( $field_options ) ) {
			return '';
		}

		return $this->render_evaluation_form_info_internal(
			$field->get_uid(), $field->get_label(), $field->is_required(), $field_options
		);
	}

	/**
	 * Internal method that renders the info, usually the label, used by the evaluation form for the given profile
	 * field. Please override this method if needed.
	 *
	 * @param string $uid
	 * @param string $label
	 * @param boolean $required
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $options
	 *
	 * @return string
	 */
	protected function render_evaluation_form_info_internal(
		$uid,
		$label,
		$required,
		BWG_Profile_Field_Options $options
	) {
		return '<label for="epf-' . esc_attr( $uid ) . '">' . esc_html( $label ) . $this->render_required( $required ) . '</label>';
	}

	/**
	 * Renders the control, e.g. the input field or the select or the radio buttons, used by the evaluation form for
	 * the given profile field. Please do not override this method, instead use
	 * render_evaluation_form_control_internal().
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $field
	 * @param string|array|null $field_value
	 *
	 * @return string
	 */
	public function render_evaluation_form_control( BWG_Profile_Field $field, $field_value = NULL ) {
		$field_options = $field->get_field_options( $this->bwg_base->profile_field_type_registry() );
		if ( is_null( $field_options ) ) {
			return '<p>Error: Field options could not be loaded.</p>';
		}

		return $this->render_evaluation_form_control_internal(
			$field->get_uid(), $field->get_label(), $field->is_required(), $field_options, $field_value
		);
	}

	/**
	 * Internal method that renders the control, e.g. the input field or the select or the radio buttons, used by the
	 * evaluation form for the given profile field. Please override this method if needed.
	 *
	 * @param string $uid
	 * @param string $label
	 * @param boolean $required
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $field_options
	 * @param string|array|null $field_value
	 *
	 * @return string
	 */
	protected function render_evaluation_form_control_internal(
		$uid,
		$label,
		$required,
		BWG_Profile_Field_Options $field_options,
		$field_value = NULL
	) {
		$ra = [
			'uid'         => $uid,
			'label'       => $label,
			'required'    => $required,
			'options'     => $field_options,
			'field_type'  => $this,
			'field_value' => $field_value,
		];

		return $this->bwg_base->utils()->render_template(
			'evaluation-form/profile-field.' . $this->get_name() . '.php',
			$ra
		);
	}

	/**
	 * Renders the strata filter box.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $field The profile field.
	 *
	 * @return string
	 */
	public function render_strata_filter_box( BWG_Profile_Field $field ) {
		if ( ! $this->is_strata_supported() ) {
			return '';
		}

		$field_options = $field->get_field_options( $this->bwg_base->profile_field_type_registry() );
		if ( is_null( $field_options ) ) {
			return '<p>Error: Field options could not be loaded.</p>';
		}

		return $this->render_strata_filter_box_internal(
			$field->get_uid(),
			$field->get_label(),
			$field_options
		);
	}

	/**
	 * Internal method that renders the strata filter box. Please override this method if needed.
	 *
	 * @param string $uid The unique id.
	 * @param string $label The label.
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $field_options The field options.
	 *
	 * @return string
	 */
	protected function render_strata_filter_box_internal( $uid, $label, BWG_Profile_Field_Options $field_options ) {
		return $this->bwg_base->utils()->render_admin_template(
			'evaluation/profile-fields/strata-filter-box.' . $this->get_name() . '.php', [
			'uid'           => $uid,
			'label'         => $label,
			'field_options' => $field_options,
			'field_type'    => $this,
		] );
	}

	/**
	 * Renders the strata stats box.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $field
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
	 *
	 * @return string
	 */
	public function render_strata_stats( BWG_Profile_Field $field, BWG_Evaluation_Post $evaluation_post ) {
		$field_options = $field->get_field_options( $this->bwg_base->profile_field_type_registry() );
		if ( is_null( $field_options ) ) {
			return '<p>Error: Field options could not be loaded.</p>';
		}

		return $this->render_strata_stats_internal(
			$field, $field_options, $evaluation_post
		);
	}

	/**
	 * Internal method that renders the strata stats box. Please override this method if needed.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $field
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $options
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
	 *
	 * @return string
	 */
	protected function render_strata_stats_internal(
		BWG_Profile_Field $field,
		BWG_Profile_Field_Options $options,
		BWG_Evaluation_Post $evaluation_post
	) {
		return '';
	}

	/**
	 * Renders the stats box.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $field
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
	 *
	 * @return string
	 */
	public function render_stats( BWG_Profile_Field $field, BWG_Evaluation_Post $evaluation_post ) {
		$field_options = $field->get_field_options( $this->bwg_base->profile_field_type_registry() );
		if ( is_null( $field_options ) ) {
			return '<p>Error: Field options could not be loaded.</p>';
		}

		return $this->render_stats_internal(
			$field, $field_options, $evaluation_post
		);
	}

	/**
	 * Internal method that renders the stats box. Please override this method if needed.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $field
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $options
	 * @param \bwg\evaluation\BWG_Evaluation_Post $evaluation_post
	 *
	 * @return string
	 */
	protected function render_stats_internal(
		BWG_Profile_Field $field,
		BWG_Profile_Field_Options $options,
		BWG_Evaluation_Post $evaluation_post
	) {
		return '';
	}

	/**
	 * Validates the given value.
	 *
	 * @param \bwg\profile\fields\BWG_Profile_Field $profile_field
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $options
	 * @param string $value
	 *
	 * @return string[]
	 */
	public function validate( BWG_Profile_Field $profile_field, BWG_Profile_Field_Options $options, $value ) {
		if ( ! $profile_field->is_required() ) {
			return [];
		}

		if ( ! empty( $value ) ) {
			return [];
		}

		return [
			sprintf( __( 'Das Bewerterfeld «%s» ist erforderlich.', 'bwg' ), $profile_field->get_label() ),
		];
	}

	/**
	 * Does this field type support strata? If strata is supported, then a profile field of this field type can be
	 * used to filter the submissions.
	 *
	 * @return bool
	 */
	abstract public function is_strata_supported();

	/**
	 * Does this field type support strata stats? Strata stats means, that this profile field would be shown in the
	 * full stats of an evaluation. If strata is supported, then usually strata stats is also supported. But there might
	 * be cases, where strata is not supported, but strata stats is (for example "checkbox" field types).
	 *
	 * @return bool
	 */
	abstract public function is_strata_stats_supported();

	/**
	 * @param \bwg\profile\fields\BWG_Profile_Field $profile_field
	 *
	 * @return bool
	 */
	abstract public function is_stats_enabled( BWG_Profile_Field $profile_field );

	/**
	 * Fills the strata collection with all possible stratas for the given field.
	 *
	 * @param \bwg\stats\strata\BWG_Stats_Strata_Collection $strata_collection
	 * @param \bwg\profile\fields\BWG_Profile_Field $profile_field
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $profile_field_options
	 */
	abstract public function fill_strata_collection(
		BWG_Stats_Strata_Collection $strata_collection,
		BWG_Profile_Field $profile_field,
		BWG_Profile_Field_Options $profile_field_options
	);

	/**
	 * Converts the given array to a field options object specific to the current type.
	 *
	 * @param array $options
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field_Options
	 */
	abstract public function field_options( $options = [] );

	/**
	 * Gets the database column info for the evaluation submission table for the current profile field type.
	 *
	 * @param string $column_name The column name.
	 * @param \bwg\profile\fields\BWG_Profile_Field_Options $options The field options.
	 *
	 * @return \bwg\database\etc\BWG_Database_Submission_Column_Info
	 */
	abstract public function db_submission_column_info( $column_name, BWG_Profile_Field_Options $options );

}
