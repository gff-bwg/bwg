<?php

declare( strict_types=1 );

namespace bwg\ui;

class BWG_UI_Accordion {

	protected ?string $id;

	/**
	 * @var array<string, BWG_UI_Accordion_Panel>
	 */
	protected array $panels = [];

	/**
	 * @param string|null $id
	 */
	public function __construct( ?string $id = NULL ) {
		$this->id = $id;
	}

	public function render(): string {
		$rendered = '<div' . ( is_null( $this->id ) ? '' : ' id="' . esc_attr( $this->id ) . '"' ) . ' class="bwg-accordion">';
		foreach ( $this->panels as $key => $panel ) {
			$rendered .= $panel->render( $key );
		}
		$rendered .= '</div>';

		return $rendered;
	}

	public function add_panel( string $key, BWG_UI_Accordion_Panel $accordion_panel ): void {
		$this->panels[ $key ] = $accordion_panel;
	}
}