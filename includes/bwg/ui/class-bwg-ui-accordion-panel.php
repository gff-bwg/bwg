<?php

declare( strict_types=1 );

namespace bwg\ui;

class BWG_UI_Accordion_Panel {

	protected BWG_UI_Accordion_Heading $heading;

	protected string $contents;

	public function __construct( BWG_UI_Accordion_Heading $heading, string $contents ) {
		$this->heading  = $heading;
		$this->contents = $contents;
	}

	public function render( string $key ): string {
		return $this->heading->render( $key ) .
		       '<div id="' . esc_attr( $key ) . '" class="bwg-accordion-panel" hidden="hidden">' . $this->contents . '</div>';
	}
}