<?php

declare( strict_types=1 );

namespace bwg\ui;

class BWG_UI_Accordion_Heading {

	protected string $title;

	protected ?BWG_UI_Accordion_Heading_Badge $badge;

	public function __construct( string $title, ?BWG_UI_Accordion_Heading_Badge $badge = NULL ) {
		$this->title = $title;
		$this->badge = $badge;
	}

	public function render( string $key ): string {
		return '<h3 class="bwg-accordion-heading">
            <button aria-expanded="false" class="bwg-accordion-trigger"
                    aria-controls="' . esc_attr( $key ) . '" type="button">
				<span class="title">' . esc_html( $this->title ) . '</span>' .
		       ( is_null( $this->badge ) ? '' : $this->badge->render() ) .
		       '<span class="icon"></span>
            </button>
        </h3>';
	}
}