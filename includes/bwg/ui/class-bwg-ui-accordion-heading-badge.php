<?php

declare( strict_types=1 );

namespace bwg\ui;

class BWG_UI_Accordion_Heading_Badge {

	const COLOR_BLUE = 'blue';

	protected string $label;

	protected string $color;

	/**
	 * @param string $label
	 * @param string $color
	 */
	public function __construct( string $label, string $color ) {
		$this->label = $label;
		$this->color = $color;
	}

	public function render(): string {
		return '<span class="badge ' . esc_attr( $this->color ) . '">' . esc_html( $this->label ) . '</span>';
	}
}