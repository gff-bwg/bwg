<?php

namespace bwg\options;

use bwg\BWG_Base;
use WP_Post;

/**
 * Class BWG_Options
 */
class BWG_Options {

	/**
	 * Administration options page key.
	 */
	const ADMIN_PAGE_KEY = 'bwg';

	/**
	 * Administration prodomo options page key.
	 */
	const ADMIN_PRODOMO_PAGE_KEY = 'bwg_prodomo';

	/**
	 * Option name.
	 */
	const OPTION_NAME = 'bwg_options';

	/**
	 * Prodomo option name.
	 */
	const PRODOMO_OPTION_NAME = 'bwg_prodomo_options';

	/**
	 * The various options section.
	 */
	const SECTION_VARIOUS = 'bwg_options_section_various';

	/**
	 * The main section.
	 */
	const SECTION_PAGES = 'bwg_options_section_pages';

	/**
	 * The texts section.
	 */
	const SECTION_TEXTS = 'bwg_options_section_texts';

	/**
	 * The prodomo section.
	 */
	const SECTION_PRODOMO = 'bwg_options_section_prodomo';

	/**
	 * Field: Footer Text.
	 */
	const FIELD_FOOTER_TEXT = 'footer_text';

	/**
	 * Field: Registration Introduction Text.
	 */
	const FIELD_REGISTRATION_INTRO_TEXT = 'registration_intro_text';

	/**
	 * Field: Registration Email Confirmation Text.
	 */
	const FIELD_REGISTRATION_EMAIL_CONFIRM_TEXT = 'registration_email_confirm_text';

	/**
	 * Field: Spider Chart Info Text.
	 */
	const FIELD_SPIDER_CHART_INFO_TEXT = 'spider_chart_info_text';

	/**
	 * Field: Spider Chart Final Info Text.
	 */
	const FIELD_SPIDER_CHART_FINAL_INFO_TEXT = 'spider_chart_final_info_text';

	/**
	 * Field: Login - Page ID.
	 */
	const FIELD_LOGIN_PAGE_ID = 'login_page_id';

	/**
	 * Field: Register - Page ID.
	 */
	const FIELD_REGISTER_PAGE_ID = 'register_page_id';

	/**
	 * Field: Registration Thank you - Page ID.
	 */
	const FIELD_REGISTRATION_THANK_YOU_PAGE_ID = 'registration_thank_you_page_id';

	/**
	 * Field: Evaluation Thank you - Page ID.
	 */
	const FIELD_EVALUATION_THANK_YOU_PAGE_ID = 'evaluation_thank_you_page_id';

	/**
	 * Field: Overview - Page ID.
	 */
	const FIELD_OVERVIEW_PAGE_ID = 'overview_page_id';

	/**
	 * Field: FEUP Registration Evaluation Field ID.
	 */
	const FIELD_FEUP_REGISTRATION_EVALUATION_FIELD_ID = 'feup_registration_evaluation_field_id';

	/**
	 * Field: FEUP Test Email User ID.
	 */
	const FIELD_FEUP_TEST_EMAIL_USER_ID = 'feup_test_email_user_id';

	/**
	 * Field: BWG Mode.
	 */
	const FIELD_BWG_MODE = 'bwg_mode';

	/**
	 * Field: Prodomo Question 1.
	 */
	const FIELD_PRODOMO_QUESTION_1 = 'prodomo_question_1';

	/**
	 * Field: Prodomo Question 2.
	 */
	const FIELD_PRODOMO_QUESTION_2 = 'prodomo_question_2';

	/**
	 * Field: Prodomo Question 3.
	 */
	const FIELD_PRODOMO_QUESTION_3 = 'prodomo_question_3';

	/**
	 * Field: Prodomo Freetext 1.
	 */
	const FIELD_PRODOMO_FREETEXT_1 = 'prodomo_freetext_1';

	/**
	 * Field: Prodomo Thank you - Page ID.
	 */
	const FIELD_PRODOMO_THANK_YOU_PAGE_ID = 'prodomo_thank_you_page_id';


	/**
	 * @var BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Options constructor.
	 *
	 * @param BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;

		add_action( 'admin_init', [ $this, 'admin_init' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
	}


	/**
	 * Hook 'admin_init'.
	 */
	public function admin_init() {
		register_setting(
			self::ADMIN_PAGE_KEY,
			self::OPTION_NAME,
			[
				'sanitize_callback' => [ $this, 'validate_inputs' ]
			]
		);


		///
		// Section: Texts.
		///
		add_settings_section(
			self::SECTION_TEXTS,
			'Texte',
			function () {
				echo $this->_bwg_base->utils()->render_admin_template( 'options/section-texts.php' );
			},
			self::ADMIN_PAGE_KEY
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_FOOTER_TEXT ),
			__( 'Footer Subtext', 'bwg' ),
			[ $this, 'setting_input_text_dn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_TEXTS,
			[
				'field_name'  => self::FIELD_FOOTER_TEXT,
				'description' => 'Subtext, welcher in PDFs und einigen E-Mails als Footer verwendet wird.',
				'label_for'   => $this->get_field_id( self::FIELD_FOOTER_TEXT ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_REGISTRATION_INTRO_TEXT ),
			'Registrierung Intro-Text',
			[ $this, 'setting_tinymce_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_TEXTS,
			[
				'field_name'  => self::FIELD_REGISTRATION_INTRO_TEXT,
				'description' => 'Text, welcher auf der Registrierungsseite beim Registrierungsformular angezeigt wird.',
				'label_for'   => $this->get_field_id( self::FIELD_REGISTRATION_INTRO_TEXT ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_REGISTRATION_EMAIL_CONFIRM_TEXT ),
			'E-Mail Bestätigungstext',
			[ $this, 'setting_tinymce_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_TEXTS,
			[
				'field_name'  => self::FIELD_REGISTRATION_EMAIL_CONFIRM_TEXT,
				'description' => 'Text, welcher auf der Registrierungsseite bei E-Mail-Bestätigung angezeigt wird. Benutzen Sie [login][/login] um den Login-Link zu setzen.',
				'label_for'   => $this->get_field_id( self::FIELD_REGISTRATION_EMAIL_CONFIRM_TEXT ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_SPIDER_CHART_INFO_TEXT ),
			'Netzdiagramm Einleitungstext (Zwischenauswertung)',
			[ $this, 'setting_tinymce_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_TEXTS,
			[
				'field_name'  => self::FIELD_SPIDER_CHART_INFO_TEXT,
				'description' => 'Text, welcher bei den Netzdiagrammen (Zwischenauswertungen) angezeigt wird.',
				'label_for'   => $this->get_field_id( self::FIELD_SPIDER_CHART_INFO_TEXT ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_SPIDER_CHART_FINAL_INFO_TEXT ),
			'Netzdiagramm Einleitungstext (Abschluss)',
			[ $this, 'setting_tinymce_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_TEXTS,
			[
				'field_name'  => self::FIELD_SPIDER_CHART_FINAL_INFO_TEXT,
				'description' => 'Text, welcher bei den Netzdiagrammen (Abschluss) angezeigt wird. Benutzern Sie [others][/others] um einen Bereich zu definieren, welcher nur dann sichtbar ist, wenn die Durchschnitte der anderen Bewertungen angezeigt werden.',
				'label_for'   => $this->get_field_id( self::FIELD_SPIDER_CHART_FINAL_INFO_TEXT ),
			]
		);


		///
		// Section: Special pages and feup stuff.
		///
		add_settings_section(
			self::SECTION_PAGES,
			'Spezialseiten und FEUP Einstellungen',
			function () {
				echo $this->_bwg_base->utils()->render_admin_template( 'options/section-pages.php' );
			},
			self::ADMIN_PAGE_KEY
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_LOGIN_PAGE_ID ),
			'Seite "Login"',
			[ $this, 'setting_page_id_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_PAGES,
			[
				'field_name'  => self::FIELD_LOGIN_PAGE_ID,
				'description' => 'Seite mit dem Shortcode [bwg_login].',
				'label_for'   => $this->get_field_id( self::FIELD_LOGIN_PAGE_ID ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_REGISTER_PAGE_ID ),
			'Seite "Registrierung"',
			[ $this, 'setting_page_id_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_PAGES,
			[
				'field_name'  => self::FIELD_REGISTER_PAGE_ID,
				'description' => 'Seite mit dem Shortcode [bwg_register].',
				'label_for'   => $this->get_field_id( self::FIELD_REGISTER_PAGE_ID ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_REGISTRATION_THANK_YOU_PAGE_ID ),
			'Seite "Vielen Dank für Ihre Registrierung"',
			[ $this, 'setting_page_id_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_PAGES,
			[
				'field_name'  => self::FIELD_REGISTRATION_THANK_YOU_PAGE_ID,
				'description' => 'Seite, welche nach erfolgreichem Registrieren angezeigt werden soll.',
				'label_for'   => $this->get_field_id( self::FIELD_REGISTRATION_THANK_YOU_PAGE_ID ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_EVALUATION_THANK_YOU_PAGE_ID ),
			'Seite "Vielen Dank für Ihre Bewertung"',
			[ $this, 'setting_page_id_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_PAGES,
			[
				'field_name'  => self::FIELD_EVALUATION_THANK_YOU_PAGE_ID,
				'description' => 'Seite, welche nach erfolgreichem Einreichen einer Bewertung angezeigt werden soll.',
				'label_for'   => $this->get_field_id( self::FIELD_EVALUATION_THANK_YOU_PAGE_ID ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_OVERVIEW_PAGE_ID ),
			'Seite "Übersicht"',
			[ $this, 'setting_page_id_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_PAGES,
			[
				'field_name'  => self::FIELD_OVERVIEW_PAGE_ID,
				'description' => 'Seite mit Shortcode [bwg_overview].',
				'label_for'   => $this->get_field_id( self::FIELD_OVERVIEW_PAGE_ID ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_FEUP_REGISTRATION_EVALUATION_FIELD_ID ),
			'FEUP RegEval Feld ID',
			[ $this, 'setting_feup_input_id_dn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_PAGES,
			[
				'field_name'  => self::FIELD_FEUP_REGISTRATION_EVALUATION_FIELD_ID,
				'description' => 'FEUP Feld, welches die Evaluation bei Registrierung speichert.',
				'label_for'   => $this->get_field_id( self::FIELD_FEUP_REGISTRATION_EVALUATION_FIELD_ID ),
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_FEUP_TEST_EMAIL_USER_ID ),
			'FEUP Test E-Mail Benutzer ID',
			[ $this, 'setting_feup_input_id_dn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_PAGES,
			[
				'field_name'  => self::FIELD_FEUP_TEST_EMAIL_USER_ID,
				'description' => 'FEUP Benutzer, welcher für die Feld-Platzhalter in Test E-Mails verwendet werden soll.',
				'label_for'   => $this->get_field_id( self::FIELD_FEUP_TEST_EMAIL_USER_ID ),
			]
		);

		///
		// Section: Various stuff.
		///
		add_settings_section(
			self::SECTION_VARIOUS,
			'Andere BWG Einstellungen',
			function () {
				echo $this->_bwg_base->utils()->render_admin_template( 'options/section-various.php' );
			},
			self::ADMIN_PAGE_KEY
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_BWG_MODE ),
			'BWG Modus',
			[ $this, 'setting_select_fn' ],
			self::ADMIN_PAGE_KEY,
			self::SECTION_VARIOUS,
			[
				'field_name'  => self::FIELD_BWG_MODE,
				'options'     => [
					'default'   => 'Standard',
					'drogothek' => 'Drogothek'
				],
				'description' => 'Stellen Sie hier den Modus für das BWG Plugin ein.',
				'label_for'   => $this->get_field_id( self::FIELD_BWG_MODE ),
			]
		);

		///
		// Prodomo Options.
		///
		register_setting(
			self::ADMIN_PRODOMO_PAGE_KEY,
			self::PRODOMO_OPTION_NAME,
			[
				'sanitize_callback' => [ $this, 'validate_prodomo_inputs' ],
			]
		);

		///
		// Section: Prodomo.
		///
		add_settings_section(
			self::SECTION_PRODOMO,
			'Prodomo',
			function () {
				echo $this->_bwg_base->utils()->render_admin_template( 'options/section-prodomo.php' );
			},
			self::ADMIN_PRODOMO_PAGE_KEY
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_PRODOMO_QUESTION_1 ),
			__( 'Bewertung 1', 'bwg' ),
			[ $this, 'setting_tinymce_fn' ],
			self::ADMIN_PRODOMO_PAGE_KEY,
			self::SECTION_PRODOMO,
			[
				'field_name'    => self::FIELD_PRODOMO_QUESTION_1,
				'option_name'   => self::PRODOMO_OPTION_NAME,
				'label_for'     => $this->get_field_id( self::FIELD_PRODOMO_QUESTION_1, self::PRODOMO_OPTION_NAME ),
				'editor_height' => 100,
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_PRODOMO_QUESTION_2 ),
			__( 'Bewertung 2', 'bwg' ),
			[ $this, 'setting_tinymce_fn' ],
			self::ADMIN_PRODOMO_PAGE_KEY,
			self::SECTION_PRODOMO,
			[
				'field_name'    => self::FIELD_PRODOMO_QUESTION_2,
				'option_name'   => self::PRODOMO_OPTION_NAME,
				'label_for'     => $this->get_field_id( self::FIELD_PRODOMO_QUESTION_2, self::PRODOMO_OPTION_NAME ),
				'editor_height' => 100,
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_PRODOMO_QUESTION_3 ),
			__( 'Bewertung 3', 'bwg' ),
			[ $this, 'setting_tinymce_fn' ],
			self::ADMIN_PRODOMO_PAGE_KEY,
			self::SECTION_PRODOMO,
			[
				'field_name'    => self::FIELD_PRODOMO_QUESTION_3,
				'option_name'   => self::PRODOMO_OPTION_NAME,
				'label_for'     => $this->get_field_id( self::FIELD_PRODOMO_QUESTION_3, self::PRODOMO_OPTION_NAME ),
				'editor_height' => 100,
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_PRODOMO_FREETEXT_1 ),
			__( 'Freitext 1', 'bwg' ),
			[ $this, 'setting_tinymce_fn' ],
			self::ADMIN_PRODOMO_PAGE_KEY,
			self::SECTION_PRODOMO,
			[
				'field_name'    => self::FIELD_PRODOMO_FREETEXT_1,
				'option_name'   => self::PRODOMO_OPTION_NAME,
				'label_for'     => $this->get_field_id( self::FIELD_PRODOMO_FREETEXT_1, self::PRODOMO_OPTION_NAME ),
				'editor_height' => 100,
			]
		);

		add_settings_field(
			$this->get_field_id( self::FIELD_PRODOMO_THANK_YOU_PAGE_ID ),
			'Seite "Prodomo Danke"',
			[ $this, 'setting_page_id_fn' ],
			self::ADMIN_PRODOMO_PAGE_KEY,
			self::SECTION_PRODOMO,
			[
				'field_name'  => self::FIELD_PRODOMO_THANK_YOU_PAGE_ID,
				'option_name' => self::PRODOMO_OPTION_NAME,
				'description' => 'Seite, welche nach erfolgreichem Abschicken der BWG Bewertung (Prodomo) angezeigt werden soll.',
				'label_for'   => $this->get_field_id( self::FIELD_PRODOMO_THANK_YOU_PAGE_ID ),
			]
		);
	}

	/**
	 * Gets the field id for a given field name.
	 *
	 * @param string $name
	 *
	 * @param string $option_name
	 *
	 * @return string
	 */
	public function get_field_id( $name, $option_name = self::OPTION_NAME ) {
		return $option_name . '_' . $name;
	}

	/**
	 * Hook 'admin_menu'.
	 */
	public function admin_menu() {
		add_options_page(
			'BWG Einstellungen',
			'BWG',
			'manage_options',
			self::ADMIN_PAGE_KEY,
			[ $this, 'options_page' ]
		);

		add_options_page(
			'BWG Prodomo Einstellungen',
			'BWG Prodomo',
			'manage_options',
			self::ADMIN_PRODOMO_PAGE_KEY,
			[ $this, 'prodomo_options_page' ]
		);
	}

	/**
	 * Callback function for the input type text setting.
	 *
	 * @param $params
	 */
	public function setting_input_text_dn( $params ) {
		if ( ! isset( $params['field_name'] ) ) {
			return;
		}
		if ( ! isset( $params['option_name'] ) ) {
			$params['option_name'] = self::OPTION_NAME;
		}

		$option   = $this->_get_option( $params['field_name'], '', $params['option_name'] );
		$field_id = $this->get_field_id( $params['field_name'], $params['option_name'] );
		echo '<input id="' . $field_id . '" name="' . $params['option_name'] . '[' . $params['field_name'] . ']" value="'
		     . esc_attr( $option ) . '" type="text" style="width:100%;">';

		if ( isset( $params['description'] ) ) {
			echo '<p class="description" id="' . $field_id . '-description">' . $params['description'] . '</p>';
		}
	}

	/**
	 * Callback function for the setting that needs a page id option.
	 *
	 * @param array $params
	 */
	public function setting_page_id_fn( $params ) {
		if ( ! isset( $params['field_name'] ) ) {
			return;
		}
		if ( ! isset( $params['option_name'] ) ) {
			$params['option_name'] = self::OPTION_NAME;
		}

		$page_ID  = $this->_get_option( $params['field_name'], 0, $params['option_name'] );
		$field_id = $this->get_field_id( $params['field_name'], $params['option_name'] );
		echo '<select id="' . $field_id . '" name="' . $params['option_name'] . '[' . $params['field_name'] . ']">';
		echo '<option value="0">---</option>';
		$pages = get_pages();
		foreach ( $pages as $page ) {
			$option = '<option value="' . $page->ID . '"' . ( $page->ID == $page_ID ? ' selected="selected"' : '' ) . '>';
			$option .= esc_html( $page->post_title );
			$option .= '</option>';

			echo $option;
		}
		echo '</select>';

		if ( isset( $params['description'] ) ) {
			echo '<p class="description" id="' . $field_id . '-description">' . $params['description'] . '</p>';
		}
	}

	/**
	 * Callback function for the feup field setting.
	 *
	 * @param $params
	 */
	public function setting_feup_input_id_dn( $params ) {
		if ( ! isset( $params['field_name'] ) ) {
			return;
		}

		$option   = $this->_get_option( $params['field_name'], 0 );
		$field_id = $this->get_field_id( $params['field_name'] );
		echo '<input id="' . $field_id . '" name="' . self::OPTION_NAME . '[' . $params['field_name'] . ']" value="' . esc_attr( $option ) . '" type="text">';

		if ( isset( $params['description'] ) ) {
			echo '<p class="description" id="' . $field_id . '-description">' . $params['description'] . '</p>';
		}
	}

	/**
	 * Callback function for a tinymce setting.
	 *
	 * @param $params
	 */
	public function setting_tinymce_fn( $params ) {
		if ( ! isset( $params['field_name'] ) ) {
			return;
		}
		if ( ! isset( $params['option_name'] ) ) {
			$params['option_name'] = self::OPTION_NAME;
		}
		if ( ! isset( $params['editor_height'] ) ) {
			$params['editor_height'] = 250;
		}

		$option_value = $this->_get_option( $params['field_name'], '', $params['option_name'] );
		$field_id     = $this->get_field_id( $params['field_name'], $params['option_name'] );
		echo wp_editor( $option_value, $field_id, [
			'textarea_name' => $params['option_name'] . '[' . $params['field_name'] . ']',
			'editor_height' => $params['editor_height'],
		] );

		if ( isset( $params['description'] ) ) {
			echo '<p class="description" id="' . $field_id . '-description">' . $params['description'] . '</p>';
		}
	}

	public function setting_select_fn( $params ) {
		$option   = $this->_get_option( $params['field_name'], 0 );
		$field_id = $this->get_field_id( $params['field_name'] );

		echo '<select id="' . $field_id . '" name="' . self::OPTION_NAME . '[' . $params['field_name'] . ']">';
		foreach ( $params['options'] as $option_value => $option_label ) {
			echo '<option value="' . $option_value . '"' . ( $option_value === $option ? ' selected' : '' ) . '>' .
			     $option_label . '</option>';
		}
		echo '</select>';

		if ( isset( $params['description'] ) ) {
			echo '<p class="description" id="' . $field_id . '-description">' . $params['description'] . '</p>';
		}
	}

	/**
	 * Validate the option inputs.
	 *
	 * @param array $inputs
	 *
	 * @return array
	 */
	public function validate_inputs( $inputs ) {
		$inputs[ self::FIELD_FOOTER_TEXT ]                     = trim( $inputs[ self::FIELD_FOOTER_TEXT ] );
		$inputs[ self::FIELD_REGISTRATION_INTRO_TEXT ]         = wp_kses_post( $inputs[ self::FIELD_REGISTRATION_INTRO_TEXT ] );
		$inputs[ self::FIELD_REGISTRATION_EMAIL_CONFIRM_TEXT ] = wp_kses_post( $inputs[ self::FIELD_REGISTRATION_EMAIL_CONFIRM_TEXT ] );
		$inputs[ self::FIELD_SPIDER_CHART_INFO_TEXT ]          = wp_kses_post( $inputs[ self::FIELD_SPIDER_CHART_INFO_TEXT ] );
		$inputs[ self::FIELD_SPIDER_CHART_FINAL_INFO_TEXT ]    = wp_kses_post( $inputs[ self::FIELD_SPIDER_CHART_FINAL_INFO_TEXT ] );

		$inputs[ self::FIELD_LOGIN_PAGE_ID ]                  = (int) $inputs[ self::FIELD_LOGIN_PAGE_ID ];
		$inputs[ self::FIELD_REGISTER_PAGE_ID ]               = (int) $inputs[ self::FIELD_REGISTER_PAGE_ID ];
		$inputs[ self::FIELD_REGISTRATION_THANK_YOU_PAGE_ID ] = (int) $inputs[ self::FIELD_REGISTRATION_THANK_YOU_PAGE_ID ];
		$inputs[ self::FIELD_EVALUATION_THANK_YOU_PAGE_ID ]   = (int) $inputs[ self::FIELD_EVALUATION_THANK_YOU_PAGE_ID ];
		$inputs[ self::FIELD_OVERVIEW_PAGE_ID ]               = (int) $inputs[ self::FIELD_OVERVIEW_PAGE_ID ];

		$inputs[ self::FIELD_FEUP_REGISTRATION_EVALUATION_FIELD_ID ] = (int) $inputs[ self::FIELD_FEUP_REGISTRATION_EVALUATION_FIELD_ID ];
		$inputs[ self::FIELD_FEUP_TEST_EMAIL_USER_ID ]               = (int) $inputs[ self::FIELD_FEUP_TEST_EMAIL_USER_ID ];

		$inputs[ self::FIELD_BWG_MODE ] = trim( $inputs[ self::FIELD_BWG_MODE ] );

		return $inputs;
	}

	/**
	 * Validate the prodomo option inputs.
	 *
	 * @param $inputs
	 *
	 * @return mixed
	 */
	public function validate_prodomo_inputs( $inputs ) {
		$inputs[ self::FIELD_PRODOMO_QUESTION_1 ]        = wp_kses_post( $inputs[ self::FIELD_PRODOMO_QUESTION_1 ] );
		$inputs[ self::FIELD_PRODOMO_QUESTION_2 ]        = wp_kses_post( $inputs[ self::FIELD_PRODOMO_QUESTION_2 ] );
		$inputs[ self::FIELD_PRODOMO_QUESTION_3 ]        = wp_kses_post( $inputs[ self::FIELD_PRODOMO_QUESTION_3 ] );
		$inputs[ self::FIELD_PRODOMO_FREETEXT_1 ]        = wp_kses_post( $inputs[ self::FIELD_PRODOMO_FREETEXT_1 ] );
		$inputs[ self::FIELD_PRODOMO_THANK_YOU_PAGE_ID ] = (int) $inputs[ self::FIELD_PRODOMO_THANK_YOU_PAGE_ID ];

		return $inputs;
	}


	/**
	 * Render the options page.
	 */
	public function options_page() {
		echo $this->_bwg_base->utils()->render_admin_template(
			'options/options.php',
			[
				'title'        => 'BWG',
				'option_group' => BWG_Options::ADMIN_PAGE_KEY,
				'page'         => BWG_Options::ADMIN_PAGE_KEY,
			]
		);
	}

	/**
	 * Render the prodomo options page.
	 */
	public function prodomo_options_page() {
		echo $this->_bwg_base->utils()->render_admin_template(
			'options/options.php',
			[
				'title'        => 'BWG Prodomo',
				'option_group' => BWG_Options::ADMIN_PRODOMO_PAGE_KEY,
				'page'         => BWG_Options::ADMIN_PRODOMO_PAGE_KEY,
			]
		);
	}

	/**
	 * Gets the options.
	 *
	 * @param string $option_name
	 *
	 * @return array|false
	 */
	public function get_options( $option_name = self::OPTION_NAME ) {
		return get_option( $option_name );
	}

	/**
	 * Gets one option or the given default value if not set.
	 *
	 * @param string $name
	 * @param mixed $default_value
	 *
	 * @param string $option_name
	 *
	 * @return mixed
	 */
	private function _get_option( $name, $default_value = NULL, $option_name = self::OPTION_NAME ) {
		static $options = [];

		if ( ! isset( $options[ $option_name ] ) ) {
			$options[ $option_name ] = $this->get_options( $option_name );
		}

		if ( isset( $options[ $option_name ][ $name ] ) ) {
			return $options[ $option_name ][ $name ];
		}

		return $default_value;
	}

	/**
	 * Gets the page link or an empty string in case the page does not exist.
	 *
	 * @param int $page_ID
	 * @param array $query_params
	 *
	 * @return string
	 */
	private function _get_option_page_link( $page_ID, $query_params = [] ) {
		if ( 0 == $page_ID ) {
			return '';
		}

		/** @var WP_Post $post */
		$post = get_post( $page_ID );
		if ( is_null( $post ) ) {
			return '';
		}

		if ( 'page' !== $post->post_type ) {
			return '';
		}

		if ( empty( $query_params ) ) {
			return get_page_link( $post );
		}

		return add_query_arg( $query_params, get_page_link( $post ) );
	}

	/**
	 * Gets the footer text.
	 *
	 * @return string
	 */
	public function get_option_footer_text() {
		return $this->_get_option( self::FIELD_FOOTER_TEXT, '' );
	}

	/**
	 * Gets the registration intro text.
	 *
	 * @return mixed
	 */
	public function get_option_registration_intro_text() {
		return $this->_get_option( self::FIELD_REGISTRATION_INTRO_TEXT, '' );
	}

	/**
	 * Gets the registration email confirm text.
	 *
	 * @return mixed
	 */
	public function get_option_registration_email_confirm_text() {
		return $this->_get_option( self::FIELD_REGISTRATION_EMAIL_CONFIRM_TEXT, '' );
	}

	/**
	 * Gets the spider chart intro text.
	 *
	 * @return mixed
	 */
	public function get_option_spider_chart_intro_text() {
		return $this->_get_option( self::FIELD_SPIDER_CHART_INFO_TEXT, '' );
	}

	/**
	 * Gets the spider chart final intro text.
	 *
	 * @return mixed
	 */
	public function get_option_spider_chart_final_intro_text() {
		return $this->_get_option( self::FIELD_SPIDER_CHART_FINAL_INFO_TEXT, '' );
	}

	/**
	 * Gets the page id of the 'login' page.
	 *
	 * @return int
	 */
	public function get_option_login_page_id() {
		return $this->_get_option( self::FIELD_LOGIN_PAGE_ID, 0 );
	}

	/**
	 * Gets the page link of the 'login' page or an empty string if none had been set.
	 *
	 * @param array $query_params
	 *
	 * @return string
	 */
	public function get_option_login_page_link( $query_params = [] ) {
		return $this->_get_option_page_link( $this->get_option_login_page_id(), $query_params );
	}

	/**
	 * Gets the page id of the 'register' page.
	 *
	 * @return int
	 */
	public function get_option_register_page_id() {
		return $this->_get_option( self::FIELD_REGISTER_PAGE_ID, 0 );
	}

	/**
	 * Gets the page link of the 'register' page or an empty string if none had been set.
	 *
	 * @param array $query_params
	 *
	 * @return string
	 */
	public function get_option_register_page_link( $query_params = [] ) {
		return $this->_get_option_page_link( $this->get_option_register_page_id(), $query_params );
	}

	/**
	 * Gets the page id of the 'registration thank you' page.
	 *
	 * @return int
	 */
	public function get_option_registration_thank_you_page_id() {
		return $this->_get_option( self::FIELD_REGISTRATION_THANK_YOU_PAGE_ID, 0 );
	}

	/**
	 * Gets the page link of the 'registration thank you' page or an empty string
	 * if none had been set.
	 *
	 * @param array $query_params
	 *
	 * @return string
	 */
	public function get_option_registration_thank_you_page_link( $query_params = [] ) {
		return $this->_get_option_page_link( $this->get_option_registration_thank_you_page_id(), $query_params );
	}

	/**
	 * Gets the page id of the 'evaluation thank you' page.
	 *
	 * @return int
	 */
	public function get_option_evaluation_thank_you_page_id() {
		return $this->_get_option( self::FIELD_EVALUATION_THANK_YOU_PAGE_ID, 0 );
	}

	/**
	 * Gets the page link of the 'evaluation thank you' page or an empty string if none had been set.
	 *
	 * @param array $query_params
	 *
	 * @return string
	 */
	public function get_option_evaluation_thank_you_page_link( $query_params = [] ) {
		return $this->_get_option_page_link( $this->get_option_evaluation_thank_you_page_id(), $query_params );
	}

	/**
	 * Gets the page id of the 'overview' page.
	 *
	 * @return int
	 */
	public function get_option_overview_page_id() {
		return $this->_get_option( self::FIELD_OVERVIEW_PAGE_ID, 0 );
	}

	/**
	 * Gets the page link of the 'overview' page or an empty string if none had been set.
	 *
	 * @param array $query_params
	 *
	 * @return string
	 */
	public function get_option_overview_page_link( $query_params = [] ) {
		return $this->_get_option_page_link( $this->get_option_overview_page_id(), $query_params );
	}

	/**
	 * Gets the feup field ID used for storing the evaluation ID used at registration.
	 *
	 * @return int
	 */
	public function get_feup_registration_evaluation_field_id() {
		return $this->_get_option( self::FIELD_FEUP_REGISTRATION_EVALUATION_FIELD_ID, 0 );
	}

	/**
	 * Gets the feup user ID used for the replacement of feup field placeholders in case of test emails.
	 *
	 * @return int
	 */
	public function get_feup_test_email_user_id() {
		return $this->_get_option( self::FIELD_FEUP_TEST_EMAIL_USER_ID, 0 );
	}

	/**
	 * Gets the bwg mode.
	 *
	 * @return string
	 */
	public function get_option_bwg_mode() {
		return $this->_get_option( self::FIELD_BWG_MODE, 'default' );
	}

	/**
	 * Gets the prodomo question 1 label.
	 *
	 * @return string
	 */
	public function get_prodomo_question_1() {
		return $this->_get_option( self::FIELD_PRODOMO_QUESTION_1, '', self::PRODOMO_OPTION_NAME );
	}

	/**
	 * Gets the prodomo question 2 label.
	 *
	 * @return string
	 */
	public function get_prodomo_question_2() {
		return $this->_get_option( self::FIELD_PRODOMO_QUESTION_2, '', self::PRODOMO_OPTION_NAME );
	}

	/**
	 * Gets the prodomo question 3 label.
	 *
	 * @return string
	 */
	public function get_prodomo_question_3() {
		return $this->_get_option( self::FIELD_PRODOMO_QUESTION_3, '', self::PRODOMO_OPTION_NAME );
	}

	/**
	 * Gets the prodomo freetext 1 label.
	 *
	 * @return string
	 */
	public function get_prodomo_freetext_1() {
		return $this->_get_option( self::FIELD_PRODOMO_FREETEXT_1, '', self::PRODOMO_OPTION_NAME );
	}

	/**
	 * Gets the page id of the 'prodomo thanks' page.
	 *
	 * @return int
	 */
	public function get_prodomo_thank_you_page_id() {
		return $this->_get_option( self::FIELD_PRODOMO_THANK_YOU_PAGE_ID, 0, self::PRODOMO_OPTION_NAME );
	}

	/**
	 * Gets the page link of the 'prodomo thanks' page or an empty string if none had been set.
	 *
	 * @param array $query_params
	 *
	 * @return string
	 */
	public function get_prodomo_thank_you_page_link( $query_params = [] ) {
		return $this->_get_option_page_link( $this->get_prodomo_thank_you_page_id(), $query_params );
	}

}