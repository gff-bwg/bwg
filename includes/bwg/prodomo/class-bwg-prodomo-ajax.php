<?php

namespace bwg\prodomo;

use bwg\BWG_Base;

/**
 * Class BWG_Prodomo_Ajax.
 *
 * @package bwg\prodomo
 */
class BWG_Prodomo_Ajax {

	/**
	 * The ajax action for the bwg prodomo form.
	 */
	const AJAX_ACTION = 'bwg_prodomo';

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Prodomo_Ajax constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;
	}

	/**
	 * Registers the public ajax callbacks.
	 *
	 * @param bool $admin
	 */
	public function register_public_ajax_callbacks( $admin = FALSE ) {
		add_action( 'wp_ajax_' . ( $admin ? '' : 'nopriv_' ) . self::AJAX_ACTION, [ $this, 'wp_ajax' ] );
	}

	/**
	 * Ajax Action Handler for the AJAX_ACTION.
	 */
	public function wp_ajax() {
		check_ajax_referer( self::AJAX_ACTION );

		$feup = new \FEUP_User();
		if ( ! $feup->Is_Logged_In() ) {
			return wp_send_json_error( [ 'error' => __( 'Sie sind nicht eingeloggt.', 'bwg' ) ] );
		}

		if ( FALSE !== $this->_bwg_base->prodomo_database_helper()->save_data(
				intval( $_REQUEST['evaluation_id'] ),
				$feup->Get_User_ID(),
				isset( $_REQUEST['prodomo_question1'] ) ? intval( $_REQUEST['prodomo_question1'] ) : NULL,
				isset( $_REQUEST['prodomo_question2'] ) ? intval( $_REQUEST['prodomo_question2'] ) : NULL,
				isset( $_REQUEST['prodomo_question3'] ) ? intval( $_REQUEST['prodomo_question3'] ) : NULL,
				isset( $_REQUEST['prodomo_freetext1'] ) ? stripslashes_deep( $_REQUEST['prodomo_freetext1'] ) : NULL
			)
		) {
			///
			// Send notification email to prodomo admin.
			///
			$subject = __( 'BWG Prodomo', 'bwg' );
			$headers = [ 'Content-Type: text/html; charset=UTF-8', ];

			$base    = $this->_bwg_base;
			$content = '<p>BWG Evaluation<br/><b>' . get_the_title( intval( $_REQUEST['evaluation_id'] ) ) . '</b></p>';
			$content .= '<hr/>';
			$content .= '<p>' . $base->options()->get_prodomo_question_1() . '<br/><b>'
			            . ( isset( $_REQUEST['prodomo_question1'] ) ? intval( $_REQUEST['prodomo_question1'] ) : '-' ) . '</b></p>';
			$content .= '<hr/>';
			$content .= '<p>' . $base->options()->get_prodomo_question_2() . '<br/><b>'
			            . ( isset( $_REQUEST['prodomo_question2'] ) ? intval( $_REQUEST['prodomo_question2'] ) : '-' ) . '</b></p>';
			$content .= '<hr/>';
			$content .= '<p>' . $base->options()->get_prodomo_question_3() . '<br/><b>'
			            . ( isset( $_REQUEST['prodomo_question3'] ) ? intval( $_REQUEST['prodomo_question3'] ) : '-' ) . '</b></p>';
			$content .= '<hr/>';
			$content .= '<p>' . $base->options()->get_prodomo_freetext_1() . '<br/><b>'
			            . (
			            isset( $_REQUEST['prodomo_freetext1'] ) ?
				            nl2br( htmlspecialchars( stripslashes_deep( $_REQUEST['prodomo_freetext1'] ) ) ) :
				            '-'
			            ) . '</b></p>';
			$content .= '<hr/>';

			$format  = get_option( 'date_format' ) . ' ' . get_option( 'time_format' );
			$content .= '<p>' . date_i18n( $format, current_time( 'timestamp' ) ) . '</p>';

			$body = $this->_bwg_base->utils()->render_template( 'mail/simple-html.php',
				[
					'content'        => '<p>' . $content . '</p>',
					'footer_subtext' => $this->_bwg_base->options()->get_option_footer_text(),
				]
			);

			wp_mail( get_bloginfo( 'admin_email' ), $subject, $body, $headers );
			
			return wp_send_json_success( [
				'redirect' => $base->options()->get_prodomo_thank_you_page_link(),
			] );
		}

		return wp_send_json_error( [
			'error' => __(
				'Entschuldigung, leider konnten die Daten nicht gespeichert werden.', 'bwg'
			),
		] );
	}
}