<?php

namespace bwg\prodomo;

use bwg\BWG_Base;

class BWG_Prodomo_Base {

	/**
	 * @var \bwg\BWG_Base
	 */
	private $_bwg_base;


	/**
	 * BWG_Prodomo_Base constructor.
	 *
	 * @param \bwg\BWG_Base $bwg_base
	 */
	public function __construct( BWG_Base $bwg_base ) {
		$this->_bwg_base = $bwg_base;

		add_action( 'init', [ $this, 'init' ] );
		if ( is_admin() ) {
			add_action( 'admin_init', [ $this, 'admin_init' ] );
			add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		}
	}

	/**
	 * Handler for 'init' action.
	 */
	public function init() {
		// Add non-admin ajax callbacks.
		$this->_bwg_base->prodomo_ajax()->register_public_ajax_callbacks();
	}

	/**
	 * Handler for 'admin_init' action.
	 */
	public function admin_init() {
		// Add admin ajax callbacks.
		$this->_bwg_base->prodomo_ajax()->register_public_ajax_callbacks( TRUE );

	}

	/**
	 * Handler for 'admin_menu' action.
	 */
	public function admin_menu() {
		add_menu_page(
			'BWG Prodomo',
			'BWG Prodomo',
			'administrator',
			'bwg_prodomo_admin',
			[ $this, 'page_callback' ],
			'dashicons-palmtree',
			8
		);
	}

	/**
	 * Admin page callback.
	 */
	public function page_callback() {
		$ra = [
			'statistics' => $this->_bwg_base->prodomo_database_helper()->get_statistics(),
			'histograms' => $this->_bwg_base->prodomo_database_helper()->get_histograms(),
			'question1'  => $this->_bwg_base->options()->get_prodomo_question_1(),
			'question2'  => $this->_bwg_base->options()->get_prodomo_question_2(),
			'question3'  => $this->_bwg_base->options()->get_prodomo_question_3(),
		];

		echo $this->_bwg_base->utils()->render_admin_template( 'prodomo/prodomo.php', $ra );
	}
}
