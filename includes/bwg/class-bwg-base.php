<?php

namespace bwg;

use bwg\cron\BWG_Cron_Handler;
use bwg\cron\BWG_Cron_Manager;
use bwg\evaluation\BWG_Evaluation;
use bwg\evaluation\BWG_Evaluation_Ajax;
use bwg\evaluation\BWG_Evaluation_Participation_Statistics;
use bwg\evaluation\BWG_Evaluation_Post_Type_Manager;
use bwg\evaluation\BWG_Evaluation_Presets;
use bwg\evaluation\BWG_Evaluation_Users_Ajax;
use bwg\evaluation\BWG_Evaluation_Utils;
use bwg\feup\BWG_FEUP_Layer;
use bwg\options\BWG_Options;
use bwg\shortcodes\BWG_Shortcode_Manager;
use FEUP_User;

/**
 * Class BWG_Base.
 */
class BWG_Base {

	const STYLE_HANDLE_FRONT = 'bwg-front';
	const STYLE_HANDLE_ADMIN = 'bwg-admin';
	const STYLE_HANDLE_ADMIN_ACCORDION = 'bwg-admin-accordion';

	const SCRIPT_HANDLE_FRONT = 'bwg-front';
	const SCRIPT_HANDLE_FRONT_PRODOMO = 'bwg-front-prodomo';
	const SCRIPT_HANDLE_ADMIN = 'bwg-admin';
	const SCRIPT_HANDLE_ADMIN_ACCORDION = 'bwg-admin-accordion';
	const SCRIPT_HANDLE_LIB_JQUERY_SCROLLFIX = 'bwg-lib-jquery-scrollfix';
	const SCRIPT_HANDLE_LIB_D3 = 'bwg-lib-d3';
	const SCRIPT_HANDLE_LIB_FILE_SAVER = 'bwg-lib-file-saver';
	const SCRIPT_HANDLE_LIB_MOMENT = 'bwg-lib-moment';
	const SCRIPT_HANDLE_LIB_IFVISIBLE = 'bwg-lib-ifvisible';

	/**
	 * @var BWG_Factory
	 */
	protected BWG_Factory $factory;

	/**
	 * @var BWG_Cron_Handler
	 */
	private BWG_Cron_Handler $cron_handler;

	/**
	 * @var \bwg\database\helpers\BWG_Analyses_Database_Helper
	 */
	private $_analyses_database_helper;

	/**
	 * @var \bwg\database\helpers\BWG_Cache_Database_Helper
	 */
	private $_cache_database_helper;

	/**
	 * @var \bwg\database\helpers\BWG_Submissions_Database_Helper
	 */
	private $_submissions_database_helper;

	/**
	 * @var \bwg\database\helpers\BWG_Prodomo_Database_Helper
	 */
	private $_prodomo_database_helper;

	/**
	 * @var \bwg\database\helpers\BWG_Selections_Database_Helper
	 */
	private $_selections_database_helper;

	/**
	 * @var \bwg\database\helpers\BWG_User_Action_Logs_Database_Helper
	 */
	private $_user_action_logs_database_helper;

	/**
	 * @var \bwg\database\helpers\BWG_User_Storage_Database_Helper
	 */
	private $_user_storage_database_helper;

	/**
	 * @var \bwg\evaluation\BWG_Evaluation
	 */
	private $_evaluation;

	/**
	 * @var \bwg\evaluation\BWG_Evaluation_Ajax
	 */
	private $_evaluation_ajax;

	/**
	 * @var \bwg\evaluation\BWG_Evaluation_Participation_Statistics
	 */
	protected BWG_Evaluation_Participation_Statistics $evaluation_participants_statistics;

	/**
	 * @var \bwg\evaluation\BWG_Evaluation_Analyses
	 */
	private $_evaluation_analyses;

	/**
	 * @var \bwg\evaluation\BWG_Evaluation_Analyses_Ajax
	 */
	private $_evaluation_analyses_ajax;

	/**
	 * @var BWG_Evaluation_Post_Type_Manager
	 */
	private $_evaluation_post_type_manager;

	/**
	 * @var BWG_Evaluation_Presets
	 */
	private $_evaluation_presets;

	/**
	 * @var \bwg\evaluation\BWG_Evaluation_Users
	 */
	private $_evaluation_users;

	private BWG_Evaluation_Users_Ajax $_evaluation_users_ajax;

	/**
	 * @var BWG_Evaluation_Utils
	 */
	private $_evaluation_utils;

	/**
	 * @var BWG_FEUP_Layer
	 */
	private $_feup_layer;

	/**
	 * @var BWG_Options
	 */
	private $_options;

	/**
	 * @var \bwg\prodomo\BWG_Prodomo_Ajax
	 */
	private $_prodomo_ajax;

	/**
	 * @var \bwg\prodomo\BWG_Prodomo_Base
	 */
	private $_prodomo_base;

	/**
	 * @var \bwg\profile\fields\BWG_Profile_Field_Type_Registry
	 */
	private $_profile_field_type_registry;

	/**
	 * @var \bwg\profile\fields\BWG_Profile_Field_Utils
	 */
	private $_profile_field_utils;

	/**
	 * @var BWG_Shortcode_Manager
	 */
	private $_shortcode_manager;

	/**
	 * @var \bwg\stats\strata\BWG_Stats_Strata_Utils
	 */
	private $_stats_strata_utils;

	/**
	 * @var \bwg\stats\BWG_Stats_Math
	 */
	private $_stats_math;

	/**
	 * @var BWG_Globals
	 */
	private $_globals;

	/**
	 * @var BWG_Utils
	 */
	private $_utils;

	/**
	 * @var \bwg\charts\BWG_Chart_Factory
	 */
	private $_chart_factory;


	/**
	 * @param BWG_Factory $factory
	 */
	public function __construct( BWG_Factory $factory ) {
		// Set the bwg factory.
		$this->factory = $factory;

		// Set the plugins_loaded action handler (note that this hook gets called before the init hook).
		add_action( 'plugins_loaded', [ $this, 'plugins_loaded' ] );

		// Set the init action handler.
		add_action( 'init', [ $this, 'init' ] );

		// Set the 'cron_schedules' filter.
		add_filter( 'cron_schedules', [ 'bwg\\cron\\BWG_Cron_Manager', 'cron_schedules' ] );

		// Set the 'gettext' filter.
		add_filter( 'gettext', [ $this, 'filter_gettext' ], 20, 3 );

		// Front- and Backend handlers. Make sure, that some handlers be instantiated.
		$this->evaluation();
		$this->evaluation_users();
		$this->evaluation_participants_statistics();
		$this->evaluation_analyses();

		$this->prodomo_base();

		$this->options();
		$this->shortcode_manager();
	}

	/**
	 * Hook 'init' handler.
	 */
	public function init() {
		add_action( 'wp_enqueue_scripts', [ $this, 'wp_enqueue_scripts' ] );

		add_action( BWG_Cron_Manager::CRON_NAME_CHECK_USERS, [ $this->cron_handler(), 'check_users' ] );
		add_action( BWG_Cron_Manager::CRON_NAME_MAINTENANCE, [ $this->cron_handler(), 'maintenance' ] );

		add_action( 'admin_init', [ $this, 'admin_init' ] );
	}

	/**
	 * Hook 'wp_enqueue_scripts' handler.
	 */
	public function wp_enqueue_scripts() {
		// Scripts.
		$this->_common_enqueue_scripts();

		$this->utils()->register_script(
			self::SCRIPT_HANDLE_LIB_IFVISIBLE,
			'js/lib/ifvisible/ifvisible.js',
			[], TRUE, FALSE
		);

		$this->utils()->register_script(
			self::SCRIPT_HANDLE_FRONT,
			'js/front/bwg.js',
			[ 'jquery', 'underscore', self::SCRIPT_HANDLE_LIB_IFVISIBLE ], TRUE, FALSE
		);

		$this->utils()->register_script(
			self::SCRIPT_HANDLE_FRONT_PRODOMO,
			'js/front/bwg-prodomo.js',
			[ 'jquery' ], TRUE, FALSE
		);

		wp_localize_script( self::SCRIPT_HANDLE_FRONT_PRODOMO, 'bwg_prodomo_l10n', [
			'please_wait'  => __( 'Bitte warten...', 'bwg' ),
			'error_server' => __( 'Der Server antwortete fehlerhaft.', 'bwg' ),
		] );

		// Styles.
		$this->utils()->register_style(
			self::STYLE_HANDLE_FRONT,
			'css/front_bwg.css', [], TRUE
		);
	}

	/**
	 * Hook 'admin_init' handler.
	 */
	public function admin_init() {
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );
	}

	/**
	 * Hook 'admin_enqueue_scripts' handler.
	 */
	public function admin_enqueue_scripts() {
		// Scripts.
		$this->_common_enqueue_scripts();

		$this->utils()->register_script(
			self::SCRIPT_HANDLE_LIB_JQUERY_SCROLLFIX,
			'js/lib/jquery-scrollfix/jquery-scrollfix.js',
			[ 'jquery' ], TRUE, FALSE
		);

		$this->utils()->register_script(
			self::SCRIPT_HANDLE_ADMIN,
			'js/admin/bwg.js',
			[ 'jquery', 'underscore', self::SCRIPT_HANDLE_LIB_MOMENT ], TRUE, FALSE
		);

		$this->utils()->register_script(
			self::SCRIPT_HANDLE_ADMIN_ACCORDION,
			'js/admin/bwg-accordion.js',
			[ 'jquery', ], TRUE, FALSE
		);

		// Styles.
		$this->utils()->register_style(
			self::STYLE_HANDLE_ADMIN,
			'css/admin_bwg.css',
			[], TRUE
		);

		$this->utils()->register_style(
			self::STYLE_HANDLE_ADMIN_ACCORDION,
			'css/admin/accordion/accordion.css',
			[], TRUE
		);
	}

	/**
	 * Enqueue common (frontend/backend) scripts.
	 */
	private function _common_enqueue_scripts() {
		$this->utils()->register_script(
			self::SCRIPT_HANDLE_LIB_D3, 'js/lib/d3/d3.js', [], TRUE, FALSE
		);
		$this->utils()->register_script(
			self::SCRIPT_HANDLE_LIB_FILE_SAVER, 'js/lib/FileSaver/FileSaver.js', [], TRUE, FALSE
		);
		$this->utils()->register_script(
			self::SCRIPT_HANDLE_LIB_MOMENT, 'js/lib/moment/moment.js', [], TRUE, FALSE
		);
	}

	/**
	 * Hook 'plugins_loaded' handler.
	 */
	public function plugins_loaded(): void {
		load_plugin_textdomain( 'bwg', FALSE, 'bwg/locales' );

		if ( get_option( 'bwg_db_version' ) !== BWG_DB_VERSION ) {
			BWG_System::update_schema();
		}
	}

	/**
	 * Filter 'gettext' handler.
	 */
	public function filter_gettext( $translated_text, $text, $domain ) {
		if ( 'front-end-only-users' === $domain ) {
			// FEUP override(s).
			if ( 'Image Number' === $text ) {
				return __( 'Image number: ', 'bwg' );
			} else if ( 'Verify Email' === $text ) {
				return __( 'Verify Email', 'bwg' );
			} else if ( 'Reset Password' === $text ) {
				return __( 'Reset Password', 'bwg' );
			} else if ( 'Please select a longer password' === $text ) {
				return __( 'Please select a longer password', 'bwg' );
			} else if ( "You need a password reset code to reset your password. Please use the 'I forgot my password' function first to acquire one." === $text ) {
				return __( "You need a password reset code to reset your password. Please use the 'I forgot my password' function first to acquire one.",
					'bwg' );
			} else if ( 'There is already a user with that Username, please select a different one.' === $text ) {
				return '<p style="margin-top:1em;"><b>' . $translated_text . '</b></p>';
			}
		}

		return $translated_text;
	}

	/**
	 * @param $message
	 *
	 * @return mixed
	 */
	public function admin_render_error_wrap( $message ) {
		return $this->utils()->render_admin_template( 'error-wrap.php', [
			'message' => $message,
		] );
	}

	/**
	 * Gets the BWGFactory.
	 *
	 * @return BWG_Factory
	 */
	public function factory() {
		return $this->factory;
	}

	/**
	 * Gets the BWG_Cron_Handler singleton.
	 *
	 * @return BWG_Cron_Handler
	 */
	public function cron_handler() {
		if ( ! isset( $this->cron_handler ) ) {
			$this->cron_handler = $this->factory()->bwg_cron_handler( $this );
		}

		return $this->cron_handler;
	}

	/**
	 * Gets the BWG_Analyses_Database_Helper singleton.
	 *
	 * @return \bwg\database\helpers\BWG_Analyses_Database_Helper
	 */
	public function analyses_database_helper() {
		if ( ! isset( $this->_analyses_database_helper ) ) {
			$this->_analyses_database_helper = $this->factory()->bwg_analyses_database_helper( $this );
		}

		return $this->_analyses_database_helper;
	}

	/**
	 * Gets the BWG_Cache_Database_Helper singleton.
	 *
	 * @return \bwg\database\helpers\BWG_Cache_Database_Helper
	 */
	public function cache_database_helper() {
		if ( ! isset( $this->_cache_database_helper ) ) {
			$this->_cache_database_helper = $this->factory()->bwg_cache_database_helper( $this );
		}

		return $this->_cache_database_helper;
	}

	/**
	 * Gets the BWG_Submissions_Database_Helper singleton.
	 *
	 * @return \bwg\database\helpers\BWG_Submissions_Database_Helper
	 */
	public function submissions_database_helper() {
		if ( ! isset( $this->_submissions_database_helper ) ) {
			$this->_submissions_database_helper = $this->factory()->bwg_submissions_database_helper( $this );
		}

		return $this->_submissions_database_helper;
	}

	/**
	 * Gets the BWG_Prodomo_Database_Helper singleton.
	 *
	 * @return \bwg\database\helpers\BWG_Prodomo_Database_Helper
	 */
	public function prodomo_database_helper() {
		if ( ! isset( $this->_prodomo_database_helper ) ) {
			$this->_prodomo_database_helper = $this->factory()->bwg_prodomo_database_helper( $this );
		}

		return $this->_prodomo_database_helper;
	}

	/**
	 * Gets the BWG_Selections_Database_Helper singleton.
	 *
	 * @return \bwg\database\helpers\BWG_Selections_Database_Helper
	 */
	public function selections_database_helper() {
		if ( ! isset( $this->_selections_database_helper ) ) {
			$this->_selections_database_helper = $this->factory()->bwg_selections_database_helper( $this );
		}

		return $this->_selections_database_helper;
	}

	/**
	 * Gets the BWG_User_Action_Logs_Database_Helper singleton.
	 *
	 * @return \bwg\database\helpers\BWG_User_Action_Logs_Database_Helper
	 */
	public function user_action_logs_database_helper() {
		if ( ! isset( $this->_user_action_logs_database_helper ) ) {
			$this->_user_action_logs_database_helper = $this->factory()->bwg_user_action_logs_database_helper( $this );
		}

		return $this->_user_action_logs_database_helper;
	}

	/**
	 * Gets the BWG_User_Storage_Database_Helper singleton.
	 *
	 * @return \bwg\database\helpers\BWG_User_Storage_Database_Helper
	 */
	public function user_storage_database_helper() {
		if ( ! isset( $this->_user_storage_database_helper ) ) {
			$this->_user_storage_database_helper = $this->factory()->bwg_user_storage_database_helper( $this );
		}

		return $this->_user_storage_database_helper;
	}

	/**
	 * Gets the BWG_Evaluation singleton.
	 *
	 * @return BWG_Evaluation
	 */
	public function evaluation() {
		if ( ! isset( $this->_evaluation ) ) {
			$this->_evaluation = $this->factory()->bwg_evaluation( $this );
		}

		return $this->_evaluation;
	}

	/**
	 * Gets the BWG_Evaluation_Ajax singleton.
	 *
	 * @return BWG_Evaluation_Ajax
	 */
	public function evaluation_ajax() {
		if ( ! isset( $this->_evaluation_ajax ) ) {
			$this->_evaluation_ajax = $this->factory()->bwg_evaluation_ajax( $this );
		}

		return $this->_evaluation_ajax;
	}

	/**
	 * @return \bwg\evaluation\BWG_Evaluation_Participation_Statistics
	 */
	public function evaluation_participants_statistics(): BWG_Evaluation_Participation_Statistics {
		if ( ! isset( $this->evaluation_participants_statistics ) ) {
			$this->evaluation_participants_statistics = $this->factory()
			                                                 ->bwg_evaluation_participants_statistics( $this );
		}

		return $this->evaluation_participants_statistics;
	}

	/**
	 * Gets the BWG_Evaluation_Analyses singleton.
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Analyses
	 */
	public function evaluation_analyses() {
		if ( ! isset( $this->_evaluation_analyses ) ) {
			$this->_evaluation_analyses = $this->factory()->bwg_evaluation_analyses( $this );
		}

		return $this->_evaluation_analyses;
	}

	/**
	 * Gets the BWG_Evaluation_Analyses_Ajax singleton.
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Analyses_Ajax
	 */
	public function evaluation_analyses_ajax() {
		if ( ! isset( $this->_evaluation_analyses_ajax ) ) {
			$this->_evaluation_analyses_ajax = $this->factory()->bwg_evaluation_analyses_ajax( $this );
		}

		return $this->_evaluation_analyses_ajax;
	}

	/**
	 * Gets the BWG_Evaluation_Post_Type_Manager singleton.
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Post_Type_Manager
	 */
	public function evaluation_post_type_manager() {
		if ( ! isset( $this->_evaluation_post_type_manager ) ) {
			$this->_evaluation_post_type_manager = $this->factory()->bwg_evaluation_post_type_manager( $this );
		}

		return $this->_evaluation_post_type_manager;
	}

	/**
	 * Gets the BWG_Evaluation_Presets singleton.
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Presets
	 */
	public function evaluation_presets() {
		if ( ! isset( $this->_evaluation_presets ) ) {
			$this->_evaluation_presets = $this->factory()->bwg_evaluation_presets( $this );
		}

		return $this->_evaluation_presets;
	}

	/**
	 * Gets the BWG_Evaluation_Users singleton.
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Users
	 */
	public function evaluation_users() {
		if ( ! isset( $this->_evaluation_users ) ) {
			$this->_evaluation_users = $this->factory()->bwg_evaluation_users( $this );
		}

		return $this->_evaluation_users;
	}

	/**
	 * Gets the `BWG_Evaluation_Users_Ajax` singleton.
	 *
	 * @return BWG_Evaluation_Users_Ajax
	 */
	public function evaluation_users_ajax(): BWG_Evaluation_Users_Ajax {
		if ( ! isset( $this->_evaluation_users_ajax ) ) {
			$this->_evaluation_users_ajax = $this->factory->bwg_evaluation_users_ajax( $this );
		}

		return $this->_evaluation_users_ajax;
	}

	/**
	 * Gets the BWG_Evaluation_Utils singleton.
	 *
	 * @return \bwg\evaluation\BWG_Evaluation_Utils
	 */
	public function evaluation_utils() {
		if ( ! isset( $this->_evaluation_utils ) ) {
			$this->_evaluation_utils = $this->factory()->bwg_evaluation_utils( $this );
		}

		return $this->_evaluation_utils;
	}

	/**
	 * Gets the BWG_FEUP_Layer singleton.
	 *
	 * @return BWG_FEUP_Layer
	 */
	public function feup_layer() {
		if ( ! isset( $this->_feup_layer ) ) {
			$this->_feup_layer = $this->factory()->bwg_feup_layer( $this, new FEUP_User() );
		}

		return $this->_feup_layer;
	}

	/**
	 * Gets the BWG_Options singleton.
	 *
	 * @return BWG_Options
	 */
	public function options() {
		if ( ! isset( $this->_options ) ) {
			$this->_options = $this->factory()->bwg_options( $this );
		}

		return $this->_options;
	}

	/**
	 * Gets the BWG_Prodomo_Ajax singleton.
	 *
	 * @return \bwg\prodomo\BWG_Prodomo_Ajax
	 */
	public function prodomo_ajax() {
		if ( ! isset( $this->_prodomo_ajax ) ) {
			$this->_prodomo_ajax = $this->factory()->bwg_prodomo_ajax( $this );
		}

		return $this->_prodomo_ajax;
	}

	/**
	 * Gets the BWG_Prodomo_Base singleton.
	 *
	 * @return \bwg\prodomo\BWG_Prodomo_Base
	 */
	public function prodomo_base() {
		if ( ! isset( $this->_prodomo_base ) ) {
			$this->_prodomo_base = $this->factory()->bwg_prodomo_base( $this );
		}

		return $this->_prodomo_base;
	}

	/**
	 * Gets the BWG_Profile_Field_Type_Registry singleton.
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field_Type_Registry
	 */
	public function profile_field_type_registry() {
		if ( ! isset( $this->_profile_field_type_registry ) ) {
			$this->_profile_field_type_registry = $this->factory()->bwg_profile_field_type_registry( $this );
		}

		return $this->_profile_field_type_registry;
	}

	/**
	 * Gets the BWG_Profile_Field_Utils singleton.
	 *
	 * @return \bwg\profile\fields\BWG_Profile_Field_Utils
	 */
	public function profile_field_utils() {
		if ( ! isset( $this->_profile_field_utils ) ) {
			$this->_profile_field_utils = $this->factory()->bwg_profile_field_utils( $this );
		}

		return $this->_profile_field_utils;
	}

	/**
	 * Gets the BWG_Shortcode_Manager singleton.
	 *
	 * @return BWG_Shortcode_Manager
	 */
	public function shortcode_manager() {
		if ( ! isset( $this->_shortcode_manager ) ) {
			$this->_shortcode_manager = $this->factory()->bwg_shortcode_manager( $this );
		}

		return $this->_shortcode_manager;
	}

	/**
	 * Gets the BWG_Stats_Strata_Utils singleton.
	 *
	 * @return \bwg\stats\strata\BWG_Stats_Strata_Utils
	 */
	public function stats_strata_utils() {
		if ( ! isset( $this->_stats_strata_utils ) ) {
			$this->_stats_strata_utils = $this->factory()->bwg_stats_strata_utils( $this );
		}

		return $this->_stats_strata_utils;
	}

	/**
	 * Gets the BWG_Stats_Math singleton.
	 *
	 * @return \bwg\stats\BWG_Stats_Math
	 */
	public function stats_math() {
		if ( ! isset( $this->_stats_math ) ) {
			$this->_stats_math = $this->factory()->bwg_stats_math( $this );
		}

		return $this->_stats_math;
	}

	/**
	 * Gets the BWG_Globals singleton.
	 *
	 * @return BWG_Globals
	 */
	public function globals() {
		if ( ! isset( $this->_globals ) ) {
			$this->_globals = $this->factory()->bwg_globals();
		}

		return $this->_globals;
	}

	/**
	 * Gets the BWG_Utils singleton.
	 *
	 * @return BWG_Utils
	 */
	public function utils() {
		if ( ! isset( $this->_utils ) ) {
			$this->_utils = $this->factory()->bwg_utils();
		}

		return $this->_utils;
	}

	/**
	 * Gets the BWG_Chart_Factory singleton.
	 *
	 * @return \bwg\charts\BWG_Chart_Factory
	 */
	public function chart_factory() {
		if ( ! isset( $this->_chart_factory ) ) {
			$this->_chart_factory = $this->factory()->bwg_chart_factory( $this );
		}

		return $this->_chart_factory;
	}

}