=== BWG ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 5.0
Tested up to: 6.2
Requires PHP: 7.4
Stable tag: 1.15
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The BWG plugin.

== Description ==

The **BWG** plugin.

== Changelog ==

= 1.16 =
* Add mailer.

= 1.15 =
* Fix division by zero.

= 1.14 =
* Fix logo.

= 1.13 =
* Added another logo for the user pdf in case of drogothek.
* Set another dynamic filename for the user pdf in case of drogothek.

= 1.12 =
* Added title suffix for the user pdf in case of drogothek.

= 1.11 =
* Added evaluation participation statistics.

= 1.10 =
* Added plugin assets.

= 1.9 =
* Added the drogothek feature.
