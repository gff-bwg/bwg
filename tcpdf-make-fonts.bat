php ../bwg-vendor/vendor/tecnickcom/tcpdf/tools/tcpdf_addfont.php -o tcpdf-fonts -b -i _fonts/Google-Web-Fonts/Lato-Regular.ttf,_fonts/Google-Web-Fonts/Lato-Italic.ttf,_fonts/Google-Web-Fonts/Lato-Bold.ttf,_fonts/Google-Web-Fonts/Lato-BoldItalic.ttf

php ../bwg-vendor/vendor/tecnickcom/tcpdf/tools/tcpdf_addfont.php -o tcpdf-fonts -b -i _fonts/Google-Web-Fonts/Lato-Light.ttf,_fonts/Google-Web-Fonts/Lato-LightItalic.ttf

php ../bwg-vendor/vendor/tecnickcom/tcpdf/tools/tcpdf_addfont.php -o tcpdf-fonts -b -i _fonts/Arial.ttf

php ../bwg-vendor/vendor/tecnickcom/tcpdf/tools/tcpdf_addfont.php -o tcpdf-fonts -b -i _fonts/Arial-Rounded-MT-Bold.ttf
